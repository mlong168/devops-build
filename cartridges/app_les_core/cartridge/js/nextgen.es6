exports.init = function init() {
	/**
		Footer Product & services menu animation:
	*/
	$(".footer-products--menu").on(
		"change.footerSubnav",
		":checkbox[name=submenu]",
		function() {
			let self = $(this);
			self
				.closest("li")
				.toggleClass("active", self.prop("checked"))
				.siblings("li.active")
				.removeClass("active")
				.end()
				.closest(".footer-products--menu")
				.toggleClass("footer-products--menu-expanded", self.prop("checked"));
		}
	);

	/**
		Scrollspy
	*/
	var lastId,
		menu = $(".jumpLinks"),
		menuHeight = menu.outerHeight() + 15,
		// All list items
		menuItems = menu.find("a"),
		// Anchors corresponding to menu items
		scrollItems = menuItems.map(function() {
			var item = $($(this).attr("href"));
			if (item.length) {
				return item;
			}
		});

	// Bind click handler to menu items
	// so we can get a fancy scroll animation
	menu.on("click.jumpLink", "a", function(e) {
		var href = $(this).attr("href");
		var offsetTop = $(href).offset().top - menuHeight + 1;
		$("html, body")
			.stop()
			.animate(
				{
					scrollTop: offsetTop
				},
				300
			);
		e.preventDefault();
	});
	if (menu && menu.length > 0) {
		// Bind to scroll
		$(window).on("scroll.jumpLink", function() {
			// Get container scroll position
			var fromTop = $(this).scrollTop() + menuHeight;

			// Get id of current scroll item
			var cur = scrollItems.map(function() {
				if ($(this).offset().top < fromTop) return this;
			});
			// Get the id of the current element
			cur = cur[cur.length - 1];
			var id = cur && cur.length ? cur[0].id : "";

			if (lastId !== id) {
				lastId = id;
				// Set/remove active class
				var link = menuItems
					.parent()
					.removeClass("active")
					.end()
					.filter("[href='#" + id + "']");
				if (link && link.length) {
					link
						.closest(".jumpLinks")
						.find(".jumpLinks__bar")
						.css({
							width: link.outerWidth(),
							left: link.offset().left
						});
					link.parent().addClass("active");
				}
			}
		});
	}

	// Community Promo logic on Store Details page
	var $communityPromo__informationWraper = $(
		".communityPromo__informationWraper"
	);
	var $communityPromoViewMore = $communityPromo__informationWraper.find("a");
	var $communityPromoli = $communityPromo__informationWraper.find("ul li");
	$communityPromoli.slice(10).addClass("hide"); // hide the ones after first 10
	if ($communityPromoli.length <= 10) {
		$communityPromo__informationWraper.find(".viewMoreLis").addClass("hide");
	}

	/**
		warranty dialogs
	*/
	$(document)
		.on("click.warranty", ".warranty-language-dialog", function() {
			var dialog;

			if ($(this).hasClass("privacy-dialog")) {
				dialog = document.querySelector("#privacyDialog");
			} else {
				dialog = document.querySelector("#warrantyLanguageDialog");
			}
			var self = $(this);
			var selfLabel = self.data("label");
			var dialogLabel = dialog.querySelector("#categoryLabel");
			var categoryLabel = null;

			if (dialogLabel !== null) {
				categoryLabel = dialogLabel.innerHTML.trim();
			}

			dialog
				.querySelector("#warranty-english")
				.setAttribute("href", self.data("url-eng"));
			dialog
				.querySelector("#warranty-spanish")
				.setAttribute("href", self.data("url-esp"));

			if (typeof selfLabel !== "undefined") {
				if (categoryLabel === null || categoryLabel !== selfLabel) {
					dialogLabel.innerHTML = selfLabel;
				}
			}

			$(dialog)
				.removeClass("hide")
				.dialog({
					modal: true,
					resizable: false,
					width: "auto",
					classes: {
						"ui-dialog": "highlight"
					},
					show: {
						effect: "fade",
						duration: 100
					},
					hide: {
						effect: "fade",
						duration: 100
					}
				});
		})
		.on("click.dialogBackground", ".ui-widget-overlay", function() {
			// close the dialog if you click the background
			$(".ui-dialog-titlebar-close").trigger("click");
		})
		.on("click.zoom", ".productImages__primary", function() {
			// image zoom
			var image = $(this)
					.find(".zoom-image")
					.clone(),
				dialog = $("<aside></aside>");
			dialog
				.append(image)
				.appendTo("body")
				.dialog({
					modal: true,
					width: "auto",
					classes: {
						"ui-dialog": "highlight"
					},
					show: {
						effect: "fade",
						duration: 100
					},
					hide: {
						effect: "fade",
						duration: 100
					}
				});
		})
		.on(
			"click.viewMoreLisClick",
			".communityPromo__informationWraper a",
			function() {
				var hiddenLi = $communityPromoli.filter(".hide:lt(10)");
				hiddenLi.removeClass("hide");
				if ($communityPromoli.filter(".hide").length === 0) {
					$communityPromoViewMore.addClass("hide");
				}
			}
		);
};
