// var imagesLoaded = require("imagesloaded");

function ProductTile() {}

// function gridViewToggle() {
// 	$(".toggle-grid").on("click", function() {
// 		$(".search-result-content").toggleClass("wide-tiles");
// 		$(this).toggleClass("wide");
// 	});
// }

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	// gridViewToggle();
	// $(".swatch-list").on("mouseleave", function() {
	// 	// Restore current thumb image
	// 	var $tile = $(this).closest(".product-tile"),
	// 		$thumb = $tile.find(".product-image .thumb-link img").eq(0),
	// 		data = $thumb.data("current");
	//
	// 	$thumb.attr({
	// 		src: data.src,
	// 		alt: data.alt,
	// 		title: data.title
	// 	});
	// });
	// $(".swatch-list .swatch")
	// 	.on("click", function(e) {
	// 		e.preventDefault();
	// 		if ($(this).hasClass("selected")) {
	// 			return;
	// 		}
	//
	// 		var $tile = $(this).closest(".product-tile");
	// 		$(this)
	// 			.closest(".swatch-list")
	// 			.find(".swatch.selected")
	// 			.removeClass("selected");
	// 		$(this).addClass("selected");
	// 		$tile.find(".thumb-link").attr("href", $(this).attr("href"));
	// 		$tile.find("name-link").attr("href", $(this).attr("href"));
	//
	// 		var data = $(this)
	// 			.children("img")
	// 			.filter(":first")
	// 			.data("thumb");
	// 		var $thumb = $tile.find(".product-image .thumb-link img").eq(0);
	// 		var currentAttrs = {
	// 			src: data.src,
	// 			alt: data.alt,
	// 			title: data.title
	// 		};
	// 		$thumb.attr(currentAttrs);
	// 		$thumb.data("current", currentAttrs);
	// 	})
	// 	.on("mouseenter", function() {
	// 		// get current thumb details
	// 		var $tile = $(this).closest(".product-tile"),
	// 			$thumb = $tile.find(".product-image .thumb-link img").eq(0),
	// 			data = $(this)
	// 				.children("img")
	// 				.filter(":first")
	// 				.data("thumb"),
	// 			current = $thumb.data("current");
	//
	// 		// If this is the first time, then record the current img
	// 		if (!current) {
	// 			$thumb.data("current", {
	// 				src: $thumb[0].src,
	// 				alt: $thumb[0].alt,
	// 				title: $thumb[0].title
	// 			});
	// 		}
	//
	// 		// Set the tile image to the values provided on the swatch data attributes
	// 		$thumb.attr({
	// 			src: data.src,
	// 			alt: data.alt,
	// 			title: data.title
	// 		});
	// 	});
	// attach listener to parent div holding all product tiles. Will control opening and closing of
	// panels for the products
	$("#cat-search-result-content").on(
		"click.toggleProductInfoTile",
		".product-info-toggle",
		function() {
			ProductTile.handleProductTileClick($(this));
		}
	);
}

ProductTile.handleProductTileClick = function($this) {
	var $allOpenedPanels = $(".product-info-panel--opened"); // used to close all panel
	var $newPanels = $(".tiles-container .product-tile"); // used to grab the next three tiles
	var tileHeight = $newPanels.first().height(); // used to set all heights to be the same
	var gutterSize = 20;
	var lastKnownIndex;
	// finding the elements that need the click elements (based on if they have an index already or not)
	$newPanels = $newPanels.filter(function() {
		if ($(this).data("idx") != undefined) {
			lastKnownIndex = $(this).data("idx");
		}
		return $(this).data("idx") === undefined;
	});

	var $panel = $this
		.closest(".product-tile")
		.toggleClass("opened")
		.next(); // Panel that should be shown
	var $tilesContainer = $panel.closest(".tiles-container"); // UL containing all tiles

	// close all panels and reset left
	$allOpenedPanels.toggleClass("product-info-panel--opened").css({ left: 0 });

	// grab how far left the panel must move to match tilesContainer
	var panelLeftVal = $tilesContainer.offset().left - $panel.offset().left;

	// open the panel, as long as it wasn't already open
	if (!$panel.is($allOpenedPanels)) {
		$panel.toggleClass("product-info-panel--opened");
		$panel.css({
			left: panelLeftVal + gutterSize / 2,
			width: $tilesContainer.width() - gutterSize
		});
	}
	// Make sure the correct tab is shown when opened
	$(".product-tile-tabs").tabs({ active: "#tabs-1" });

	// Used to make sure all panels have explict height and finally add index to show the element is setup
	$newPanels.each(function() {
		$(this)
			.height(tileHeight)
			.data("idx", ++lastKnownIndex);
	});

	// add resize to panel
	$(window)
		.off(".productTileResize")
		.on("resize.productTileResize", function() {
			$panel.css({ left: 0 });
			var newLeftPanelVal =
				$tilesContainer.offset().left - $panel.offset().left;

			if (newLeftPanelVal !== 0) {
				$panel.css({
					left: newLeftPanelVal + gutterSize / 2,
					width: $tilesContainer.width() - gutterSize
				});
			} else {
				$panel.css({
					width: $tilesContainer.width()
				});
			}
		});
};

ProductTile.init = function() {
	// var $tiles = $(".tiles-container .product-tile");
	// if ($tiles.length === 0) {
	// 	return;
	// }
	// imagesLoaded(".tiles-container").on("done", function() {
	// 	$tiles.syncHeight().each(function(idx) {
	// 		$(this).data("idx", idx);
	// 	});
	// });
	initializeEvents();
};

module.exports = ProductTile;
