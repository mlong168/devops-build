var dialog = require("./dialog");

var login = {
	/**
	 * @private
	 * @function
	 * @description init events for the loginPage
	 */
	init: function() {
		// email login field is hidden by default

		// o-auth binding for which icon is clicked
		$(".oAuthIcon").bind("click", function() {
			$("#OAuthProvider").val(this.id);
		});
		$(".passwordField").on("keyup", function() {
			var val = $.trim($(this).val());
			if (!val && $(".form-password-hint li").length) {
				$(".form-password-hint li").removeClass("tick");
			}
		});

		// toggle the value of the rememberme checkbox
		$("#dwfrm_login_rememberme").bind("change", function() {
			if ($("#dwfrm_login_rememberme").attr("checked")) {
				$("#rememberme").val("true");
			} else {
				$("#rememberme").val("false");
			}
		});

		// snippet to display sign with email block
		$("button[name='signInEmail']").on("click", function() {
			$(".login-other").hide();
			$(".login-email").removeClass("hide");
		});

		if (
			$("input[name='passwordReset']").val() == "true" ||
			$("input[name='passwordIncorrect']").val() == "true" ||
			$("input[name='invalidLogin']").val() == "true"
		) {
			$("button[name='signInEmail']").trigger("click");
		}

		// handle show/hide of password in password fields
		$(".show-password").bind("click.showPassword", function() {
			var input = $(this).siblings("input");
			input.attr("type") === "password"
				? input.attr("type", "text")
				: input.attr("type", "password");
			$(this).toggleClass("show-password__shown"); // changes appearance of button
		});
		// TODO: this should not be needed:
		$(document).on("click", ".logout", function(e) {
			e.preventDefault();
			var url = Urls.logout;
			dialog.open({
				url: url,
				options: {
					width: 468,
					dialogClass: "logout-dialog",
					show: {
						effect: "fade",
						duration: 100
					},
					hide: {
						effect: "fade",
						duration: 100
					}
				}
			});
		});

		$(document).on("click", ".cancellogout", function() {
			$(".logout-dialog .ui-dialog-titlebar-close").trigger("click");
		});
	}
};

module.exports = login;
