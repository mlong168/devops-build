var giftcert = require("../giftcert"),
	util = require("../util"),
	dialog = require("../dialog"),
	page = require("../page"),
	login = require("../login"),
	validator = require("../validator");

function showProfileSummary() {
	$.ajax({
		url: util.appendParamToURL(Urls.showProfileSummary, "format", "ajax")
	}).done(function(data) {
		if (data) {
			$(".personal-info-section").replaceWith(data);
		}
		page.refresh();
	});
}

/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder() {
	$(".order-items")
		.find("li.hidden:first")
		.prev("li")
		.append('<a class="toggle">View All</a>')
		.children(".toggle")
		.click(function() {
			$(this)
				.parent()
				.siblings("li.hidden")
				.show();
			$(this).remove();
		});
}
/**
 * @private
 * @function
 * @description runs the address-fields AJAX call and shows fields
 */
function showAddressEvents(clickedClass) {
	var url = Urls.addAddressFields;
	$.ajax({
		url: url,
		type: "GET",
		dataType: "html"
	}).done(function(data) {
		$(clickedClass).hide();
		$(".address-fields").html(data);
		$(".address-fields").show();
		$('input[name="isSaveAddress"]').val(true);
	});
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {
	$(".add-card").on("click", function(e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr("href"),
			options: {
				open: initializePaymentForm
			}
		});
	});

	var paymentList = $(".payment-list");
	if (paymentList.length === 0) {
		return;
	}

	util.setDeleteConfirmation(
		paymentList,
		String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD)
	);

	$('form[name="payment-remove"]').on("submit", function(e) {
		e.preventDefault();
		// override form submission in order to prevent refresh issues
		var button = $(this).find(".delete");
		$("<input/>")
			.attr({
				type: "hidden",
				name: button.attr("name"),
				value: button.attr("value") || "delete card"
			})
			.appendTo($(this));
		var data = $(this).serialize();
		$.ajax({
			type: "POST",
			url: $(this).attr("action"),
			data: data
		}).done(function() {
			page.redirect(Urls.paymentsList);
		});
	});
}

function initializePaymentForm() {
	$("#CreditCardForm").on("click", ".cancel-button", function(e) {
		e.preventDefault();
		dialog.close();
	});
}

/**
 * @private
 * @function
 * @description Binds the events of profile contact form
 */
function initializeProfileFormEvents() {
	$(document).on("click", ".edit-profile", function() {
		$.ajax({
			url: util.appendParamToURL(Urls.editProfile, "format", "ajax")
		}).done(function(data) {
			if (data) {
				$(".personal-info-section").html(data);
				validator.init();
			} else {
				page.refresh();
			}
			$(".address-fields").hide();
			$(".add-address").on("click", function() {
				showAddressEvents(this);
			});
			$(".edit-address").on("click", function() {
				showAddressEvents(this);
			});
			populatePhoneValues();
		});
	});

	$(document).on("click", ".edit-profile-cancel", function() {
		showProfileSummary();
	});

	$(document).on("click", ".save-profile", function(e) {
		e.preventDefault();
		var isSaveAddress = $('input[name="isSaveAddress"]').val();
		if (isSaveAddress) {
			$(this)
				.closest("form")
				.find(".address-fields input")
				.each(function() {
					// remove required class
					$(this).removeClass("required");
				});
		}

		var $form = $("#edit-profile-form");

		if (!$form.valid()) {
			return false;
		}

		var url = util.appendParamToURL($form.attr("action"), "format", "ajax");
		var applyName = $form.find(".save-profile").attr("name");
		var options = {
			url: url,
			data: $form.serialize() + "&" + applyName + "=x",
			type: "POST"
		};
		$.ajax(options).done(function(data) {
			if (typeof data !== "string") {
				if (data.success) {
					showProfileSummary();
				} else if (data.error) {
					page.redirect(Urls.csrffailed);
				}
			}
		});
	});

	$(document).on("change", ".input-select.phonetype", function() {
		populatePhoneValues();
	});

	$(document).ready(function() {
		$(".edit-vehicle").hide();
		$(".edit-vehicle-cancel").hide();
		$(".show-edit-vehicle").show();
	});

	$(document).on("click", ".edit-vehicle-cancel", function() {
		$(".edit-vehicle").hide();
		$(".edit-vehicle-cancel").hide();
		$(".show-edit-vehicle").show();
	});

	$(document).on("click", ".show-edit-vehicle", function() {
		$(".edit-vehicle").show();
		$(".edit-vehicle-cancel").show();
		$(".show-edit-vehicle").hide();
	});

	// Delete Vehicle
	$(document).on("click", ".delete-vehicle", function(e) {
		e.preventDefault();

		var vehicleKey = $(this).data("id");
		var parentDiv = $(this).closest(".vehicle");
		$.ajax({
			type: "POST",
			url: Urls.deleteVehicle + "?vehiclekey=" + vehicleKey,
			success: function(response) {
				// update UI
				if (response.success) {
					parentDiv.remove();
					$("#service-response").show();
					$("#service-response").html(response.response);
					checkEmptyState();
				} else {
					alert(Resources.DELETE_ERROR);
				}
			}
		});
	});

	// Delete Tires
	$(document).on("click", ".delete-tire", function(e) {
		e.preventDefault();

		var tireKey = $(this).data("id");
		var parentDiv = $(this).closest(".tire");
		$.ajax({
			type: "POST",
			url: Urls.deleteTire + "?tirekey=" + tireKey,
			success: function(response) {
				// update UI
				if (response.success) {
					parentDiv.remove();
					$("#service-response").show();
					$("#service-response").html(response.response);
					checkEmptyState();
				} else {
					alert(Resources.DELETE_ERROR);
				}
			}
		});
	});

	// Delete Wheels
	$(document).on("click", ".delete-wheel", function(e) {
		e.preventDefault();

		var wheelKey = $(this).data("id");
		var parentDiv = $(this).closest(".wheel");
		$.ajax({
			type: "POST",
			url: Urls.deleteWheel + "?wheelkey=" + wheelKey,
			success: function(response) {
				// update UI
				if (response.success) {
					parentDiv.remove();
					$("#service-response").show();
					$("#service-response").html(response.response);
					checkEmptyState();
				} else {
					alert(Resources.DELETE_ERROR);
				}
			}
		});
	});
}

function checkEmptyState() {
	var wheelCount = $(".vehicle").length;
	var vehicleCount = $(".tire").length;
	var tireCount = $(".wheel").length;

	if (tireCount == 0 && wheelCount == 0 && vehicleCount == 0) {
		$("#no-vehicle-present").removeClass("hide");
		$("#no-vehicle-present").show();
		$("#vehicle-present").hide();
		$(".edit-vehicle-cancel").hide();
		$(".show-edit-vehicle").show();
	}
}

function populatePhoneValues() {
	var phoneType = $(".input-select.phonetype").val();
	var phoneField = $('input[name$="phonetemp"]');
	var previousValue; // used to store previous phone number that was used
	var actualValue; // what the value should be (if we already store it!)

	if (phoneType == "mobile") {
		actualValue = $('input[name$="_phonemobile"]').val();
	} else if (phoneType == "work") {
		actualValue = $('input[name$="_phonework"]').val();
	} else if (phoneType == "home") {
		actualValue = $('input[name$="_phonehome"]').val();
	}

	previousValue = phoneField.data("val");

	if (actualValue && actualValue != "") {
		// we have their number already, populate the field
		phoneField.val(actualValue);
	} else if (phoneField.val() === previousValue) {
		// the user didn't edit their phone, let clear it out for the new type
		phoneField.val("");
	}

	phoneField.data("val", phoneField.val()); // store previous value for later
}

/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
	toggleFullOrder();
	initPaymentEvents();
	login.init();
	initializeProfileFormEvents();
}

var account = {
	init: function() {
		initializeEvents();
		giftcert.init();

		$("button[name='changePassword']").on("click", function() {
			$(".settings-cp-button").hide();
			$(".settings-cp-cancel-button").removeClass("hide");
			$(".settings-cp-form").removeClass("hide");
		});

		$(".textSelected").on("change", function() {
			if ($(this).is(":checked") && $(".phoneExist").length == 0) {
				$(this)
					.parents(".toggleText")
					.find(".input-Text")
					.removeClass("hide");
			}
		});

		$(".save-Text").on("click", function(e) {
			e.preventDefault();
			var phoneNumber =
				$("input[name='billPayNumber']").val() ||
				$("input[name='appointmentNumber']").val() ||
				$("input[name='maintenanceNumber']").val();
			var prefName = $(this)
				.parents(".toggleText")
				.find(".textSelected")
				.attr("name");

			var url = util.appendParamsToUrl(Urls.saveTextSettings, {
				phoneNumber: phoneNumber,
				prefName: prefName
			});
			$.ajax({
				url: url,
				method: "POST"
			}).done(function() {
				location.reload();
			});
		});
		// give active class to secondary nav li item that is currently active
		$('.secondary-navigation a[href^="' + location.href + '"]')
			.parent()
			.addClass("active");
	},
	initCartLogin: function() {
		login.init();
	}
};

module.exports = account;
