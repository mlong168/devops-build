// var addProductToCart = require("./product/addToCart"),
var ajax = require("../ajax"),
	page = require("../page");
// productTile = require("../product-tile"),
// quickview = require("../quickview");

/**
 * @private
 * @function
 * @description Binds the click events to the remove-link and quick-view button
 */
function initializeEvents() {
	$("#compare-table").on("click", ".compare__table__head__remove", function(e) {
		e.preventDefault();
		ajax.getJson({
			url: this.href,
			callback: function() {
				page.refresh();
			}
		});
	});
}

exports.init = function() {
	initializeEvents();
};
