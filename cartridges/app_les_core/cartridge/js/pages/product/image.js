// var dialog = require("../../dialog");
var util = require("../../util");
// var qs = require("qs");
// var url = require("url");
// var _ = require("lodash");

// var zoomMediaQuery = matchMedia("(min-width: 960px)");
//
// /**
//  * @description Enables the zoom viewer on the product detail page
//  * @param zmq {Media Query List}
//  */
// function loadZoom() {
// 	var $imgZoom = $("#pdpMain .main-image").trigger("zoom.destroy"),
// 		hiresUrl;
// 	if (
// 		$imgZoom.length === 0 ||
// 		dialog.isActive() ||
// 		util.isMobile() ||
// 		!zoomMediaQuery.matches
// 	) {
// 		// remove zoom
// 		//$imgZoom.trigger("zoom.destroy");
// 		return;
// 	}
// 	hiresUrl = $imgZoom.attr("href");
//
// 	if (
// 		hiresUrl &&
// 		hiresUrl !== "null" &&
// 		hiresUrl.indexOf("noimagelarge") === -1 &&
// 		zoomMediaQuery.matches
// 	) {
// 		$imgZoom.zoom({
// 			url: hiresUrl
// 		});
// 	}
// }
//
// zoomMediaQuery.addListener(loadZoom);

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
function setMainImage(atts) {
	$("#pdpMain .primary-image")
		.attr({
			src: atts.url,
			alt: atts.alt,
			title: atts.title,
			srcset: atts.srcset
		})
		.closest(".main-image")
		.css({ backgroundImage: atts.hires });

	// updatePinButton(atts.url);
	// if (!dialog.isActive() && !util.isMobile()) {
	// 	$("#pdpMain .main-image").attr("href", atts.hires);
	// }
	// loadZoom();
}

// function updatePinButton(imageUrl) {
// 	var pinButton = document.querySelector(".share-icon[data-share=pinterest]");
// 	if (!pinButton) {
// 		return;
// 	}
// 	var newUrl = imageUrl;
// 	if (!imageUrl) {
// 		newUrl = document
// 			.querySelector("#pdpMain .primary-image")
// 			.getAttribute("src");
// 	}
// 	var href = url.parse(pinButton.href);
// 	var query = qs.parse(href.query);
// 	query.media = url.resolve(window.location.href, newUrl);
// 	query.url = window.location.href;
// 	var newHref = url.format(
// 		_.extend({}, href, {
// 			query: query, // query is only used if search is absent
// 			search: qs.stringify(query)
// 		})
// 	);
// 	pinButton.href = newHref;
// }

/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
function replaceImages() {
	var $newImages = $("#update-images"),
		$imageContainer = $("#pdpMain .productImages");
	if ($newImages.length === 0) {
		return;
	}

	$imageContainer.html($newImages.html());
	$newImages.remove();
	//	loadZoom();
}

/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function() {
	// if (dialog.isActive() || util.isMobile()) {
	// 	$("#pdpMain .main-image").removeAttr("href");
	// }
	// updatePinButton();
	// loadZoom();
	var mainImage = $("#pdpMain .main-image"),
		zoomImage,
		handleClick = function(el) {
			var image = $(el).find("img.productthumbnail"),
				data = image.data("image");
			// switch indicator
			image
				.closest(".productImages__thumbnails")
				.find(".thumb.selected")
				.removeClass("selected");
			image.closest(".thumb").addClass("selected");

			setMainImage(data);
			mainImage.addClass("loading");
			if (!util.isMobile()) {
				zoomImage
					.attr({
						src: data.hires,
						srcset: data.hiresSrcset
					})
					.css("display", "none")
					.on("load", function() {
						$(this).css({
							display: "block",
							width: this.naturalWidth + "px",
							height: this.naturalHeight + "px"
						});
						mainImage.removeClass("loading");
					});
			}
		};

	if (!util.isMobile()) {
		zoomImage = mainImage.find(".zoom-image");
		// zoomImage.css({
		// 	width: zoomImage.get(0).naturalWidth + "px",
		// 	height: zoomImage.get(0).naturalHeight + "px"
		// });
		mainImage.on("mousemove.zoom", function(e) {
			var dimensions = this.getBoundingClientRect(),
				x = Math.round(100 / (dimensions.width / (e.pageX - dimensions.left))),
				y = Math.round(100 / (dimensions.height / (e.pageY - dimensions.top))),
				xRatio = (zoomImage.outerWidth() - dimensions.width) / dimensions.width,
				yRatio =
					(zoomImage.outerHeight() - dimensions.height) / dimensions.height;
			zoomImage.css({
				left: "-" + x * xRatio + "%",
				top: "-" + y * yRatio + "%"
			});
		});
	}
	$("#pdpMain")
		.on("click.thumbs touch.carousel", ".thumbnail-link", function(e) {
			e.stopPropagation();
			handleClick(this);
		})
		.on("click.carousel touch.carousel", ".carousel__button", function(e) {
			e.stopPropagation();
			var self = $(this),
				thumbs = self
					.siblings(".productImages__thumbnails")
					.find(".thumb")
					.toArray(),
				selectedInd = thumbs.findIndex(function(element) {
					return element.className.indexOf("selected") > -1;
				});
			if (self.hasClass("carousel__button--prev")) {
				if (selectedInd < 1) {
					handleClick(thumbs[thumbs.length - 1]);
				} else {
					handleClick(thumbs[selectedInd - 1]);
				}
			} else if (selectedInd > thumbs.length - 2) {
				handleClick(thumbs[0]);
			} else {
				handleClick(thumbs[selectedInd + 1]);
			}
		});
};
// module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;
