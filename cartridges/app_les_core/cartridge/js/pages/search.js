var compareWidget = require("../compare-widget"),
	productTile = require("../product-tile"),
	progress = require("../progress"),
	util = require("../util");
var GTM = require("../gtm-util");

/**
 * @private
 * @function
 * @description used to load more products into categoryproducthits.isml - used for tires and wheels
 */
// function loadMoreProducts(element) {
function loadMoreProducts(element) {
	var loadingPlaceHolder = $(element).closest(
			".view-more-products-placeholder"
		),
		gridUrl = loadingPlaceHolder.attr("data-grid-url");
	var gridUrlformat = loadingPlaceHolder.attr("data-format");
	gridUrl = gridUrl + "&format=" + gridUrlformat;
	progress.show($("#main"));

	$.ajax({
		type: "GET",
		dataType: "html",
		url: gridUrl,
		success: function(response) {
			var searchContentSelector = ".search-result-items";
			loadingPlaceHolder.closest(searchContentSelector).append(
				$(response)
					.filter(searchContentSelector)
					.children()
			);
			loadingPlaceHolder.remove();
			GTM.productImpressions();
		},
		complete: function() {
			progress.hide();
		}
	});
}

/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url, isCatLandingProductHits) {
	if (!url || url === window.location.href) {
		return;
	}
	progress.show($("#main"));

	if (isCatLandingProductHits) {
		var landingGrid = $("#landing-producthits");
		$.ajax({
			type: "GET",
			dataType: "html",
			url: util.appendParamToURL(url, "format", "ajax"),
			success: function(response) {
				landingGrid.html(response);
			},
			complete: function() {
				$("#cat-search-result-content").on(
					"click.toggleProductInfoTile",
					".product-info-toggle",
					function() {
						productTile.handleProductTileClick($(this));
					}
				);
				progress.hide();
			}
		});
	} else {
		$.get(util.appendParamToURL(url, "format", "ajax"), function(data) {
			var docF = $("<div></div>").append(data),
				topBarSelector = ".topBar",
				searchResultsSelector = ".search-result-content";
			if (docF.find(topBarSelector)) {
				$(topBarSelector).replaceWith(docF.find(topBarSelector));
				// unshift products;
				$(".search__content").css("margin-left", "auto");
			}
			if (docF.find(searchResultsSelector)) {
				$(searchResultsSelector).replaceWith(docF.find(searchResultsSelector));
			}
			progress.hide();
			history.pushState(undefined, "", url);
			GTM.productImpressions();
		});
	}
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $("#main");

	$(document).on("click", ".view-more-product-link", function(e) {
		e.preventDefault();
		getMoreProducts($(this));
	});

	$(document).on("click", ".view-more-products", function(e) {
		e.preventDefault();
		loadMoreProducts($(this));
	});

	// compare checked -- PRESERVE
	$main.on("click", 'input[type="checkbox"].compare-check', function() {
		var cb = $(this);
		var tile = cb.closest(".product-tile");

		var func = this.checked
			? compareWidget.addProduct
			: compareWidget.removeProduct;
		var itemImg = tile.find(".product-tile-image").first(),
			name = tile.data("name");
		func({
			itemid: tile.data("itemid"),
			uuid: tile[0].id,
			img: itemImg,
			cb: cb,
			name: name // tile.find(".product-name").first()
		});
	});

	// handle events for updating grid
	$main.on("click.updateListing", ".js-update-listing", function(e) {
		e.preventDefault();
		updateProductListing(this.href);
	});

	// handle events for category landing updating grid
	$main.on("change", ".refinements select", function(e) {
		// don't intercept for category and folder refinements, as well as unselectable
		var $this = $(this);
		if (
			$this.parents(".category-refinement").length > 0 ||
			$this.parents(".folder-refinement").length > 0 ||
			$this.parent().hasClass("unselectable")
		) {
			return;
		}
		e.preventDefault();
		var isCatLandingProductHits = $main.find("#landing-producthits").length > 0;
		updateProductListing(
			$this.find("option:selected").val(),
			isCatLandingProductHits
		);
	});

	// handle events item click. append params.
	$main.on("click", '.product-tile a:not("#quickviewbutton")', function() {
		var a = $(this);
		// get current page refinement values
		var wl = window.location;

		var qsParams =
			wl.search.length > 1
				? util.getQueryStringParams(wl.search.substr(1))
				: {};
		var hashParams =
			wl.hash.length > 1 ? util.getQueryStringParams(wl.hash.substr(1)) : {};

		// merge hash params with querystring params
		var params = $.extend(hashParams, qsParams);
		if (!params.start) {
			params.start = 0;
		}
		// get the index of the selected item and save as start parameter
		var tile = a.closest(".product-tile");
		var idx = tile.data("idx") ? +tile.data("idx") : 0;

		// convert params.start to integer and add index
		params.start = +params.start + (idx + 1);
		// set the hash and allow normal action to continue
		a[0].hash = $.param(params);
	});

	// handle sorting change
	$main
		.on("change", ".sort-by select", function(e) {
			e.preventDefault();
			var option = $(this).find("option:selected"),
				dataLayer = window.dataLayer || [];

			updateProductListing(option.val());

			dataLayer.push({
				event: "Sort",
				category: "Sort",
				action: option.text()
			});
		})
		.on("change", ".items-per-page select", function() {
			var refineUrl = $(this)
				.find("option:selected")
				.val();
			if (refineUrl === "INFINITE_SCROLL") {
				$("html")
					.addClass("infinite-scroll")
					.removeClass("disable-infinite-scroll");
			} else {
				$("html")
					.addClass("disable-infinite-scroll")
					.removeClass("infinite-scroll");
				updateProductListing(refineUrl);
			}
		});
	//* ***************new
	$main
		.on("click.sidebartoggle", ".topBar__otherfilters", function() {
			var self = $(this),
				sidebar = $(".topBar__filtersidebar"),
				contentSection = $(".search__content"),
				footer = $("footer.footer");
			// toggle search results grid columns
			if (!self.hasClass("open")) {
				let offset = contentSection.offset(),
					widthAvailable = offset.left * 2 > 400 ? 400 : offset.left * 2,
					panelWidth = Math.max(Math.min(widthAvailable, 400), 300);
				sidebar.css({
					width: panelWidth,
					// height: contentSection.outerHeight(),
					height: $(window).height() - $(".topBar").height(),
					left: Math.max(panelWidth, offset.left) - panelWidth,
					right: Math.max(panelWidth, offset.left)
				});
				footer.css({ paddingLeft: panelWidth + 20 });
				contentSection.animate(
					{
						marginLeft: Math.max(panelWidth, offset.left) + "px"
					},
					500
				);
			} else {
				contentSection.css("margin-left", "auto");
				sidebar.css({ left: "-400px", right: "100%" });
				footer.css("padding-left", "");
			}
			self.add(sidebar).toggleClass("open", !self.hasClass("open"));
		})
		.on(
			"click.sibebartoggle_mobile",
			".topBar__main__filters__otherfilters",
			function() {
				$(".topBar__filtersidebar").addClass("open");
			}
		)
		.on(
			"click.sibebartoggle_mobile",
			".topBar__filtersidebar__hide",
			function() {
				$(".topBar__filtersidebar").removeClass("open");
			}
		)
		.on("click.compare", ".topBar__compare__header", function() {
			$(this)
				.closest(".topBar__compare")
				.toggleClass("topBar__compare--open");
		})
		.on("click.tabs", ".button-group--switch button", function() {
			// tab switch functionality
			$(".search__tab").addClass("search__tab--inactive");
			$(
				$(this)
					.addClass("active")
					.siblings()
					.removeClass("active")
					.end()
					.data("target")
			).removeClass("search__tab--inactive");
		});
	// sticky topBar
	var topBar = $(".topBar"),
		originalOffset = null,
		height = topBar.height(),
		placeholder = null;
	if (topBar && topBar.length > 0) {
		$(window).on("scroll.sticky", function() {
			// topBar might've been replaced or changed height
			topBar = $(".topBar");
			height = topBar.height();
			// originalOffset might not be the most accurate one since DOM changes might've occured
			if (!originalOffset) {
				originalOffset = topBar.offset().top;
			}
			var isSticky = $(this).scrollTop() > originalOffset + height;
			if (isSticky) {
				if (!placeholder) {
					placeholder = $("<div/>")
						.insertAfter(topBar)
						.css("height", height);
				}
			} else if (placeholder && placeholder.remove) {
				placeholder.remove();
				placeholder = null;
			}
			topBar.toggleClass("topBar--sticky", isSticky);
		});
	}
}

/**
 * @private
 * @function
 * @description used to load more products into categoryproducthits2.isml - used for batteries and chains
 */
function getMoreProducts(element) {
	var viewMorePlaceHolder = element.closest(".view-more-placeholder");
	var gridUrl = viewMorePlaceHolder.attr("view-more-url");
	var $element = element;

	$.ajax({
		type: "GET",
		dataType: "html",
		url: gridUrl,
		success: function(response) {
			// update UI
			viewMorePlaceHolder.removeClass("view-more-placeholder");
			var result = $(response);
			$("#cat-search-result-content").append(result);
			$element.remove();
		}
	});
}

exports.init = function() {
	compareWidget.init();
	// productTile.init();
	initializeEvents();
};
