"use strict";

var startElement = 1;
var salesPage = false;
var salesPageSlot = "";
var GTMUtils = {
	productDetails: function() {
		// action : detail
		if (!SitePreferences.ENABLE_GTM) {
			return;
		}
		var product = this.dataLayerGetProductDetails();
		this.dataLayerPushEvent({
			ecommerce: {
				detail: {
					products: product
				}
			}
		});
	},
	productImpression: function(pid, isRecommendation) {
		// action : impression
		if (!SitePreferences.ENABLE_GTM) {
			return;
		}
		var productImpression = this.dataLayerPushProductImpression(
			pid,
			isRecommendation
		);
		if (!productImpression) {
			return;
		}
		this.dataLayerPushEvent({
			event: "productClick",
			ecommerce: {
				click: {
					actionField: { list: productImpression[0].list },
					products: productImpression
				}
			}
		});
	},
	productImpressions: function() {
		// list of products in category, search pages
		if (!SitePreferences.ENABLE_GTM) {
			return;
		}
		this.dataLayerPushProductListImpression();
	},
	dataLayerPushEvent: function(eventObj) {
		if (window.universal_variable) {
			window.dataLayer.push(eventObj);
		}
	},
	dataLayerGetProductDetails: function() {
		if (window.universal_variable && window.universal_variable.product) {
			var product = window.universal_variable.product;
			var variant = GTMUtils.getVariantArray(product);
			var productData = {
				id: product.id,
				name: product.name,
				brand: product.brand,
				category: product.category,
				variant: variant,
				price: product.unit_sale_price,
				monthlyPrice: product.monthly_payment_price,
				loadIndex: product.loadIndex,
				aspectRatio: product.aspect_ratio
			};
			if (product.category == "Tires") {
				productData.rimSize = product.rimSize;
			} else {
				productData.diameter = product.diameter;
			}
			return [productData];
		}
	},
	dataLayerPushProduct: function(qty) {
		if (window.universal_variable && window.universal_variable.product) {
			var product = window.universal_variable.product;
			var productData = {
				id: product.id,
				name: product.name,
				brand: product.brand,
				category: product.category,
				variant:
					product.attributes.variations.length > 0
						? product.attributes.variations[0].value
						: "",
				price: product.unit_sale_price,
				quantity: qty != undefined && qty != null && qty > 0 ? qty : 1,
				position: 0,
				loadIndex: product.loadIndex,
				aspectRatio: product.aspect_ratio
			};
			if (product.category == "Tires") {
				productData.rimSize = product.rimSize;
			} else {
				productData.diameter = product.diameter;
			}
			return [productData];
		}
	},
	dataLayerPushProductImpression: function(
		pid,
		isRecommendation,
		isProductSet
	) {
		var items = [];

		if (
			isRecommendation &&
			window.universal_variable &&
			window.universal_variable.recommendation
		) {
			items = window.universal_variable.recommendation.items;
		} else if (
			isProductSet &&
			window.universal_variable &&
			window.universal_variable.product &&
			window.universal_variable.product.linked_products
		) {
			items = window.universal_variable.product.linked_products;
		} else if (window.universal_variable && window.universal_variable.listing) {
			items = window.universal_variable.listing.items;
			if (
				window.universal_variable.listing.recommendedProducts &&
				window.universal_variable.listing.recommendedProducts.length > 0
			) {
				items = window.universal_variable.listing.recommendedProducts;
				items = items.concat(window.universal_variable.listing.items);
			}
		} else if (
			window.universal_variable &&
			(window.universal_variable.slotProductListing1 ||
				window.universal_variable.slotProductListing2)
		) {
			items = window.universal_variable.slotProductListing1.items;
			if (
				window.universal_variable.slotProductListing1 &&
				window.universal_variable.slotProductListing1.items.length > 0
			) {
				items = items.concat(
					window.universal_variable.slotProductListing2.items
				);
			} else {
				items = window.universal_variable.slotProductListing2.items;
			}
		}

		if (items && items.length > 0) {
			var item, variant;
			var index = 0;
			for (var i = 0; i < items.length; i++) {
				if (items[i].id == pid || items[i].sku_code == pid) {
					item = items[i];
					index = i + 1;
					break;
				}
			}
			if (!item) {
				return null;
			}
			var listName;
			if (isRecommendation) {
				listName = "Recommendation";
			} else if (salesPage) {
				listName = "Sale Listing";
			} else {
				listName = window.universal_variable.page.category;
			}

			variant = GTMUtils.getVariantArray(item);
			var productData = {
				id: item.id,
				name: item.name,
				brand: item.brand,
				list: listName,
				category: item.category,
				variant: variant,
				price: item.unit_sale_price,
				position: index,
				monthlyPrice: item.monthly_payment_price,
				loadIndex: item.loadIndex,
				aspectRatio: item.aspect_ratio
			};
			if (item.category == "Tires") {
				productData.rimSize = item.rimSize;
			} else {
				productData.diameter = item.diameter;
			}
			if (salesPage) {
				if (salesPageSlot == "passenger-car-tires") {
					productData.recommendedTires = "Passenger car tires";
				} else if (salesPageSlot == "truck-suv-tires") {
					productData.recommendedTires = "Truck tires";
				}
			}
			return [productData];
		}
	},
	getVariantArray: function(product) {
		var variant = [];
		var item = product;
		var warranty;
		var dataObj = {};
		dataObj.sale = item.onSale;
		dataObj.size = item.productSize;
		if (item.category == "Wheels") {
			if (item.warranty) {
				warranty = item.warranty + " " + "year";
			}
			dataObj.warranty = warranty;
			dataObj.diameter = item.diameter;
			dataObj.finish = item.finish;
		} else if (item.category == "Tires") {
			if (item.warranty) {
				warranty = item.warranty + " " + "miles";
			}
			dataObj.rimSize = item.rimSize;
			dataObj.warranty = warranty;
			dataObj.loadIndex = item.loadIndex;
		}
		variant.push(dataObj);
		return variant;
	},
	dataLayerPushProductListImpression: function() {
		var productList = [];
		if (!SitePreferences.ENABLE_GTM) {
			return;
		}
		if (
			window.universal_variable &&
			window.universal_variable.listing.currentItems
		) {
			if (
				window.universal_variable &&
				window.universal_variable.listing &&
				window.universal_variable.listing.currentItems.length > 0
			) {
				var productListItems = window.universal_variable.listing.currentItems;
				var startIndex;
				var variant;
				var position;
				var recProdId = [];
				if (
					window.universal_variable.listing.recommendedProducts &&
					window.universal_variable.listing.recommendedProducts.length > 0
				) {
					var recProds = window.universal_variable.listing.recommendedProducts;
					if (window.universal_variable.listing.pagination.currentPage == 0) {
						productListItems = recProds;
						productListItems = productListItems.concat(
							window.universal_variable.listing.currentItems
						);
					}
					for (var i = 0; i < recProds.length; i++) {
						recProdId.push(recProds[i].id);
					}
				}
				startIndex = startElement;
				if (window.universal_variable.listing.pagination.currentPage > 0) {
					startIndex = startElement + 1;
				}
				var index = 0;
				productListItems.forEach(function(product) {
					if (
						(window.universal_variable.listing.pagination.currentPage == 0 &&
							index < 3) ||
						recProdId.indexOf(product.id) == -1
					) {
						variant = GTMUtils.getVariantArray(product);
						position = startIndex + index;
						index++;
						var productData = {
							id: product.id,
							name: product.name,
							list: window.universal_variable.page.category,
							brand: product.brand,
							category: product.category,
							variant: variant,
							price: product.unit_sale_price,
							position: position,
							monthlyPrice: product.monthly_payment_price,
							loadIndex: product.loadIndex,
							aspectRatio: product.aspect_ratio
						};
						if (product.category == "Tires") {
							productData.rimSize = product.rimSize;
						} else {
							productData.diameter = product.diameter;
						}
						productList.push(productData);
						startElement = position;
					}
				});
				window.dataLayer.push({
					ecommerce: {
						impressions: productList
					}
				});
			}
		}
	},
	dataLayerPushSalesProductListImpression: function() {
		var productList = [];
		if (!SitePreferences.ENABLE_GTM) {
			return;
		}
		if (
			window.universal_variable &&
			((window.universal_variable.slotProductListing1 &&
				window.universal_variable.slotProductListing1.items.length > 0) ||
				(window.universal_variable.slotProductListing2 &&
					window.universal_variable.slotProductListing2.items.length > 0))
		) {
			var productListItems;
			productListItems = window.universal_variable.slotProductListing1.items;
			if (
				window.universal_variable.slotProductListing1 &&
				window.universal_variable.slotProductListing1.items.length > 0
			) {
				productListItems = productListItems.concat(
					window.universal_variable.slotProductListing2.items
				);
			} else {
				productListItems = window.universal_variable.slotProductListing2.items;
			}

			var variant;
			var index = 0;
			productListItems.forEach(function(product) {
				variant = GTMUtils.getVariantArray(product);
				index++;
				var productData = {
					id: product.id,
					name: product.name,
					list: "Sale Listing",
					brand: product.brand,
					category: product.category,
					variant: variant,
					price: product.unit_sale_price,
					position: index,
					monthlyPrice: product.monthly_payment_price,
					loadIndex: product.loadIndex,
					aspectRatio: product.aspect_ratio
				};
				if (product.category == "Tires") {
					productData.rimSize = product.rimSize;
				} else {
					productData.diameter = product.diameter;
				}
				productList.push(productData);
			});
			window.dataLayer.push({
				ecommerce: {
					impressions: productList
				}
			});
		}
	},
	initProductListEvent: function() {
		GTMUtils.productImpressions();
		$(document).on(
			"click",
			".product-tile.product-tile--withPrice .button.button--flexWide",
			function() {
				var pid = $(this)
					.parents(".product-tile")
					.data("itemid");
				if (pid) {
					GTMUtils.productImpression(pid, false);
				}
			}
		);
	},
	initProductRecommendedListEvent: function() {
		$(document).on(
			"click",
			".product-tile.product-tile--withPrice .button.button--flexWide",
			function() {
				var pid = $(this)
					.parents(".product-tile")
					.data("itemid");
				if (pid) {
					GTMUtils.productImpression(pid, true);
				}
			}
		);
	},
	initSalesPageEvent: function() {
		$(document).on(
			"click",
			".passenger-car-tires .productRecommendations .recommendations .product-tile.product-tile--withPrice .button.button--flexWide",
			function() {
				var pid = $(this)
					.parents(".product-tile")
					.data("itemid");
				if (pid) {
					salesPage = true;
					salesPageSlot = "passenger-car-tires";
					GTMUtils.productImpression(pid, false);
					salesPage = false;
				}
			}
		);
		$(document).on(
			"click",
			".truck-suv-tires .productRecommendations .recommendations .product-tile.product-tile--withPrice .button.button--flexWide",
			function() {
				var pid = $(this)
					.parents(".product-tile")
					.data("itemid");
				if (pid) {
					salesPage = true;
					salesPageSlot = "truck-suv-tires";
					GTMUtils.productImpression(pid, false);
					salesPage = false;
				}
			}
		);
	},

	initializeEvents: function() {
		if (!SitePreferences.ENABLE_GTM) {
			return;
		}
		window.dataLayer = window.dataLayer || [];
		var dlObject = window.universal_variable;

		if (dlObject && dlObject.product) {
			this.initProductRecommendedListEvent();
			this.productDetails();
		}
		if (
			dlObject &&
			(dlObject.slotProductListing1 || dlObject.slotProductListing2)
		) {
			this.initSalesPageEvent();
			this.dataLayerPushSalesProductListImpression();
		}
		if (dlObject && dlObject.page) {
			var pageCat = window.universal_variable.page.category;
			if (
				pageCat.indexOf("Search Results") > -1 ||
				pageCat.indexOf("Category Listing") > -1 ||
				pageCat.indexOf("Category Landing") > -1
			) {
				this.initProductListEvent();
			}
		}
	}
};
module.exports = GTMUtils;
