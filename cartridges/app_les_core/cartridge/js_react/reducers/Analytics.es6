import types from "actions/Types";
import moment from "moment";

const dataLayer = window.dataLayer || [];

const logger = store => next => action => {
	const state = store.getState();
	switch (action.type) {
		case types.REACT_RENDER_COMPLETE:
			break;
		case types.SELECTOR:
			window.dataLayer.push({
				event: "Vehicle Entry",
				category: action.eventData.category,
				action: action.eventData.action,
				label: action.eventData.label
			});
			break;
		case types.GLOBAL_SEARCH:
			window.dataLayer.push({
				event: "Search",
				action: action.action,
				label: action.query
			});
			break;
		case types.STORE_SEARCH:
			window.dataLayer.push({
				event: "Store Locator",
				category: "Store Locator",
				action: action.action,
				label: action.label
			});
			break;
		case types.SET_MY_STORE:
			if (!action.isNoLog) {
				window.dataLayer.push({
					event: "Store Locator",
					category: "Store Locator",
					action: "Make it My Store",
					label: action.storeId
				});
				//updating mystore id in universal_variable used by GTM
				if (window.universal_variable && window.universal_variable.session) {
					window.universal_variable.session.storeId = action.storeId;
				}
			}
			break;
		case types.SELECT_COLOR:
			window.dataLayer.push({
				event: "Visualizer",
				category: "Colorizer",
				action: "Color selected",
				label: action.color
			});
			break;
		case types.VISUALIZER_PREVIEW:
			window.dataLayer.push({
				event: "Visualizer",
				category: "Visualizer",
				action: "Wheel selected",
				label: action.data
			});
			break;
		case types.ERROR_ENCOUNTERED:
			window.dataLayer.push({
				event: "Error",
				category: "Error",
				action: action.location || "Unspecified",
				label: action.error
			});
			break;
		//Appointment scheduler
		case types.VALIDATION_ERROR:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Error",
				action: action.error,
				label: ""
			});
			break;
		case types.NEXT_STEP:
			dataLayer.push(
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page + 1,
					label: "Complete"
				},
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page + 2,
					label: "Start"
				}
			);
			if (state.appState.page === 1) {
				dataLayer.push({
					event: "MakeAppt",
					category: "Appt Date",
					action: Math.abs(
						moment(
							state.dateState.date + state.dateState.time,
							"YYYY-MM-DDHmm"
						).diff(moment(), "hours")
					),
					label: state.dateState.time
				});
			}
			break;
		case types.PREV_STEP:
			dataLayer.push(
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page + 1,
					label: "Complete"
				},
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page,
					label: "Start"
				}
			);
			break;
		case types.PHONE_CLICKED:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Call",
				action: action.location,
				label: ""
			});
			break;
		case types.USER_SELECTED_SERVICES:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Category",
				action: action.selectedServices,
				label: ""
			});
			break;
		// case types.RECEIVE_STORE_INFO:
		// 	dataLayer.push({
		// 		event: "MakeAppt",
		// 		category: "Appt Step",
		// 		StoreIDVar: action.storeInfo.storeId,
		// 		action: 1,
		// 		label: "Start"
		// 	});
		// 	break;
		case types.PAGE_VIEW:
			dataLayer.push(action.data);
			break;
		case types.RECEIVE_APPOINTMENT_CONFIRMATION:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Step",
				action: 3,
				label: "Complete"
			});
			try {
				window.usabilla_live("trigger", "appointment trigger");
			} catch (a) {
				break;
			}
			break;
	}
	return next(action);
};

export default logger;
