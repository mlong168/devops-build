import types from "actions/Types";

const apptState = (
	state = {
		page: 0,
		isLoading: false,
		error: null,
		confirmationData: null,
		showHeader: true
	},
	action
) => {
	switch (action.type) {
		case types.SUBMIT_REQUEST:
			return { ...state, isLoading: true };
		case types.RECEIVE_APPOINTMENT_CONFIRMATION:
			return {
				...state,
				isLoading: false,
				page: -1,
				confirmationData: action.data
			};
		case types.ERROR_ENCOUNTERED:
			return { ...state, error: action.error, isLoading: false };
		case types.NEXT_STEP:
			return {
				...state,
				page: state.page >= 2 ? state.page : state.page + 1
			};
		case types.PREV_STEP:
			return { ...state, page: state.page <= 0 ? 0 : state.page - 1 };
		//case types.RECEIVE_STORE_INFO:
		case types.TIMES_RECEIVED:
			return {
				...state,
				isLoading: false
			};
		case types.TOGGLE_HEADER:
			return { ...state, showHeader: !state.showHeader };
		default:
			return state;
	}
};

const servicesState = (
	state = {
		selectedServices: [],
		additionalInfo: "",
		vehicleDescription: "",
		tireSize: ""
	},
	action
) => {
	switch (action.type) {
		case types.USER_SELECTED_SERVICES:
			return {
				...state,
				selectedServices: action.selectedServices.slice(),
				additionalInfo: action.additionalInfo || ""
			};
		// case types.RECEIVE_STORE_INFO:
		// 	return {
		// 		...state,
		// 		storeId: action.storeInfo.storeId,
		// 		storeInfo: action.storeInfo,
		// 		selectedServices: []
		// 	};
		case types.LOAD_URL_PARAMS:
			return {
				...state,
				vehicleDescription: action.vehicleDescription,
				tireSize: action.tireSize
			};
		default:
			return state;
	}
};

const dateState = (
	state = {
		date: null,
		time: null,
		timesMatrix: {},
		isLoading: false
	},
	action
) => {
	switch (action.type) {
		case types.TIMES_REQUESTED:
			return { ...state, isLoading: true };
		case types.TIME_SELECTED:
			return { ...state, time: action.time };
		case types.TIMES_RECEIVED:
			return {
				...state,
				isLoading: false,
				timesMatrix: {
					...state.timesMatrix,
					[action.date]: action.times
				}
			};
		case types.DATE_SELECTED:
			return { ...state, date: action.date };
		default:
			return state;
	}
};

export { apptState, servicesState, dateState };
