import { combineReducers } from "redux";
import types from "actions/Types";
import analytics from "./Analytics";
import { apptState, servicesState, dateState } from "./Appointment";
import config from "config";

// const defaultState = {
// 	//hold name of the currently active "greedy" component - similar to modal behavior,
// 	//when only one is allowed to be open at the given moment
// 	greedyComponent: null,
// 	isLoading: false,
// 	vehicles: [],
// 	tires: null,
// 	wheels: null,
// 	//contains list of stores used by store locator to populate map markers
// 	stores: [],
// 	myStore: null,
// 	selectedStore: null,
// 	color: null,
// 	boltpatterns: null,
// 	uLat: null,
// 	uLng: null
// };

function addStore(stores, store) {
	let newStores = stores,
		existingIndex = stores.findIndex(el => el.id === store.id);
	newStores.splice(existingIndex, existingIndex > -1 ? 1 : 0, store);
	return newStores;
}
const globalState = (state = {}, action) => {
	switch (action.type) {
		case types.SET_USER_LOCATION:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({
					...state,
					uLat: action.uLat,
					uLng: action.uLng
				})
			);
			return { ...state, uLat: action.uLat, uLng: action.uLng };
		case types.SELECT_COLOR:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({ ...state, color: action.color })
			);
			return { ...state, color: action.color };
		case types.SET_BOLTPATTERNS:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({ ...state, boltpatterns: action.boltpatterns })
			);
			return { ...state, boltpatterns: action.boltpatterns };
		case types.SUBMIT_REQUEST:
			return { ...state, isLoading: true };
		case types.COMPLETE_REQUEST:
			return { ...state, isLoading: false };
		case types.SET_MY_STORE:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({ ...state, myStore: action.storeId })
			);
			return { ...state, myStore: action.storeId, isLoading: false };
		case types.ADD_STORE_DETAILS:
			return {
				...state,
				stores: addStore(state.stores, action.store)
				//myStore: action.store.isMystore ? action.store.id : state.myStore
			};
		case types.SET_SELECTED_STORE:
			return {
				...state,
				selectedStore: action.id
			};
		case types.ADD_VEHICLE:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({
					...state,
					vehicles: [action.vehicle],
					tires: [],
					wheels: []
				}) //TODO: currently only saving one
			);
			return {
				...state,
				vehicles: [action.vehicle],
				tires: [],
				wheels: [],
				isLoading: false
			};
		case types.ADD_TIRES:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({
					...state,
					tires: [action.tires],
					vehicles: [],
					wheels: []
				})
			);
			return {
				...state,
				tires: [action.tires],
				vehicles: [],
				wheels: [],
				isLoading: false
			};
		case types.ADD_WHEELS:
			localStorage.setItem(
				config.localStorageId,
				JSON.stringify({
					...state,
					wheels: [action.wheels],
					tires: [],
					vehicles: []
				})
			);
			return {
				...state,
				wheels: [action.wheels],
				tires: [],
				vehicles: [],
				isLoading: false
			};
		case types.TOGGLE_GREEDY_COMPONENT:
			return { ...state, greedyComponent: action.greedyComponent };
		case types.REACT_RENDER_COMPLETE:
		default:
			return state;
	}
};

const rootReducer = combineReducers({
	globalState,
	apptState,
	servicesState,
	dateState
});
export { analytics, rootReducer };
