import React from "react";
import VehicleSelectorCTAView from "components/VehicleSelectorCTA";

const VehicleSelectorCTA = props => <VehicleSelectorCTAView {...props} />;

export default VehicleSelectorCTA;
