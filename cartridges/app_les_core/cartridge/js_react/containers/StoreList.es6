import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import StoreDetails from "containers/StoreDetails";
import { errorEncountered } from "../actions/Global";
/**
	Store list - renders store details modules with distance and index information.
*/

class StoreList extends React.Component {
	componentDidMount() {
		if (this.props.storeList.length < 1) {
			this.props.dispatch(errorEncountered("No results", "Store locator"));
		}
	}
	componentDidUpdate() {
		if (this.props.storeList.length < 1) {
			this.props.dispatch(errorEncountered("No results", "Store locator"));
		}
	}
	render() {
		let list = null;
		let className = "storeList";
		if (this.props.storeList.length < 1) {
			list = (
				<div>
					<p className="storeList__noResultsMessage">
						{this.props.noResultsMessage}
					</p>
					<p>{this.props.noResultsSubMessage}</p>
				</div>
			);
			className += " storeList--noResults";
		} else {
			list = (
				<ul>
					{this.props.storeList.map((store, ind) => (
						<li key={ind}>
							<StoreDetails {...store} index={ind} />
						</li>
					))}
				</ul>
			);
		}
		return <div className={className}>{list}</div>;
	}
}
StoreList.propTypes = {
	componentName: PropTypes.string,
	noResultsMessage: PropTypes.string,
	noResultsSubMessage: PropTypes.string,
	storeList: PropTypes.array,
	dispatch: PropTypes.func
};
const mapStateToProps = state => ({
	selectedStore: state.globalState.selectedStore
});
export default connect(mapStateToProps)(StoreList);
