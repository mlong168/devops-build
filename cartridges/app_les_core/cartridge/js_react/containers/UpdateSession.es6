import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
	postMyStore,
	postVehicleDetails,
	postUserLocation
} from "../actions/Global";
import config from "config";
/**
	Component that updates server side with user data stored on the clientside
*/

class UpdateSession extends React.Component {
	componentWillMount() {
		//attemt to locate user
		navigator.geolocation.getCurrentPosition(
			position => {
				//setting user location
				this.props.dispatch(
					postUserLocation(this.props.postUserLocationUrl, {
						uLat: position.coords.latitude,
						uLng: position.coords.longitude
					})
				);
			},
			() => {},
			config.geolocationClientOptions
		);
		if (this.props.vehicles && this.props.vehicles.length > 0) {
			this.props.dispatch(
				postVehicleDetails(this.props.postVehicleDetailsUrl, {
					vehicle: this.props.vehicles[0]
				})
			);
		}
		if (this.props.tires && this.props.tires.length > 0) {
			this.props.dispatch(
				postVehicleDetails(this.props.postVehicleDetailsUrl, {
					tires: this.props.tires[0]
				})
			);
		}
		if (this.props.wheels && this.props.wheels.length > 0) {
			this.props.dispatch(
				postVehicleDetails(this.props.postVehicleDetailsUrl, {
					wheels: this.props.wheels[0]
				})
			);
		}
		if (this.props.myStore) {
			this.props.dispatch(
				postMyStore(this.props.postStoreUrl, this.props.myStore, true)
			);
		}
	}
	render() {
		return null;
	}
}
UpdateSession.propTypes = {
	componentName: PropTypes.string,
	postStoreUrl: PropTypes.string,
	postVehicleDetailsUrl: PropTypes.string,
	postUserLocationUrl: PropTypes.string,
	myStore: PropTypes.string,
	vehicles: PropTypes.array,
	tires: PropTypes.array,
	wheels: PropTypes.array,
	dispatch: PropTypes.func
};
const mapStateToProps = state => ({
	myStore: state.globalState.myStore,
	vehicles: state.globalState.vehicles,
	tires: state.globalState.tires,
	wheels: state.globalState.wheels
});
export default connect(mapStateToProps)(UpdateSession);
