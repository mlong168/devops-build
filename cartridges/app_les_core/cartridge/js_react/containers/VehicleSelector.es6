import React from "react";
import { connect } from "react-redux";
import { VehicleSelectorProps } from "util/Props";
import VehicleSelectorView from "components/VehicleSelector";

import {
	addVehicle,
	addTires,
	addWheels,
	postVehicleDetails,
	getBoltpatterns
} from "../actions/Global";
import { logSelectorEvent } from "../actions/Analytics";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class VehicleSelector extends React.Component {
	constructor(props) {
		super(props);
		this.addVehicle = this.addVehicle.bind(this);
		this.addTires = this.addTires.bind(this);
		this.addWheels = this.addWheels.bind(this);
		this.logEvent = this.logEvent.bind(this);
		//Need to fetch boltpatterns from service - component does not need to wait for them as they will be populated in redux
		if (!props.boltpatterns) {
			props.dispatch(getBoltpatterns(props.apiUrls.GetBoltpatterns));
		}
	}
	logEvent(category, action, label) {
		this.props.dispatch(
			logSelectorEvent({
				category: category,
				action: action,
				label: label
			})
		);
	}
	addVehicle(details) {
		//save vehicle details on clientside
		this.props.dispatch(addVehicle(details));
		//POST vehicle details to BE for session storage
		return this.props.dispatch(
			postVehicleDetails(this.props.apiUrls.PostVehicleData, {
				vehicle: details
			})
		);
	}
	addTires(details) {
		//save tires details on clientside
		this.props.dispatch(addTires(details));
		//POST tires details to BE for session storage
		return this.props.dispatch(
			postVehicleDetails(this.props.apiUrls.PostVehicleData, {
				tires: details
			})
		);
	}
	addWheels(details) {
		//save wheels details on clientside
		this.props.dispatch(addWheels(details));
		//POST wheels details to BE for session storage
		return this.props.dispatch(
			postVehicleDetails(this.props.apiUrls.PostVehicleData, {
				wheels: details
			})
		);
	}
	render() {
		return (
			<VehicleSelectorView
				{...this.props}
				addVehicle={this.addVehicle}
				addTires={this.addTires}
				addWheels={this.addWheels}
				logEvent={this.logEvent}
			/>
		);
	}
}

VehicleSelector.propTypes = VehicleSelectorProps;

const mapStateToProps = state => ({
	vehicles: state.globalState.vehicles,
	boltpatterns: state.globalState.boltpatterns
});
export default connect(mapStateToProps)(VehicleSelector);
