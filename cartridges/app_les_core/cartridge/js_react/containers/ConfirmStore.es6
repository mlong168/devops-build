import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { StoreDetailProps } from "util/Props";
import StoreDetails from "containers/StoreDetails";
import { postMyStore } from "../actions/Global";
import Measure from "react-measure";
/**
	Confirm Store - component wrapper requireing user to confirm store selection
*/

class ConfirmStore extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isConfirmed:
				props.myStore === props.storeData.id ||
				(!props.myStore && props.storeData.isMystore)
		};
		this.handleConfirmation = this.handleConfirmation.bind(this);
	}
	componentWillReceiveProps(nP) {
		this.setState({
			isConfirmed:
				nP.myStore === nP.storeData.id ||
				(!nP.myStore && nP.storeData.isMystore)
		});
	}
	handleConfirmation() {
		this.props.dispatch(
			postMyStore(this.props.makeMyStoreUrl, this.props.storeData.id)
		);
	}
	render() {
		if (this.state.isConfirmed) {
			return this.props.confirmOnly ? null : (
				<StoreDetails {...this.props.storeData} />
			);
		} else {
			let { storeData } = this.props;
			return (
				<Measure
					bounds
					onResize={contentRect => {
						this.setState({ dimensions: contentRect.bounds });
					}}
				>
					{({ measureRef, contentRect }) => {
						let classes = "storeDetails storeDetails--confirmation";
						if (contentRect.bounds.width > 500) {
							classes += " storeDetails--confirmation--full";
						}
						return (
							<aside ref={measureRef} className={classes}>
								<header className="confirmation__child confirmationHeader">
									<h6 className="confirmationHeader__text">
										<strong>{this.props.confirmYourStoreLabel}</strong>
										<span>
											{storeData.address1}, {storeData.city},{" "}
											{storeData.stateCode}
										</span>
									</h6>
								</header>
								<div className="button-group confirmation__child">
									<button
										className="button button--tinyText"
										onClick={this.handleConfirmation}
									>
										{this.props.makeMyStoreLabel}
									</button>
									<a
										href={this.props.changeStoreUrl}
										className="button button--gray button--tinyText"
									>
										{this.props.changeStoreLabel}
									</a>
								</div>
								<p className="confirmation__child paragraph paragraph--small">
									{this.props.locationInfoLabel}
								</p>
							</aside>
						);
					}}
				</Measure>
			);
		}
	}
}
ConfirmStore.propTypes = {
	componentName: PropTypes.string,
	storeData: PropTypes.shape(StoreDetailProps),
	changeStoreUrl: PropTypes.string,
	changeStoreLabel: PropTypes.string,
	makeMyStoreLabel: PropTypes.string,
	makeMyStoreUrl: PropTypes.string,
	confirmYourStoreLabel: PropTypes.string,
	locationInfoLabel: PropTypes.string,
	myStore: PropTypes.string,
	dispatch: PropTypes.func,
	confirmOnly: PropTypes.bool
};
const mapStateToProps = state => ({
	myStore: state.globalState.myStore
});
export default connect(mapStateToProps)(ConfirmStore);
