import React from "react";
import { connect } from "react-redux";
import { StoreDetailProps } from "util/Props";
import StoreDetailsTiny from "components/StoreDetailsTiny";
import StoreDetailsBrief from "components/StoreDetailsBrief";
import StoreDetailsFull from "components/StoreDetailsFull";
import StoreDetailsExtended from "components/StoreDetailsExtended";
import StoreDetailsHeader from "components/StoreDetailsHeader";
import StoreDetailsPDP from "components/StoreDetailsPDP";
import {
	addStoreDetails,
	postMyStore,
	setSelectedStore
} from "../actions/Global";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class StoreDetails extends React.Component {
	constructor(props) {
		super(props);
		this.addStoreDetails = this.addStoreDetails.bind(this);
		this.postMyStore = this.postMyStore.bind(this);
		this.setSelectedStore = this.setSelectedStore.bind(this);
	}
	addStoreDetails(details) {
		this.props.dispatch(addStoreDetails(details));
	}
	postMyStore(url, id) {
		this.props.dispatch(postMyStore(url, id));
	}
	setSelectedStore(id) {
		this.props.dispatch(setSelectedStore(id));
	}
	render() {
		let DetailsView;
		switch (this.props.type) {
			case "header":
				//DetailsView = StoreDetailsFull;
				DetailsView = StoreDetailsHeader;
				break;
			case "full":
				DetailsView = StoreDetailsFull;
				break;
			case "extended":
				DetailsView = StoreDetailsExtended;
				break;
			case "tiny":
				DetailsView = StoreDetailsTiny;
				break;
			case "PDP":
				DetailsView = StoreDetailsPDP;
				break;
			case "brief":
			default:
				DetailsView = StoreDetailsBrief;
		}
		return (
			<DetailsView
				{...this.props}
				isSelectedStore={this.props.selectedStore === this.props.id}
				setSelectedStore={this.setSelectedStore}
				handleMount={this.addStoreDetails}
				postMyStore={this.postMyStore}
				noMap={this.props.noMap}
			/>
		);
	}
}

StoreDetails.propTypes = StoreDetailProps;
StoreDetails.defaultProps = {
	noMap: false
};

const mapStateToProps = state => ({
	myStore: state.globalState.myStore,
	selectedStore: state.globalState.selectedStore
});
export default connect(mapStateToProps)(StoreDetails);
