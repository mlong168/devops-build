import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
	// fetchStoreInfo,
	userSelectedServices,
	loadURLParams
	// virtualPageView
} from "actions/AppointmentActions";
import ServicesView from "components/appointment/ServicesView";
// import "isomorphic-fetch";

class Services extends React.Component {
	constructor(props) {
		super(props);
		this.submitServices = this.submitServices.bind(this);
		this.handleLoadURLParams = this.handleLoadURLParams.bind(this);
	}
	submitServices(selectedServices, additionalInfo) {
		this.props.dispatch(
			userSelectedServices(selectedServices, additionalInfo || null)
		);
	}
	handleLoadURLParams(vehicleDescription, tireSize) {
		this.props.dispatch(loadURLParams(vehicleDescription, tireSize));
	}

	componentDidMount() {
		//virtual page view trigger
		// this.props.dispatch(
		// 	virtualPageView({
		// 		event: "VirtualPageview",
		// 		virtualPageURL: "/Step1/Services",
		// 		virtualPageTitle: "MakeAppt1-Services"
		// 	})
		// );
	}

	render() {
		return (
			<ServicesView
				servicesState={this.props.servicesState}
				submitServices={this.submitServices}
				userStore={this.props.userStore}
				handleNext={this.props.handleNext}
				handleAddressClick={this.props.handleAddressClick}
				handleValidationError={this.props.handleValidationError}
				handleLoadURLParams={this.handleLoadURLParams}
			/>
		);
	}
}
Services.propTypes = {
	dispatch: PropTypes.func,
	servicesState: PropTypes.object,
	userStore: PropTypes.object,
	handleNext: PropTypes.func,
	handlePrev: PropTypes.func,
	handleSelection: PropTypes.func,
	handleAddressClick: PropTypes.func,
	handleValidationError: PropTypes.func
};

const mapStateToProps = state => ({
	servicesState: state.servicesState,
	userStore: state.globalState.userStore
});

export default connect(mapStateToProps)(Services);
