// import React from "react";
// import PropTypes from "prop-types";
// import {connect} from "react-redux";
// import {postAppointmentRequest} from "../actions/Contact";
// import {virtualPageView} from "../actions/AppointmentActions";
// import ContactView from "components/appScheduler/ContactView";
//
// class Contact extends React.Component {
// 	constructor(props) {
// 		super(props);
// 		this.submitServices = this.submitServices.bind(this);
// 	}
//
// 	submitServices(userInfo) {
// 		this.props.dispatch(postAppointmentRequest(userInfo));
// 	}
// 	componentDidMount() {
// 		//virtual page view trigger
// 		this.props.dispatch(
// 			virtualPageView({
// 				event: "VirtualPageview",
// 				virtualPageURL: "/Step3/Contact",
// 				virtualPageTitle: "Make Appt3–Contact"
// 			})
// 		);
// 	}
//
// 	render() {
// 		return (
// 			<ContactView
// 				handlePrev={this.props.handlePrev}
// 				handleSubmit={this.submitServices}
// 				handleValidationError={this.props.handleValidationError}
// 			/>
// 		);
// 	}
// }
// Contact.propTypes = {
// 	dispatch: PropTypes.func,
// 	handleNext: PropTypes.func,
// 	handlePrev: PropTypes.func,
// 	handleValidationError: PropTypes.func
// };
//
// const mapStateToProps = () => ({});
//
// export default connect(mapStateToProps)(Contact);
