import React from "react";
import WheelVisualizerView from "components/WheelVisualizer";
import { connect } from "react-redux";
import { selectColor } from "../actions/Global";
import { logVisualizerPreview } from "../actions/Analytics";
import { WheelVisualizerProps } from "util/Props";

class WheelVisualizer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedColor: props.selectedColor,
			isVisible: props.vehicle && props.vehicle.categoryId
		};
		this.selectColor = this.selectColor.bind(this);
		this.logVisualizerPreview = this.logVisualizerPreview.bind(this);
	}
	componentWillReceiveProps(nP) {
		//toggle component if received vehicle data
		this.setState({
			isVisible: nP.vehicle && nP.vehicle.categoryId
		});
	}
	selectColor(color) {
		//save color details on clientside
		this.setState({ selectedColor: color });
		this.props.dispatch(selectColor(color));
	}
	logVisualizerPreview(data) {
		//analytics
		this.props.dispatch(logVisualizerPreview(data));
	}
	render() {
		return this.state.isVisible ? (
			<WheelVisualizerView
				{...this.props}
				selectedColor={this.state.selectedColor}
				selectColor={this.selectColor}
				logVisualizerPreview={this.logVisualizerPreview}
			/>
		) : null;
	}
}
WheelVisualizer.propTypes = WheelVisualizerProps;

const mapStateToProps = state => ({
	vehicle:
		state.globalState.vehicles && state.globalState.vehicles.length > 0
			? state.globalState.vehicles[0]
			: null,
	selectedColor: state.globalState.color,
	myStore: state.globalState.myStore
});
export default connect(mapStateToProps)(WheelVisualizer);
