// import React from "react";
// import PropTypes from "prop-types";
// import {connect} from "react-redux";
// import {
// 	selectDate,
// 	selectTime,
// 	fetchTimesForADate,
// 	toggleHeader
// } from "../actions/DateTime";
// import {virtualPageView} from "../actions/AppointmentActions";
// import DateTimeView from "components/appScheduler/DateTimeView";
// import "isomorphic-fetch";
//
// class DateTimeContainer extends React.Component {
// 	constructor(props) {
// 		super(props);
//
// 		this.selectDate = this.selectDate.bind(this);
// 		this.selectTime = this.selectTime.bind(this);
// 		this.toggleHeader = this.toggleHeader.bind(this);
// 	}
//
// 	selectDate(date) {
// 		this.props.dispatch(selectDate(date));
// 		this.props.dispatch(fetchTimesForADate(date));
// 	}
// 	componentDidMount() {
// 		//virtual page view trigger
// 		this.props.dispatch(
// 			virtualPageView({
// 				event: "VirtualPageview",
// 				virtualPageURL: "/Step2/Date",
// 				virtualPageTitle: "Make Appt2–Date"
// 			})
// 		);
// 	}
//
// 	selectTime(time) {
// 		this.props.dispatch(selectTime(time));
// 	}
//
// 	toggleHeader() {
// 		this.props.dispatch(toggleHeader());
// 	}
// 	render() {
// 		return (
// 			<DateTimeView
// 				{...this.props}
// 				handleSelectDate={this.selectDate}
// 				handleSelectTime={this.selectTime}
// 				handleToggleHeader={this.toggleHeader}
// 			/>
// 		);
// 	}
// }
// DateTimeContainer.propTypes = {
// 	dispatch: PropTypes.func,
// 	date: PropTypes.string,
// 	time: PropTypes.string,
// 	handleNext: PropTypes.func,
// 	handlePrev: PropTypes.func,
// 	handleSelection: PropTypes.func,
// 	handleAddressClick: PropTypes.func,
// 	handleValidationError: PropTypes.func
// };
//
// const mapStateToProps = state => ({
// 	isLoading: state.dateState.isLoading, //used to show/hide loading of list of times
// 	date: state.dateState.date, //current date for appointment
// 	time: state.dateState.time, //current time for appointment
// 	phoneNumber: state.servicesState.storeInfo.phoneNumber, //phone number displayed in step
// 	timesOnDate: (() => {
// 		//This is populated by the api call: /api/schedule/602/2017-10-18
// 		//We only want this array to be populated if the hasAvailability is true
// 		//and availability array has data
// 		if (
// 			state.dateState.date &&
// 			state.dateState.timesMatrix[state.dateState.date] &&
// 			state.dateState.timesMatrix[state.dateState.date].availability &&
// 			state.dateState.timesMatrix[state.dateState.date].hasAvailability
// 		) {
// 			return state.dateState.timesMatrix[state.dateState.date].availability;
// 		}
// 		return [];
// 	})()
// });
//
// export default connect(mapStateToProps)(DateTimeContainer);
