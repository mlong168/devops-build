import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { StoreDetailProps, StoreSearchFormProps } from "util/Props";
import MainNav from "components/MainNav";
import TopBar from "components/TopBar";
import config from "config";
import Overlay from "../components/Overlay";
import { logSearchEvent } from "../actions/Analytics";

class Header extends React.Component {
	constructor(props) {
		super(props);

		this.toggleSearchOpen = this.toggleSearchOpen.bind(this);
		this.toggleDesktopNav = this.toggleDesktopNav.bind(this);
		this.toggleMobileNav = this.toggleMobileNav.bind(this);
		this.toggleStoreDrop = this.toggleStoreDrop.bind(this);
		this.toggleStoreDropOnly = this.toggleStoreDropOnly.bind(this);
		this.toggleTobBarVisibility = this.toggleTobBarVisibility.bind(this);
		this.logSearchEvent = this.logSearchEvent.bind(this);

		this.state = {
			isSearchOpen: false,
			isMobileNavOpen: false,
			isDesktopNavOpen: false,
			isStoreDropOpen: false,
			isTopBarVisible: true
		};
	}
	logSearchEvent(action, query) {
		this.props.dispatch(logSearchEvent(action, query));
	}
	//Search was clicked, lets close Nav + Store and open/close Search
	toggleSearchOpen() {
		var isAnotherPanelOpened =
			this.state.isMobileNavOpen || this.state.isStoreDropOpen;
		var isTopBarVisible =
			window.innerWidth <= config.breakpoints.maxMobile
				? !this.state.isTopBarVisible
				: isTopBarVisible;
		this.setState({
			isSearchOpen: !this.state.isSearchOpen,
			isMobileNavOpen: false,
			isStoreDropOpen: false,
			isDesktopNavOpen: false,
			isTopBarVisible: isTopBarVisible
		});
		this.toggleBodyOverflow(isAnotherPanelOpened);
		this.mainNav.clearActiveCategory();
	}
	//Hamburger was clicked, lets close Search + Store and open/close MobileNav
	toggleMobileNav() {
		var isAnotherPanelOpened =
			this.state.isSearchOpen || this.state.isStoreDropOpen;
		this.setState({
			isMobileNavOpen: !this.state.isMobileNavOpen,
			isSearchOpen: false,
			isStoreDropOpen: false,
			isDesktopNavOpen: false
		});
		this.toggleBodyOverflow(isAnotherPanelOpened);
		this.mainNav.clearActiveCategory();
	}
	//Store Icon was clicked, lets close Hamburger + Search and open/close Store
	toggleStoreDrop() {
		var isAnotherPanelOpened =
			this.state.isSearchOpen || this.state.isMobileNavOpen;
		this.setState({
			isStoreDropOpen: !this.state.isStoreDropOpen,
			isSearchOpen: false,
			isMobileNavOpen: false,
			isDesktopNavOpen: false
		});
		this.toggleBodyOverflow(isAnotherPanelOpened);
		this.mainNav.clearActiveCategory();
	}
	//Full nav was clicked, lets close
	toggleDesktopNav() {
		this.setState(
			{
				isDesktopNavOpen: !this.state.isDesktopNavOpen,
				isSearchOpen: false,
				isStoreDropOpen: false
			},
			function() {
				if (this.state.isDesktopNavOpen === false) {
					this.mainNav.clearActiveCategory();
				}
			}
		);
	}
	toggleTobBarVisibility() {
		this.setState({
			isTopBarVisible: !this.state.isTopBarVisible
		});
	}
	toggleStoreDropOnly() {
		this.setState({
			isStoreDropOpen: !this.state.isStoreDropOpen
		});
	}
	toggleBodyOverflow(isAnotherPanelOpened) {
		if (isAnotherPanelOpened != true) {
			if (window.innerWidth <= config.breakpoints.maxMobile) {
				document.body.classList.toggle("no-scroll"); //turn off/on scroll for body
				document.documentElement.classList.toggle("no-scroll"); //turn off/on scroll for html
			}
		}
	}
	render() {
		return (
			<div className="navigationWrapper hide-for-print">
				<TopBar
					{...this.props.customer}
					logo={this.props.logo}
					pinIconURL={this.props.pinIconURL}
					billPay={this.props.billPay}
					storeData={this.props.storeData}
					storeSearchForm={this.props.storeSearchForm}
					toggleMobileNav={this.toggleMobileNav}
					isStoreDropOpen={this.state.isStoreDropOpen}
					toggleStoreDrop={this.toggleStoreDrop}
					isTopBarVisible={this.state.isTopBarVisible}
					isMobileNavOpen={this.state.isMobileNavOpen}
					myStore={this.props.myStore}
					hamburgerIconURL={this.props.hamburgerIconURL}
					closeIconRedURL={this.props.closeIconRedURL}
				/>
				<MainNav
					{...this.props}
					toggleSearchOpen={this.toggleSearchOpen}
					isMobileNavOpen={this.state.isMobileNavOpen}
					isSearchOpen={this.state.isSearchOpen}
					toggleTobBarVisibility={this.toggleTobBarVisibility}
					toggleDesktopNav={this.toggleDesktopNav}
					isDesktopNavOpen={this.state.isDesktopNavOpen}
					ref={mainNav => (this.mainNav = mainNav)}
					logSearchEvent={this.logSearchEvent}
				/>
				{this.state.isStoreDropOpen && (
					<Overlay clickHandler={this.toggleStoreDropOnly} />
				)}
			</div>
		);
	}
}

Header.propTypes = {
	dispatch: PropTypes.func,
	componentName: PropTypes.string.isRequired,
	logo: PropTypes.shape({
		imageURL: PropTypes.string,
		url: PropTypes.string,
		title: PropTypes.string,
		alt: PropTypes.string
	}),
	hamburgerIconURL: PropTypes.string,
	closeIconRedURL: PropTypes.string,
	pinIconURL: PropTypes.string,
	billPay: PropTypes.object,
	categories: PropTypes.array,
	customer: PropTypes.object,
	storeData: PropTypes.shape(StoreDetailProps),
	storeSearchForm: PropTypes.shape(StoreSearchFormProps),
	search: PropTypes.object,
	myStore: PropTypes.string
};

const mapStateToProps = state => ({
	vehicles: state.globalState.vehicles,
	myStore: state.globalState.myStore
});
export default connect(mapStateToProps)(Header);
