import React from "react";
import VehicleRenderView from "components/VehicleRender";
import { connect } from "react-redux";

const VehicleRender = props => <VehicleRenderView {...props} />;
const mapStateToProps = state => ({
	vehicle:
		state.globalState.vehicles && state.globalState.vehicles.length > 0
			? state.globalState.vehicles[0]
			: null,
	//TODO: color should be part of vehicle object
	color: state.globalState.color
});
export default connect(mapStateToProps)(VehicleRender);
