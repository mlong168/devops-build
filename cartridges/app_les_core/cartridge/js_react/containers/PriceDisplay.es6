import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import PriceDisplayView from "components/PriceDisplay";
/**
	Price Display - component wrapper hiding prices until user confirms a store
*/

class PriceDisplay extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isConfirmed: props.myStore
		};
	}
	componentWillReceiveProps(nP) {
		this.setState({
			isConfirmed: nP.myStore || false
		});
	}
	componentDidUpdate() {
		//update installed price element on PDP
		if (this.state.isConfirmed) {
			let el = document.getElementById("installed-price");
			if (el) {
				el.classList.remove("hide");
			}
		}
	}
	render() {
		if (this.state.isConfirmed) {
			return <PriceDisplayView {...this.props} />;
		} else {
			return (
				<div>
					<p>{this.props.selectStoreLabel}</p>
				</div>
			);
		}
	}
}
PriceDisplay.propTypes = {
	componentName: PropTypes.string,
	selectStoreLabel: PropTypes.string,
	myStore: PropTypes.string
};
const mapStateToProps = state => ({
	myStore: state.globalState.myStore
});
export default connect(mapStateToProps)(PriceDisplay);
