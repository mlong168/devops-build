import React from "react";
import VehicleInfoView from "components/VehicleInfo";
import { connect } from "react-redux";

class VehicleInfo extends React.Component {
	render() {
		return <VehicleInfoView {...this.props} />;
	}
}
const mapStateToProps = state => ({
	vehicle:
		state.globalState.vehicles && state.globalState.vehicles.length > 0
			? state.globalState.vehicles[0]
			: null,
	tires:
		state.globalState.tires && state.globalState.tires.length > 0
			? state.globalState.tires[0]
			: null,
	wheels:
		state.globalState.wheels && state.globalState.wheels.length > 0
			? state.globalState.wheels[0]
			: null,
	//TODO: color should be part of vehicle object
	color: state.globalState.color
});
export default connect(mapStateToProps)(VehicleInfo);
