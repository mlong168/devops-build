import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
	nextStep,
	prevStep,
	phoneClicked,
	validationError
} from "../actions/AppointmentActions";
import Main from "../components/appointment/Main";
class AppointmentScheduler extends React.Component {
	constructor(props) {
		super(props);

		this.handleNext = this.handleNext.bind(this);
		this.handlePrev = this.handlePrev.bind(this);
		this.handleValidationError = this.handleValidationError.bind(this);
		this.handleAddressClick = this.handleAddressClick.bind(this);
	}
	handleNext() {
		this.props.dispatch(nextStep());
	}
	handlePrev() {
		this.props.dispatch(prevStep());
	}
	handleAddressClick(actionLocation) {
		this.props.dispatch(phoneClicked(actionLocation));
	}
	handleValidationError(error) {
		this.props.dispatch(validationError(error));
	}
	render() {
		return (
			<Main
				{...this.props}
				handleNext={this.handleNext}
				handlePrev={this.handlePrev}
				handleAddressClick={this.handleAddressClick}
				handleValidationError={this.handleValidationError}
			/>
		);
	}
}
AppointmentScheduler.propTypes = {
	page: PropTypes.number.isRequired,
	isLoading: PropTypes.bool,
	dispatch: PropTypes.func
};
AppointmentScheduler.defaultProps = {
	page: 1
};
const mapStateToProps = state => {
	const { apptState } = state;
	return {
		page: apptState.page,
		isLoading: apptState.isLoading,
		error: apptState.error,
		confirmationData: apptState.confirmationData,
		showHeader: apptState.showHeader
	};
};

export default connect(mapStateToProps)(AppointmentScheduler);
