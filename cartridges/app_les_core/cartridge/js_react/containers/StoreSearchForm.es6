import React from "react";
import { connect } from "react-redux";
import StoreSearchFormView from "components/StoreSearchForm";
import { StoreSearchFormProps } from "util/Props";
import { setSelectedStore } from "../actions/Global";
import { logStoreSearchEvent } from "../actions/Analytics";

/**
	Store search form with google maps integration
*/
class StoreSearchForm extends React.Component {
	constructor(props) {
		super(props);
		this.setSelectedStore = this.setSelectedStore.bind(this);
		this.logStoreSearchEvent = this.logStoreSearchEvent.bind(this);
	}
	logStoreSearchEvent(action, label) {
		this.props.dispatch(logStoreSearchEvent(action, label));
	}
	setSelectedStore(id) {
		this.props.dispatch(setSelectedStore(id));
	}
	render() {
		return (
			<StoreSearchFormView
				{...this.props}
				setSelectedStore={this.setSelectedStore}
				logStoreSearchEvent={this.logStoreSearchEvent}
			/>
		);
	}
}

StoreSearchForm.propTypes = StoreSearchFormProps;

const mapStateToProps = state => ({
	stores: state.globalState.stores,
	selectedStore: state.globalState.selectedStore,
	uLat: state.globalState.uLat,
	uLng: state.globalState.uLng
});
export default connect(mapStateToProps)(StoreSearchForm);
