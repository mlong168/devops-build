import React from "react";
import { AppointmentCTAProps } from "util/Props";
import AppointmentCTAView from "components/AppointmentCTA";

class AppointmentCTA extends React.Component {
	render() {
		return <AppointmentCTAView {...this.props} />;
	}
}

AppointmentCTA.propTypes = AppointmentCTAProps;

export default AppointmentCTA;
