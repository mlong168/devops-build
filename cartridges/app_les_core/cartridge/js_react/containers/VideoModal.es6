import React from "react";
import PropTypes from "prop-types";
import Overlay from "components/Overlay";
import classNames from "classnames";

class VideoModal extends React.Component {
	constructor(props) {
		super(props);

		this.handleClose = this.handleClose.bind(this);
		this.handleOpen = this.handleOpen.bind(this);

		this.state = {
			isActive: false
		};
	}
	handleClose() {
		this.setState({ isActive: false });
	}
	handleOpen() {
		this.setState({ isActive: true });
	}
	componentDidMount() {
		if (this.props.trigger) {
			this.externalTrigger = document.querySelector(this.props.trigger);
			if (this.externalTrigger) {
				this.externalTrigger.addEventListener("click", this.handleOpen);
			}
		}
	}
	componentWillUnmount() {
		if (this.externalTrigger) {
			this.externalTrigger.removeEventListener("click", this.handleOpen);
		}
	}
	render() {
		let thumbnail =
				this.props.thumbnail ||
				`http://img.youtube.com/vi/${this.props.videoId}/0.jpg`,
			altText = this.props.videoImgAlt || `video thumbnail`;
		return (
			<article
				className={classNames("videoModal", {
					"videoModal--noThumbnail": this.props.noThumbnail
				})}
			>
				{this.props.videoTitle && (
					<h6 className="videoModal__title" onClick={this.handleOpen}>
						{this.props.videoTitle}
					</h6>
				)}
				{!this.props.noThumbnail && (
					<img
						src={thumbnail}
						alt={altText}
						onClick={this.handleOpen}
						aria-label="Click here to open the video"
					/>
				)}
				{this.state.isActive && (
					<div className="videoModal__wrapper">
						<Overlay clickHandler={this.handleClose} zindex="20" />
						<div className="videoModal__popup">
							<a onClick={this.handleClose} className="videoModal__close" />
							<div className="videoModal__popup__wrapper">
								<iframe
									width="640"
									height="360"
									src={`https://www.youtube.com/embed/${
										this.props.videoId
									}?rel=0&autoplay=1`}
									frameBorder="0"
									gesture="media"
									allow="encrypted-media"
									allowFullScreen
								/>
							</div>
						</div>
					</div>
				)}
			</article>
		);
	}
}

VideoModal.propTypes = {
	componentName: PropTypes.string,
	thumbnail: PropTypes.string,
	noThumbnail: PropTypes.bool,
	trigger: PropTypes.string,
	videoId: PropTypes.string.required,
	videoTitle: PropTypes.string,
	videoImgAlt: PropTypes.string
};

export default VideoModal;
