import React from "react";
import Overlay from "components/Overlay";
import VehicleSelector from "containers/VehicleSelector";
import { VehicleEditDialogProps } from "util/Props";
import config from "config";

class VehicleEditDialog extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			selectorData: null,
			error: null
		};
		this.getSelectorData = this.getSelectorData.bind(this);
	}
	componentDidMount() {
		this.getSelectorData();
	}
	getSelectorData() {
		fetch(
			this.props.getVehicleSelectorDataUrl +
				(this.props.category !== null
					? "?category=" + this.props.category
					: ""),
			{
				method: "GET",
				credentials: "same-origin" //must have to send cookies
			}
		)
			.then(response => {
				if (!response.ok) {
					throw Error(response.statusText);
				}
				return response.json();
			})
			.then(json => this.setState({ selectorData: json, isLoading: false }))
			.catch(error => this.setState({ isLoading: false, error: error }));
	}
	render() {
		let hgroup;
		switch (this.props.activeFlow) {
			case "byVehicle":
				hgroup = (
					<hgroup>
						<h3>Set Vehicle</h3>
						<h6>Set Vehicle Type</h6>
					</hgroup>
				);
				break;
			case "byTire":
				hgroup = null;
				break;
			case "byWheel":
				hgroup = null;
				break;
			default:
				hgroup = (
					<hgroup>
						<h3>Set Vehicle</h3>
					</hgroup>
				);
		}
		return (
			<Overlay zindex={30}>
				<article
					className={
						"vehicleEditDialog" +
						(this.state.isLoading ? " vehicleEditDialog--loading" : "")
					}
				>
					<button
						className="vehicleEditDialog__closeButton"
						onClick={this.props.toggleDialog}
						aria-label="Close Dialog"
					/>
					{this.state.error && (
						<div className="error">
							<h6>{config.fetchErrorHeading}</h6>
							<p data-error={this.state.error}>{config.fetchErrorDetail}</p>
						</div>
					)}
					{hgroup}
					{this.state.selectorData && (
						<VehicleSelector
							{...this.state.selectorData}
							storeData={this.props.storeData}
							isCompact={true}
							activeFlow={this.props.activeFlow}
						/>
					)}
				</article>
			</Overlay>
		);
	}
}
VehicleEditDialog.defaultProps = {
	category: null
};
VehicleEditDialog.propTypes = VehicleEditDialogProps;

export default VehicleEditDialog;
