/**
	Component for vehicle selection by license plate flow.
*/
import React from "react";
import PropTypes from "prop-types";

class LicenseFlow extends React.Component {
	constructor(props) {
		super(props);

		this.handleFind = this.handleFind.bind(this);
		this.handleChange = this.handleChange.bind(this);

		this.state = {
			isValid: false,
			isFetching: false,
			vehicle: null,
			licenseNumber: "",
			state: ""
		};
	}
	handleChange(field, event) {
		let val = event.target.value;
		this.setState(prevState => {
			let newState = {
				...prevState,
				[field]: val
			};
			return {
				...newState,
				isValid: newState.state !== "" && newState.licenseNumber !== ""
			};
		});
	}
	handleFind() {
		if (
			this.numberField.value === "" &&
			this.stateSelect.selectedOptions &&
			this.stateSelect.selectedOptions[0].value === ""
		) {
			this.setState({ isValid: false });
			return;
		}
		this.setState({ isFetching: true });
		//TODO: integrate once endpoint/contract is available
		/*fetch(`someurl/${this.numberField.value}`, {
			method: "GET"
			//mode: "cors",
			//headers: new Headers(config.api_headers)
		})
			.then(response => {
				if (!response.ok) {
					throw Error(response.statusText);
				}
				return response.json();
			})
			.then(json => {
				this.setState({ isFetching: false, vehicle: json.vehicle });
				//handle empty response
			});*/
		//.catch(error => dispatch(errorEncountered(error.message)));
	}
	render() {
		return (
			<section>
				<h4>{this.props.title}</h4>
				<p>{this.props.description}</p>
				<form action={this.props.findVehicleUrl}>
					<input
						type="text"
						required
						placeholder={this.props.licensePlateNumberPlaceholder}
						value={this.state.licenseNumber}
						onChange={event => this.handleChange("licenseNumber", event)}
					/>
					<select
						required
						value={this.state.state}
						onChange={event => this.handleChange("state", event)}
					>
						<option value="" disabled>
							{this.props.statesPlaceholder}
						</option>
						{this.props.states.map((state, i) => (
							<option value={state} key={i}>
								{state}
							</option>
						))}
					</select>
					<button
						className="button"
						onClick={this.handleFind}
						disabled={this.state.isValid}
					>
						{this.props.findVehicleLabel}
					</button>
				</form>
			</section>
		);
	}
}
LicenseFlow.propTypes = {
	id: PropTypes.string,
	title: PropTypes.string,
	description: PropTypes.string,
	licensePlateNumberPlaceholder: PropTypes.string,
	states: PropTypes.array,
	statesPlaceholder: PropTypes.string,
	findVehicleLabel: PropTypes.string,
	findVehicleUrl: PropTypes.string,
	vehicleFoundTitle: PropTypes.string,
	vehicleNotFoundTitle: PropTypes.string,
	vehicleNotFoundHint: PropTypes.string,
	shopLabel: PropTypes.string,
	shopUrl: PropTypes.string,
	viewAllLabel: PropTypes.string,
	viewAllUrl: PropTypes.string
};

export default LicenseFlow;
