import React from "react";
import PropTypes from "prop-types";
import config from "config";
import GoogleComponentHOC from "components/GoogleComponentHOC";
import LocationSearchBar from "./LocationSearchBar";

/**
	Store search form with google maps integration
*/
class StoreSearchForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			searchValue: "",
			doLocationSearch: false,
			longitude: this.props.longitude,
			latitude: this.props.latitude,
			uLng: this.props.uLng,
			uLat: this.props.uLat,
			geocodeComplete: false
		};

		this.form = null;

		this.geocodeAddress = this.geocodeAddress.bind(this);
		this.findByLocation = this.findByLocation.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.inputChange = this.inputChange.bind(this);
	}
	componentDidUpdate() {
		//when geocoding is complete and lat/long were rendered into form - submit.
		if (this.state.geocodeComplete && this.state.searchValue) {
			this.submitForm();
		}
		if (!this.props.noMap) {
			this.props.populateMarkers(
				this.props.stores.map(store => ({
					latitude: store.latitude,
					longitude: store.longitude,
					label: !isNaN(store.index) && `${store.index + 1}`,
					title: store.title,
					id: store.id,
					distance: store.distance
				}))
			);
		}
	}
	inputChange(event) {
		this.setState({ searchValue: event.target.value });
	}
	geocodeAddress() {
		if (!this.state.searchValue || this.state.searchValue === "") {
			return;
		}
		this.props.geocoder.geocode(
			{
				address: `${this.state.searchValue} ${this.props.countryCode}`,
				bounds: config.googleMaps.geocodeBounds,
				componentRestrictions: config.googleMaps.componentRestrictions
			},
			(results, status) => {
				if (status == this.props.google.maps.GeocoderStatus.OK) {
					if (status != this.props.google.maps.GeocoderStatus.ZERO_RESULTS) {
						//filtering out "invalid" matches, that belong to a list of forbidden types:
						let resultLocations = results.filter(res =>
							res.types.every(
								type => config.disallowedLocationTypes.indexOf(type) < 0
							)
						);
						if (resultLocations != null && resultLocations.length > 0) {
							this.setState({
								latitude: resultLocations[0].geometry.location.lat(),
								longitude: resultLocations[0].geometry.location.lng(),
								geocodeComplete: true
							});
						}
					} else {
						this.setState({
							latitude: null,
							longitude: null,
							geocodeComplete: true
						});
					}
				} else {
					this.setState({
						latitude: null,
						longitude: null,
						geocodeComplete: true
					});
				}
			}
		);
	}
	findByLocation() {
		this.props.logStoreSearchEvent("Find Stores Nearby", "");
		this.setState({ doLocationSearch: true }, () => this.form.submit());
	}
	handleSubmit(e) {
		if (!this.state.geocodeComplete) {
			if (e) {
				e.preventDefault();
			}
			this.geocodeAddress();
			return false;
		}
		this.submitForm();
	}
	submitForm() {
		this.props.logStoreSearchEvent("Search by entry", this.state.searchValue);
		this.form.submit();
	}
	render() {
		return (
			<div className="storeSearch">
				<form
					className="storeSearch__form"
					action={this.props.postUrl}
					method="GET"
					onSubmit={this.handleSubmit}
					ref={form => (this.form = form)}
				>
					{this.state.doLocationSearch && (
						// Sending user location obtained by clientside geolocation
						<fieldset>
							<input type="hidden" name="uLat" value={this.state.uLat} />
							<input type="hidden" name="uLng" value={this.state.uLng} />
						</fieldset>
					)}
					{this.state.latitude &&
						this.state.longitude && (
							<fieldset>
								<input
									type="hidden"
									name={this.props.latitudeField}
									value={this.state.latitude}
								/>
								<input
									type="hidden"
									name={this.props.longitudeField}
									value={this.state.longitude}
								/>
							</fieldset>
						)}
					{!this.props.noHeader && <h6>{this.props.formLabel}</h6>}
					<LocationSearchBar
						findByCurrentLocationTitle={this.props.findByCurrentLocationTitle}
						findByAddressTitle={this.props.findByAddressTitle}
						onClickCurrentHandler={this.findByLocation}
						inputFieldName={this.props.fieldName}
						inputFieldPlaceholder={this.props.fieldPlaceholder}
						inputLabelText={this.props.inputLabelText}
						inputFieldValue={this.state.searchValue}
						inputFieldChangeHandler={this.inputChange}
					/>
				</form>
				{!this.props.noMap && this.props.children}
			</div>
		);
	}
}

StoreSearchForm.defaultProps = {
	noHeader: false
};
StoreSearchForm.propTypes = {
	children: PropTypes.element,
	componentName: PropTypes.string,
	countryCode: PropTypes.string,
	dispatch: PropTypes.func,
	fieldName: PropTypes.string,
	fieldPlaceholder: PropTypes.string,
	findByAddressAction: PropTypes.string,
	findByAddressTitle: PropTypes.string,
	findByCurrentLocationAction: PropTypes.string,
	findByCurrentLocationTitle: PropTypes.string,
	formLabel: PropTypes.string,
	geocoder: PropTypes.object,
	google: PropTypes.object,
	inputLabelText: PropTypes.string,
	latitudeField: PropTypes.string,
	longitudeField: PropTypes.string,
	map: PropTypes.object,
	noMap: PropTypes.bool,
	noHeader: PropTypes.bool,
	populateMarkers: PropTypes.func,
	postUrl: PropTypes.string,
	stores: PropTypes.array,
	setSelectedStore: PropTypes.func,
	selectedStore: PropTypes.string,
	logStoreSearchEvent: PropTypes.func,
	latitude: PropTypes.number,
	longitude: PropTypes.number,
	uLat: PropTypes.number,
	uLng: PropTypes.number,
	isStateSearch: PropTypes.bool,
	isSearchByLocation: PropTypes.bool
};

export default GoogleComponentHOC(StoreSearchForm);
