import React from "react";
import PropTypes from "prop-types";

class VehicleSelectorBreadCrumbs extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		let breadcrumbsLength = this.props.breadcrumbs.length;
		return (
			<section>
				{this.props.breadcrumbs.map((breadcrumb, index) => {
					if (breadcrumbsLength === index + 1) {
						return <span key={index}>{breadcrumb}</span>;
					} else {
						return (
							<a
								key={index}
								onClick={this.props.handleBreadcrumbClick.bind(this, index)}
							>
								{breadcrumb}
							</a>
						);
					}
				})}
			</section>
		);
	}
}

VehicleSelectorBreadCrumbs.propTypes = {
	breadcrumbs: PropTypes.array,
	handleBreadcrumbClick: PropTypes.func
};

export default VehicleSelectorBreadCrumbs;
