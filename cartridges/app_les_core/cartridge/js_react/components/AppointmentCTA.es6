import React from "react";
import { AppointmentCTAProps } from "util/Props";

/*
	Display Schedule appointment CTA
	Destination url is constructed based on params
*/
class AppointmentCTA extends React.Component {
	constructor(props) {
		super(props);
		this.clickHandler = this.clickHandler.bind(this);
	}
	clickHandler() {
		//Building url query
		let params = "?";
		if (this.props.pid && this.props.pid !== "") {
			params += `pid=${this.props.pid}`;
		}
		if (this.props.qty) {
			params += `&qty=${this.props.qty}`;
		}
		if (this.props.sids && this.props.sids.length > 0) {
			this.props.sids.forEach((el, ind) => (params += `&sid${ind + 1}=${el}`));
		}
		window.location.href = this.props.ctaUrl + params;
	}
	render() {
		return (
			<div className="make-an-appointment-cta">
				<button className="button" onClick={this.clickHandler}>
					{this.props.ctaLabel}
				</button>
			</div>
		);
	}
}

AppointmentCTA.propTypes = AppointmentCTAProps;

export default AppointmentCTA;
