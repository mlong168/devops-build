import React from "react";
import { WheelVisualizerProps } from "util/Props";
import ColorSelector from "components/ColorSelector";
import config from "config";
import { sanitize } from "util/helpers";
import MediaQuery from "react-responsive";
import Slider from "react-slick";
import GTMUtils from "gtm-util";

class WheelVisualizer extends React.Component {
	constructor(props) {
		super(props);

		this.imgW = 870;
		this.imgH = 340;

		let vCategoryId =
			(props.vehicle && props.vehicle.categoryId) || props.vehicleCategoryId;

		this.state = {
			isLoading: false,
			renderingError: !vCategoryId ? true : false, //indicates broken image
			vehicleCategoryId: vCategoryId,
			color: props.selectedColor || (props.vehicle && props.vehicle.color),
			wheels: [],
			activeWheelIndex: 0,
			activeScrollIndex: 0,
			myStore: props.myStore || null
		};

		this.generateSrc = this.generateSrc.bind(this);
		this.findWheelData = this.findWheelData.bind(this);
		this.handleTileSelection = this.handleTileSelection.bind(this);
		this.seeOnVehicleHandler = this.seeOnVehicleHandler.bind(this);
	}
	componentDidMount() {
		this.observer = new MutationObserver(mutationsList => {
			for (var mutation of mutationsList) {
				if (mutation.type == "childList" && mutation.addedNodes.length > 0) {
					this.findWheelData();
				}
			}
		});
		let node = document.querySelector(".search__content");
		if (node) {
			this.observer.observe(node, {
				childList: true,
				subtree: true
			});
		}
	}
	componentWillReceiveProps(nP) {
		let vCategoryId =
				(nP.vehicle && nP.vehicle.categoryId) || nP.vehicleCategoryId,
			color = nP.selectedColor || (nP.vehicle && nP.vehicle.color),
			isLoading = false;

		if (
			(vCategoryId && vCategoryId !== this.state.vehicleCategoryId) ||
			color !== this.state.color
		) {
			isLoading = true;
		}
		this.setState({
			vehicleCategoryId: vCategoryId,
			renderingError: !vCategoryId ? true : false,
			color: color,
			isLoading: isLoading, //!vCategoryId? false : true,
			myStore: nP.myStore
		});
	}
	componentWillMount() {
		this.setState({
			isLoading: this.state.vehicleCategoryId ? true : false
		});
		this.findWheelData();
		//preview product based on the external product-tile trigger
		document
			.getElementsByClassName("search-result-content")
			.item(0)
			.addEventListener("click", this.seeOnVehicleHandler.bind(this));
	}
	componentWillUnmount() {
		document
			.getElementsByClassName("search-result-content")
			.item(0)
			.removeEventListener("click", this.seeOnVehicleHandler.bind(this));
	}
	/**Handles external link action that activates wheel preview based on the item id*/
	seeOnVehicleHandler(e) {
		let target = e.target;
		if (
			target.className.indexOf("seeOnVehicle") > -1 &&
			this.state.wheels.length
		) {
			let itemId = target.getAttribute("data-itemid"),
				wheelIndex = this.state.wheels.findIndex(
					el => el.wheelItemId && el.wheelItemId === itemId
				);
			if (itemId && wheelIndex > -1) {
				this.selfRef.scrollIntoView();
				this.handleTileSelection(wheelIndex);
			}
		}
	}
	generateSrc(mult) {
		mult = mult || 1;
		let wheelItemId = this.state.activeWheelIndex
			? `&WheelItemNumber=${
					this.state.wheels[this.state.activeWheelIndex].wheelItemId
			  }`
			: "";
		return `${this.props.vehicleRenderApiUrl}&VehicleConfiguration=${
			this.state.vehicleCategoryId
		}&Width=${this.imgW * mult}&Height=${this.imgH * mult}&PaintColor=%23${
			this.state.color
		}${wheelItemId}`;
	}
	/*Find and extract data from json attribute on product tiles*/
	findWheelData() {
		let matches = document.querySelectorAll(".product-tile"),
			wheels = [];
		//Iterating through NodeList
		for (let i = 0, len = matches.length; i < len; i++) {
			let data = matches[i].getAttribute("data-json");
			if (data && data !== null && data !== "null") {
				wheels.push(JSON.parse(data));
			}
		}

		this.setState({
			wheels: wheels,
			activeWheelIndex: 0
		});
	}
	handleTileSelection(index) {
		if (index === this.state.activeWheelIndex) {
			return;
		}
		this.setState({
			activeWheelIndex: index,
			isLoading: this.state.vehicleCategoryId ? true : false
		});
		//Analytics
		this.props.logVisualizerPreview(this.state.wheels[index].wheelItemId);
	}
	render() {
		let infoPanel = null,
			sliderSettings = {
				dots: false,
				infinite: this.state.wheels.length < 8 ? false : true,
				arrows: this.state.wheels.length < 8 ? false : true,
				speed: 500,
				slidesToShow:
					this.state.wheels.length < 8 ? this.state.wheels.length : 1,
				slidesToScroll: 1,
				variableWidth: this.state.wheels.length > 8 ? true : false,
				className: "visualizer__carousel",
				responsive: [
					{
						breakpoint: config.breakpoints.maxMobile,
						settings: {
							arrows: false
						}
					}
				]
			};
		if (
			this.state.wheels.length &&
			this.state.wheels[this.state.activeWheelIndex]
		) {
			let wheel = this.state.wheels[this.state.activeWheelIndex];
			infoPanel = (
				<ul className="visualizer__canvas__info">
					{wheel.brand && (
						<li className="tiny">
							<strong>{wheel.brand}</strong>
						</li>
					)}
					<li>
						<h5>{wheel.name}</h5>
					</li>
					{wheel.finish && <li>{wheel.finish}</li>}
					{wheel.price &&
						this.state.myStore && (
							<li>
								<span
									className="money"
									dangerouslySetInnerHTML={sanitize(wheel.price)}
								/>{" "}
								{wheel.priceMod}
							</li>
						)}
					<li className="visualizer__canvas__info__cta">
						<a
							href={wheel.detailsUrl}
							className="button button--tinyText"
							onClick={() =>
								GTMUtils.productImpression(wheel.wheelItemId, false)
							}
						>
							{wheel.detailsLabel}
						</a>
					</li>
				</ul>
			);
		}
		return (
			<section
				className="visualizer"
				ref={el => {
					this.selfRef = el;
				}}
			>
				<div
					className={
						"visualizer__canvas" +
						(this.state.isLoading ? " visualizer__canvas--loading" : "")
					}
				>
					<div
						className={
							"visualizer__canvas__image" +
							(this.state.renderingError
								? " visualizer__canvas__image--error"
								: "")
						}
					>
						{this.state.vehicleCategoryId && (
							<img
								src={this.generateSrc()}
								srcSet={
									this.generateSrc() +
									" " +
									this.imgW +
									"w, " +
									this.generateSrc(2) +
									" " +
									this.imgW * 2 +
									"w"
								}
								sizes={this.imgW + "px"}
								onLoad={() => this.setState({ isLoading: false })}
								onError={() =>
									this.setState({
										renderingError: true,
										isLoading: false
									})
								}
							/>
						)}
					</div>
					<div className="visualizer__canvas__aside">
						<ColorSelector {...this.props} />
						<MediaQuery minWidth={config.breakpoints.maxMobile + 1}>
							{infoPanel}
						</MediaQuery>
					</div>
				</div>
				<Slider {...sliderSettings}>
					{this.state.wheels.map((wheel, ind) => (
						<div
							key={"wheel_" + ind}
							onClick={() => this.handleTileSelection(ind)}
							className={this.state.activeWheelIndex === ind ? "active" : ""}
						>
							<img
								src={wheel.thumbnail.src}
								srcSet={wheel.thumbnail.srcset}
								alt={wheel.thumbnail.alt}
								sizes="(max-width: 768) 300px, 120px"
							/>
						</div>
					))}
				</Slider>
				<MediaQuery maxWidth={config.breakpoints.maxMobile}>
					{infoPanel}
				</MediaQuery>
			</section>
		);
	}
}

WheelVisualizer.propTypes = WheelVisualizerProps;

export default WheelVisualizer;
