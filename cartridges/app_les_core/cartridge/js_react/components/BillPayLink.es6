import React from "react";
import PropTypes from "prop-types";

/* Bill Pay link, used in TopBar.es6 and MainNav.es6 */
const billPay = props => {
	return (
		<a className="billPayLink" href={props.billPay.url}>
			{props.billPay.label}
		</a>
	);
};

billPay.propTypes = {
	billPay: PropTypes.object
};

export default billPay;
