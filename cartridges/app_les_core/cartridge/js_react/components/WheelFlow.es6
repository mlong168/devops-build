/**
	Component for vehicle selection by wheel size.
*/
import React from "react";
import PropTypes from "prop-types";
import SelectorFlow from "components/SelectorFlow";
import VehicleSelectorGrid from "components/VehicleSelectorGrid";
import VehicleSelectorBreadCrumbs from "components/VehicleSelectorBreadCrumbs";
import classNames from "classnames";
import MediaQuery from "react-responsive";
import config from "config";

class WheelFlow extends SelectorFlow {
	constructor(props) {
		super(props);

		this.type = "wheelSizeFlow";

		this.breadcrumbs = [];
		this.createBreadCrumbs = this.createBreadCrumbs.bind(this);
		this.lastBreadcrumbsFront = [];
		this.lastBreadcrumbsRear = [];
		this.toggleFrontButton = this.toggleFrontButton.bind(this);
		this.toggleRearButton = this.toggleRearButton.bind(this);
		this.selectPattern = this.selectPattern.bind(this);
		this.selectDiameter = this.selectDiameter.bind(this);
		this.toggleRearWheel = this.toggleRearWheel.bind(this);
		this.handleShopCTA = this.handleShopCTA.bind(this);
		this.renderFieldset = this.renderFieldset.bind(this);
		this.handleBreadcrumbClick = this.handleBreadcrumbClick.bind(this);

		this.state = {
			isRearDifferent: false, //used for mobile, and link button to show/hide rear fields
			isShowingRear: false, //used soley for desktop, showing/hiding rear fields
			selectedPattern: "",
			selectedDiameter: "",
			selectedRearPattern: "",
			selectedRearDiameter: "",
			patterns: this.props.patterns || [],
			diameters: this.props.diameters || [],
			rearDiameters: []
		};
	}
	createBreadCrumbs() {
		if (this.state.isRearDifferent && this.state.isShowingRear) {
			if (
				this.state.selectedRearPattern === "" ||
				this.state.rearDiameters === null
			) {
				this.breadcrumbs = [this.props.boltPatternPlaceholder];
			} else {
				this.breadcrumbs = [
					this.state.patterns[this.state.selectedRearPattern].replace(
						/["\\]|m+/g,
						""
					),
					this.state.selectedRearDiameter !== ""
						? this.state.rearDiameters[this.state.selectedRearDiameter]
						: this.props.diameterPlaceholder
				];
			}
		} else {
			if (this.state.selectedPattern === "" || this.state.diameters === null) {
				this.breadcrumbs = [this.props.boltPatternPlaceholder];
			} else {
				this.breadcrumbs = [
					this.state.patterns[this.state.selectedPattern].replace(
						/["\\]|m+/g,
						""
					),
					this.state.selectedDiameter !== ""
						? this.state.diameters[this.state.selectedDiameter]
						: this.props.diameterPlaceholder
				];
			}
		}
	}
	handleBreadcrumbClick(index) {
		let isShowingRear = this.state.isShowingRear;
		switch (index) {
			default:
				this.setState({
					diameters: [],
					selectedPattern: !isShowingRear ? "" : this.state.selectedPattern,
					selectedDiameter: !isShowingRear ? "" : this.state.selectedDiameter,
					selectedRearPattern: isShowingRear
						? ""
						: this.state.selectedRearPattern,
					selectedRearDiameter: isShowingRear
						? ""
						: this.state.selectedRearDiameter
				});
				this.breadcrumbs.length = 0;
				this.breadcrumbs.push(this.props.boltPatternPlaceholder);
				break;
		}
	}
	handleShopCTA(event) {
		event.preventDefault();
		let data = {
			boltPattern:
				this.state.patterns[this.state.selectedPattern].replace(
					/["\\]|m+/g,
					""
				) +
				(this.state.isRearDifferent
					? "|" +
					  this.state.patterns[this.state.selectedRearPattern].replace(
							/["\\]|m+/g,
							""
					  )
					: ""),
			diameter:
				this.state.diameters[this.state.selectedDiameter] +
				(this.state.isRearDifferent
					? "|" + this.state.rearDiameters[this.state.selectedRearDiameter]
					: "")
		};
		this.props
			.addWheels({
				front: {
					boltpattern: this.state.patterns[this.state.selectedPattern].replace(
						/["\\]|m+/g,
						""
					),
					diameter: this.state.diameters[this.state.selectedDiameter]
				},
				rear: this.state.isRearDifferent
					? {
							boltpattern: this.state.patterns[
								this.state.selectedRearPattern
							].replace(/["\\]|m+/g, ""),
							diameter: this.state.rearDiameters[
								this.state.selectedRearDiameter
							]
					  }
					: null
			})
			.then(() => {
				//redirect once posting is done
				window.location = this.populateShopUrl(data);
			});
		//analytics logging
		this.props.logEvent("Vehicle Entry By Wheels", "Shop CTA clicked", data);
	}
	selectPattern(event, isRear) {
		var selectedPatternNumber;
		if (isRear === undefined) {
			isRear = this.state.isShowingRear;
		}
		if (!isNaN(parseInt(event.target.getAttribute("data-index"), 10))) {
			selectedPatternNumber = parseInt(
				event.target.getAttribute("data-index"),
				10
			);
		} else {
			selectedPatternNumber = parseInt(event.target.value, 10);
		}

		this.setState(
			{
				[isRear ? "selectedRearPattern" : "selectedPattern"]: parseInt(
					selectedPatternNumber,
					10
				),
				[isRear ? "selectedRearDiameter" : "selectedDiameter"]: ""
			},
			function() {
				this.fetchFromApi(
					"GetWheelDiameter",
					json => {
						this.setState({
							isFetching: false,
							[isRear ? "rearDiameters" : "diameters"]: Object.is(
								json.diameters,
								undefined
							)
								? null
								: json.diameters.sort()
						});
					},
					{
						boltpattern: this.state.patterns[selectedPatternNumber].replace(
							/["\\]|m+/g,
							""
						)
					}
				);
			}
		);
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Wheels",
			"Pattern selected " + (isRear ? "(rear)" : "(front)"),
			this.state.patterns[selectedPatternNumber].replace(/["\\]|m+/g, "")
		);
	}
	selectDiameter(event, isRear) {
		var selectedDiameterNumber;
		if (isRear === undefined) {
			isRear = this.state.isShowingRear;
		}
		if (!isNaN(parseInt(event.target.getAttribute("data-index"), 10))) {
			selectedDiameterNumber = parseInt(
				event.target.getAttribute("data-index"),
				10
			);
		} else {
			selectedDiameterNumber = parseInt(event.target.value, 10);
		}
		this.setState({
			[isRear
				? "selectedRearDiameter"
				: "selectedDiameter"]: selectedDiameterNumber
		});
		//if rear is different and user has selected front diameter(so done with front), switch to rear
		if (this.state.isRearDifferent && !isRear) {
			this.toggleRearButton();
		}
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Wheels",
			"Diameter selected " + (isRear ? "(rear)" : "(front)"),
			{
				[isRear ? "selectedRearDiameter" : "selectedDiameter"]: this.state[
					isRear ? "rearDiameters" : "diameters"
				][selectedDiameterNumber]
			}
		);
	}
	toggleRearWheel() {
		//if front details have been populated, switch to rear entry
		if (!this.state.isRearDifferent && this.state.selectedDiameter !== "") {
			this.toggleRearButton();
		}
		if (this.state.isRearDifferent) {
			this.setState({ isShowingRear: false });
		}
		this.setState({ isRearDifferent: !this.state.isRearDifferent });
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Wheels",
			"Toggle Rear Wheel",
			!this.state.isRearDifferent
		);
	}
	toggleFrontButton() {
		this.lastBreadcrumbsRear = this.breadcrumbs;
		this.setState({ isShowingRear: false });
		this.breadcrumbs = this.lastBreadcrumbsFront;
	}
	toggleRearButton() {
		this.lastBreadcrumbsFront = this.breadcrumbs;
		this.setState({ isShowingRear: true });
		this.breadcrumbs = this.lastBreadcrumbsRear;
	}
	renderFieldset(isRear) {
		let patterns = this.state.patterns;
		let diameters = this.state[isRear ? "rearDiameters" : "diameters"];
		let labels;
		if (isRear) {
			labels = ["patternSelectRear", "diameterSelectRear"];
		} else {
			labels = ["patternSelect", "diameterSelect"];
		}
		return (
			<fieldset>
				<label htmlFor={labels[0]} className="show-for-sr">
					{this.props.boltPatternPlaceholder}
				</label>
				<select
					value={
						isRear ? this.state.selectedRearPattern : this.state.selectedPattern
					}
					onChange={e => this.selectPattern(e, isRear)}
					id={labels[0]}
				>
					<option disabled value="">
						{this.props.boltPatternPlaceholder}
					</option>
					{patterns.map((item, i) => (
						<option key={i} value={i}>
							{item}
						</option>
					))}
				</select>
				<label htmlFor={labels[1]} className="show-for-sr">
					{this.props.diameterPlaceholder}
				</label>
				<select
					disabled={
						this.state[isRear ? "selectedRearPattern" : "selectedPattern"] ===
						""
					}
					value={
						isRear
							? this.state.selectedRearDiameter
							: this.state.selectedDiameter
					}
					onChange={e => this.selectDiameter(e, isRear)}
					id={labels[1]}
				>
					<option disabled value="">
						{this.props.diameterPlaceholder}
					</option>
					{diameters.map((item, i) => (
						<option key={i} value={i}>
							{item}
						</option>
					))}
				</select>
			</fieldset>
		);
	}
	render() {
		let dataTitleToGrid,
			dataValuesToGrid,
			selectMethodToGrid,
			isLastStep,
			showError = false;
		let titles = [];
		let values = [];
		let disabledShop = true;
		if (this.state.isRearDifferent && this.state.isShowingRear) {
			if (
				this.state.selectedRearPattern === "" ||
				this.state.rearDiameters === null
			) {
				titles = this.state.patterns;
				selectMethodToGrid = this.selectPattern;
				isLastStep = false;
				this.breadcrumbs = [this.props.boltPatternPlaceholder];

				if (this.state.rearDiameters === null) {
					showError = true;
				}
			} else {
				titles = this.state.rearDiameters;
				selectMethodToGrid = this.selectDiameter;
				isLastStep = true;
			}
		} else {
			if (this.state.selectedPattern === "" || this.state.diameters === null) {
				titles = this.state.patterns;
				selectMethodToGrid = this.selectPattern;
				isLastStep = false;
				this.breadcrumbs = [this.props.boltPatternPlaceholder];

				if (this.state.diameters === null) {
					showError = true;
				}
			} else {
				titles = this.state.diameters;
				selectMethodToGrid = this.selectDiameter;
				isLastStep = true;
			}
		}
		for (let i = 0; i < titles.length; i++) {
			values.push(i);
		}
		this.createBreadCrumbs(); //form the breadcrumbs
		dataTitleToGrid = titles;
		dataValuesToGrid = values;

		if (!this.state.isRearDifferent) {
			disabledShop = this.state.selectedDiameter === "";
		} else {
			disabledShop =
				this.state.selectedDiameter === "" ||
				this.state.selectedRearDiameter === "";
		}

		return (
			<MediaQuery maxWidth={config.breakpoints.maxMobile}>
				{matches => {
					if (matches || this.props.isCompact) {
						return (
							<section>
								<h4>{this.props.title}</h4>
								{this.state.isRearDifferent && (
									<h5>{this.props.frontWheelSubtitle}</h5>
								)}
								{this.renderFieldset(false)}
								{this.state.isRearDifferent && (
									<div>
										<h5>{this.props.rearWheelSubtitle}</h5>
										{this.renderFieldset(true)}
									</div>
								)}
								<p>
									<a onClick={this.toggleRearWheel}>
										{this.state.isRearDifferent
											? this.props.removeDifferentRearWheelSizeLabel
											: this.props.addDifferentRearWheelSizeLabel}
									</a>
								</p>
								{showError && (
									<span className="VSErrorMessage">
										The selection you have made has returned no results. Please
										try a different option.
									</span>
								)}
								<div>
									<a
										className="button"
										onClick={this.handleShopCTA}
										disabled={disabledShop}
									>
										{this.props.isCompact
											? this.props.saveLabel
											: this.props.shopLabel}
									</a>
								</div>
							</section>
						);
					} else {
						return (
							<section>
								<section className="titleAndCrumbs">
									{this.state.isRearDifferent && (
										<div className="VSToggleButton__wrapper">
											<a
												className={classNames("VSToggleButton", {
													VSToggleButton__isActive: !this.state.isShowingRear
												})}
												onClick={this.toggleFrontButton}
											>
												Front
											</a>
											<a
												className={classNames("VSToggleButton", {
													VSToggleButton__isActive: this.state.isShowingRear
												})}
												onClick={this.toggleRearButton}
											>
												Rear
											</a>
										</div>
									)}
									<h6>{this.props.title}</h6>
									<VehicleSelectorBreadCrumbs
										breadcrumbs={this.breadcrumbs}
										handleBreadcrumbClick={this.handleBreadcrumbClick}
									/>
								</section>
								{this.state.isRearDifferent && (
									<p className="rearAxleHelpTip">
										{this.state.isShowingRear
											? this.props.rearEntryLabel
											: this.props.frontEntryLabel}
									</p>
								)}
								<VehicleSelectorGrid
									gridDataTitles={dataTitleToGrid}
									gridDataValues={dataValuesToGrid}
									selectDataMethod={selectMethodToGrid}
									isLastStep={isLastStep}
									handleShopCTA={this.handleShopCTA}
									shopLabel={this.props.shopLabel}
									isRearDifferent={this.state.isRearDifferent}
									toggleRear={this.toggleRearWheel}
									addRearLabel={this.props.addDifferentRearWheelSizeLabel}
									removeRearLabel={this.props.removeDifferentRearWheelSizeLabel}
									showError={showError}
									isSubmitDisabled={disabledShop}
								/>
							</section>
						);
					}
				}}
			</MediaQuery>
		);
	}
}
WheelFlow.propTypes = {
	addWheels: PropTypes.func,
	apiUrls: PropTypes.object,
	id: PropTypes.string,
	title: PropTypes.string,
	frontWheelSubtitle: PropTypes.string,
	rearWheelSubtitle: PropTypes.string,
	boltPatternPlaceholder: PropTypes.string,
	diameterPlaceholder: PropTypes.string,
	addDifferentRearWheelSizeLabel: PropTypes.string,
	removeDifferentRearWheelSizeLabel: PropTypes.string,
	shopLabel: PropTypes.string,
	shopUrl: PropTypes.string,
	patterns: PropTypes.array,
	diameters: PropTypes.array,
	isCompact: PropTypes.bool,
	frontEntryLabel: PropTypes.string,
	rearEntryLabel: PropTypes.string
};
export default WheelFlow;
