import React from "react";
import PropTypes from "prop-types";
import Overlay from "./Overlay";
import SearchBar from "./SearchBar";
import classNames from "classnames";
import BillPayLink from "components/BillPayLink";
import Category from "components/Category";
import config from "config";

/*
	Actual Navigation (the section that is under the TopBar)
*/
class MainNav extends React.Component {
	constructor(props) {
		super(props);
		this.handleMouseHover = this.handleMouseHover.bind(this);
		this.handleMouseLeave = this.handleMouseLeave.bind(this);
		this.handleMobileClick = this.handleMobileClick.bind(this);
		this.handleOverlayClick = this.handleOverlayClick.bind(this);
		this.clearActiveCategory = this.clearActiveCategory.bind(this);
		this.handleBackPress = this.handleBackPress.bind(this);

		this.hoverTimeout = null;
		this.state = {
			activeCategory: null
		};
	}
	//closes the Overlay, by clearing the category and letting the Header.es6 know it should be closed.
	handleOverlayClick() {
		this.setState({ activeCategory: null });
		this.props.toggleDesktopNav(); //used to let other components know the nav is closed
	}
	handleMouseHover(index) {
		//used only to open the nav, or open another category.
		//closing the nav will be done by Overlay click
		let hasSubNav =
			this.props.categories[index].panel.services.items.length !== 0;
		var thisComponent = this;
		this.hoverTimeout = setTimeout(function() {
			//only call this when not on mobile and there is a nav to show
			if (window.innerWidth > config.breakpoints.maxMobile && hasSubNav) {
				var originalIndexValue = thisComponent.state.activeCategory;

				if (index !== null) {
					thisComponent.setState(
						{
							activeCategory: index
						},
						function() {
							//if the value swapped from number to null or vice versa
							if (
								(originalIndexValue === null &&
									thisComponent.state.activeCategory !== null) ||
								(originalIndexValue !== null &&
									thisComponent.state.activeCategory === null)
							) {
								thisComponent.props.toggleDesktopNav(); //used to let other components know the desktop nav is open
							}
						}
					);
				}
			} else if (
				window.innerWidth > config.breakpoints.maxMobile &&
				!hasSubNav
			) {
				//no sub nav, just close the Desktop Nav
				if (thisComponent.state.activeCategory !== null) {
					thisComponent.props.toggleDesktopNav(); //used to let other components know the desktop nav is open
				}
			}
		}, config.navMouseHoverTimeout); //delay to trigger the navigation
	}
	handleMobileClick(index) {
		//open nav if we are mobile and not currently showing an active category
		let hasSubNav =
			this.props.categories[index].panel.services.items.length !== 0 ||
			index === null;
		if (hasSubNav && this.state.activeCategory === null) {
			this.setState({
				activeCategory: index
			});
			if (window.innerWidth > config.breakpoints.maxMobile) {
				this.props.toggleDesktopNav(); //used to let other components know the desktop nav is open
			}
		}
	}
	//Mobile Button Back Button
	handleBackPress() {
		this.setState({
			activeCategory: null
		});
	}
	clearActiveCategory() {
		//called by parent, Header.es6
		this.setState({
			activeCategory: null
		});
	}
	//handles non mobile mouse leave, if person is hovering a nav item but not long enough to open it.
	handleMouseLeave() {
		if (window.innerWidth > config.breakpoints.maxMobile) {
			clearTimeout(this.hoverTimeout);
		}
	}
	render() {
		let nav = this.props.categories.map((item, i) => (
			<Category
				{...item}
				storeData={this.props.storeData}
				key={"topLevel" + i}
				handleMouseHover={() => this.handleMouseHover(i)}
				handleMouseLeave={this.handleMouseLeave}
				handleMobileClick={() => this.handleMobileClick(i)}
				handleBackPress={this.handleBackPress}
				isActive={
					i === this.state.activeCategory &&
					(this.props.isDesktopNavOpen || this.props.isMobileNavOpen)
				}
				vehicles={this.props.vehicles}
				toggleDesktopNav={this.props.toggleDesktopNav}
			/>
		));
		return (
			<div>
				{this.state.activeCategory !== null &&
					this.props.isDesktopNavOpen && (
						<Overlay clickHandler={this.handleOverlayClick} />
					)}
				<nav
					className={`navigation grid-x`}
					key="main"
					ref={node => {
						this.node = node;
					}}
				>
					<section className="navigation__logo">
						<a href={this.props.logo.url} title={this.props.logo.title}>
							<img src={this.props.logo.imageURL} alt="Les Schwab Tires Logo" />
						</a>
					</section>
					<ul
						className={classNames("navigation__menu", {
							"navigation__search--open": this.props.isSearchOpen,
							navigation__menu__mobileNavOpen: this.props.isMobileNavOpen,
							navigation__menu__categoryIsOpen:
								this.state.activeCategory !== null
						})}
					>
						{nav}
						<section className="navigation__billPay">
							<BillPayLink billPay={this.props.billPay} />
						</section>
					</ul>
					<section
						className={classNames("navigation__search", {
							"navigation__search--open": this.props.isSearchOpen
						})}
					>
						<SearchBar
							{...this.props.search}
							toggleSearchOpen={this.props.toggleSearchOpen}
							isSearchOpen={this.props.isSearchOpen}
							toggleTobBarVisibility={this.props.toggleTobBarVisibility}
							logSearchEvent={this.props.logSearchEvent}
						/>
					</section>
				</nav>
			</div>
		);
	}
}

MainNav.propTypes = {
	componentName: PropTypes.string.isRequired,
	logo: PropTypes.shape({
		imageURL: PropTypes.string,
		url: PropTypes.string,
		title: PropTypes.string
	}),
	billPay: PropTypes.object,
	categories: PropTypes.array,
	search: PropTypes.object,
	vehicles: PropTypes.array,
	toggleSearchOpen: PropTypes.func,
	isMobileNavOpen: PropTypes.bool,
	isSearchOpen: PropTypes.bool,
	toggleTobBarVisibility: PropTypes.func,
	toggleDesktopNav: PropTypes.func,
	isDesktopNavOpen: PropTypes.bool,
	logSearchEvent: PropTypes.func,
	storeData: PropTypes.object
};

export default MainNav;
