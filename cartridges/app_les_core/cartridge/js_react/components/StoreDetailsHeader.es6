import React from "react";
import { StoreDetailProps } from "util/Props";
import AppointmentCTA from "components/AppointmentCTA";
import StoreDetailsBase from "components/StoreDetailsBase";
import MakeMyStore from "components/MakeMyStore";
import config from "config";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class StoreDetailsHeader extends StoreDetailsBase {
	render() {
		let address = [];
		let wrapperClassName = "storeDetails__wrapper cell small-12";

		address.push(this.props.city);
		address.push(this.props.stateCode);
		address.push(this.props.postalCode);

		return (
			<div className="storeDetails storeDetails--header grid-x">
				<div className={wrapperClassName}>
					<address>
						<span className="storeDetails__streetName" key={address}>
							{this.props.index != null && this.props.index + 1 + ". "}
							{this.props.address1}{" "}
						</span>
						<span>{address.join(", ")}</span>
						{this.props.distance && (
							<span className="storeDetails__distance">
								<strong>
									{`${this.roundDistance(this.props.distance)} ${
										config.distanceMeasurement
									}`}
								</strong>
							</span>
						)}
						<a
							className="storeDetails__mobileDetailsLink"
							href={this.props.storeDetailsUrl}
							title={this.props.storeDetailsLabel}
							aria-label={this.props.storeDetailsLabel}
						/>
					</address>
					<ul className="storeDetails__contact">
						<li>
							<span className="show-for-medium">
								{this.props.phone !== null && this.formatPhone()}
								{this.props.phone === null && this.props.noPhoneMessage}
							</span>
							<a
								className="show-for-small-only button button--gray"
								href={this.getDirections()}
								target="_blank"
							>
								{this.props.directionsLabel}
							</a>
						</li>
						<li className="show-for-small-only">
							{this.props.phone !== null && (
								<a
									className="show-for-small-only button button--gray"
									href={"tel:" + this.props.phone}
								>
									Call Us
								</a>
							)}
							{this.props.phone === null && (
								<span className="show-for-small-only">
									{this.props.noPhoneMessage}
								</span>
							)}
						</li>
						<li className="show-for-medium">
							<a href={this.props.storeDetailsUrl}>
								{this.props.storeDetailsLabel}
							</a>
						</li>
						<li className="show-for-medium">
							<a href={this.getDirections()} target="_blank">
								{this.props.directionsLabel}
							</a>
						</li>
					</ul>
					<AppointmentCTA
						ctaLabel={this.props.appointmentLabel}
						ctaUrl=""
						classes="storeDetails__button"
					/>
					<MakeMyStore
						id={this.props.id}
						isMystore={this.state.isMystore}
						handleMystoreAction={this.handleMystoreAction}
						myStoreLabel={this.props.myStoreLabel}
						makeMyStoreLabel={this.props.makeMyStoreLabel}
					/>
					<div className="storeDetails__information">
						{this.props.manager &&
							this.props.manager !== "" && (
								<section>
									<h6>{this.props.managerLabel}</h6>
									<p>{this.props.manager}</p>
								</section>
							)}
					</div>
					{this.props.managerImage &&
						this.props.managerImage !== "" && (
							<img src={this.props.managerImage} alt={this.props.manager} />
						)}
				</div>
			</div>
		);
	}
}

StoreDetailsHeader.propTypes = StoreDetailProps;

export default StoreDetailsHeader;
