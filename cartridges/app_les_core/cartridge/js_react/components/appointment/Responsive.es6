import React from "react";
import PropTypes from "prop-types";
import Responsive from "react-responsive";
import config from "config";

//Use NonMobile to wrap non mobile content, Mobile for mobile content
//<NonMobile>this will only show on desktop/tablet</NonMobile>
//For the breakpoints, doesn't work if the min/Max are the same (will cause both breakpoints to appear)

const NonDesktop = ({ children }) => (
	<Responsive maxWidth={config.breakpoints.maxTablet}>{children}</Responsive>
);
NonDesktop.propTypes = {
	children: PropTypes.element.isRequired
};
const NonMobile = ({ children }) => (
	<Responsive minWidth={config.breakpoints.maxMobile}>{children}</Responsive>
);
NonMobile.propTypes = {
	children: PropTypes.element.isRequired
};
const Mobile = ({ children }) => (
	<Responsive maxWidth={config.breakpoints.maxMobile - 1}>
		{children}
	</Responsive>
);
Mobile.propTypes = {
	children: PropTypes.element.isRequired
};

const ResponsiveExport = {
	NonMobile: NonMobile,
	Mobile: Mobile,
	NonDesktop: NonDesktop
};

export default ResponsiveExport;
