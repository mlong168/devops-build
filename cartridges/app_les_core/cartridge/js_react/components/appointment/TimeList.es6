import React from "react";
import PropTypes from "prop-types";
import css from "css/components/TimeList";
import greenCheckMark from "images/icons/greenCheckMark.svg";
import i18n from "i18n/en.json";
import moment from "moment";
import layout from "css/components/Layout";

export class TimeOption extends React.Component {
	constructor(props) {
		super(props);

		this.handleToggleKeyPress = this.handleToggleKeyPress.bind(this);

		this.handleSelection = this.handleSelection.bind(this);
		this.handleInfoKeyPress = this.handleInfoKeyPress.bind(this);

		this.state = {
			isSelected: this.props.selectedTime === this.props.time.time,
			isAvailable: this.props.time.available
		};
	}
	handleInfoKeyPress(event) {
		if (event.charCode === 13) {
			this.handleToggle();
		}
	}
	handleToggleKeyPress(e) {
		this.inp.checked = !this.inp.checked;
		if (e.charCode === 13) {
			this.handleSelection(e);
		}
	}
	handleSelection(e) {
		this.props.handleSelectTime(e.target.value);
	}
	componentWillReceiveProps(nP) {
		this.setState({
			isSelected: nP.selectedTime === nP.time.time,
			isAvailable: nP.time.available
		});
	}
	render() {
		let timeSlotClassName = this.state.isAvailable
			? css.timeListElement
			: css.timeListElement + " " + css.unavailable;
		return (
			<li className={timeSlotClassName}>
				{this.state.isAvailable && (
					<input
						type="radio"
						value={this.props.time.time}
						id={this.props.time.time}
						name="times"
						aria-label={i18n.dateTimeView.radioButtonAria}
						onClick={this.handleSelection}
						onKeyPress={this.handleToggleKeyPress}
						ref={input => (this.inp = input)}
						defaultChecked={this.state.isSelected}
					/>
				)}
				<span className={css.topLabel}>
					<label htmlFor={this.props.time.time}>
						{moment(this.props.time.time, "Hmm").format("h:mm a")}
					</label>
				</span>
				{this.state.isAvailable &&
					this.state.isSelected && (
						<img
							src={greenCheckMark}
							alt={i18n.greenCheckMark}
							className={css.checkMark}
						/>
					)}
			</li>
		);
	}
}

TimeOption.propTypes = {
	time: PropTypes.object.isRequired,
	selectedTime: PropTypes.string,
	phoneNumber: PropTypes.string,
	handleSelectTime: PropTypes.func,
	handleAddressClick: PropTypes.func
};

export default class TimeList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedTime: this.props.selectedTime
		};
		this.handleSelectTime = this.handleSelectTime.bind(this);
	}
	handleSelectTime(time) {
		this.setState({ selectedTime: time });
		this.props.handleSelectTime(time);
	}
	render() {
		return (
			<div className={this.props.isLoading && css.loading}>
				<h4 className={layout.sectionHeader}>{i18n.dateTimeView.timeTitle}</h4>
				{this.props.times.length < 1 ? (
					<div>
						<p>{i18n.dateTimeView.noTimeOptions}</p>
						<p>{i18n.dateTimeView.noTimeOptionsSub}</p>
					</div>
				) : (
					<div>
						<p>{i18n.dateTimeView.walkInText}</p>
						<ul className={css.ul}>
							{this.props.times &&
								this.props.times.length > 0 &&
								this.props.times.map((time, index) => (
									<TimeOption
										key={"time_" + index}
										time={time}
										selectedTime={this.state.selectedTime}
										phoneNumber={this.props.phoneNumber}
										handleSelectTime={this.handleSelectTime}
										handleAddressClick={this.props.handleAddressClick}
									>
										{time}
									</TimeOption>
								))}
						</ul>
					</div>
				)}
			</div>
		);
	}
}
TimeList.propTypes = {
	isLoading: PropTypes.bool,
	times: PropTypes.array,
	phoneNumber: PropTypes.string,
	hasAvailability: PropTypes.bool,
	selectedTime: PropTypes.string,
	handleSelectTime: PropTypes.func,
	handleAddressClick: PropTypes.func,
	onChange: PropTypes.func
};
