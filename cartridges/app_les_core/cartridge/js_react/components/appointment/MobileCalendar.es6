import React from "react";
import css from "css/components/MobileCalendar";
import i18n from "i18n/en.json";
import PropTypes from "prop-types";
import moment from "moment";
import config from "config";

class MobileCalendar extends React.Component {
	constructor(props) {
		super(props);
		let currentDaySelection;
		let showPreviousButton = false;

		if (this.props.selectedDay) {
			currentDaySelection = moment(this.props.selectedDay).startOf("day");
			if (currentDaySelection.isAfter(moment(), "day")) {
				//next day will not be today
				showPreviousButton = true;
			}
		} else {
			currentDaySelection = moment().startOf("day");
		}

		this.changeDay = this.changeDay.bind(this);

		this.state = {
			currentDaySelection: currentDaySelection,
			currentDayDisplay: currentDaySelection.format("MMMM Do YYYY"),
			showPreviousButton: showPreviousButton,
			showNextButton: true
		};
	}
	componentWillMount() {
		this.props.handleSelectDate(
			this.state.currentDaySelection.format(config.date_format)
		);
	}
	changeDay(buttonString) {
		let nextShownDay,
			showPreviousButton = this.state.showPreviousButton,
			showNextButton = this.state.showNextButton,
			lastPossibleDay = moment()
				.add(1, "month")
				.endOf("month");

		//determining which button was clicked and what the next day shown should be
		buttonString === "previous"
			? (nextShownDay = moment(this.state.currentDaySelection).add(-1, "days"))
			: (nextShownDay = moment(this.state.currentDaySelection).add(1, "days"));

		//logic to decide if we need to show previous button
		showPreviousButton = nextShownDay.isAfter(new Date(), "day");

		//logic to decide if we need to show the next button
		showNextButton = !lastPossibleDay.isSame(nextShownDay);

		this.setState({
			currentDaySelection: nextShownDay,
			currentDayDisplay: nextShownDay.format(config.mobile_calendar_format),
			showPreviousButton: showPreviousButton,
			showNextButton: showNextButton
		});

		//update Redux state
		this.props.handleSelectDate(nextShownDay.format(config.date_format));
	}
	render() {
		return (
			<nav className={css.nav}>
				{this.state.showPreviousButton && (
					<a
						className={css.previous}
						onClick={() => this.changeDay("previous")}
					>
						{i18n.dateTimeView.previousMonth}
					</a>
				)}
				{!this.state.showPreviousButton && (
					<a
						className={css.disabledMonth}
						aria-hidden="true"
						aria-label="previous"
					/>
				)}

				<span className={css.currentDate}>{this.state.currentDayDisplay}</span>

				{this.state.showNextButton && (
					<a className={css.next} onClick={() => this.changeDay("next")}>
						{i18n.dateTimeView.nextMonth}
					</a>
				)}
				{!this.state.showNextButton && (
					<a
						className={css.disabledMonth}
						aria-hidden="true"
						aria-label="next"
					/>
				)}
			</nav>
		);
	}
}
MobileCalendar.propTypes = {
	selectedDay: PropTypes.string,
	handleSelectDate: PropTypes.func,
	handleSelectTime: PropTypes.func,
	handleRequestTimes: PropTypes.func
};

export default MobileCalendar;
