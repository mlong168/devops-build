import React from "react";
import PropTypes from "prop-types";
import Header from "components/appointment/Header";
import Services from "containers/ASServices";
// import DateTime from "containers/ASDateTime";
// import Contact from "containers/ASContact";
// import ConfirmationView from "components/appointment/ConfirmationView";
import i18n from "i18n/en.json";

class Main extends React.Component {
	componentDidUpdate() {
		window.scrollTo({
			top: 0,
			left: 0,
			behavior: "smooth"
		});
	}
	render() {
		let view;
		switch (this.props.page) {
			// case -1:
			// 	view = (
			// 		<main
			// 			className={
			// 				css.container + " " + (this.props.isLoading ? css.loading : "")
			// 			}>
			// 			<Header
			// 				page={this.props.page}
			// 				showFull={false}
			// 				showHeader={this.props.showHeader}
			// 			/>
			// 			<ConfirmationView
			// 				confirmationData={this.props.confirmationData}
			// 				handleAddressClick={this.props.handleAddressClick}
			// 				dispatch={this.props.dispatch}
			// 			/>
			// 		</main>
			// 	);
			// 	break;
			case 0:
				view = (
					<main
						className={"container " + (this.props.isLoading ? "loading" : "")}
					>
						<Header
							page={this.props.page}
							showFull={!this.props.error}
							showHeader={this.props.showHeader}
						/>
						<Services {...this.props} />
					</main>
				);
				break;
			// case 1:
			// 	view = (
			// 		<main
			// 			className={
			// 				css.container + " " + (this.props.isLoading ? css.loading : "")
			// 			}>
			// 			<Header
			// 				page={this.props.page}
			// 				showFull={!this.props.error}
			// 				showHeader={this.props.showHeader}
			// 			/>
			// 			<DateTime {...this.props} />
			// 		</main>
			// 	);
			// 	break;
			// case 2:
			// 	view = (
			// 		<main
			// 			className={
			// 				css.container + " " + (this.props.isLoading ? css.loading : "")
			// 			}>
			// 			<Header
			// 				page={this.props.page}
			// 				showFull={!this.props.error}
			// 				showHeader={this.props.showHeader}
			// 			/>
			// 			<Contact {...this.props} />
			// 		</main>
			// 	);
			// 	break;
			// case 3:
			// 	view.push(<ConfirmationView {...this.props} />);
			// 	break;
		}

		if (this.props.error !== null) {
			view = (
				<main
					className={"container " + (this.props.isLoading ? "loading" : "")}
				>
					<Header showFull={false} showHeader={this.props.showHeader} />
					<section className="errorContainer">
						<h1>{i18n.globalError}</h1>
					</section>
				</main>
			);
		}
		return view;
	}
}

Main.propTypes = {
	page: PropTypes.number,
	dispatch: PropTypes.func,
	isLoading: PropTypes.bool,
	error: PropTypes.string,
	showHeader: PropTypes.bool,
	confirmationData: PropTypes.object,
	handlePrev: PropTypes.func,
	handleNext: PropTypes.func,
	handleAddressClick: PropTypes.func,
	handleValidationError: PropTypes.func
};

export default Main;
