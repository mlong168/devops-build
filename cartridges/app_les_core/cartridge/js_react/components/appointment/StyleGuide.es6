import React from "react";
import layout from "css/components/Layout";
import Button from "./Button";
import Accordion from "./Accordion";

export default class StyleGuide extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		return (
			<div className={layout.wrapper}>
				<h1>Welcome to the LS Style Guide</h1>

				<div className={layout.grid}>
					<div className={layout.cell}>
						<h1>H1 - Almost Black</h1>
						<h2>H2 - Almost Black</h2>
						<h3>H3 - Almost Black</h3>
						<h4>H4 - Almost Black</h4>
						<h5>H5 - Almost Black</h5>
					</div>

					<div className={layout.cell}>
						<h1 className={css.heroCopy}>H1 - White</h1>
						<h2 className={css.heroCopy}>H2 - White</h2>
						<h3 className={css.heroCopy}>H3 - White</h3>
						<h4 className={css.heroCopy}>H4 - White</h4>
						<h5 className={css.heroCopy}>H5 - White</h5>
					</div>

					<div className={layout.cell}>
						<p className={css.largeCopy}>
							This is large sized copy. Use this when mediumn sized copy feels
							too small.
						</p>
						<p>This is normal sized copy. This is normal sized copy.</p>
						<p className={layout.smallCopy}>
							This is small sized body copy. This is small sized body copy. This
							is small sized body copy.
						</p>
						<p className={layout.tinyCopy}>
							This is tiny sized copy. Use this when mediumn sized copy feels
							too big.
						</p>
					</div>

					<div>
						<p className={layout.centeredCopy}>
							This is medium sized body copy. This is medium sized body copy.
							This is medium sized body copy.
						</p>
					</div>

					<div className={layout.cell}>
						<a href="/" className={css.largeCopy}>
							This is a large text example link
						</a>
						<a href="/">This is a medium text example link</a>
						<a href="/" className={css.smallCopy}>
							This is a small text example link
						</a>
					</div>

					<div className={layout.cell}>
						<Button size="large">Find Store</Button>
						<Button>Find Store</Button>
						<Button size="small">Find Store</Button>
						<Button size="tiny">Find Store</Button>
					</div>

					<div className={layout.cell}>
						<Button size="large" isFlex isDisabled>
							disabled button example
						</Button>
						<Button isFlex>OK</Button>
						<Button size="small" isFlex>
							OK
						</Button>
					</div>

					<div className={layout.cell}>
						<Button isGray size="large">
							Find Store
						</Button>
						<Button isGray>Find Store</Button>
						<Button isGray size="small">
							Find Store
						</Button>
					</div>

					<div className={layout.cell}>
						<Button isGray size="large" isFlex>
							Cancel
						</Button>
						<Button isGray isFlex>
							Cancel
						</Button>
						<Button isGray size="small" isFlex>
							Cancel
						</Button>
					</div>
					<div className={layout.cell}>
						<textarea />
						<input type="text" />
						<input
							type="checkbox"
							name="example-checkbox"
							id="example-checkbox"
						/>
						<label htmlFor="example-checkbox">Example Checkbox</label>
						<input type="radio" id="genderChoice1" value="male" />
						<label htmlFor="genderChoice1">Male</label>
						<input type="radio" id="genderChoice2" value="female" />
						<label htmlFor="genderChoice2">Female</label>
						<Accordion
							services={[
								{
									title: "Accordion 1",
									serviceItems: [
										{
											serviceTitle: "Tires",
											description: "This is a test description",
											serviceID: 1
										},
										{
											serviceTitle: "Repair Tires",
											description: "This is a test description",
											serviceID: 2
										}
									]
								},
								{
									title: "Accordion 2",
									serviceItems: [
										{
											serviceTitle: "Tires",
											description: "This is a test description",
											serviceID: 3
										}
									]
								},
								{
									title: "Accordion 3",
									serviceItems: [
										{
											serviceTitle: "Tires",
											description: "This is a test description",
											serviceID: 4
										}
									]
								}
							]}
						/>
					</div>
				</div>
			</div>
		);
	}
}
