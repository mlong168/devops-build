import React from "react";
import PropTypes from "prop-types";
import { dateTimeView } from "i18n/en.json";
import Button from "components/Button";
import Calendar from "components/Calendar";
import MobileCalendar from "components/MobileCalendar";
import DialogCalendar from "components/DialogCalendar";
import TimeList from "components/TimeList";
import Responsive from "components/Responsive";
import css from "css/components/DateTime";
import layout from "css/components/Layout";
import i18n from "i18n/en.json";

class DateTimeView extends React.Component {
	constructor(props) {
		super(props);

		this.handleMobileIconClick = this.handleMobileIconClick.bind(this);
		this.handleHideDialog = this.handleHideDialog.bind(this);
		this.handleNextButton = this.handleNextButton.bind(this);

		this.state = {
			isValid: props.date !== null && props.time !== null,
			showFullCalendar: false,
			showError: false
		};
	}
	handleMobileIconClick() {
		this.setState({ showFullCalendar: !this.state.showFullCalendar });
		this.props.handleToggleHeader();
	}
	handleHideDialog() {
		this.setState({ showFullCalendar: false });
		this.props.handleToggleHeader();
	}
	handleNextButton() {
		if (this.state.isValid === false) {
			this.setState({ showError: true });
		} else {
			this.setState({ showError: false });
			this.props.handleNext();
		}
	}
	componentWillReceiveProps(nP) {
		this.setState({ isValid: nP.date !== null && nP.time !== null });
	}
	componentDidUpdate() {
		if (this.state.showError) {
			this.props.handleValidationError(i18n.dateTimeView.invalidSelection);
		}
	}
	render() {
		let showFullCalendar = this.state.showFullCalendar;
		return (
			<div>
				{showFullCalendar && (
					<DialogCalendar
						date={this.props.date}
						handleSelectDate={this.props.handleSelectDate}
						handleSelectTime={this.props.handleSelectTime}
						handleRequestTimes={this.props.handleRequestTimes}
						handleHideDialog={this.handleHideDialog}
					/>
				)}
				{!showFullCalendar && (
					<div className={css.wrapper}>
						<h2 className={css.dateViewHeader}>{dateTimeView.subTitle}</h2>
						<article className={css.grid}>
							<section className={css.leftCell}>
								<h4 className={layout.sectionHeader}>Date</h4>
								<Responsive.Mobile>
									<a
										className={css.calendarIcon}
										onClick={this.handleMobileIconClick}
									/>
								</Responsive.Mobile>
								<Responsive.NonMobile>
									<Calendar
										selectedDay={this.props.date}
										handleSelectDate={this.props.handleSelectDate}
										handleSelectTime={this.props.handleSelectTime}
										handleRequestTimes={this.props.handleRequestTimes}
									/>
								</Responsive.NonMobile>
								<Responsive.Mobile>
									<MobileCalendar
										selectedDay={this.props.date}
										handleSelectDate={this.props.handleSelectDate}
										handleSelectTime={this.props.handleSelectTime}
										handleRequestTimes={this.props.handleRequestTimes}
									/>
								</Responsive.Mobile>
							</section>
							<section className={css.rightCell}>
								<TimeList
									isLoading={this.props.isLoading}
									selectedTime={this.props.time}
									times={this.props.timesOnDate}
									handleSelectTime={this.props.handleSelectTime}
									phoneNumber={this.props.phoneNumber}
									handleAddressClick={this.props.handleAddressClick}
								/>
								{this.props.phoneNumber && (
									<p>
										<span>{i18n.dateTimeView.contactUs}</span>
										<a
											aria-label={i18n.phoneAria}
											onClick={() => this.props.handleAddressClick("Date")}
											href={
												"tel:" + this.props.phoneNumber.replace(/[\s()-]+/g, "")
											}
										>
											{this.props.phoneNumber}
										</a>
									</p>
								)}
								{this.state.showError && (
									<span className={css.errorSpan}>
										{i18n.dateTimeView.invalidSelection}
									</span>
								)}
							</section>
							<section className={css.buttonRow}>
								<Button
									onClick={this.props.handlePrev}
									ariaLabel={i18n.previousButtonAria}
									size="small"
									isGray
								>
									{i18n.backButton}
								</Button>
								<Button
									onClick={this.handleNextButton}
									ariaLabel={i18n.nextButtonAria}
									size="small"
								>
									{dateTimeView.nextButton}
								</Button>
							</section>
						</article>
					</div>
				)}
			</div>
		);
	}
}

DateTimeView.propTypes = {
	isLoading: PropTypes.bool,
	date: PropTypes.string,
	time: PropTypes.string,
	timesOnDate: PropTypes.array,
	phoneNumber: PropTypes.string,
	handleNext: PropTypes.func,
	handlePrev: PropTypes.func,
	handleSelectDate: PropTypes.func,
	handleSelectTime: PropTypes.func,
	handleRequestTimes: PropTypes.func,
	handleToggleHeader: PropTypes.func,
	handleAddressClick: PropTypes.func,
	handleValidationError: PropTypes.func
};

export default DateTimeView;
