import React from "react";
import PropTypes from "prop-types";
import { asYouType } from "libphonenumber-js";
import Responsive from "components/Responsive";
import Button from "components/Button";
import i18n from "i18n/en.json";
import css from "css/components/Contact";
import layout from "css/components/Layout";

export default class Contact extends React.Component {
	constructor(props) {
		super(props);

		this.checkIfValid = this.checkIfValid.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleOnBlur = this.handleOnBlur.bind(this);

		this.state = {
			isValid: false
		};
	}

	componentDidMount() {
		//Field's are created, so lets add them to the state of the Contact Form
		this.setState({
			fields: {
				firstName: this.firstName,
				lastName: this.lastName,
				email: this.email,
				phoneNumber: this.phoneNumber
			}
		});
	}

	handleSubmit() {
		if (this.state.isValid) {
			this.props.handleSubmit({ ...this.state.fields });
		} else {
			//These will check the validity of the inputs, without having to onBlur/onClick
			this.firstName.selfCheck();
			this.lastName.selfCheck();
			this.email.selfCheck();
			this.phoneNumber.selfCheck();
		}
	}

	handleOnBlur() {
		let isValid = this.checkIfValid();
		this.setState({ isValid: isValid });
	}

	//Check if whole form is valid based on state's values
	checkIfValid() {
		let fields = this.state.fields;

		for (let field in fields) {
			if (!fields[field].state.isValid) {
				return false;
			}
		}
		return true;
	}
	render() {
		return (
			<div className={layout.wrapper}>
				<Responsive.NonMobile>
					<h5 className={css.sectionHeader}>{i18n.contactForm.header}</h5>
				</Responsive.NonMobile>
				<Responsive.Mobile>
					<h6 className={css.mobileSectionHeader}>{i18n.contactForm.header}</h6>
				</Responsive.Mobile>
				<div className={css.contactSubHeader}>{i18n.contactForm.subHeader}</div>
				<form className={layout.grid}>
					<Field
						className={layout.halfCell}
						label={i18n.contactForm.firstName}
						errorMessage={i18n.contactForm.invalidFirstName}
						id="firstName"
						type="text"
						ref={field => (this.firstName = field)}
						validate={val => /^[a-zA-Z\s]{1,}$/.test(val)}
						handleOnBlur={this.handleOnBlur}
						handleValidationError={this.props.handleValidationError}
					/>
					<Field
						className={layout.halfCell}
						label={i18n.contactForm.lastName}
						errorMessage={i18n.contactForm.invalidLastName}
						id="lastName"
						type="text"
						ref={field => (this.lastName = field)}
						validate={val => /^[a-zA-Z\s]{1,}$/.test(val)}
						handleOnBlur={this.handleOnBlur}
						handleValidationError={this.props.handleValidationError}
					/>
					<Field
						className={layout.cell}
						label={i18n.contactForm.email}
						errorMessage={i18n.contactForm.invalidEmail}
						id="email"
						type="text"
						ref={field => (this.email = field)}
						validate={val =>
							/[a-z0-9A-Z!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9A-Z-]+\.+[a-z0-9A-Z-]+/.test(
								val
							)
						}
						handleOnBlur={this.handleOnBlur}
						handleValidationError={this.props.handleValidationError}
					/>
					<Field
						className={layout.cell}
						label={i18n.contactForm.phoneNumber}
						errorMessage={i18n.contactForm.invalidPhoneNumber}
						id="phoneNumber"
						type="text"
						ref={field => (this.phoneNumber = field)}
						validate={val => /^[0-9\s-())]{9,20}$/.test(val)}
						placeholder={i18n.contactForm.phoneNumberPlaceholder}
						handleOnBlur={this.handleOnBlur}
						handleValidationError={this.props.handleValidationError}
						maxlength="14"
					/>
				</form>
				<section className={layout.cell}>
					<div className={css.buttonRow}>
						<Button
							onClick={this.props.handlePrev}
							size="small"
							ariaLabel={i18n.previousButtonAria}
							isGray
						>
							{i18n.backButton}
						</Button>
						<Button
							onClick={this.handleSubmit}
							ariaLabel={i18n.finishButtonAria}
							size="small"
						>
							{i18n.finishButton}
						</Button>
					</div>
				</section>
			</div>
		);
	}
}

Contact.propTypes = {
	handlePrev: PropTypes.func,
	handleSubmit: PropTypes.func,
	handleValidationError: PropTypes.func
};

class Field extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isValid: false,
			showError: false,
			value: ""
		};
		this.handleInput = this.handleInput.bind(this);
		this.handleOnChange = this.handleOnChange.bind(this);
		this.checkIfValid = this.checkIfValid.bind(this);
	}
	checkIfValid(argValue) {
		const isValid = this.props.validate ? this.props.validate(argValue) : true;

		//let analytics know that the user inputted an invalid input.
		if (!isValid) {
			this.props.handleValidationError("Invalid contact information");
		}

		return isValid;
	}
	//called by onBlur, check if valid
	handleInput(event) {
		const isValid = this.checkIfValid(event.target.value);

		this.setState({
			value: event.target.value,
			isValid: isValid,
			showError: !isValid
		});

		//update form's valid/not valid if changed
		this.props.handleOnBlur();
	}
	selfCheck() {
		//this is called by the Contact component.
		//without user input, it is used to check and change state of input on submit press
		const isValid = this.checkIfValid(this.inp.value);

		this.setState({
			value: this.inp.value,
			isValid: isValid,
			showError: !isValid
		});
	}
	handleOnChange(inputData) {
		let previousValue = this.state.value;
		let inputValue = inputData.target.value;
		let isCharRemoved = previousValue.length > 0 && previousValue > inputValue;
		const isValid = this.props.validate
			? this.props.validate(inputData.target.value)
			: true;

		//since phonenumber is "formatted" by asYouType, there is some extra logic of its value and how its handled
		if (inputData.target.id == "phoneNumber") {
			//since the character was removed from number, just show the same formatted number -1 character
			if (isCharRemoved) {
				this.setState({ value: inputData.target.value });
			} else {
				//we added a character, so time to format the string!
				let formattedPhoneNumber = new asYouType("US").input(
					inputData.target.value
				);
				this.setState({ value: formattedPhoneNumber });
			}
		} else {
			//not a phone number, just write to the state
			this.setState({
				value: inputData.target.value
			});
		}

		//check if it is valid, simply to remove the error message if applicable.
		//If this input is not valid at the moment, it may be later, so only change to invalid only on onBlur.
		if (isValid) {
			this.setState({
				isValid: true,
				showError: !isValid
			});
		}
	}
	render() {
		return (
			<section className={this.props.className}>
				<label className={css.contactLabel} htmlFor={this.props.id}>
					{this.props.label}
				</label>
				<input
					label={this.props.label}
					id={this.props.id}
					name={this.props.id}
					type={this.props.type}
					onBlur={this.handleInput}
					onChange={this.handleOnChange}
					placeholder={this.props.placeholder}
					value={this.state.value}
					maxLength={this.props.maxlength}
					className={this.state.showError ? css.isInvalid : ""}
					ref={input => (this.inp = input)}
				/>
				{this.state.showError ? (
					<span className={css.errorSpan}>{this.props.errorMessage}</span>
				) : null}
			</section>
		);
	}
}

Field.propTypes = {
	label: PropTypes.string,
	errorMessage: PropTypes.string,
	id: PropTypes.string.isRequired,
	value: PropTypes.string,
	handleOnBlur: PropTypes.func,
	handleOnChange: PropTypes.func,
	handleValidationError: PropTypes.func,
	type: PropTypes.string,
	placeholder: PropTypes.string,
	validate: PropTypes.func,
	className: PropTypes.string,
	maxlength: PropTypes.string
};
