import React from "react";
import PropTypes from "prop-types";

const Button = props => {
	let {
		size,
		isGray,
		isFlex,
		disabled,
		className,
		ariaLabel,
		...passProps
	} = props;

	const createClassName = () => {
		let finalClassName = ["button", className];

		if (isGray) {
			finalClassName.push("grayButton");
		}
		if (isFlex) {
			finalClassName.push("flexButton");
		}
		if (disabled) {
			finalClassName.push("disabledButton");
		}

		if (size === "tiny") {
			finalClassName.push("tiny");
		} else if (size === "small") {
			finalClassName.push("small");
		} else if (size === "large") {
			finalClassName.push("large");
		}
		return finalClassName.join(" "); //need to also include the button class
	};

	return (
		<button
			{...passProps}
			aria-label={ariaLabel}
			className={createClassName()}
			disabled={disabled}
		>
			{props.children}
		</button>
	);
};

Button.defaultProps = {
	size: "default", //changes size of font (may change size of width in the future)
	isGray: false, //controls color
	isFlex: false, //either a fix width or width: %;
	disabled: false
};

Button.propTypes = {
	children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
	size: PropTypes.string,
	isGray: PropTypes.bool,
	isFlex: PropTypes.bool,
	disabled: PropTypes.bool,
	className: PropTypes.string,
	ariaLabel: PropTypes.string
};

export default Button;
