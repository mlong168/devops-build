import React from "react";
import PropTypes from "prop-types";
import css from "css/components/Dropdown";

export default class Dropdown extends React.Component {
	createClassName() {
		let finalClassName = "";

		if (this.props.size === "large") {
			finalClassName += css.largeDropdown + " ";
		} else if (this.props.size === "small") {
			finalClassName += css.smallDropdown + " ";
		}

		if (this.props.isDisabled) {
			finalClassName += css.disabledDropdown;
		}

		return finalClassName + " " + css.dropdown;
	}

	render() {
		return (
			<select
				className={this.createClassName()}
				disabled={this.props.isDisabled}
			>
				{this.props.options.map(item => (
					<option key={item.key} value={item.key}>
						{item.value}
					</option>
				))};
			</select>
		);
	}
}

Dropdown.defaultProps = {
	size: "MEDIUM",
	options: [],
	isDisabled: false
};

Dropdown.propTypes = {
	size: PropTypes.string,
	options: PropTypes.array,
	isDisabled: PropTypes.bool
};
