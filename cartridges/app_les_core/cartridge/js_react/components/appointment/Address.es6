import React from "react";
import PropTypes from "prop-types";
import i18n from "i18n/en.json";
import { formatNumber } from "libphonenumber-js";
//Takes the redux store's storeInfo and formats to an address section
const Address = props => {
	const { address1, city, stateCode, postalCode, phone } = props.storeInfo;
	return (
		<section className="cell">
			<address>
				<span className="topAddress">{address1}</span>
				<span className="bottomAddress">
					{city}, {stateCode} {postalCode}
				</span>
				<div>
					<a
						aria-label={i18n.phoneAria}
						href={"tel:" + phone.replace(/[\s()-]+/g, "")}
						onClick={props.handleAddressClick}
					>
						{formatNumber(phone, "National")}
					</a>
				</div>
			</address>
		</section>
	);
};

Address.propTypes = {
	storeInfo: PropTypes.object,
	handleAddressClick: PropTypes.func
};

export default Address;
