import React from "react";
import PropTypes from "prop-types";
import css from "css/components/Calendar";
import i18n from "i18n/en.json";
import config from "config";
import moment from "moment";

export default class Calendar extends React.Component {
	constructor(props) {
		super(props);
		//stores date as string "2017-10-18"
		let startDate =
			this.props.selectedDay !== null
				? moment(this.props.selectedDay, config.date_format)
				: moment().startOf("day");
		//stores month as a Moment object
		let startMonth = startDate.clone();

		this.state = {
			month: startMonth,
			currentDaySelection: startDate
		};

		this.previous = this.previous.bind(this);
		this.next = this.next.bind(this);
		this.selectDayHandler = this.selectDayHandler.bind(this);
		this.handleMonthKeyPress = this.handleMonthKeyPress.bind(this);
	}

	componentWillMount() {
		//select the date after the component mounts
		this.props.handleSelectDate(
			this.state.currentDaySelection.format(config.date_format)
		);
	}

	handleMonthKeyPress(event, direction) {
		if (event.charCode === 13) {
			if (direction === "next") {
				this.next();
			} else {
				this.previous();
			}
		}
	}

	previous() {
		let month = this.state.month;
		month.add(-1, "M");
		this.setState({ month: month });
	}

	next() {
		let month = this.state.month;
		month.add(1, "M");
		this.setState({ month: month });
	}

	selectDayHandler(day) {
		this.props.handleSelectDate(day.format(config.date_format));
		this.setState({ currentDaySelection: day });
	}

	render() {
		const renderWeeks = () => {
			let weeks = [],
				done = false,
				lastWeek = false,
				//start on Sunday of -1 week from 1st of this month
				date = this.state.month
					.clone()
					.startOf("month")
					.add("w" - 1)
					.day("Sunday"),
				clonedDate = date.clone(),
				monthIndex = date.month(),
				count = 0;

			while (!done) {
				clonedDate.add(1, "w");
				lastWeek = count > 2 && monthIndex !== clonedDate.month(); //will notify week if its the last one

				weeks.push(
					<Week
						key={date.valueOf()}
						date={date.clone()}
						count={count}
						lastWeek={lastWeek}
						selectHandler={this.selectDayHandler}
						selected={this.state.currentDaySelection}
						handleRequestTimes={this.props.handleRequestTimes}
						handleSelectTime={this.props.handleSelectTime}
					/>
				);
				date.add(1, "w");
				done = count++ > 2 && monthIndex !== date.month();
				monthIndex = date.month();
			}

			return weeks;
		};
		return (
			<article className={css.calendar}>
				<nav className={css.header}>
					{this.state.month.isAfter(new Date(), "month") ? (
						<a
							className={css.previous}
							onClick={this.previous}
							onKeyPress={event => this.handleMonthKeyPress(event, "previous")}
							tabIndex="0"
							aria-label="previous"
						>
							{i18n.dateTimeView.previousMonth}
						</a>
					) : (
						<a className={css.disabledMonth} aria-hidden="true" />
					)}
					<span>{this.state.month.format("MMMM YYYY")}</span>
					{this.state.month.isSame(new Date(), "month") ? (
						<a
							className={css.next}
							onClick={this.next}
							onKeyPress={event => this.handleMonthKeyPress(event, "next")}
							tabIndex="0"
							aria-label="next"
						>
							{i18n.dateTimeView.nextMonth}
						</a>
					) : (
						<a className={css.disabledMonth} aria-hidden="true" />
					)}
				</nav>
				<DayNames />
				{renderWeeks()}
			</article>
		);
	}
}

Calendar.propTypes = {
	selectedDay: PropTypes.string,
	handleSelectDate: PropTypes.func,
	handleSelectTime: PropTypes.func,
	handleRequestTimes: PropTypes.func
};

const DayNames = () => (
	<header className={css.weekNames}>
		{i18n.dateTimeView.weekNames.map((day, i) => (
			<span className={css.day} key={"day_name_" + i}>
				{day}
			</span>
		))}
	</header>
);

const Week = props => {
	let days = [],
		i = 0,
		date = props.date.clone();

	while (i < 7) {
		let isBlank = false; //for days that are from a previous month
		let isDeactivated = false;

		if (props.count == 0 && date.date() > 7) {
			//on the first week of the month, dont incldue the "30th" day from the previous month
			isBlank = true;
		}
		if (props.lastWeek && date.date() == 1) {
			break; //stop adding days, as the calendar is complete
		}
		if (i === 0) {
			isDeactivated = true; //deactivating sundays
		}
		days.push(
			<Day
				selectHandler={props.selectHandler}
				key={date.valueOf()}
				date={date.clone()}
				isDisabled={!date.isSameOrAfter(new Date(), "day") || isDeactivated}
				isBlank={isBlank}
				selected={props.selected}
				handleRequestTimes={props.handleRequestTimes}
				handleSelectTime={props.handleSelectTime}
			/>
		);
		date.add(1, "d");
		i++;
	}

	return <section className={css.week}>{days}</section>;
};

Week.propTypes = {
	date: PropTypes.object,
	selected: PropTypes.object,
	selectHandler: PropTypes.func,
	handleRequestTimes: PropTypes.func,
	handleSelectTime: PropTypes.func,
	count: PropTypes.number,
	lastWeek: PropTypes.bool
};

class Day extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isSelected: props.date.isSame(props.selected)
		};
		this.handleSelection = this.handleSelection.bind(this);
		this.handleSelectionKeyPress = this.handleSelectionKeyPress.bind(this);
	}
	componentWillReceiveProps(nP) {
		this.setState({
			isSelected: nP.date.isSame(nP.selected)
		});
	}
	handleSelectionKeyPress(event) {
		if (event.charCode === 13) {
			this.handleSelection();
		}
	}
	handleSelection() {
		this.setState({ isSelected: true });
		this.props.selectHandler(this.props.date);
	}
	render() {
		let classNames = [css.day];
		let renderedDay = null;
		if (this.props.date.isSame(new Date(), "day")) {
			classNames.push(css.today);
		}
		if (this.state.isSelected && !this.props.isBlank) {
			classNames.push(css.selectedDay);
		}

		if (this.props.isBlank) {
			//day is there for spacing sakes
			renderedDay = (
				<span className={classNames.join(" ") + " " + css.pastDay} />
			);
		} else if (this.props.isDisabled) {
			//day is unable to be clicked
			renderedDay = (
				<span className={classNames.join(" ") + " " + css.pastDay}>
					{this.props.date.date()}
				</span>
			);
		} else {
			renderedDay = (
				<a
					tabIndex="0"
					className={classNames.join(" ")}
					onClick={this.handleSelection}
					onKeyPress={this.handleSelectionKeyPress}
					aria-label={
						i18n.dateTimeView.calendarDayAria + this.props.date.date()
					}
				>
					{this.props.date.date()}
				</a>
			);
		}

		return renderedDay;
	}
}

Day.propTypes = {
	date: PropTypes.object.isRequired,
	selectHandler: PropTypes.func,
	isDisabled: PropTypes.bool,
	selected: PropTypes.object,
	isBlank: PropTypes.bool
};
