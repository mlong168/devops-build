import React from "react";
import PropTypes from "prop-types";
import Responsive from "components/appointment/Responsive";
import i18n from "i18n/en.json";
import { servicesView } from "i18n/en.json";
import Accordion from "components/appointment/Accordion";
import Button from "components/appointment/Button";
import Address from "components/appointment/Address";

class IDontKnowComponent extends React.Component {
	constructor(props) {
		super(props);

		this.handleToggle = this.handleToggle.bind(this);
		this.handleToggleKeyPress = this.handleToggleKeyPress.bind(this);

		this.handleInputKeyPress = this.handleInputKeyPress.bind(this);

		this.state = {
			isOpen: false
		};
	}
	componentDidMount() {
		if (this.inp) {
			this.inp.checked = this.shouldBeSelected();
		}
	}
	handleToggleKeyPress(event) {
		if (event.charCode === 13) {
			this.handleToggle(event);
		}
	}
	handleInputKeyPress(event) {
		let checkedState = !this.inp.checked;
		this.inp.checked = checkedState;
		if (event.charCode === 13) {
			this.props.onChange(servicesView.iDontKnowKey, checkedState);
		}
	}
	handleToggle(event) {
		this.setState({ isOpen: !this.state.isOpen });
		event.preventDefault();
	}
	shouldBeSelected() {
		//if any other service is selected - this one shouldn't be
		return (
			this.props.preSelectedServices.indexOf(servicesView.iDontKnowKey) > -1 &&
			this.props.preSelectedServices.length < 2
		);
	}
	render() {
		let descriptionSection = null;
		if (this.state.isOpen) {
			descriptionSection = (
				<div className="description">{servicesView.iDontKnowMessage}</div>
			);
		}
		if (this.props.preSelectedServices.length > 0 && !this.shouldBeSelected()) {
			return null;
		}
		return (
			<label className="cell checkboxListElement">
				<div className="checkboxContainer">
					<input
						type="checkbox"
						ref={inp => {
							this.inp = inp;
						}}
						defaultChecked={this.shouldBeSelected()}
						onChange={e => {
							this.props.onChange(servicesView.iDontKnowKey, e.target.checked);
						}}
						tabIndex="0"
						id="iDontKnow"
						onKeyPress={this.handleInputKeyPress}
					/>
					<label
						tabIndex="0"
						htmlFor="iDontKnow"
						onKeyPress={this.handleInputKeyPress}
						onChange={e => {
							this.props.onChange(servicesView.iDontKnowKey, e.target.checked);
						}}
					/>
				</div>
				<span>{servicesView.iDontKnow}</span>
				<a
					className={this.state.isOpen ? "closeIcon icon" : "infoIcon icon"}
					onClick={this.handleToggle}
					aria-label={servicesView.infoIcon}
					tabIndex="0"
					onKeyPress={this.handleToggleKeyPress}
				/>
				{descriptionSection}
			</label>
		);
	}
}
IDontKnowComponent.propTypes = {
	onChange: PropTypes.func,
	preSelectedServices: PropTypes.array
};

class ServicesView extends React.Component {
	constructor(props) {
		super(props);
		//let urlParams = queryString.parse(window.location.search);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSelection = this.handleSelection.bind(this);

		// this.props.handleLoadURLParams(
		// 	urlParams["vehicle-name"],
		// 	urlParams["tire-size"]
		// );

		this.state = {
			isValid: this.props.servicesState.selectedServices.length > 0,
			selectedServices: this.props.servicesState.selectedServices,
			showError: false
		};
	}
	handleSubmit() {
		let isTextAreaEmpty = this.addInfoArea.value.trim() === "";

		if (this.state.selectedServices.length === 0 && isTextAreaEmpty) {
			this.setState({ showError: true });
		} else {
			this.setState({ showError: false });
			this.props.submitServices(
				this.state.selectedServices.slice(),
				this.addInfoArea.value
			);
			this.props.handleNext();
		}
	}
	componentDidMount() {
		this.addInfoArea.value = this.props.servicesState.additionalInfo;
	}
	componentDidUpdate() {
		if (this.state.showError) {
			this.props.handleValidationError(servicesView.noServicesSelected);
		}
	}
	handleSelection(serviceId, isSelected) {
		let selectedServices = this.state.selectedServices.slice();
		let ind = selectedServices.indexOf(serviceId);
		if (ind !== -1) {
			selectedServices.splice(ind, 1);
		}
		if (isSelected) {
			/*Checking if iDontknow option has been selected - in which case it needs to be removed*/
			let iDontKnowIndex = selectedServices.indexOf(servicesView.iDontKnowKey);
			if (serviceId !== servicesView.iDontKnowKey && iDontKnowIndex > -1) {
				selectedServices.splice(iDontKnowIndex, 1);
			}
			selectedServices.push(serviceId);
		}
		this.setState({
			...this.state,
			selectedServices: selectedServices,
			isValid: selectedServices.length > 0
		});
	}
	render() {
		return (
			<div className="wrapper">
				<article className="grid">
					<Responsive.NonMobile>
						<h5 className="sectionHeader">{servicesView.addressTitle}</h5>
					</Responsive.NonMobile>
					<Responsive.Mobile>
						<h6 className="sectionHeader">{servicesView.addressTitle}</h6>
					</Responsive.Mobile>
					<section className="section">
						<Address
							storeInfo={this.props.userStore}
							handleAddressClick={() =>
								this.props.handleAddressClick("Services")
							}
						/>
					</section>
					<Responsive.NonMobile>
						<h5 className="sectionHeader">{servicesView.servicesTitle}</h5>
					</Responsive.NonMobile>
					<Responsive.Mobile>
						<h6 className="sectionHeader">{servicesView.servicesTitle}</h6>
					</Responsive.Mobile>
					<section className="section">
						<section className="cell">
							<Accordion
								services={this.props.servicesState.storeInfo.services}
								preSelectedServices={this.props.servicesState.selectedServices}
								handleSelection={this.handleSelection}
							/>
						</section>
						<IDontKnowComponent
							onChange={this.handleSelection}
							preSelectedServices={this.state.selectedServices}
						/>
					</section>
					<Responsive.NonMobile>
						<h5 className="sectionHeader" id="additionalInfoHeading">
							{servicesView.additionalInformation}
						</h5>
					</Responsive.NonMobile>
					<Responsive.Mobile>
						<h6 className="sectionHeader" id="additionalInfoHeading">
							{servicesView.additionalInformation}
						</h6>
					</Responsive.Mobile>
					<section className="section">
						<textarea
							aria-labelledby="additionalInfoHeading"
							name="additionalInformation"
							className="cell"
							placeholder={servicesView.addInfoPlaceholder}
							ref={textarea => {
								this.addInfoArea = textarea;
							}}
						/>
						<div className="cell">
							{this.state.showError && (
								<span className="errorSpan">
									{servicesView.noServicesSelected}
								</span>
							)}
							<Responsive.NonMobile>
								<Button
									onClick={this.handleSubmit}
									ariaLabel={i18n.nextButtonAria}
								>
									{servicesView.nextButton}
								</Button>
							</Responsive.NonMobile>
							<Responsive.Mobile>
								<Button
									onClick={this.handleSubmit}
									isFlex
									size="small"
									ariaLabel={i18n.nextButtonAria}
								>
									{servicesView.nextButton}
								</Button>
							</Responsive.Mobile>
						</div>
					</section>
				</article>
			</div>
		);
	}
}

ServicesView.propTypes = {
	handleNext: PropTypes.func,
	userStore: PropTypes.object,
	handleAddressClick: PropTypes.func,
	handleValidationError: PropTypes.func,
	submitServices: PropTypes.func,
	servicesState: PropTypes.object,
	handleLoadURLParams: PropTypes.func
};

export default ServicesView;
