import React from "react";
import PropTypes from "prop-types";
import Responsive from "components/Responsive";
import Address from "components/Address";
import css from "css/components/ConfirmationView";
import layout from "css/components/Layout";
import { virtualPageView } from "../actions/AppointmentActions";
import i18n from "i18n/en.json";
import logo2x from "images/confirmationImage@2x.png";
import confirmationCheck from "images/icons/confirmationCheck.svg";

//creates a list of services, based off an array of services that were selected
const ServicesList = props => (
	<div className={css.servicesList}>
		{props.data.map((service, index) => (
			<span key={"service-" + index}>{service}</span>
		))}
	</div>
);
ServicesList.propTypes = {
	data: PropTypes.array.isRequired
};

export default class Confirmation extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			heading: this.props.confirmationData.message || i18n.sampleHeading
		};
	}
	componentDidMount() {
		this.props.dispatch(
			virtualPageView({
				event: "VirtualPageview",
				virtualPageURL: "/Step4/Confirmation",
				virtualPageTitle: "Make Appt4–Confirmation"
			})
		);
	}
	render() {
		let data = this.props.confirmationData;
		return (
			<div>
				<Responsive.NonMobile>
					<section className={css.topBanner}>
						<img
							src={logo2x}
							alt={i18n.confirmationView.altBannerText}
							className={css.topBannerImage}
						/>
						<section className={css.infoTopBanner}>
							<img
								src={confirmationCheck}
								alt={i18n.confirmationView.checkMark}
								className={css.checkMark}
							/>
							<h1>{i18n.confirmationView.finshed}</h1>
							<p>{i18n.confirmationView.confirmationEmail}</p>
							<span className={css.bold}>{data.emailAddress}</span>
						</section>
					</section>
				</Responsive.NonMobile>
				<Responsive.Mobile>
					<section className={css.topBannerMobile}>
						<section className={css.infoTopBannerMobile}>
							<img
								src={confirmationCheck}
								alt={i18n.confirmationView.checkMark}
								className={css.checkMark}
							/>
							<h1>{i18n.confirmationView.finshed}</h1>
						</section>
					</section>
				</Responsive.Mobile>
				<Responsive.Mobile>
					<section className={css.mobileConfirmation + " " + css.section}>
						<p>
							{i18n.confirmationView.confirmationEmail}
							<span className={css.bold}>{data.emailAddress}</span>
						</p>
					</section>
				</Responsive.Mobile>
				<div className={layout.wrapper}>
					<section className={layout.grid}>
						<Responsive.NonMobile>
							<h5 className={layout.sectionHeader}>
								{i18n.confirmationView.when}
							</h5>
						</Responsive.NonMobile>
						<Responsive.Mobile>
							<h6 className={layout.sectionHeader}>
								{i18n.confirmationView.when}
							</h6>
						</Responsive.Mobile>
						<section className={css.section}>
							<p className={css.whenParagraph}>
								<span className={css.bold + " " + css.inline}>
									{data.formattedDate}
								</span>
							</p>
						</section>
						<Responsive.NonMobile>
							<h5 className={layout.sectionHeader}>
								{i18n.confirmationView.where}
							</h5>
						</Responsive.NonMobile>
						<Responsive.Mobile>
							<h6 className={layout.sectionHeader}>
								{i18n.confirmationView.where}
							</h6>
						</Responsive.Mobile>
						<section className={css.section}>
							<Address
								storeInfo={{
									address: data.address,
									city: data.city,
									state: data.state,
									zipCode: data.zipCode,
									phoneNumber: data.storePhoneNumber
								}}
								handleAddressClick={() =>
									this.props.handleAddressClick("Confirmation")
								}
							/>
						</section>

						<Responsive.NonMobile>
							<h5 className={layout.sectionHeader}>
								{i18n.confirmationView.contact}
							</h5>
						</Responsive.NonMobile>
						<Responsive.Mobile>
							<h6 className={layout.sectionHeader}>
								{i18n.confirmationView.contact}
							</h6>
						</Responsive.Mobile>
						<section className={css.section}>
							<p>
								<span>
									{data.firstName} {data.lastName}
								</span>
								<span>{data.emailAddress}</span>
								<span>{data.phoneNumber}</span>
							</p>
						</section>

						{(data.vehicleDescription || data.vehicleTireSize) && (
							<section className={css.section}>
								<div className={css.reactWrapper}>
									<Responsive.NonMobile>
										<h5 className={layout.sectionHeader}>
											{i18n.confirmationView.vehicle}
										</h5>
									</Responsive.NonMobile>
									<Responsive.Mobile>
										<h6 className={layout.sectionHeader}>
											{i18n.confirmationView.vehicle}
										</h6>
									</Responsive.Mobile>
									{data.vehicleDescription && (
										<span className={css.spanSpacing}>
											{i18n.confirmationView.vehicleDescription}
											{": "}
											{data.vehicleDescription}
										</span>
									)}
									{data.vehicleTireSize && (
										<span className={css.spanSpacing}>
											{i18n.confirmationView.tireSize}
											{": "}
											{data.vehicleTireSize}
										</span>
									)}
								</div>
							</section>
						)}
						<Responsive.NonMobile>
							<h5 className={layout.sectionHeader}>
								{i18n.confirmationView.services}
							</h5>
						</Responsive.NonMobile>
						<Responsive.Mobile>
							<h6 className={layout.sectionHeader}>
								{i18n.confirmationView.services}
							</h6>
						</Responsive.Mobile>
						<section className={css.section}>
							{data.services.length === 0 && (
								<span className={css.spacing}>
									{i18n.confirmationView.noServices}
								</span>
							)}
							{data.services.length >= 1 && (
								<ServicesList data={data.services} />
							)}
							{data.comments.length > 1 && (
								<span className={css.spanSpacing}>
									{i18n.confirmationView.comment} {data.comments}
								</span>
							)}
							<span>{i18n.confirmationView.cancelAppointment}</span>
							<span>
								{i18n.confirmationView.prePhone}{" "}
								<a
									className={css.bold}
									aria-label={i18n.phoneAria}
									onClick={() => this.props.handleAddressClick("Confirmation")}
									href={"tel:" + data.storePhoneNumber.replace(/[\s()-]+/g, "")}
								>
									{data.storePhoneNumber}
								</a>
							</span>
						</section>
					</section>
				</div>
			</div>
		);
	}
}
Confirmation.propTypes = {
	dispatch: PropTypes.func,
	confirmationData: PropTypes.object,
	handleAddressClick: PropTypes.func
};
