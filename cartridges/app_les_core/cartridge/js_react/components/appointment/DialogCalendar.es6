import React from "react";
import PropTypes from "prop-types";
import Calendar from "components/Calendar";
import Button from "components/Button";
import css from "css/components/DialogCalendar";
import i18n from "i18n/en.json";
import moment from "moment";
import config from "config";

class DialogCalendar extends React.Component {
	constructor(props) {
		super(props);
		let currentDate = this.props.date;

		if (currentDate === null) {
			currentDate = moment()
				.startOf("day")
				.add(1, "d")
				.format(config.date_format); //when Calendar recieves null, it starts at today
		}
		this.handleMobileCancel = this.handleMobileCancel.bind(this);
		this.handleConfirmDate = this.handleConfirmDate.bind(this);
		this.handleTodayClick = this.handleTodayClick.bind(this);

		this.state = {
			savedOriginalDate: currentDate
		};
	}
	handleMobileCancel() {
		this.props.handleSelectDate(this.state.savedOriginalDate);
		this.props.handleHideDialog();
	}
	handleConfirmDate() {
		this.props.handleHideDialog();
	}
	handleTodayClick() {
		let today = moment()
			.startOf("day")
			.format(config.date_format);

		this.props.handleSelectDate(today);
		this.props.handleHideDialog();
	}
	render() {
		return (
			<div>
				<nav className={css.dialogCalendarNav}>
					<a onClick={this.handleTodayClick}>{i18n.dateTimeView.today}</a>
					<a onClick={this.handleMobileCancel}>{i18n.dateTimeView.cancel}</a>
				</nav>
				<Calendar
					selectedDay={this.props.date}
					handleSelectDate={this.props.handleSelectDate}
					handleSelectTime={this.props.handleSelectTime}
					handleRequestTimes={this.props.handleRequestTimes}
				/>
				<div className={css.dialogSelectButton}>
					<Button onClick={this.handleConfirmDate} isFlex>
						{i18n.dateTimeView.select}
					</Button>
				</div>
			</div>
		);
	}
}
DialogCalendar.propTypes = {
	date: PropTypes.string,
	handleSelectDate: PropTypes.func,
	handleSelectTime: PropTypes.func,
	handleRequestTimes: PropTypes.func,
	handleHideDialog: PropTypes.func
};

export default DialogCalendar;
