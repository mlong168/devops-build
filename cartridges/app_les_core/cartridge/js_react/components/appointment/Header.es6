import React from "react";
import PropTypes from "prop-types";
import Responsive from "components/appointment/Responsive";
import ProgressBar from "components/appointment/ProgressBar";
import i18n from "i18n/en.json";

const Header = props => {
	let content = null;
	if (props.showHeader) {
		content = (
			<header>
				{!props.showFull ? null : (
					<section className="heading">
						<Responsive.NonMobile>
							<h1>{i18n.heading}</h1>
						</Responsive.NonMobile>
						<Responsive.Mobile>
							<h4>{i18n.heading}</h4>
						</Responsive.Mobile>
					</section>
				)}
				{!props.showFull || !props.page === null ? null : (
					<ProgressBar page={props.page} />
				)}
			</header>
		);
	}
	return content;
};
Header.propTypes = {
	page: PropTypes.number,
	showFull: PropTypes.bool,
	showHeader: PropTypes.bool
};
export default Header;
