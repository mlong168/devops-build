import React from "react";
import PropTypes from "prop-types";
import { servicesView } from "i18n/en.json";

//Handles one of the accordion items within a category
class AccordionCategoryItem extends React.Component {
	constructor(props) {
		super(props);

		this.handleToggle = this.handleToggle.bind(this);
		this.handleToggleKeyPress = this.handleToggleKeyPress.bind(this);

		this.handleSelected = this.handleSelected.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);

		this.state = {
			isOpen: false, //is the description open
			//flag to see if the item is selected, used when coming back to the acccordion
			isSelected:
				this.props.preSelectedServices.indexOf(this.props.serviceId) > -1
		};
	}
	//called by an onKeyPress, to allow keyboard navigation
	handleKeyPress(event) {
		if (event.charCode === 13) {
			//enter key pressed
			event.preventDefault();
			this.handleSelected();
		}
	}
	//called by an onKeyPress, to allow keyboard navigation
	handleToggleKeyPress(event) {
		if (event.charCode === 13) {
			//enter key pressed
			event.preventDefault();
			this.handleToggle();
		}
	}
	//toggles the showing/hiding of description of item
	handleToggle(event) {
		this.setState({ ...this.state, isOpen: !this.state.isOpen });
		if (event) {
			event.preventDefault(); //dont run handleSelected if this ran!
		}
	}
	//handles the selection of the item inside of the accordion section
	handleSelected() {
		const isSelected = !this.state.isSelected;
		this.checkbox.checked = isSelected;
		this.setState({ ...this.state, isSelected: isSelected });
		this.props.handleSelection(this.props.serviceId, isSelected); //update redux state
	}
	render() {
		const { serviceId, title, description } = this.props;
		let descriptionSection = null,
			infoIcon = null;
		if (this.state.isOpen) {
			descriptionSection = <div className="description">{description}</div>;
		}
		if (description && description !== "") {
			infoIcon = (
				<a
					className={this.state.isOpen ? "closeIcon icon" : "infoIcon icon "}
					onClick={this.handleToggle}
					aria-label={servicesView.infoIcon}
					tabIndex="0"
					onKeyPress={this.handleToggleKeyPress}
				/>
			);
		}
		return (
			<li className="categoryItem">
				<section className="categoryInputs">
					<div className="checkboxContainer">
						<input
							type="checkbox"
							ref={input => (this.checkbox = input)}
							id={serviceId}
							value={serviceId}
							defaultChecked={this.state.isSelected}
							onChange={this.handleSelected}
						/>
						<label
							htmlFor={serviceId}
							tabIndex="0"
							onKeyPress={this.handleKeyPress}
						/>
					</div>
					<label htmlFor={serviceId}>
						{title}
						{infoIcon}
					</label>
				</section>
				{descriptionSection}
			</li>
		);
	}
}

AccordionCategoryItem.propTypes = {
	serviceId: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	handleSelection: PropTypes.func,
	preSelectedServices: PropTypes.array
};

//Handles the whole category of items (Tires section, Brakes Section, etc)
class AccordionCategory extends React.Component {
	constructor(props) {
		super(props);

		this.handleToggle = this.handleToggle.bind(this);
		this.handleSelection = this.handleSelection.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);

		//used to handle the state when coming back to an accordion that is already filled out
		const matches = props.services.filter(
			item => props.preSelectedServices.indexOf(item.serviceId) > -1
		);

		this.state = {
			isOpen: matches.length > 0,
			//selectedItems: props.preSelectedServices || []
			selectedItems: matches.reduce((arr, item) => {
				arr.push(item.serviceId);
				return arr;
			}, [])
		};
	}
	//allow pressing enter key to open the accordion
	handleKeyPress(event) {
		if (event.charCode === 13) {
			//enter key pressed
			event.preventDefault();
			this.handleToggle();
		}
	}
	//open/close accoridon section
	handleToggle() {
		this.setState({
			...this.state,
			isOpen: !this.state.isOpen
		});
	}
	//select/deselect option, and update store object
	handleSelection(serviceId, isChecked) {
		let selectedItemsSnapshot = this.state.selectedItems.slice();
		if (isChecked) {
			selectedItemsSnapshot.push(serviceId);
			this.setState({
				selectedItems: selectedItemsSnapshot
			});
		} else {
			selectedItemsSnapshot.splice(
				this.state.selectedItems.indexOf(serviceId),
				1
			);
			this.setState({
				selectedItems: selectedItemsSnapshot
			});
		}
		this.props.handleSelection(serviceId, isChecked);
	}
	render() {
		const { title, services } = this.props;
		let numberOfItemsSelected = null;
		let content = (
			<ul>
				{services.map((service, index) => (
					<AccordionCategoryItem
						key={"item_" + index}
						tabIndex={index}
						{...service}
						preSelectedServices={this.state.selectedItems}
						handleSelection={this.handleSelection}
					/>
				))}
			</ul>
		);
		if (!this.state.isOpen) {
			content = null;
		}
		if (this.state.isOpen || this.state.selectedItems.length > 0) {
			numberOfItemsSelected = (
				<span className="numberOfItemsSelected">
					{this.state.selectedItems.length}
				</span>
			);
		}
		return (
			<li className={"accordionCategory" + (this.state.isOpen ? " open" : "")}>
				<a
					className={title.replace(/\s+/g, "") + "-accordianIcon"}
					tabIndex="0"
					onClick={this.handleToggle}
					aria-label={servicesView.anchorTag}
					onKeyPress={this.handleKeyPress}
				>
					{title}
					{numberOfItemsSelected}
				</a>
				{content}
			</li>
		);
	}
}

AccordionCategory.propTypes = {
	title: PropTypes.string.isRequired,
	preSelectedServices: PropTypes.array,
	services: PropTypes.arrayOf(
		PropTypes.shape({
			serviceId: PropTypes.string.isRequired,
			title: PropTypes.string.isRequired,
			description: PropTypes.string
		})
	),
	handleSelection: PropTypes.func
};

export default class Accordion extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const serviceCategories = this.props.services;

		return (
			<ul className="accordion">
				{serviceCategories.map((category, index) => (
					<AccordionCategory
						key={"category_" + index}
						tabIndex={index}
						{...category}
						preSelectedServices={this.props.preSelectedServices}
						handleSelection={this.props.handleSelection}
					/>
				))}
			</ul>
		);
	}
}

Accordion.propTypes = {
	preSelectedServices: PropTypes.array,
	services: PropTypes.arrayOf(
		PropTypes.shape({
			title: PropTypes.string.isRequired,
			services: PropTypes.arrayOf(
				PropTypes.shape({
					serviceId: PropTypes.string.isRequired,
					title: PropTypes.string.isRequired,
					description: PropTypes.string
				})
			)
		}).isRequired
	).isRequired,
	handleSelection: PropTypes.func
};
