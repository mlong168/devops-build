import React from "react";
import PropTypes from "prop-types";
import i18n from "i18n/en.json";

const ProgressBar = props => {
	const items = [
		i18n.progressBar.services,
		i18n.progressBar.date,
		i18n.progressBar.contact
	].map((item, index) => {
		const activeClass = props.page === index ? "active" : "";
		return (
			<li data-value={index + 1} key={"step_" + index} className={activeClass}>
				<span>{item}</span>
			</li>
		);
	});
	return (
		<section className="progressBar">
			<ol>{items}</ol>
		</section>
	);
};

ProgressBar.propTypes = {
	page: PropTypes.number.isRequired
};

export default ProgressBar;
