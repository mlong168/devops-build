/**
	Displays a button that expands into a panel with vehicle selection options. Upon click on one of the options, launches "vehicleEditDialog" with
	selected flow activated.
*/
import React from "react";
import PropTypes from "prop-types";
import Overlay from "./Overlay";
import VehicleEditDialog from "components/VehicleEditDialog";
import MediaQuery from "react-responsive";
import config from "config";

class VehicleSelectorCTA extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false,
			activeFlow: null
		};

		this.openToggle = this.openToggle.bind(this);
		this.handleSelection = this.handleSelection.bind(this);
	}
	openToggle() {
		//flip open state AND if closing, remove flow selection
		this.setState({
			isOpen: !this.state.isOpen,
			activeFlow: this.state.isOpen ? null : this.state.activeFlow
		});
	}
	handleSelection(flow) {
		this.setState({ activeFlow: flow, isOpen: false });
	}
	render() {
		let panel;
		if (this.state.isOpen) {
			panel = (
				<article
					className={
						"vehicleSelectorCTA" + this.state.isLoading
							? " vehicleSelectorCTA--loading"
							: ""
					}
				>
					<MediaQuery minWidth={config.breakpoints.maxMobile}>
						<Overlay clickHandler={this.openToggle} zindex={30} />
					</MediaQuery>
					<section className="vehicleSelectorCTA__panel">
						<h5
							className="vehicleSelectorCTA__panel__heading"
							onClick={this.openToggle}
						>
							{this.props.ctaLabel}
						</h5>
						{this.props.helpTip && <p>{this.props.helpTip}</p>}
						<ul>
							<li className="byVehicleType">
								<a
									className="button button--vehicleSelector"
									onClick={() => this.handleSelection("byVehicleType")}
								>
									{this.props.byVehicleLabel}
								</a>
							</li>
							<li className="byTire">
								<a
									className="button button--vehicleSelector"
									onClick={() => this.handleSelection("byTire")}
								>
									{this.props.byTireLabel}
								</a>
							</li>
						</ul>
					</section>
				</article>
			);
		} else {
			panel = (
				<article className="vehicleSelectorCTA">
					<div className="shop-for-tires-cta">
						<button className="button" onClick={this.openToggle}>
							{this.props.ctaLabel}
						</button>
					</div>
					{this.state.activeFlow && (
						<VehicleEditDialog
							storeData={this.props.storeData}
							isCompact={true}
							activeFlow={this.state.activeFlow}
							getVehicleSelectorDataUrl={this.props.getVehicleSelectorDataUrl}
							toggleDialog={() => this.handleSelection(null)}
						/>
					)}
				</article>
			);
		}
		return panel;
	}
}

VehicleSelectorCTA.propTypes = {
	componentName: PropTypes.string,
	ctaLabel: PropTypes.string.isRequired,
	helpTip: PropTypes.string,
	byVehicleLabel: PropTypes.string,
	byTireLabel: PropTypes.string,
	storeData: PropTypes.object,
	getVehicleSelectorDataUrl: PropTypes.string.isRequired
};

export default VehicleSelectorCTA;
