import React from "react";
import { StoreDetailProps } from "util/Props";
import config from "config";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class StoreDetailsBase extends React.Component {
	constructor(props) {
		super(props);
		this.getDirections = this.getDirections.bind(this);
		this.handleMystoreAction = this.handleMystoreAction.bind(this);
		this.roundDistance = this.roundDistance.bind(this);
		this.formatPhone = this.formatPhone.bind(this);

		this.state = {
			userLocation: null, //TODO: normalize location object and hook up props
			isMystore:
				this.props.myStore !== null
					? this.props.myStore === this.props.id
					: this.props.isMystore
		};
	}
	//result: 10.0mi
	roundDistance(num) {
		return Number.parseFloat(num).toFixed(1);
	}
	//format comes in as just a string of numbers ex 5555551234
	formatPhone() {
		let match = String(this.props.phone).match(/^(\d{3})(\d{3})(\d{4})$/);
		return !match ? null : "(" + match[1] + ") " + match[2] + "-" + match[3];
	}
	componentWillReceiveProps(nP) {
		this.setState({
			isMystore:
				nP.myStore !== null ? nP.myStore === this.props.id : nP.isMystore
		});
	}
	componentDidMount() {
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition(pos =>
				this.setState({ userLocation: pos })
			);
		}
		this.props.handleMount({
			index: this.props.index || null,
			id: this.props.id,
			latitude: this.props.latitude,
			longitude: this.props.longitude,
			address: this.props.address1,
			isMystore: this.state.isMystore,
			isSelectedStore: this.props.isSelectedStore,
			distance: this.props.distance
		});
	}
	handleMystoreAction(event) {
		const target = event.target;
		if (target.checked && !this.isMystore) {
			this.props.postMyStore(this.props.makeMyStoreUrl, this.props.id);
		}
	}
	//returns URL for the directions button
	getDirections() {
		let params = [
			"api=1",
			`origin=${
				this.state.userLocation
					? this.state.userLocation.coords.latitude +
					  "," +
					  this.state.userLocation.coords.longitude
					: ""
			}`,
			`destination=${this.props.address1},${this.props.city},${
				this.props.stateCode
			},${this.props.postalCode}`
		];

		return `${config.googleMaps.url}?` + encodeURI(params.join("&"));
	}
	//this is abstract
	render() {
		return null;
	}
}

StoreDetailsBase.propTypes = StoreDetailProps;

export default StoreDetailsBase;
