import React from "react";
import { StoreDetailProps } from "util/Props";
import AppointmentCTA from "components/AppointmentCTA";
import StoreDetailsBase from "components/StoreDetailsBase";
import MakeMyStore from "components/MakeMyStore";
import config from "config";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class StoreDetailsBrief extends StoreDetailsBase {
	//add class to selectedStoreID to show the different styles for the selected ID store
	//() => to pass the store id to prop function
	render() {
		let address = [];
		let storeDetalsClass = "storeDetails storeDetails--brief";
		if (this.props.isSelectedStore) {
			storeDetalsClass += " storeDetails--selected";
		}
		address.push(this.props.city);
		address.push(this.props.stateCode);
		address.push(this.props.postalCode);
		return (
			<div
				ref={div => {
					this.element = div;
				}}
				className={storeDetalsClass}
				onClick={() => this.props.setSelectedStore(this.props.id)}
			>
				{/*
						Address Information
				*/}
				<address>
					<span className="storeDetails__streetName" key={address}>
						{this.props.index != null && this.props.index + 1 + ". "}
						{this.props.address1}{" "}
					</span>
					<span>{address.join(", ")}</span>
					<span className="storeDetails__distance">
						{this.props.distance && (
							<strong>
								{`${this.roundDistance(this.props.distance)} ${
									config.distanceMeasurement
								}`}
							</strong>
						)}
					</span>
					<a
						className="storeDetails__mobileDetailsLink"
						href={this.props.storeDetailsUrl}
						title={this.props.storeDetailsLabel}
						aria-label={this.props.storeDetailsLabel}
					/>
				</address>
				{/*
						Contact, ordering is important due to mobile + desktop being different
				*/}
				<ul className="storeDetails__contact">
					<li>
						<span className="show-for-medium">
							{this.props.phone !== null && this.formatPhone()}
							{this.props.phone === null && this.props.noPhoneMessage}
						</span>
						<a
							className="show-for-small-only button button--gray"
							href={this.getDirections()}
							target="_blank"
						>
							{this.props.directionsLabel}
						</a>
					</li>
					<li>
						<a
							className="show-for-medium"
							href={this.getDirections()}
							target="_blank"
						>
							{this.props.directionsLabel}
						</a>
						{this.props.phone !== null && (
							<a
								className="show-for-small-only button button--gray"
								href={"tel:" + this.props.phone}
							>
								Call Us
							</a>
						)}
						{this.props.phone === null && (
							<span className="show-for-small-only">
								{this.props.noPhoneMessage}
							</span>
						)}
					</li>
					<li className="show-for-medium">
						<a href={this.props.storeDetailsUrl}>
							{this.props.storeDetailsLabel}
						</a>
					</li>
				</ul>
				{/* {this.props.description && (
					<span className="storeDetails__description">
						{this.props.description}
					</span>
				)} */}
				<AppointmentCTA
					ctaLabel={this.props.appointmentLabel}
					ctaUrl=""
					classes="storeDetails__button"
				/>
				<MakeMyStore
					id={this.props.id}
					isMystore={this.state.isMystore}
					handleMystoreAction={this.handleMystoreAction}
					myStoreLabel={this.props.myStoreLabel}
					makeMyStoreLabel={this.props.makeMyStoreLabel}
				/>
			</div>
		);
	}
}

StoreDetailsBrief.propTypes = StoreDetailProps;

export default StoreDetailsBrief;
