import React from "react";
import { StoreDetailProps } from "util/Props";
import StoreDetailsBase from "components/StoreDetailsBase";
/**
	Store details on product detail pages
*/
class StoreDetailsPDP extends StoreDetailsBase {
	render() {
		let address = [];
		address.push(this.props.city);
		address.push(this.props.stateCode);
		address.push(this.props.postalCode);
		return (
			<div className="storeDetails storeDetails--pdp">
				<h6>{this.props.stopByForInstallation}</h6>
				{this.props.isMystore && (
					<strong className="storeDetails__storeHeader">
						{this.props.myStoreHeader}
					</strong>
				)}
				{!this.props.isMystore && (
					<strong className="storeDetails__storeHeader">
						{this.props.nearestStoreHeader}
					</strong>
				)}
				<address>
					<span className="storeDetails__streetName" key={address}>
						{this.props.index != null && this.props.index + 1 + ". "}
						{this.props.address1}{" "}
					</span>
					<span>{address.join(", ")}</span>
					<a className="changeCta" href={this.props.changeStoreUrl}>
						{this.props.changeStoreLabel}
					</a>
					{/* <a
						className="show-for-small-only button button--gray"
						href={"tel:" + this.props.phone}
					>
						Call Us
					</a> */}
				</address>
				<ul className="storeDetails__cta">
					<li>
						<a
							className="button button--flexWide"
							href={this.getDirections()}
							target="_blank"
						>
							{this.props.directionsLabel}
						</a>
					</li>
					<li>
						<a
							className="button button--gray button--flexWide"
							href={this.props.creditPageCtaUrl}
						>
							{this.props.creditPageCtaLabel}
						</a>
					</li>
				</ul>
			</div>
		);
	}
}

StoreDetailsPDP.propTypes = StoreDetailProps;

export default StoreDetailsPDP;
