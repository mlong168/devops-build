import React from "react";
import PropTypes from "prop-types";
import { StoreDetailProps, StoreSearchFormProps } from "util/Props";
import StoreSearchForm from "containers/StoreSearchForm";
import StoreDetails from "containers/StoreDetails";

/*
	Header Store Component that is used in TopBar.es6
	isMyStore controls if the button should be clickable or not
	isStoreDropOpen is passed down and lets the component know if it should open or not. 
		from /containers/Header.es6

*/
class HeaderStore extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isMystore:
				this.props.myStore !== null
					? this.props.myStore === this.props.id
					: this.props.isMystore
		};
	}
	componentWillReceiveProps(nP) {
		this.setState({
			isMystore:
				nP.myStore !== null ? nP.myStore === this.props.id : nP.isMystore
		});
	}

	render() {
		let address = [];
		let mobileIconClass = "customerHeader__location--icon";
		if (this.props.isStoreDropOpen) {
			mobileIconClass += " customerHeader__location--openIcon";
		}
		address.push(this.props.address1);
		address.push(this.props.city);
		address.push(this.props.stateCode);
		return (
			<div className="customerHeader__location">
				{/* Below div is part of header
				for non mobile */}
				<div
					className="customerHeader__location--full"
					onClick={this.props.toggleStoreDrop}
				>
					{!this.props.isStoreDropOpen && (
						<address>{address.join(", ")}</address>
					)}
					{this.props.isStoreDropOpen && (
						<address className="customerHeader__location__address--open">
							{address.join(", ")}
						</address>
					)}
				</div>
				<button
					onClick={this.props.toggleStoreDrop}
					className={mobileIconClass}
				>
					<img
						src={
							this.props.isStoreDropOpen
								? this.props.closeIconRedURL
								: this.props.pinIconURL
						}
						alt={
							this.props.isStoreDropOpen
								? "Close Store Locator"
								: "Open Store Locator"
						}
					/>
				</button>
				{/* <!-- Below div will be part of the store flyout --> */}
				{this.props.isStoreDropOpen && (
					<div className="myStoreDetails">
						<div className="myStoreDetails__top">
							<h6 className="header--sectionHeaderSmall header--large">
								{this.state.isMystore
									? this.props.myStoreHeader
									: this.props.nearestStoreHeader}
							</h6>
							<StoreDetails {...this.props} noMap={true} />
						</div>
						<div className="myStoreDetails__bottom">
							<h6>{this.props.findAnotherStoreLabel}</h6>
							<StoreSearchForm
								{...this.props.storeSearchForm}
								noMap={true}
								noHeader={true}
							/>
						</div>
					</div>
				)}
			</div>
		);
	}
}

HeaderStore.propTypes = {
	...StoreDetailProps,
	pinIconURL: PropTypes.string,
	closeIconRedURL: PropTypes.string,
	storeSearchForm: PropTypes.shape(StoreSearchFormProps),
	toggleStoreDrop: PropTypes.func,
	isStoreDropOpen: PropTypes.bool,
	myStore: PropTypes.string
};

export default HeaderStore;
