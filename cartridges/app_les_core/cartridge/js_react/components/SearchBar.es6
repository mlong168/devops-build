import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

/*
	Search Bar
		isLoading: currently unused, but is storing if the suggestions are loading or not
		isPanelOpen: shows/hides the panel with the results
		query: the user's input into the search bar
		suggestions: list of all search results, is an HTML element created in suggestions.isml
*/
class SearchBar extends React.Component {
	constructor(props) {
		super(props);

		this.handleSearchClick = this.handleSearchClick.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleClear = this.handleClear.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.renderSuggestions = this.renderSuggestions.bind(this);

		this.state = {
			isLoading: false,
			isPanelOpen: false,
			query: "",
			suggestions: null
		};
	}
	//when someone clicks on the search bar's X button
	handleClear() {
		this.setState({ query: "", suggestions: null, isPanelOpen: false });
	}
	//change of input
	handleChange(event) {
		let query = event.target.value;
		this.setState({ query: query });
		if (query.length > 2) {
			this.setState({ isLoading: true });
			fetch(this.props.suggestionsUrl + "?q=" + query, {
				method: "GET",
				credentials: "same-origin" //must have to send cookies
			})
				.then(response => {
					if (!response.ok) {
						throw Error(response.statusText);
					}
					this.setState({ isLoading: false });
					return response.text();
				})
				.then(text => {
					var cleanedText = text.replace(/\n/g, "");
					var isPanelOpen = cleanedText !== "";
					this.setState({
						suggestions: cleanedText,
						isPanelOpen: isPanelOpen
					});
				})
				.catch(error => console.log(error.message));
		} else {
			this.setState({ suggestions: null, isPanelOpen: false });
		}
		this.renderSuggestions();
		this.props.logSearchEvent("Search suggestions", query);
	}
	handleSubmit() {
		this.props.logSearchEvent("Search submit", this.state.query);
	}
	handleSearchClick() {
		if (this.state.isPanelOpen) {
			this.setState({
				isPanelOpen: false
			});
			this.props.toggleTobBarVisibility();
		}
		this.props.toggleSearchOpen();
	}
	renderSuggestions() {
		if (!this.state.suggestions || !this.props.isSearchOpen) {
			if (this.state.isPanelOpen === true) {
				//was the panel showing before?
				this.setState({
					isPanelOpen: false
				});
				this.props.toggleTobBarVisibility();
			}
		} else {
			if (this.state.isPanelOpen === false) {
				//was the panel hiding before?
				this.setState({
					isPanelOpen: true
				});
				this.props.toggleTobBarVisibility();
			}
		}
	}
	render() {
		return (
			<div
				className={classNames("search__input", {
					"search__input--open": this.props.isSearchOpen,
					"search__input--showingSuggestions": this.state.isPanelOpen
				})}
			>
				{this.props.isSearchOpen && (
					<form
						action={this.props.url}
						method="get"
						onSubmit={this.handleSubmit}
					>
						<input
							type="search"
							name="q"
							autoFocus
							autoComplete="off"
							placeholder={this.props.placeholder}
							value={this.state.query}
							onChange={this.handleChange}
							ref={headerSearch => (this.headerSearch = headerSearch)}
						/>
						{this.state.query &&
							this.state.query !== "" && (
								<a
									className="search__input__clear"
									onClick={this.handleClear}
								/>
							)}
						<a
							className="search__input__cancel"
							onClick={this.handleSearchClick}
						>
							Cancel
						</a>
					</form>
				)}
				{!this.props.isSearchOpen && (
					<span onClick={this.handleSearchClick}>Search</span>
				)}
				{this.state.isPanelOpen &&
					this.props.isSearchOpen && (
						<div
							className="search__input__suggestions"
							dangerouslySetInnerHTML={{
								__html: this.state.suggestions
							}}
						/>
					)}
			</div>
		);
	}
}

SearchBar.propTypes = {
	placeholder: PropTypes.string,
	url: PropTypes.string,
	suggestionsUrl: PropTypes.string,
	toggleSearchOpen: PropTypes.func,
	isSearchOpen: PropTypes.bool,
	toggleTobBarVisibility: PropTypes.func,
	handleSearchClick: PropTypes.func,
	logSearchEvent: PropTypes.func
};

export default SearchBar;
