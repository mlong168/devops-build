import React from "react";
import PropTypes from "prop-types";

/*
	Location Search Bar, used in the StoreSearchForm.es6 file
*/
const LocationSearchBar = props => (
	<div className="LocationSearchBar">
		<button
			className="button LocationSearchBar__locationSearch"
			onClick={props.onClickCurrentHandler}
			title={props.findByCurrentLocationTitle}
			aria-label={props.findByCurrentLocationTitle}
			type="button"
		/>
		<label htmlFor="LocationSearchBar__input" className="show-for-sr">
			{props.inputLabelText}
		</label>
		<input
			type="text"
			name={props.inputFieldValue ? props.inputFieldName : null}
			placeholder={props.inputFieldPlaceholder}
			value={props.inputFieldValue}
			onChange={props.inputFieldChangeHandler}
			id="LocationSearchBar__input"
		/>
		<button
			className="button LocationSearchBar__magnifyingSearch"
			type="submit"
			aria-label={props.findByAddressTitle}
		/>
	</div>
);

LocationSearchBar.propTypes = {
	findByCurrentLocationTitle: PropTypes.string,
	findByAddressTitle: PropTypes.string,
	onClickCurrentHandler: PropTypes.func,
	inputFieldName: PropTypes.string,
	inputFieldPlaceholder: PropTypes.string,
	inputLabelText: PropTypes.string,
	inputFieldValue: PropTypes.string,
	inputFieldChangeHandler: PropTypes.func
};

export default LocationSearchBar;
