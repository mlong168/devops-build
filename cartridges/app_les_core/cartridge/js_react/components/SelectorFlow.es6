/**
	Abstract class for selector flows defining common functionality
*/
import React from "react";
import { SelectorFlowProps } from "util/Props";

class SelectorFlow extends React.Component {
	constructor(props) {
		super(props);

		this.fetchFromApi = this.fetchFromApi.bind(this);
		this.populateShopUrl = this.populateShopUrl.bind(this);
		this.handleHintClick = this.handleHintClick.bind(this);

		this.state = {
			error: null,
			isFetching: false,
			showHint: false
		};
	}
	handleHintClick() {
		this.setState({ showHint: !this.state.showHint });
	}
	populateShopUrl(data) {
		let firstQ = this.props.shopUrl.indexOf("?") > -1 ? "&" : "?";
		return encodeURI(
			this.props.shopUrl +
				Object.keys(data).reduce((res, key, ind) => {
					if (typeof data[key] === "object" && data[key].min && data[key].max) {
						return (
							res +
							`${ind === 0 ? firstQ : "&"}prefn${ind + 1}=${key}&prefmin${ind +
								1}=${data[key].min}&prefmax${ind + 1}=${data[key].max}`
						);
					} else {
						return (
							res +
							`${ind === 0 ? firstQ : "&"}prefn${ind + 1}=${key}&prefv${ind +
								1}=${data[key]}`
						);
					}
				}, "") +
				`&source=${this.type}`
		);
	}
	fetchFromApi(what, handle, data = {}) {
		this.setState({ isFetching: true });
		fetch(
			`${this.props.apiUrls[what]}?` +
				encodeURI(
					Object.keys(data).reduce(
						(res, key, ind) =>
							res + (ind > 0 ? "&" : "") + `${key}=${data[key]}`,
						""
					)
				),
			{
				method: "GET",
				credentials: "same-origin" //must have to send cookies
			}
		)
			.then(response => {
				if (!response.ok) {
					throw Error(response.statusText);
				}
				let json = response.json();
				if (json.error && json.error === 500) {
					throw Error(json.errorMsg);
				}
				return json;
			})
			.then(handle)
			.catch(err => this.setState({ isFetching: false, error: err }));
	}
	render() {
		return null;
	}
}
SelectorFlow.propTypes = SelectorFlowProps;
export default SelectorFlow;
