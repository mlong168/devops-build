import React from "react";
import PropTypes from "prop-types";

/* This was copied over from Top Bar. It will not be included in phase 1, but its current
WIP state will be saved here and completed for the next phase */
class AccountLoginButton extends React.Component {
	constructor(props) {
		super(props);

		this.handleAccountClick = this.handleAccountClick.bind(this);
		this.toggleStoreDrop = this.toggleStoreDrop.bind(this);
		this.state = {
			isStoreDropOpen: false
		};
	}
	handleAccountClick() {
		this.setState({
			isAccountDropOpen: !this.state.isAccountDropOpen
		});
	}
	toggleStoreDrop() {
		if (this.state.isAccountDropOpen === true) {
			this.setState({
				isAccountDropOpen: !this.state.isAccountDropOpen
			});
		}
	}
	render() {
		let accountLinks = [];
		let overlayClass = "customerHeader__login__overlayTrigger";
		if (this.props.title && this.props.links) {
			this.props.links.forEach(link => {
				accountLinks.push(
					<li>
						<a href={link.href} title={link.title}>
							{link.label}
						</a>
					</li>
				);
			});
		}
		if (this.state.isAccountDropOpen) {
			overlayClass += " customerHeader__login__overlayTrigger--open";
		}
		return (
			<div className="customerHeader__login show-for-medium">
				<span className={overlayClass} onClick={this.handleAccountClick}>
					{this.props.title}
				</span>
				{this.state.isAccountDropOpen && (
					<div className="customerHeader__login__overlay">
						<div className="grid-x">
							<div className="cell medium-9 small-12">
								VEHICLE INFORMATION WILL GO HERE
							</div>
							<div className="secondary-navigation cell medium-3 small-12">
								<ul>
									{accountLinks}
									<li>
										<a href={this.props.logoutUrl}>{this.props.logoutLabel}</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

AccountLoginButton.propTypes = {
	logoutUrl: PropTypes.string,
	logoutLabel: PropTypes.string,
	title: PropTypes.string,
	links: PropTypes.array
};

export default AccountLoginButton;
