import React from "react";
import PropTypes from "prop-types";
import HeaderStore from "components/HeaderStore";
import BillPayLink from "components/BillPayLink";
import classNames from "classnames";
//TODO: disabling for release 1 import AccountLoginButton from "components/AccountLoginButton";
import { StoreDetailProps, StoreSearchFormProps } from "util/Props";

class TopBar extends React.Component {
	constructor(props) {
		super(props);
	}
	/*handleAccountClick() {
		this.setState({
			isAccountDropOpen: !this.state.isAccountDropOpen
		});
		if (this.state.isStoreDropOpen === true) {
			this.setState({
				isStoreDropOpen: !this.state.isStoreDropOpen
			});
		}
	}*/

	render() {
		return (
			<nav
				className={classNames("customerHeader", {
					customerHeader__hide: !this.props.isTopBarVisible
				})}
			>
				<section className="customerHeader__buttonWrapper">
					<button
						className={classNames("customerHeader__hamburger", {
							"customerHeader__hamburger--open": this.props.isMobileNavOpen
						})}
						onClick={this.props.toggleMobileNav}
					>
						<img
							src={
								this.props.isMobileNavOpen
									? this.props.closeIconRedURL
									: this.props.hamburgerIconURL
							}
							alt={
								this.props.isMobileNavOpen
									? "Close Navigation Menu"
									: "Open Navigation Menu"
							}
						/>
					</button>
				</section>
				<section className="customerHeader__logo">
					<a href={this.props.logo.url} title={this.props.logo.title}>
						<img src={this.props.logo.imageURL} alt="Les Schwab Tires Logo" />
					</a>
				</section>
				{/* Store Information */}
				<HeaderStore
					{...this.props.storeData}
					myStore={this.props.myStore}
					pinIconURL={this.props.pinIconURL}
					closeIconRedURL={this.props.closeIconRedURL}
					storeSearchForm={this.props.storeSearchForm}
					toggleStoreDrop={this.props.toggleStoreDrop}
					isStoreDropOpen={this.props.isStoreDropOpen}
				/>
				<section className="customerHeader__billPay">
					<BillPayLink billPay={this.props.billPay} />
				</section>
				{/* Account Access
				TODO: disabling for release 1
				{this.props.loginLabel && (
					<div className="customerHeader__login show-for-medium">
						<a href={this.props.loginUrl}>{this.props.loginLabel}</a>
					</div>
				)}
			  {
				this.props.title &&
					this.props.links && (
						<AccountLoginButton
							logoutUrl={this.props.logoutUrl}
							title={this.props.title}
							links={this.props.links}
						/>
					)
				*/}
			</nav>
		);
	}
}

TopBar.propTypes = {
	title: PropTypes.string,
	links: PropTypes.array,
	loginLabel: PropTypes.string,
	loginUrl: PropTypes.string,
	logoutLabel: PropTypes.string,
	logoutUrl: PropTypes.string,
	storeData: PropTypes.shape(StoreDetailProps),
	storeSearchForm: PropTypes.shape(StoreSearchFormProps),
	logo: PropTypes.object,
	pinIconURL: PropTypes.string,
	billPay: PropTypes.object,
	hamburgerIconURL: PropTypes.string,
	closeIconRedURL: PropTypes.string,
	toggleMobileNav: PropTypes.func,
	toggleStoreDrop: PropTypes.func,
	isStoreDropOpen: PropTypes.bool,
	isTopBarVisible: PropTypes.bool,
	isMobileNavOpen: PropTypes.bool,
	myStore: PropTypes.string
};

export default TopBar;
