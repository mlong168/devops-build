import React from "react";
import { VehicleSelectorProps } from "util/Props";
import LicenseFlow from "components/LicenseFlow";
import VehicleFlow from "components/VehicleFlow";
import TireFlow from "components/TireSizeFlow";
import WheelFlow from "components/WheelFlow";
import { sanitize } from "util/helpers";
import MediaQuery from "react-responsive";
import config from "config";

class VehicleSelector extends React.Component {
	constructor(props) {
		super(props);

		this.handleCategoryClick = this.handleCategoryClick.bind(this);
		this.handleBack = this.handleBack.bind(this);

		this.state = {
			vehicles: props.vehicles || [],
			activeFlow: this.props.activeFlow || null
		};
	}
	renderFlow(id) {
		let data = this.props.items.find(el => el.id === id);
		switch (id) {
			case "byLicense":
				return <LicenseFlow {...data} apiUrls={this.props.apiUrls} />;
			case "byVehicleType":
				return (
					<VehicleFlow
						{...data}
						apiUrls={this.props.apiUrls}
						addVehicle={this.props.addVehicle}
						logEvent={this.props.logEvent}
						isCompact={this.props.isCompact}
						setCategory={this.handleCategoryClick}
						boltpatterns={this.props.boltpatterns}
						toggleDropdown={this.props.toggleDropdown}
						staggeredMessage={this.props.staggeredMessage}
						storeData={this.props.storeData}
					/>
				);
			case "byTire":
				return (
					<TireFlow
						{...data}
						apiUrls={this.props.apiUrls}
						addTires={this.props.addTires}
						logEvent={this.props.logEvent}
						isCompact={this.props.isCompact}
					/>
				);
			case "byWheel":
				return (
					<WheelFlow
						{...data}
						apiUrls={this.props.apiUrls}
						addWheels={this.props.addWheels}
						logEvent={this.props.logEvent}
						isCompact={this.props.isCompact}
					/>
				);
			default:
				return;
		}
	}
	handleCategoryClick(id) {
		this.setState({ activeFlow: id });
		this.props.togglePartial(true);
	}
	handleBack() {
		this.setState({ activeFlow: null });
		this.props.togglePartial(false);
	}
	render() {
		let body = "";
		//if (!this.state.vehicles || this.state.vehicles.length < 1 ) {
		switch (this.state.activeFlow) {
			case "byLicense":
			case "byVehicleType":
			case "byTire":
			case "byWheel":
				body = this.renderFlow(this.state.activeFlow);
				break;
			default:
				//no flow is initiated yet
				body = (
					<section>
						<h6>{this.props.title}</h6>
						<p>{this.props.description}</p>
						<ul>
							{this.props.items.map((item, i) => (
								<li key={i} className={item.id}>
									<a
										className="button button--vehicleSelector"
										onClick={() => this.handleCategoryClick(item.id)}
										dangerouslySetInnerHTML={sanitize(item.label)}
									/>
								</li>
							))}
						</ul>
						<a href={this.props.viewAllUrl}>{this.props.viewAllLabel}</a>
					</section>
				);
		}
		// } else {
		// 	//vehicle info is available
		// }
		let classes = this.state.activeFlow
			? "cell small-12"
			: "cell small-12 medium-5 leftColumn";
		return (
			<div className={classes}>
				<MediaQuery minWidth={config.breakpoints.maxMobile}>
					{matches => {
						if (matches && this.state.activeFlow && !this.props.isCompact) {
							return (
								<header className="selector__header">
									<h6>
										<a onClick={this.handleBack} className="backLink">
											Back
										</a>
										Find The Right {this.props.parentCategory}
									</h6>
								</header>
							);
						} else {
							return null;
						}
					}}
				</MediaQuery>
				{body}
			</div>
		);
	}
}

VehicleSelector.propTypes = VehicleSelectorProps;
VehicleSelector.defaultProps = {
	togglePartial: () => {},
	isCompact: false
};
export default VehicleSelector;
