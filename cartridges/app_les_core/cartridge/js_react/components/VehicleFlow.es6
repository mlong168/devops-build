/**
	Component for vehicle selection by vehicle type flow.
*/
import React from "react";
import PropTypes from "prop-types";
import SelectorFlow from "components/SelectorFlow";
import VehicleSelectorGrid from "components/VehicleSelectorGrid";
import VehicleSelectorBreadCrumbs from "components/VehicleSelectorBreadCrumbs";
import StoreDetails from "containers/StoreDetails";
import MediaQuery from "react-responsive";
import config from "config";

//TODO: remove storeData from all vehicle selector related components once staggered fitments are implemented

class VehicleFlow extends SelectorFlow {
	constructor(props) {
		super(props);
		//type is used as url parameter to identify for a BE type of flow
		this.type =
			this.props.category === "wheels" ? "wheelVehicleFlow" : "tireVehicleFlow";

		this.breadcrumbs = [];
		this.selectYear = this.selectYear.bind(this);
		this.selectMake = this.selectMake.bind(this);
		this.selectModel = this.selectModel.bind(this);
		this.selectTrim = this.selectTrim.bind(this);
		this.selectFitment = this.selectFitment.bind(this);
		this.handleBreadcrumbClick = this.handleBreadcrumbClick.bind(this);
		this.handleShopCTA = this.handleShopCTA.bind(this);

		this.state = {
			isShowRightColumn: false,
			//indicates staggered fitment
			isStaggered: false,
			//indexes of selected items
			selectedYear: "",
			selectedMake: "",
			selectedModel: "",
			selectedTrim: "",
			selectedFitment: "",
			//items fetched from APIs - full objects
			years: [],
			makes: [],
			models: [],
			trims: [],
			fitments: [],
			//Boltpatterns prefetched from service
			boltpatterns: props.boltpatterns || null,
			wheelData: null
		};
	}
	handleBreadcrumbClick(index) {
		switch (index) {
			case 0:
				this.setState({
					makes: [],
					models: [],
					trims: [],
					fitments: [],
					selectedFitment: "",
					selectedMake: "",
					selectedModel: "",
					selectedTrim: "",
					selectedYear: "",
					isShowRightColumn: false,
					isStaggered: false
				});
				this.breadcrumbs.length = 0;
				this.breadcrumbs.push("Select Year");
				break;
			case 1:
				//keep year info
				this.setState({
					models: [],
					trims: [],
					fitments: [],
					selectedFitment: "",
					selectedMake: "",
					selectedModel: "",
					selectedTrim: "",
					isStaggered: false,
					isShowRightColumn: false
				});
				this.breadcrumbs.length = 1;
				this.breadcrumbs.push("Select Model");
				break;
			case 2:
				//keep year + make info
				this.setState({
					trims: [],
					fitments: [],
					selectedFitment: "",
					selectedModel: "",
					selectedTrim: "",
					isStaggered: false,
					isShowRightColumn: false
				});
				this.breadcrumbs.length = 2;
				this.breadcrumbs.push("Select Make");
				break;
			case 3:
				//keep year + make + model info
				this.setState({
					fitments: [],
					selectedFitment: "",
					selectedTrim: "",
					isStaggered: false
				});
				this.breadcrumbs.length = 3;
				this.breadcrumbs.push("Select Trim");
				break;
			default:
				//same as case 0
				this.setState({
					makes: [],
					models: [],
					trims: [],
					fitments: [],
					selectedFitment: "",
					selectedMake: "",
					selectedModel: "",
					selectedTrim: "",
					selectedYear: "",
					isStaggered: false
				});
		}
	}
	handleShopCTA(event) {
		if (event && event.preventDefault) {
			event.preventDefault();
		}

		let data;
		if (this.props.category === "wheels") {
			data = this.state.wheelData;
		} else {
			let { Front: front, Rear: rear } = this.state.fitments[
				this.state.selectedFitment
			];
			data = {
				width:
					front.Width + (rear.Width !== front.Width ? `|${rear.Width}` : ""),
				rimSize:
					front.InsideDiameter +
					(rear.InsideDiameter !== front.InsideDiameter
						? `|${rear.InsideDiameter}`
						: ""),
				aspectRatio:
					front.AspectRatio +
					(rear.AspectRatio !== front.AspectRatio
						? `|${rear.AspectRatio}`
						: ""),
				loadIndex: {
					min: Math.min(
						front.LoadIndex,
						rear.LoadIndex !== front.LoadIndex
							? rear.LoadIndex
							: front.LoadIndex
					),
					max: config.maxLoadIndex
				}
				/*speedRating:
					front.SpeedRating +
					(rear.SpeedRating !== front.SpeedRating ? `|${rear.SpeedRating}` : "")*/
			};
		}
		this.props
			.addVehicle({
				categoryId: this.state.trims[this.state.selectedTrim].ConfigurationID,
				year: this.state.selectedYear,
				make: this.state.makes[this.state.selectedMake].VehicleMakeName,
				model: this.state.models[this.state.selectedModel].VehicleModelName,
				trim: this.state.trims[this.state.selectedTrim].TrimName,
				color: config.vehicleDefaultColor
			})
			.then(() => {
				//redirect once posting is done
				this.props.toggleDropdown();
				window.location = this.populateShopUrl(data);
			});
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Vehicle (" + this.props.category + ")",
			"Shop CTA clicked",
			data
		);
	}
	selectYear(event) {
		let yearValue =
			event.target.getAttribute("data-value") || event.target.value;
		this.setState(
			{
				selectedYear: yearValue,
				selectedMake: "",
				selectedModel: "",
				selectedTrim: "",
				selectedFitment: ""
			},
			() => {
				this.breadcrumbs.pop(); //remove previous Select text
				this.breadcrumbs.push(this.state.selectedYear);
				this.breadcrumbs.push("Select Make");
				this.fetchFromApi(
					"GetMakes",
					json => {
						this.setState({
							isFetching: false,
							selectedModel: "",
							selectedMake: "",
							selectedTrim: "",
							makes: json.Makes,
							models: [],
							trims: [],
							fitments: []
						});
					},
					{ Year: yearValue }
				);
			}
		);
		//analytics logging
		this.props.logEvent("Vehicle Entry By Vehicle", "Year selected", yearValue);
	}
	selectMake(event) {
		var selectedMakeNumber;
		if (!isNaN(parseInt(event.target.getAttribute("data-index"), 10))) {
			selectedMakeNumber = parseInt(
				event.target.getAttribute("data-index"),
				10
			);
		} else {
			selectedMakeNumber = parseInt(event.target.value, 10);
		}
		this.setState(
			{
				selectedMake: selectedMakeNumber,
				selectedModel: "",
				selectedTrim: "",
				selectedFitment: ""
			},
			() => {
				this.breadcrumbs.pop(); //remove previous Select text
				this.breadcrumbs.push(
					this.state.makes[this.state.selectedMake].VehicleMakeName
				);
				this.breadcrumbs.push("Select Model");
				this.fetchFromApi(
					"GetModels",
					json => {
						this.setState({
							isFetching: false,
							selectedModel: "",
							selectedTrim: "",
							models: json.Models,
							trims: [],
							fitments: []
						});
					},
					{
						VehicleMake: this.state.makes[this.state.selectedMake]
							.VehicleMakeID,
						Year: this.state.selectedYear
					}
				);
			}
		);
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Vehicle",
			"Make selected",
			this.state.makes[selectedMakeNumber].VehicleMakeName
		);
	}
	selectModel(event) {
		var selectedModelNumber;
		if (!isNaN(parseInt(event.target.getAttribute("data-index"), 10))) {
			selectedModelNumber = parseInt(
				event.target.getAttribute("data-index"),
				10
			);
		} else {
			selectedModelNumber = parseInt(event.target.value, 10);
		}
		this.setState(
			{
				selectedModel: selectedModelNumber,
				selectedTrim: "",
				selectedFitment: "",
				isShowRightColumn: true
			},
			() => {
				this.breadcrumbs.pop(); //remove previous Select text
				this.breadcrumbs.push(
					this.state.models[this.state.selectedModel].VehicleModelName
				);
				this.breadcrumbs.push("Select Trim");
				this.fetchFromApi(
					"GetDescriptions",
					json => {
						this.setState({
							isFetching: false,
							selectedTrim: "",
							selectedFitment: "",
							trims: json.Descriptions,
							fitments: []
						});
					},
					{
						VehicleModel: this.state.models[this.state.selectedModel]
							.VehicleModelID,
						Year: this.state.selectedYear
					}
				);
			}
		);
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Vehicle",
			"Model selected",
			this.state.models[selectedModelNumber].VehicleModelName
		);
	}
	selectTrim(event) {
		var selectedTrimNumber;
		if (!isNaN(parseInt(event.target.getAttribute("data-index"), 10))) {
			selectedTrimNumber = parseInt(
				event.target.getAttribute("data-index"),
				10
			);
		} else {
			selectedTrimNumber = parseInt(event.target.value, 10);
		}
		//Last step of selection is different for wheels and tires
		if (this.props.category === "wheels") {
			this.setState(
				{
					selectedTrim: selectedTrimNumber,
					selectedFitment: ""
				},
				() => {
					this.fetchFromApi(
						"GetFitments",
						json => {
							let fitments = json.Fitments,
								diameterMin = fitments[0].VehicleFitmentDiameterMin,
								diameterMax = fitments[0].VehicleFitmentDiameterMax,
								offsetMin = fitments[0].VehicleFitmentOffsetMin,
								offsetMax = fitments[0].VehicleFitmentOffsetMax,
								widthMin = fitments[0].VehicleFitmentWidthMin,
								widthMax = fitments[0].VehicleFitmentWidthMax,
								boltpattern = null;
							fitments.forEach(fitment => {
								diameterMin = Math.max(
									diameterMin,
									fitment.VehicleFitmentDiameterMin
								);
								diameterMax = Math.min(
									diameterMax,
									fitment.VehicleFitmentDiameterMax
								);
								offsetMin = Math.max(
									offsetMin,
									fitment.VehicleFitmentOffsetMin
								);
								offsetMax = Math.min(
									offsetMax,
									fitment.VehicleFitmentOffsetMax
								);
								widthMin = Math.max(widthMin, fitment.VehicleFitmentWidthMin);
								widthMax = Math.min(widthMax, fitment.VehicleFitmentWidthMax);
								//find bolt pattern description in the prefetched data - all fitments for a vehicle should have the same boltpattern
								if (!boltpattern) {
									boltpattern = this.state.boltpatterns.find(
										pattern =>
											pattern.BoltPatternID ===
											fitment.VehicleFitment_BoltPatternID
									);
								}
							});

							this.setState({
								isFetching: false,
								fitments: json.Fitments,
								selectedFitment: 0,
								wheelData: {
									boltPattern: `${boltpattern.BoltPatternBoltCount}-${
										boltpattern.BoltPatternSpacingMM
									}|${boltpattern.BoltPatternBoltCount}-${
										boltpattern.BoltPatternSpacingIN
									}`,
									wheelWidth: {
										min: widthMin,
										max: widthMax
									},
									diameter: {
										min: diameterMin,
										max: diameterMax
									},
									offset: { min: offsetMin, max: offsetMax }
								}
							});
						},
						{
							VehicleConfiguration: this.state.trims[this.state.selectedTrim]
								.ConfigurationID
						}
					);
				}
			);
		} else {
			this.setState(
				{
					selectedTrim: selectedTrimNumber,
					isShowRightColumn: true
				},
				() => {
					this.breadcrumbs.pop(); //remove previous Select text
					this.breadcrumbs.push(
						this.state.trims[this.state.selectedTrim].TrimName
					);
					this.breadcrumbs.push("Select Tire Size");
					this.fetchFromApi(
						"GetTireOptionDetails",
						json => {
							let { Front, Rear } = json.Details[0];
							this.setState({
								isStaggered: Front.Description !== Rear.Description,
								isFetching: false,
								selectedFitment: "",
								fitments: json.Details
							});
						},
						{
							VehicleConfiguration: this.state.trims[this.state.selectedTrim]
								.ConfigurationID
						}
					);
				}
			);
		}
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Vehicle",
			"Trim selected",
			this.state.trims[selectedTrimNumber].TrimDescription
		);
	}
	selectFitment(event) {
		var selectedFitmentNumber;
		if (!isNaN(parseInt(event.target.getAttribute("data-index"), 10))) {
			selectedFitmentNumber = parseInt(
				event.target.getAttribute("data-index"),
				10
			);
		} else {
			selectedFitmentNumber = parseInt(event.target.value, 10);
		}
		this.setState({
			selectedFitment: selectedFitmentNumber
		});
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Vehicle",
			"Fitment selected",
			event.target.textContent
			//this.state.fitments[selectedFitmentNumber].ConfigurationID
		);
	}

	componentDidMount() {
		this.fetchFromApi("GetYears", json => {
			this.setState({ isFetching: false, years: json.Years });
		});
	}
	render() {
		if (this.state.error) {
			return (
				<div className="error">
					<h6>{config.fetchErrorHeading}</h6>
					<p data-error={this.state.error}>{config.fetchErrorDetail}</p>
				</div>
			);
		}
		let dataTitleToGrid, dataValuesToGrid, selectMethodToGrid, isLastStep;
		let titles = [];
		let values = [];

		if (this.state.selectedYear === "") {
			titles = this.state.years;
			values = this.state.years;
			selectMethodToGrid = this.selectYear;
			isLastStep = false;
			this.breadcrumbs = ["Select Year"];
		} else if (this.state.selectedMake === "") {
			for (let i = 0; i < this.state.makes.length; i++) {
				titles.push(this.state.makes[i].VehicleMakeName);
				values.push(this.state.makes[i].VehicleMakeID);
			}
			selectMethodToGrid = this.selectMake;
		} else if (this.state.selectedModel === "") {
			for (let i = 0; i < this.state.models.length; i++) {
				titles.push(this.state.models[i].VehicleModelName);
				values.push(this.state.models[i].VehicleModelID);
			}
			selectMethodToGrid = this.selectModel;
		} else if (
			this.state.selectedTrim === "" ||
			(this.props.category === "wheels" && this.state.selectedTrim !== "")
		) {
			for (let i = 0; i < this.state.trims.length; i++) {
				titles.push(this.state.trims[i].TrimDescription);
				values.push(this.state.trims[i].ConfigurationID);
			}
			selectMethodToGrid = this.selectTrim;
			if (this.props.category === "wheels") {
				isLastStep = true;
			}
		} else {
			if (this.props.category !== "wheels") {
				for (let i = 0; i < this.state.fitments.length; i++) {
					let description = this.state.fitments[i].Front.Description;
					if (
						this.state.fitments[i].Front.Description !==
						this.state.fitments[i].Rear.Description
					) {
						description =
							"Front: " +
							this.state.fitments[i].Front.Description +
							" Rear: " +
							this.state.fitments[i].Rear.Description;
					}
					titles.push(description);
					values.push(this.state.fitments[i].TireOptionID);
				}
				selectMethodToGrid = this.selectFitment;
				isLastStep = true;
			}
		}
		dataTitleToGrid = titles;
		dataValuesToGrid = values;
		return (
			<MediaQuery maxWidth={config.breakpoints.maxMobile}>
				{matches => {
					if (matches || this.props.isCompact) {
						return (
							<section>
								<label htmlFor="yearSelect" className="show-for-sr">
									{this.props.yearPlaceholder}
								</label>
								<select
									value={this.state.selectedYear}
									onChange={this.selectYear.bind(this)}
									id="yearSelect"
								>
									<option disabled value="">
										{this.props.yearPlaceholder}
									</option>
									{this.state.years.map((item, i) => (
										<option key={i} value={item}>
											{item}
										</option>
									))}
								</select>
								<label htmlFor="makeSelect" className="show-for-sr">
									{this.props.makePlaceholder}
								</label>
								<select
									disabled={!this.state.makes.length}
									value={this.state.selectedMake}
									onChange={this.selectMake}
									id="makeSelect"
								>
									<option disabled value="">
										{this.props.makePlaceholder}
									</option>
									{this.state.makes.map((item, i) => (
										<option key={item.VehicleMakeID} value={i}>
											{item.VehicleMakeName}
										</option>
									))}
								</select>
								<label htmlFor="modelSelect" className="show-for-sr">
									{this.props.modelPlaceholder}
								</label>
								<select
									disabled={!this.state.models.length}
									value={this.state.selectedModel}
									onChange={this.selectModel}
									id="modelSelect"
								>
									<option disabled value="">
										{this.props.modelPlaceholder}
									</option>
									{this.state.models.map((item, i) => (
										<option key={item.VehicleModelID} value={i}>
											{item.VehicleModelName}
										</option>
									))}
								</select>
								<label htmlFor="trimSelect" className="show-for-sr">
									{this.props.trimPlaceholder}
								</label>
								<select
									disabled={!this.state.trims.length}
									value={this.state.selectedTrim}
									onChange={this.selectTrim}
									id="trimSelect"
								>
									<option disabled value="">
										{this.props.trimPlaceholder}
									</option>
									{this.state.trims.map((item, i) => (
										<option key={item.ConfigurationID} value={i}>
											{item.TrimDescription}
										</option>
									))}
								</select>
								{this.state.isStaggered && (
									<div className="staggered">
										<h5>{this.props.staggeredMessage}</h5>
										{this.props.storeData && (
											<StoreDetails
												{...this.props.storeData}
												noMap={true}
												type="tiny"
											/>
										)}
									</div>
								)}
								{this.state.selectedTrim !== "" &&
									this.props.category !== "wheels" &&
									!this.state.isStaggered && (
										<section>
											<select
												disabled={!this.state.fitments.length}
												value={this.state.selectedFitment}
												onChange={this.selectFitment.bind(this)}
											>
												<option disabled value="">
													{this.props.tireSizePlaceholder}
												</option>
												{this.state.fitments.map((item, i) => (
													<option key={item.TireOptionID} value={i}>
														{item.Front.Description !== item.Rear.Description &&
															`Front: ${item.Front.Description} Rear:
															${item.Rear.Description}`}
														{item.Front.Description === item.Rear.Description &&
															`${item.Front.Description}`}
													</option>
												))}
											</select>
											<a
												className="vehicleSelectorMessage"
												onClick={this.handleHintClick}
											>
												{this.props.tireSizeHelpTipLabel}
											</a>
											{this.state.showHint && (
												<div>
													<h6>{this.props.tireSizeHelpTipTitle}</h6>
													<img
														src={this.props.tireSizeHelpTipImageSrc}
														srcSet={this.props.tireSizeHelpTipImageSrcSet}
														sizes="250px"
														alt={this.props.tireSizeHelpTipAlt}
														className="vehicleSelectorHintImage"
													/>
												</div>
											)}
										</section>
									)}
								{!this.state.isStaggered && (
									<a
										className="button"
										onClick={this.handleShopCTA}
										disabled={this.state.selectedFitment === ""}
									>
										{this.props.isCompact
											? this.props.saveLabel
											: this.props.shopLabel}
									</a>
								)}
								<section className="vehicleSelectorGridMessage">
									If you modified your vehicle to fit a non-standard{" "}
									{this.props.category === "wheels" ? "wheel" : "tire"} size,{" "}
									<a
										key={0}
										onClick={() =>
											this.props.setCategory(
												this.props.category === "wheels" ? "byWheel" : "byTire"
											)
										}
									>
										set {this.props.category === "wheels" ? "wheel" : "tire"}{" "}
										size
									</a>, not vehicle type.
								</section>
							</section>
						);
					} else {
						let selector = (
							<VehicleSelectorGrid
								gridDataTitles={dataTitleToGrid}
								gridDataValues={dataValuesToGrid}
								selectDataMethod={selectMethodToGrid}
								isLastStep={isLastStep}
								handleShopCTA={this.handleShopCTA}
								shopLabel={this.props.shopLabel}
								isSubmitDisabled={this.state.selectedFitment === ""}
								isShowRightColumn={this.state.isShowRightColumn}
								tireSizeHelpTipTitle={this.props.tireSizeHelpTipTitle}
								tireSizeHelpTipImageSrc={this.props.tireSizeHelpTipImageSrc}
								tireSizeHelpTipImageSrcSet={
									this.props.tireSizeHelpTipImageSrcSet
								}
								tireSizeHelpTipAlt={this.props.tireSizeHelpTipAlt}
								belowMessage={[
									`If you modified your vehicle to fit a non-standard ${
										this.props.category === "wheels" ? "wheel" : "tire"
									} size, `,
									<a
										key={0}
										onClick={() =>
											this.props.setCategory(
												this.props.category === "wheels" ? "byWheel" : "byTire"
											)
										}
									>
										set {this.props.category === "wheels" ? "wheel" : "tire"}{" "}
										size
									</a>,
									" not vehicle type."
								]}
							/>
						);
						if (this.state.isStaggered) {
							selector = (
								<div className="staggered">
									<h5>{this.props.staggeredMessage}</h5>
									{this.props.storeData && (
										<StoreDetails
											{...this.props.storeData}
											noMap={true}
											type="tiny"
										/>
									)}
								</div>
							);
						}
						return (
							<section>
								<section className="titleAndCrumbs">
									<h6>{this.props.title}</h6>
									<VehicleSelectorBreadCrumbs
										breadcrumbs={this.breadcrumbs}
										handleBreadcrumbClick={this.handleBreadcrumbClick}
									/>
								</section>
								{selector}
							</section>
						);
					}
				}}
			</MediaQuery>
		);
	}
}
VehicleFlow.defaultProps = {
	toggleDropdown: () => {}
};
VehicleFlow.propTypes = {
	addVehicle: PropTypes.func,
	apiUrls: PropTypes.object,
	id: PropTypes.string,
	title: PropTypes.string,
	staggeredMessage: PropTypes.string,
	yearPlaceholder: PropTypes.string,
	makePlaceholder: PropTypes.string,
	modelPlaceholder: PropTypes.string,
	trimPlaceholder: PropTypes.string,
	tireSizePlaceholder: PropTypes.string,
	shopLabel: PropTypes.string,
	shopUrl: PropTypes.string,
	tireSizeHelpTipLabel: PropTypes.string,
	tireSizeHelpTipTitle: PropTypes.string,
	tireSizeHelpTipAlt: PropTypes.string,
	tireSizeHelpTipImageSrc: PropTypes.string,
	tireSizeHelpTipImageSrcSet: PropTypes.string,
	setCategory: PropTypes.func,
	logEvent: PropTypes.func,
	isCompact: PropTypes.bool,
	boltpatterns: PropTypes.array,
	toggleDropdown: PropTypes.func,
	storeData: PropTypes.object
};
export default VehicleFlow;
