/**
	Component for vehicle selection by tire size.
*/
import React from "react";
import PropTypes from "prop-types";
import SelectorFlow from "components/SelectorFlow";
import VehicleSelectorGrid from "components/VehicleSelectorGrid";
import VehicleSelectorBreadCrumbs from "components/VehicleSelectorBreadCrumbs";
import classNames from "classnames";
import MediaQuery from "react-responsive";
import config from "config";

class TireSizeFlow extends SelectorFlow {
	constructor(props) {
		super(props);

		this.type = "tireSizeFlow";

		this.breadcrumbs = [];
		this.createBreadCrumbs = this.createBreadCrumbs.bind(this);
		this.lastBreadcrumbsFront = [];
		this.lastBreadcrumbsRear = [];
		this.selectWidth = this.selectWidth.bind(this);
		this.selectRatio = this.selectRatio.bind(this);
		this.selectRim = this.selectRim.bind(this);
		this.toggleRearTire = this.toggleRearTire.bind(this);
		this.toggleFrontButton = this.toggleFrontButton.bind(this);
		this.toggleRearButton = this.toggleRearButton.bind(this);
		this.renderFiledset = this.renderFieldset.bind(this);
		this.handleShopCTA = this.handleShopCTA.bind(this);
		this.handleBreadcrumbClick = this.handleBreadcrumbClick.bind(this);

		this.state = {
			isRearDifferent: false, //used for mobile, and link button to show/hide rear fields
			isShowingRear: false, //used soley for desktop, showing/hiding rear fields
			selectedWidth: "",
			selectedRatio: "",
			selectedRim: "",
			selectedRearWidth: "",
			selectedRearRatio: "",
			selectedRearRim: "",
			widths: this.props.widths ? this.props.widths : [],
			ratios: this.props.ratios || [],
			rearRatios: this.props.ratios || [],
			rims: this.props.rims || [],
			rearRims: this.props.rims || []
		};
	}
	handleBreadcrumbClick(index) {
		switch (index) {
			case 0:
				this.setState({
					ratios: [],
					trims: [],
					rims: [],
					selectedWidth: "",
					selectedRatio: "",
					selectedTrim: ""
				});
				this.breadcrumbs.length = 0;
				this.breadcrumbs.push(this.props.widthPlaceholder);
				break;
			case 1:
				//keep widths info
				this.setState({
					trims: [],
					rims: [],
					selectedTrim: "",
					selectedRatio: "",
					selectedRim: ""
				});
				this.breadcrumbs.length = 1;
				this.breadcrumbs.push(this.props.aspectRatioPlaceholder);
				break;
			default:
				//same as case 0
				this.setState({
					ratios: [],
					trims: [],
					rims: [],
					selectedWidth: "",
					selectedRatio: "",
					selectedTrim: ""
				});
		}
	}
	createBreadCrumbs() {
		if (this.state.isRearDifferent && this.state.isShowingRear) {
			if (
				this.state.selectedRearWidth === "" ||
				this.state.rearRatios === null
			) {
				this.breadcrumbs = [this.props.widthPlaceholder];
			} else if (
				this.state.selectedRearRatio === "" ||
				this.state.rearRims === null
			) {
				this.breadcrumbs = [
					this.state.widths[this.state.selectedRearWidth],
					this.props.aspectRatioPlaceholder
				];
			} else {
				this.breadcrumbs = [
					this.state.widths[this.state.selectedRearWidth],
					this.state.rearRatios[this.state.selectedRearRatio],
					this.state.selectedRearRim !== ""
						? this.state.rearRims[this.state.selectedRearRim]
						: this.props.rimPlaceholder
				];
			}
		} else {
			if (this.state.selectedWidth === "" || this.state.ratios === null) {
				this.breadcrumbs = [this.props.widthPlaceholder];
			} else if (this.state.selectedRatio === "" || this.state.rims === null) {
				this.breadcrumbs = [
					this.state.widths[this.state.selectedWidth],
					this.props.aspectRatioPlaceholder
				];
			} else {
				this.breadcrumbs = [
					this.state.widths[this.state.selectedWidth],
					this.state.ratios[this.state.selectedRatio],
					this.state.selectedRim !== ""
						? this.state.rims[this.state.selectedRim]
						: this.props.rimPlaceholder
				];
			}
		}
	}
	handleShopCTA(event) {
		event.preventDefault();
		let data = {
			width:
				this.state.widths[this.state.selectedWidth] +
				(this.state.isRearDifferent
					? "|" + this.state.widths[this.state.selectedRearWidth]
					: ""),
			rimSize:
				this.state.rims[this.state.selectedRim] +
				(this.state.isRearDifferent
					? "|" + this.state.rearRims[this.state.selectedRearRim]
					: ""),
			aspectRatio:
				this.state.ratios[this.state.selectedRatio] +
				(this.state.isRearDifferent
					? "|" + this.state.rearRatios[this.state.selectedRearRatio]
					: "")
		};
		this.props
			.addTires({
				front: {
					width: this.state.widths[this.state.selectedWidth],
					ratio: this.state.ratios[this.state.selectedRatio],
					rim: this.state.rims[this.state.selectedRim]
				},
				rear: this.state.isRearDifferent
					? {
							width: this.state.widths[this.state.selectedRearWidth],
							ratio: this.state.rearRatios[this.state.selectedRearRatio],
							rim: this.state.rearRims[this.state.selectedRearRim]
					  }
					: null
			})
			.then(() => {
				//redirect once posting is done
				window.location = this.populateShopUrl(data);
			});
		//analytics logging
		this.props.logEvent("Vehicle Entry By Tires", "Shop CTA clicked", data);
	}
	selectWidth(event, isRear) {
		if (isRear === undefined) {
			isRear = this.state.isShowingRear;
		}
		var selectedWidth =
			event.target.value || event.target.getAttribute("data-value");
		this.setState(
			{
				[isRear ? "selectedRearWidth" : "selectedWidth"]: parseInt(
					selectedWidth,
					10
				),
				[isRear ? "selectedRearRatio" : "selectedRatio"]: "",
				[isRear ? "selectedRearRim" : "selectedRim"]: ""
			},
			() => {
				this.fetchFromApi(
					"GetTireAspectRatio",
					json => {
						this.setState({
							isFetching: false,
							[isRear ? "rearRatios" : "ratios"]: Object.is(
								json.aspectratios,
								undefined
							)
								? null
								: json.aspectratios.sort()
						});
					},
					{ width: this.state.widths[selectedWidth] }
				);
			}
		);
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Tires",
			`Width selected (${isRear ? "rear" : "front"})`,
			this.state.widths[selectedWidth]
		);
	}
	selectRatio(event, isRear) {
		if (isRear === undefined) {
			isRear = this.state.isShowingRear;
		}
		var selectedRatioValue =
			event.target.value || event.target.getAttribute("data-value");
		this.setState(
			{
				[isRear ? "selectedRearRatio" : "selectedRatio"]: parseInt(
					selectedRatioValue,
					10
				),
				[isRear ? "selectedRearRim" : "selectedRim"]: ""
			},
			() => {
				this.fetchFromApi(
					"GetTireRimSize",
					json => {
						this.setState({
							isFetching: false,
							[isRear ? "rearRims" : "rims"]: Object.is(
								json.rimsizes,
								undefined
							)
								? null
								: json.rimsizes.sort()
						});
					},
					{
						aspectratio: this.state.ratios[parseInt(selectedRatioValue, 10)]
					}
				);
			}
		);
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Tires",
			`Ratio selected (${isRear ? "rear" : "front"})`,
			this.state.ratios[parseInt(selectedRatioValue, 10)]
		);
	}
	selectRim(event, isRear) {
		if (isRear === undefined) {
			isRear = this.state.isShowingRear;
		}
		var seletedRim =
			event.target.value || event.target.getAttribute("data-value");
		this.setState({
			[isRear ? "selectedRearRim" : "selectedRim"]: parseInt(seletedRim, 10)
		});
		//if rear is different and user has selected front rim(so done with front), switch to rear
		if (this.state.isRearDifferent && !isRear) {
			this.toggleRearButton();
		}
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Tires " + (isRear ? "(rear)" : "(front)"),
			"Rim selected " + (isRear ? "(rear)" : "(front)"),
			{
				[isRear ? "selectedRearRim" : "selectedRim"]: this.state[
					isRear ? "rearRims" : "rims"
				][parseInt(seletedRim, 10)]
			}
		);
	}
	toggleRearTire() {
		//if front details have been populated, switch to rear entry
		if (!this.state.isRearDifferent && this.state.selectedRim !== "") {
			this.toggleRearButton();
		}
		if (this.state.isRearDifferent) {
			this.setState({ isShowingRear: false });
		}
		this.setState({ isRearDifferent: !this.state.isRearDifferent });
		//analytics logging
		this.props.logEvent(
			"Vehicle Entry By Tires",
			"Toggle Rear Tire",
			!this.state.isRearDifferent
		);
	}
	toggleFrontButton() {
		this.lastBreadcrumbsRear = this.breadcrumbs;
		this.setState({ isShowingRear: false });
		this.breadcrumbs = this.lastBreadcrumbsFront;
		//analytics logging
		this.props.logEvent("Vehicle Entry By Tires", "Toggle Rear Tire", false);
	}
	toggleRearButton() {
		this.lastBreadcrumbsFront = this.breadcrumbs;
		this.setState({ isShowingRear: true });
		this.breadcrumbs = this.lastBreadcrumbsRear;
	}
	renderFieldset(isRear) {
		let ratio = this.state[isRear ? "rearRatios" : "ratios"];
		let rims = this.state[isRear ? "rearRims" : "rims"];

		let labels;
		if (isRear) {
			labels = ["widthSelectRear", "ratioSelectRear", "rimSelectRear"];
		} else {
			labels = ["widthSelect", "ratioSelect", "rimSelect"];
		}

		return (
			<fieldset>
				<label htmlFor={labels[0]} className="show-for-sr">
					{this.props.widthPlaceholder}
				</label>
				<select
					value={
						isRear ? this.state.selectedRearWidth : this.state.selectedWidth
					}
					onChange={e => this.selectWidth(e, isRear)}
					id={labels[0]}
				>
					<option disabled value="">
						{this.props.widthPlaceholder}
					</option>
					{this.state.widths.map((item, i) => (
						<option key={i} value={i}>
							{item}
						</option>
					))}
				</select>
				<label htmlFor={labels[1]} className="show-for-sr">
					{this.props.aspectRatioPlaceholder}
				</label>
				<select
					disabled={!ratio || !ratio.length}
					value={
						isRear ? this.state.selectedRearRatio : this.state.selectedRatio
					}
					onChange={e => this.selectRatio(e, isRear)}
					id={labels[1]}
				>
					<option disabled value="">
						{this.props.aspectRatioPlaceholder}
					</option>
					{ratio &&
						ratio.map((item, i) => (
							<option key={i} value={i}>
								{item}
							</option>
						))}
				</select>
				<label htmlFor={labels[2]} className="show-for-sr">
					{this.props.rimPlaceholder}
				</label>
				<select
					disabled={!rims || !rims.length}
					value={isRear ? this.state.selectedRearRim : this.state.selectedRim}
					onChange={e => this.selectRim(e, isRear)}
					id={labels[2]}
				>
					<option disabled value="">
						{this.props.rimPlaceholder}
					</option>
					{rims &&
						rims.map((item, i) => (
							<option key={i} value={i}>
								{item}
							</option>
						))}
				</select>
			</fieldset>
		);
	}
	render() {
		let dataTitleToGrid,
			dataValuesToGrid,
			selectMethodToGrid,
			isLastStep,
			showError = false;
		let titles = [];
		let values = [];
		if (this.state.isRearDifferent && this.state.isShowingRear) {
			if (
				this.state.selectedRearWidth === "" ||
				this.state.rearRatios === null
			) {
				titles = this.state.widths;
				selectMethodToGrid = this.selectWidth;
				isLastStep = false;

				if (this.state.rearRatios === null) {
					showError = true;
				}
			} else if (
				this.state.selectedRearRatio === "" ||
				this.state.rearRims === null
			) {
				titles = this.state.rearRatios;
				selectMethodToGrid = this.selectRatio;
				isLastStep = false;
				if (this.state.rearRims === null) {
					showError = true;
				}
			} else {
				titles = this.state.rearRims;
				selectMethodToGrid = this.selectRim;
				isLastStep = true;
			}
		} else {
			if (this.state.selectedWidth === "" || this.state.ratios === null) {
				titles = this.state.widths;
				selectMethodToGrid = this.selectWidth;
				isLastStep = false;

				if (this.state.ratios === null) {
					showError = true;
				}
			} else if (this.state.selectedRatio === "" || this.state.rims === null) {
				titles = this.state.ratios;
				selectMethodToGrid = this.selectRatio;
				isLastStep = false;

				if (this.state.rims === null) {
					showError = true;
				}
			} else {
				titles = this.state.rims;
				selectMethodToGrid = this.selectRim;
				isLastStep = true;
			}
		}
		for (let i = 0; i < titles.length; i++) {
			values.push(i);
		}
		this.createBreadCrumbs(); //form the breadcrumbs
		dataTitleToGrid = titles;
		dataValuesToGrid = values;
		return (
			<MediaQuery maxWidth={config.breakpoints.maxMobile}>
				{matches => {
					if (matches || this.props.isCompact) {
						return (
							<section className="selector--mobile">
								<h4>{this.props.title}</h4>
								<p className="selector__helpLabel">
									<a
										className="vehicleSelectorMessage"
										onClick={this.handleHintClick}
									>
										{this.props.tireSizeHelpTipLabel}
									</a>
								</p>
								{this.state.showHint && (
									<figure>
										<h6>{this.props.tireSizeHelpTipTitle}</h6>
										<img
											src={this.props.tireSizeHelpTipImageSrc}
											srcSet={this.props.tireSizeHelpTipImageSrcSet}
											sizes="250px"
											alt={this.props.tireSizeHelpTipAlt}
											className="vehicleSelectorHintImage"
										/>
									</figure>
								)}
								{this.state.isRearDifferent && (
									<h5>{this.props.frontTiresSubtitle}</h5>
								)}
								{this.renderFieldset(false)}
								{this.state.isRearDifferent && (
									<div>
										<h5>{this.props.rearTiresSubtitle}</h5>
										{this.renderFieldset(true)}
									</div>
								)}
								{showError && (
									<span className="VSErrorMessage">
										The selection you have made has returned no results. Please
										try a different option.
									</span>
								)}
								<p>
									<a
										className="vehicleSelectorMessage"
										onClick={this.toggleRearTire}
									>
										{this.state.isRearDifferent
											? this.props.removeDifferentRearTireSizeLabel
											: this.props.addDifferentRearTireSizeLabel}
									</a>
								</p>
								<a
									className="button"
									onClick={this.handleShopCTA}
									disabled={
										this.state.selectedRim === "" ||
										(this.state.selectedRearRim === "" &&
											this.state.isRearDifferent)
									}
								>
									{this.props.isCompact
										? this.props.saveLabel
										: this.props.shopLabel}
								</a>
							</section>
						);
					} else {
						return (
							<section>
								<section className="titleAndCrumbs">
									{this.state.isRearDifferent && (
										<div className="VSToggleButton__wrapper">
											<a
												className={classNames("VSToggleButton", {
													VSToggleButton__isActive: !this.state.isShowingRear
												})}
												onClick={this.toggleFrontButton}
											>
												Front
											</a>
											<a
												className={classNames("VSToggleButton", {
													VSToggleButton__isActive: this.state.isShowingRear
												})}
												onClick={this.toggleRearButton}
											>
												Rear
											</a>
										</div>
									)}
									<h6>{this.props.title}</h6>
									<VehicleSelectorBreadCrumbs
										breadcrumbs={this.breadcrumbs}
										handleBreadcrumbClick={this.handleBreadcrumbClick}
									/>
								</section>
								{this.state.isRearDifferent && (
									<p className="rearAxleHelpTip">
										{this.state.isShowingRear
											? this.props.rearEntryLabel
											: this.props.frontEntryLabel}
									</p>
								)}
								<VehicleSelectorGrid
									gridDataTitles={dataTitleToGrid}
									gridDataValues={dataValuesToGrid}
									selectDataMethod={selectMethodToGrid}
									isLastStep={isLastStep}
									showError={showError}
									handleShopCTA={this.handleShopCTA}
									shopLabel={this.props.shopLabel}
									isRearDifferent={this.state.isRearDifferent}
									toggleRear={this.toggleRearTire}
									addRearLabel={this.props.addDifferentRearTireSizeLabel}
									removeRearLabel={this.props.removeDifferentRearTireSizeLabel}
									isShowRightColumn={true}
									tireSizeHelpTipTitle={this.props.tireSizeHelpTipTitle}
									tireSizeHelpTipImageSrc={this.props.tireSizeHelpTipImageSrc}
									tireSizeHelpTipImageSrcSet={
										this.props.tireSizeHelpTipImageSrcSet
									}
									tireSizeHelpTipAlt={this.props.tireSizeHelpTipAlt}
									isSubmitDisabled={
										this.state.selectedRim === "" ||
										(this.state.selectedRearRim === "" &&
											this.state.isRearDifferent)
									}
								/>
							</section>
						);
					}
				}}
			</MediaQuery>
		);
	}
}
TireSizeFlow.propTypes = {
	id: PropTypes.string,
	addTires: PropTypes.func,
	title: PropTypes.string,
	frontTiresSubtitle: PropTypes.string,
	rearTiresSubtitle: PropTypes.string,
	widthPlaceholder: PropTypes.string,
	aspectRatioPlaceholder: PropTypes.string,
	tireSizeHelpTipTitle: PropTypes.string,
	rimPlaceholder: PropTypes.string,
	addDifferentRearTireSizeLabel: PropTypes.string,
	removeDifferentRearTireSizeLabel: PropTypes.string,
	shopLabel: PropTypes.string,
	shopUrl: PropTypes.string,
	widths: PropTypes.array,
	ratios: PropTypes.array,
	rims: PropTypes.array,
	logEvent: PropTypes.func,
	isCompact: PropTypes.bool,
	frontEntryLabel: PropTypes.string,
	rearEntryLabel: PropTypes.string
};
export default TireSizeFlow;
