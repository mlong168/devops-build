import React from "react";
import { VehicleRenderProps } from "util/Props";

class VehicleRender extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			renderingError: false,
			vehicle: this.props.vehicle || this.props.vehicleDetails,
			color:
				this.props.color ||
				(this.props.vehicle
					? this.props.vehicle.color
					: this.props.vehicleDetails
						? this.props.vehicleDetails.color
						: null)
		};

		this.generateSrc = this.generateSrc.bind(this);
		this.handleRenderError = this.handleRenderError.bind(this);
	}
	componentWillReceiveProps(nP) {
		this.setState({
			vehicle: nP.vehicle || nP.vehicleDetails,
			color:
				nP.color || (nP.vehicle ? nP.vehicle.color : nP.vehicleDetails.color),
			renderingError: false
		});
	}
	generateSrc() {
		let str =
			this.props.vehicleRenderApiUrl +
			`&VehicleConfiguration=${
				this.state.vehicle.categoryId
			}&Width=1260&Height=472&PaintColor=%23${this.state.color}`;
		if (this.props.wheelSku) {
			str += `&WheelItemNumber=${this.props.wheelSku}`;
		}
		return str;
	}
	handleRenderError() {
		this.setState({ renderingError: true });
	}
	render() {
		const body = this.state.vehicle &&
			!this.state.renderingError && (
				<img src={this.generateSrc()} onError={this.handleRenderError} />
			);

		return <aside className="vehicleRender">{body}</aside>;
	}
}

VehicleRender.propTypes = VehicleRenderProps;

export default VehicleRender;
