/**
 *	Implementation of a full page overlay that shows up underneath active state of a certain components
 *	@param clickHandler - optional callback for overlay click event
 */
import React from "react";
import PropTypes from "prop-types";

/*
	Transparent black overlay that is shown when the navigation is opended.
*/
const Overlay = props => {
	let style = {
		position: "fixed",
		background: "rgba(0,0,0,0.5)",
		width: "100%",
		height: "100%",
		top: 0,
		left: 0,
		zIndex: props.zindex || 10
	};
	return (
		<div className="overlay" style={style} onClick={props.clickHandler}>
			{props.children}
		</div>
	);
};

Overlay.propTypes = {
	clickHandler: PropTypes.func,
	zindex: PropTypes.number,
	children: PropTypes.object
};

export default Overlay;
