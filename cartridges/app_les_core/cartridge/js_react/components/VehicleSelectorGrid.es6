import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Measure from "react-measure";
import config from "config";

class VehicleSelectorGrid extends React.Component {
	constructor(props) {
		super(props);

		this.handleNextPage = this.handleNextPage.bind(this);
		this.handlePrevPage = this.handlePrevPage.bind(this);
		this.handleClick = this.handleClick.bind(this);

		this.state = {
			currentGridPage: 1, //first page
			gridPageSize: 42, //TODO, make this a prop instead
			noSpaceBetween: false,
			longestTitle: 1,
			containerWidth: config.breakpoints.maxTablet,
			activeIndex: -1
		};
	}
	shouldComponentUpdate(nP) {
		if (
			this.props.gridDataTitles.length === 0 &&
			nP.gridDataTitles.length === 0
		) {
			return false;
		}
		return true;
	}
	handleNextPage() {
		this.setState({
			currentGridPage: this.state.currentGridPage + 1
		});
	}
	handlePrevPage() {
		this.setState({
			currentGridPage: this.state.currentGridPage - 1
		});
	}
	handleClick(e, ind) {
		this.setState({ activeIndex: ind });
		this.props.selectDataMethod(e);
	}
	updateGridPageSize(newProps) {
		//TODO: When the user grows/shrinks the grid, it does not update the currentGridPage
		//We will need to calculate which page number it should show, based on the new number of columns.
		var numOfColumns, longestTitle;

		if (newProps === null) {
			newProps = this.props;
		}

		longestTitle = newProps.gridDataTitles.reduce(
			(length, title) => Math.max(length, String(title).length),
			-1
		);
		if (this.props.gridDataTitles.join() !== newProps.gridDataTitles.join()) {
			this.setState({ activeIndex: -1 });
		}

		numOfColumns = Math.floor(
			this.state.containerWidth / (longestTitle * 10 + 30)
		); //10 per character + 30 padding
		if (numOfColumns === 0) {
			numOfColumns = 1;
		}

		this.setState({
			longestTitle: longestTitle,
			noSpaceBetween: newProps.gridDataTitles.length < 7 * 2 + 1, //7 items per column - checking if there are less than 3 columns
			gridPageSize: numOfColumns * 7 //7 items per column.
		});
	}
	componentWillReceiveProps(newProps) {
		this.setState({
			currentGridPage: 1
		});
		this.updateGridPageSize(newProps);
	}
	componentDidMount() {
		window.addEventListener("resize", this.updateGridPageSize.bind(this, null));
	}
	componentWillUnmount() {
		window.removeEventListener(
			"resize",
			this.updateGridPageSize.bind(this, null)
		);
	}
	render() {
		let firstGridIndex,
			lastGridIndex,
			lastPage,
			gridItems = [];

		if (this.props.gridDataTitles) {
			firstGridIndex =
				(this.state.currentGridPage - 1) * this.state.gridPageSize;
			lastGridIndex = this.state.currentGridPage * this.state.gridPageSize;
			lastPage = Math.ceil(
				this.props.gridDataTitles.length / this.state.gridPageSize
			);

			for (let i = firstGridIndex; i < lastGridIndex; i++) {
				if (this.props.gridDataTitles[i] !== undefined) {
					gridItems.push(
						<li key={i}>
							<a
								data-value={this.props.gridDataValues[i]}
								data-index={i}
								onClick={e => this.handleClick(e, i)}
								className={classNames("gridItem", {
									"gridItem--active": this.state.activeIndex === i
								})}
								// onClick={this.props.selectDataMethod}
							>
								{this.props.gridDataTitles[i]}
							</a>
						</li>
					);
				}
			}
		}
		return (
			<div
				className={classNames("vehicleSelectorGrid", {
					"vehicleSelectorGrid--showPrev": this.state.currentGridPage != 1,
					"vehicleSelectorGrid--rightColumn": this.props.isShowRightColumn,
					"vehicleSelectorGrid--justifyLeft": this.state.noSpaceBetween,
					"vehicleSelectorGrid--fixedHeight":
						this.props.gridDataTitles && this.props.gridDataTitles.length > 6
				})}
			>
				<Measure
					bounds
					onResize={contentRect => {
						this.setState({
							containerWidth: contentRect.bounds.width
						});
					}}
				>
					{({ measureRef }) => <ul ref={measureRef}>{gridItems}</ul>}
				</Measure>
				{this.props.showError && (
					<p className="VSErrorMessage">
						The selection you have made has returned no results. Please try a
						different option.
					</p>
				)}
				{this.props.isLastStep && (
					<a
						className="button"
						onClick={this.props.handleShopCTA}
						disabled={this.props.isSubmitDisabled}
					>
						{this.props.shopLabel}
					</a>
				)}
				{this.state.currentGridPage !== 1 && (
					<button
						className="carousel__button carousel__button--prev"
						onClick={this.handlePrevPage}
					/>
				)}
				{this.state.currentGridPage !== lastPage && (
					<button
						className="carousel__button carousel__button--next"
						onClick={this.handleNextPage}
					/>
				)}
				{this.props.isShowRightColumn && (
					<section className="vehicleSelectorGridImageWrap">
						<h6>{this.props.tireSizeHelpTipTitle}</h6>
						<img
							src={this.props.tireSizeHelpTipImageSrc}
							srcSet={this.props.tireSizeHelpTipImageSrcSet}
							sizes="250px"
							alt={this.props.tireSizeHelpTipAlt}
							className="vehicleSelectorHintImage"
						/>
					</section>
				)}
				{(this.props.removeRearLabel || this.props.addRearLabel) && (
					<a className="vehicleSelectorMessage" onClick={this.props.toggleRear}>
						{this.props.isRearDifferent
							? this.props.removeRearLabel
							: this.props.addRearLabel}
					</a>
				)}
				{this.props.belowMessage && (
					<section className="vehicleSelectorGridMessage">
						{this.props.belowMessage}
					</section>
				)}
			</div>
		);
	}
}

VehicleSelectorGrid.propTypes = {
	gridDataTitles: PropTypes.array,
	gridDataValues: PropTypes.array,
	handleShopCTA: PropTypes.func,
	selectDataMethod: PropTypes.func,
	isLastStep: PropTypes.bool,
	shopLabel: PropTypes.string,
	isSubmitDisabled: PropTypes.bool,
	belowMessage: PropTypes.array,
	isRearDifferent: PropTypes.bool,
	toggleRear: PropTypes.func,
	isShowRightColumn: PropTypes.bool,
	addRearLabel: PropTypes.string,
	removeRearLabel: PropTypes.string,
	tireSizeHelpTipImageSrc: PropTypes.string,
	tireSizeHelpTipImageSrcSet: PropTypes.string,
	tireSizeHelpTipAlt: PropTypes.string,
	tireSizeHelpTipTitle: PropTypes.string,
	showError: PropTypes.bool
};

export default VehicleSelectorGrid;
