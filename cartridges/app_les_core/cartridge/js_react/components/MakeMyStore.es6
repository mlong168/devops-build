import React from "react";
import PropTypes from "prop-types";

/*
	Make my Store button, used to either select the store or show the user
	that this store is "my store"
*/
class MakeMyStore extends React.Component {
	render() {
		return (
			<div className="MakeMyStore">
				{this.props.isMystore && (
					<span className="MakeMyStore--selected">My Store</span>
				)}
				{!this.props.isMystore && (
					<label
						className="checkboxWrapper storeDetails__button button button--tertiary"
						htmlFor={this.props.id}
					>
						<div className="checkboxContainer">
							<input
								className="make-my-store"
								type="checkbox"
								name={this.props.id}
								id={this.props.id}
								checked={this.props.isMystore}
								onChange={this.props.handleMystoreAction}
							/>
							<div className="checkboxContainer__box">
								<span className="show-for-sr">
									{this.props.isMystore
										? this.props.myStoreLabel
										: this.props.makeMyStoreLabel}
								</span>
							</div>
						</div>
						<span>
							{this.props.isMystore
								? this.props.myStoreLabel
								: this.props.makeMyStoreLabel}
						</span>
					</label>
				)}
			</div>
		);
	}
}

MakeMyStore.propTypes = {
	id: PropTypes.string,
	isMystore: PropTypes.bool,
	handleMystoreAction: PropTypes.func,
	myStoreLabel: PropTypes.string,
	makeMyStoreLabel: PropTypes.string
};

export default MakeMyStore;
