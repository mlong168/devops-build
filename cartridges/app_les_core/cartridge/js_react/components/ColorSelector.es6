import React from "react";
import { ColorSelectorProps } from "util/Props";
import config from "config";

/*
	Color Selector that is used on the WheelVisualizer.es6
		isOpen controls the hiding/showing of the panel
		selectedColor is the selected color and alerts parent component.
		
*/
class ColorSelector extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false,
			selectedColor: this.props.selectedColor
		};

		this.toggleOpen = this.toggleOpen.bind(this);
		this.handleSelect = this.handleSelect.bind(this);
	}
	toggleOpen() {
		this.setState({ isOpen: !this.state.isOpen });
	}
	handleSelect(color) {
		if (this.state.selectedColor !== color) {
			this.props.selectColor(color);
		}
		this.setState({ selectedColor: color, isOpen: false });
	}
	render() {
		return (
			<section className="colorSelector">
				{!this.state.isOpen && (
					<button
						className="colorSelector__toggle"
						onClick={this.toggleOpen}
						title="Open color selection"
					/>
				)}
				{this.state.isOpen && (
					<div className="colorSelector__panel">
						<p className="colorSelector__panel__header">
							<strong>{this.props.colorSelectorHeading}</strong>
							<a onClick={this.toggleOpen}>{this.props.colorCancelLabel}</a>
						</p>
						<ul className="colorSelector__panel__options">
							{config.availableColors &&
								config.availableColors.map(color => (
									<li
										key={color}
										className={
											this.state.selectedColor === color ? "selected" : ""
										}
									>
										<a
											className={color === "FFFFFF" && "white"}
											style={{
												backgroundColor: "#" + color
											}}
											onClick={() => this.handleSelect(color)}
										/>
									</li>
								))}
						</ul>
					</div>
				)}
			</section>
		);
	}
}

ColorSelector.propTypes = ColorSelectorProps;

export default ColorSelector;
