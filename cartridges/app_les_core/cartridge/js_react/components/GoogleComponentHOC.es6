import React from "react";
import PropTypes from "prop-types";
import config from "config";
import { staticUrl } from "util/helpers";
import mapMarker from "icons/yellowPin.svg";
import mapMarkerSelected from "icons/mapMarker.svg";

/**
	Higher order component that wraps any given component with google maps API related functionality
*/
const GoogleComponentHOC = WrappedComponent => {
	class GoogleComponentWrapper extends React.Component {
		constructor(props) {
			super(props);

			this.state = {
				google: null,
				geocoder: null,
				map: null,
				latitude:
					this.props.latitude || config.googleMaps.defaultPosition.latitude,
				longitude:
					this.props.longitude || config.googleMaps.defaultPosition.longitude
			};
			this.mapContainer = null;
			this.markers = [];

			this.initializeGoogle = this.initializeGoogle.bind(this);
			this.populateMarkers = this.populateMarkers.bind(this);
		}
		populateMarkers(markers) {
			/*Marker={
				latitude,
				longitude,
				icon,
				label,
				id,
				distance
			}*/
			if (this.state.google && this.state.map) {
				let bounds = new this.state.google.maps.LatLngBounds(),
					centerOn = null;
				this.markers.forEach(markerObj => markerObj.marker.setMap(null));
				this.markers = markers
					? markers.map((marker, ind) => {
							const isSelected = this.props.selectedStore === marker.id;

							let gMarker = new this.state.google.maps.Marker({
								position: new this.state.google.maps.LatLng(
									marker.latitude,
									marker.longitude
								),
								map: this.state.map,
								icon: {
									url: marker.icon
										? marker.icon
										: isSelected
											? staticUrl(mapMarkerSelected)
											: staticUrl(mapMarker),
									size: new this.state.google.maps.Size(30, 38),
									// anchor: new this.state.google.maps.Point(0, 19),
									labelOrigin: new this.state.google.maps.Point(15, 15),
									scaledSize: new this.state.google.maps.Size(30, 38)
								},
								label:
									!marker.label || marker.label === ""
										? ""
										: {
												text: marker.label,
												color: isSelected ? "white" : "#1A2226",
												fontSize: "12px",
												fontWeight: "bold"
										  },
								title: marker.title
							});
							//only zoom in on first 10 stores under 50 mile radius
							if (
								this.props.isStateSearch ||
								(ind < 10 && (!marker.distance || marker.distance < 51))
							) {
								bounds.extend(gMarker.getPosition());
							}
							//if current marker is selected:
							if (this.props.selectedStore === marker.id) {
								centerOn = gMarker.getPosition();
							}
							//marker click logic
							let self = this,
								storeId = marker.id;
							gMarker.addListener("click", () => {
								this.state.map.setCenter(gMarker.getPosition());
								this.state.map.setZoom(15);
								if (self.props.setSelectedStore) {
									self.props.setSelectedStore(storeId);
								}
							});
							return { marker: gMarker, id: storeId };
					  })
					: [];
				//in case there are several stores that we are zooming on
				if (this.markers && this.markers.length > 1 && !bounds.isEmpty()) {
					this.state.map.fitBounds(bounds, config.googleMaps.boundsPadding);
				} else if (this.markers && this.markers.length === 1) {
					//if one particular store is selected
					this.state.map.setCenter(this.markers[0].marker.getPosition());
					this.state.map.setZoom(15);
				}
				if (centerOn) {
					this.state.map.setCenter(centerOn);
					this.state.map.setZoom(15);
				}
			}
		}
		initializeGoogle() {
			let opt = {
				center: new window.google.maps.LatLng(
					this.state.latitude,
					this.state.longitude
				),
				mapTypeId: window.google.maps.MapTypeId.ROADMAP,
				zoomControlOptions: {
					position: window.google.maps.ControlPosition.RIGHT_BOTTOM,
					style: window.google.maps.ZoomControlStyle.LARGE
				}
			};
			this.setState({
				google: window.google,
				geocoder: new window.google.maps.Geocoder()
			});

			if (!this.props.noMap && !this.state.map) {
				this.setState({
					map: new window.google.maps.Map(this.mapContainer, {
						...config.googleMaps.mapOptions,
						...opt
					})
				});
			}
		}
		componentWillReceiveProps(nP) {
			if (
				nP.latitude &&
				nP.longitude &&
				(nP.latitude !== this.state.latitude ||
					nP.longitude !== this.state.longitude)
			) {
				this.setState({
					latitude: nP.latitude,
					longitude: nP.longitude
				});
			}
		}
		componentDidMount() {
			//if there's no lat/long data from server, attempt to locate user and fallback to default location
			if (!this.state.latitude || !this.state.longitude) {
				navigator.geolocation.getCurrentPosition(
					position => {
						this.setState({
							latitude: position.coords.latitude,
							longitude: position.coords.longitude
						});
					},
					() => {
						//This is error handler
						this.setState({
							latitude: config.googleMaps.defaultPosition.latitude,
							longitude: config.googleMaps.defaultPosition.longitude
						});
						if (this.state.map) {
							this.state.map.panTo(
								new window.google.maps.LatLng(
									this.state.latitude,
									this.state.longitude
								)
							);
						}
					},
					config.geolocationClientOptions
				);
			}

			if (window.googleHasLoaded) {
				this.initializeGoogle();
			} else {
				window.document.addEventListener(
					"googleApiLoaded",
					this.initializeGoogle
				);
			}
		}
		componentWillUnmount() {
			window.document.removeEventListener(
				"googleApiLoaded",
				this.initializeGoogle
			);
		}
		render() {
			return (
				<WrappedComponent
					{...this.props}
					{...this.state}
					populateMarkers={this.populateMarkers}
				>
					<div
						ref={elem => (this.mapContainer = elem)}
						className="storeSearch__map"
					/>
				</WrappedComponent>
			);
		}
	}
	GoogleComponentWrapper.propTypes = {
		noMap: PropTypes.bool,
		isStateSearch: PropTypes.bool,
		setSelectedStore: PropTypes.func,
		selectedStore: PropTypes.string,
		latitude: PropTypes.number,
		longitude: PropTypes.number
	};
	return GoogleComponentWrapper;
};

export default GoogleComponentHOC;
