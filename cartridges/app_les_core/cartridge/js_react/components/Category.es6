import React from "react";
import PropTypes from "prop-types";
import { sanitize } from "util/helpers";
import VehicleSelector from "containers/VehicleSelector";
import classNames from "classnames";
import MediaQuery from "react-responsive";
import config from "config";

/*
	Navigation Category
	Includes:
		isActive: state to handle showing/hiding of self
		isPartial: state to handle showing/hiding of right column of panel
*/
class Category extends React.Component {
	constructor(props) {
		super(props);

		this.togglePartial = this.togglePartial.bind(this);

		this.state = {
			isActive: this.props.isActive,
			isPartial: false
		};
	}
	togglePartial(manual) {
		this.setState({
			isPartial: typeof manual === "boolean" ? manual : !this.state.isPartial
		});
	}
	componentWillReceiveProps(nP) {
		this.setState({ isActive: nP.isActive });
	}
	render() {
		let dropdown,
			link,
			{
				label,
				url,
				panel,
				vehicles,
				handleMouseHover,
				handleMouseLeave,
				handleMobileClick
			} = this.props;
		let rows = [],
			cells = [];
		if (panel !== null) {
			let { vehicleSelector, services, warranty, brochure } = panel;
			let warrantyLinkClass;
			//creates the warranty link, examples: button--alignment-warranty, button--battery-warranty
			if (warranty) {
				warrantyLinkClass =
					"warrantyLink--" +
					warranty.ctaLabel
						.toString()
						.toLowerCase()
						.split(" ")
						.join("-")
						.replace("&", "") +
					" warranty-language-dialog";
			}
			//If the category has a VS, add the VS component.
			if (vehicleSelector) {
				cells.push(
					<VehicleSelector
						key="vehicleselector"
						{...vehicleSelector}
						vehicles={vehicles}
						togglePartial={this.togglePartial}
						toggleDropdown={this.props.toggleDesktopNav}
						parentCategory={label}
						storeData={this.props.storeData}
					/>
				);
			}
			if (services && !this.state.isPartial) {
				cells.push(
					<div className="cell auto" key="services">
						<h6>{services.title}</h6>
						<ul className="navigation__productList">
							{services.items.map((item, i) => (
								<li key={"productItem" + i}>
									<a
										href={item.url}
										dangerouslySetInnerHTML={sanitize(item.label)}
									/>
								</li>
							))}
							{warranty &&
								warranty.ctaLabel && (
									<li>
										<a
											data-label={warranty.warrantyTitle}
											data-url-eng={warranty.engUrl}
											data-url-esp={warranty.spanishUrl}
											className={warrantyLinkClass}
										>
											{warranty.ctaLabel}
										</a>
									</li>
								)}
							{brochure &&
								brochure.ctaLabel && (
									<li>
										<a
											href={brochure.ctaUrl}
											target="_blank"
											className="brochureLink"
										>
											{brochure.ctaLabel}
										</a>
									</li>
								)}
						</ul>
						{services.bottomItems.length !== 0 && (
							<section className="navigation__panel__bottom">
								{services.bottomItems.map((item, i) => (
									<div key={"bottomItem" + i}>
										<a
											href={item.url}
											dangerouslySetInnerHTML={sanitize(item.label)}
										/>
										<span dangerouslySetInnerHTML={sanitize(" " + item.text)} />
									</div>
								))}
							</section>
						)}
					</div>
				);
			}
			rows.push(
				<div className="grid-x grid-padding-x" key="allcells">
					{cells}
				</div>
			);
		}
		dropdown = <section className="navigation__panel">{rows}</section>;
		if (url) {
			link = (
				<a
					className="navigation__category__title"
					href={url}
					role="menu"
					tabIndex="0"
					onClick={event => event.stopPropagation()}
				>
					{label}
				</a>
			);
		} else {
			link = (
				<span className="navigation__category__title" role="menu" tabIndex="0">
					{label}
				</span>
			);
		}
		//render everything, include a back button if the user is on a mobile device
		return (
			<li
				className={classNames("navigation__category", {
					"navigation__category--open": this.state.isActive,
					"navigation__category--noSubNav":
						this.props.panel.services.items.length === 0
				})}
				onMouseEnter={handleMouseHover}
				onMouseLeave={handleMouseLeave}
				onClick={handleMobileClick}
			>
				<MediaQuery
					query={"(max-width: " + config.breakpoints.maxMobilePx + ")"}
				>
					<a
						className="navigation__category__back backLink"
						onClick={this.props.handleBackPress}
					>
						Back
					</a>
				</MediaQuery>
				{link}
				{this.state.isActive && dropdown}
			</li>
		);
	}
}

Category.propTypes = {
	panel: PropTypes.object,
	storeData: PropTypes.object,
	label: PropTypes.string,
	url: PropTypes.string,
	handleMouseHover: PropTypes.func,
	handleMouseLeave: PropTypes.func,
	handleBackPress: PropTypes.func,
	handleMobileClick: PropTypes.func,
	isActive: PropTypes.bool,
	vehicles: PropTypes.array,
	toggleDesktopNav: PropTypes.func
};

export default Category;
