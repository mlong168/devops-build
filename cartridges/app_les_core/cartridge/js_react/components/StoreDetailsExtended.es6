import React from "react";
import { StoreDetailProps } from "util/Props";
import StoreDetailsBase from "components/StoreDetailsBase";
import GoogleComponentHOC from "components/GoogleComponentHOC";
import AppointmentCTA from "components/AppointmentCTA";
import { staticUrl } from "util/helpers";
import mapMarker from "icons/mapMarkerStar.svg";

/**
	Store details Extended has additional information and a map - use on: homepage
*/
class StoreDetailsExtended extends StoreDetailsBase {
	componentDidUpdate() {
		this.props.populateMarkers([
			{
				latitude: this.props.latitude,
				longitude: this.props.longitude,
				icon: staticUrl(mapMarker),
				label: "",
				title: this.props.title,
				distance: this.props.distance
			}
		]);
		if (this.props.map) {
			this.props.map.setCenter(
				new this.props.google.maps.LatLng(
					this.props.latitude,
					this.props.longitude
				)
			);
		}
	}
	render() {
		let address = [];
		address.push(this.props.city);
		address.push(this.props.stateCode);
		address.push(this.props.postalCode);
		return (
			<div className="storeDetails storeDetails--extended grid-x grid-margin-x grid-margin-y">
				<section className="cell medium-6 small-12 small-order-2 medium-order-1">
					<header className="storeDetails__header show-for-medium">
						<h6 className="storeDetails__header__title header--large">
							{this.state.isMystore
								? this.props.myStoreHeader
								: this.props.nearestStoreHeader}
						</h6>
						<a
							className="storeDetails__header__changeCta"
							href={this.props.changeStoreUrl}
						>
							{this.props.changeStoreLabel}
						</a>
					</header>
					{this.props.walkinsText !== "" &&
						this.props.walkinsText !== null && (
							<p>
								<strong>{this.props.walkinsText}</strong>
							</p>
						)}
					<address>
						<div className="storeDetails__streetName" key={address}>
							{this.props.index != null && this.props.index + 1 + ". "}
							{this.props.address1}{" "}
							<a
								className="changeCta show-for-small-only"
								href={this.props.changeStoreUrl}
							>
								{this.props.changeStoreLabel}
							</a>
						</div>
						<span>{address.join(", ")}</span>
					</address>
					{/*
						Contact, ordering is important due to mobile + desktop being different
					*/}
					<ul className="storeDetails__contact">
						<li>
							<span className="show-for-medium">
								{this.props.phone !== null && this.formatPhone()}
								{this.props.phone === null && this.props.noPhoneMessage}
							</span>
							<a
								className="show-for-small-only button button--gray"
								href={this.getDirections()}
								target="_blank"
							>
								{this.props.directionsLabel}
							</a>
						</li>
						<li>
							<a
								className="show-for-medium"
								href={this.getDirections()}
								target="_blank"
							>
								{this.props.directionsLabel}
							</a>
							{this.props.phone !== null && (
								<a
									className="show-for-small-only button button--gray"
									href={"tel:" + this.props.phone}
								>
									Call Us
								</a>
							)}
							{this.props.phone === null && (
								<span className="show-for-small-only">
									{this.props.noPhoneMessage}
								</span>
							)}
						</li>
						<li className="show-for-medium">
							<a href={this.props.storeDetailsUrl}>
								{this.props.storeDetailsLabel}
							</a>
						</li>
					</ul>
					<AppointmentCTA
						ctaLabel={this.props.appointmentLabel}
						ctaUrl=""
						classes="storeDetails__button hide-for-small-only"
					/>
				</section>
				<section className="cell auto small-order-1 medium-order-2">
					{this.props.children}
				</section>
			</div>
		);
	}
}

StoreDetailsExtended.propTypes = StoreDetailProps;

export default GoogleComponentHOC(StoreDetailsExtended);
