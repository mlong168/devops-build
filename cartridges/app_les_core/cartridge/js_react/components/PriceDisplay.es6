import React from "react";
import PropTypes from "prop-types";

/*
	Price Display for the Product Tiles
*/
const PriceDisplay = props => {
	let formatMoney = (money, classes) => {
		let f = money.toFixed(2).split(".");
		return (
			<span className={classes ? classes + " money" : "money"}>
				<sup>{props.currencySymbol}</sup>
				{f[0]}
				<sup>{f[1]}</sup>
			</span>
		);
	};
	return (
		<div className="product__price">
			<div className="product__price__sales">
				{!props.productPrice &&
					props.noPriceLabel && <span>{props.noPriceLabel}</span>}
				{props.productPrice && formatMoney(props.productPrice)}
				{props.productPrice && <span>{props.perCategoryLabel}</span>}
				{props.installedPriceUrl &&
					props.category !== "wheel" && (
						<a href={props.installedPriceUrl} alt={props.installedPriceLabel}>
							&nbsp;{props.installedPriceLabel}
						</a>
					)}
				{props.onSaleLabel && (
					<span className="product__price__onsale">{props.onSaleLabel}</span>
				)}
			</div>

			{props.totalWithoutSales && <hr />}
			{props.totalWithoutSales && (
				<div className="product__price__calculated">
					<div className="product__price__calculated">
						{props.fromLabel}
						{formatMoney(props.totalWithoutSales, "money--secondary")}
						<div>
							{props.totalWithoutSalesLabel}
							{props.withCreditUrl && props.withCreditWith}
							{props.withCreditUrl && (
								<a href={props.withCreditUrl} alt={props.withCreditLabel}>
									&nbsp;{props.withCreditLabel}
								</a>
							)}
						</div>
					</div>
				</div>
			)}
		</div>
	);
};

PriceDisplay.propTypes = {
	productPrice: PropTypes.number,
	currencySymbol: PropTypes.string,
	perCategoryLabel: PropTypes.string,
	onSaleLabel: PropTypes.string,
	totalWithoutSales: PropTypes.number,
	fromLabel: PropTypes.string,
	totalWithoutSalesLabel: PropTypes.string,
	noPriceLabel: PropTypes.string,
	withCreditUrl: PropTypes.string,
	withCreditLabel: PropTypes.string,
	withCreditWith: PropTypes.string,
	installedPriceUrl: PropTypes.string,
	installedPriceLabel: PropTypes.string,
	category: PropTypes.string
};

export default PriceDisplay;
