import React from "react";
import VehicleEditDialog from "components/VehicleEditDialog";
import { VehicleInfoProps } from "util/Props";

class VehicleInfo extends React.Component {
	constructor(props) {
		super(props);
		let vehicle = this.props.vehicle || null; //this.props.vehicleDetails;
		this.state = {
			renderingError: false,
			vehicle: vehicle,
			tires: this.props.tires, //|| this.props.tiresDetails,
			wheels: this.props.wheels, // || this.props.wheelsDetails,
			color: this.props.color
		};

		this.generateSrc = this.generateSrc.bind(this);
		this.handleRenderError = this.handleRenderError.bind(this);
		this.handleEdit = this.handleEdit.bind(this);
		this.toggleDialog = this.toggleDialog.bind(this);
	}
	componentWillReceiveProps(nP) {
		let vehicle = nP.vehicle || nP.vehicleDetails,
			tires = nP.tires || nP.tiresDetails,
			wheels = nP.wheels || nP.wheelsDetails;

		this.setState({
			vehicle: vehicle,
			tires: tires,
			wheels: wheels,
			color: nP.color,
			renderingError: false,
			editDialogOpen: false
		});
	}
	generateSrc() {
		return (
			this.props.vehicleRenderApiUrl +
			`&VehicleConfiguration=${
				this.state.vehicle.categoryId
			}&Width=220&Height=80&PaintColor=%23${this.state.color ||
				this.state.vehicle.color}`
		);
	}
	formatTireInfo(tire) {
		return `${tire.width}/${tire.ratio}R-${tire.rim}`;
	}
	formatWheelInfo(wheel) {
		return `${wheel.boltpattern} - ${wheel.diameter}`;
	}
	handleRenderError() {
		this.setState({ renderingError: true });
	}
	handleEdit() {
		this.toggleDialog();
	}
	toggleDialog() {
		this.setState({ editDialogOpen: !this.state.editDialogOpen });
	}
	render() {
		let body;
		if (this.state.vehicle) {
			body = (
				<aside className="vehicleInfo">
					<div className="vehicleInfo__image">
						{this.state.vehicle &&
							!this.state.renderingError && (
								<img
									src={this.generateSrc()}
									onError={this.handleRenderError}
								/>
							)}
					</div>
					<h3>
						{this.state.vehicle.year} {this.state.vehicle.make}{" "}
						{this.state.vehicle.model} {this.state.vehicle.trim}
					</h3>
					<a
						aria-label="Edit vehicle selection"
						className="vehicleInfo__edit"
						onClick={this.handleEdit}
					/>
					{this.state.editDialogOpen && (
						<VehicleEditDialog
							storeData={this.props.storeData}
							category={this.props.category}
							activeFlow="byVehicleType"
							toggleDialog={this.toggleDialog}
							getVehicleSelectorDataUrl={this.props.getVehicleSelectorDataUrl}
						/>
					)}
				</aside>
			);
		} else if (
			this.props.category &&
			this.props.category === "tire" &&
			this.state.tires
		) {
			body = (
				<aside className="vehicleInfo">
					<div className="vehicleInfo__image vehicleInfo__image--tire" />
					{this.state.tires.front &&
						this.state.tires.rear && (
							<h3 className="vehicleInfo__text">
								<small>Front:</small>
								{this.formatTireInfo(this.state.tires.front)}
								<small>Rear:</small>
								{this.formatTireInfo(this.state.tires.rear)}
							</h3>
						)}
					{this.state.tires.front &&
						!this.state.tires.rear && (
							<h3 className="vehicleInfo__text">
								{this.formatTireInfo(this.state.tires.front)}
							</h3>
						)}
					<a
						aria-label="Edit tire selection"
						className="vehicleInfo__edit"
						onClick={this.handleEdit}
					/>
					{this.state.editDialogOpen && (
						<VehicleEditDialog
							category={this.props.category}
							activeFlow="byTire"
							toggleDialog={this.toggleDialog}
							getVehicleSelectorDataUrl={this.props.getVehicleSelectorDataUrl}
						/>
					)}
				</aside>
			);
		} else if (
			this.props.category &&
			this.props.category === "wheel" &&
			this.state.wheels
		) {
			body = (
				<aside className="vehicleInfo">
					<div className="vehicleInfo__image vehicleInfo__image--wheel" />
					{this.state.wheels.front &&
						this.state.wheels.rear && (
							<h3 className="vehicleInfo__text">
								<small>Front:</small>
								{this.formatWheelInfo(this.state.wheels.front)}
								<small>Rear:</small>
								{this.formatWheelInfo(this.state.wheels.rear)}
							</h3>
						)}
					{this.state.wheels.front &&
						!this.state.wheels.rear && (
							<h3 className="vehicleInfo__text">
								{this.formatWheelInfo(this.state.wheels.front)}
							</h3>
						)}
					<a
						aria-label="Edit wheel selection"
						className="vehicleInfo__edit"
						onClick={this.handleEdit}
					/>
					{this.state.editDialogOpen && (
						<VehicleEditDialog
							category={this.props.category}
							activeFlow="byWheel"
							toggleDialog={this.toggleDialog}
							getVehicleSelectorDataUrl={this.props.getVehicleSelectorDataUrl}
						/>
					)}
				</aside>
			);
		} else {
			body = (
				<aside className="vehicleInfo">
					<div className="vehicleInfo__image" />
					<a aria-label="Fine-tune your search" onClick={this.handleEdit}>
						Fine-tune Your Search
					</a>
					{this.state.editDialogOpen && (
						<VehicleEditDialog
							category={this.props.category}
							toggleDialog={this.toggleDialog}
							getVehicleSelectorDataUrl={this.props.getVehicleSelectorDataUrl}
						/>
					)}
				</aside>
			);
		}
		return body;
	}
}

VehicleInfo.propTypes = VehicleInfoProps;

export default VehicleInfo;
