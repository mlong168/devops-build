import React from "react";
import { StoreDetailProps } from "util/Props";
import AppointmentCTA from "components/AppointmentCTA";
import StoreDetailsBase from "components/StoreDetailsBase";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class StoreDetailsTiny extends StoreDetailsBase {
	render() {
		let address = [];
		address.push(this.props.city);
		address.push(this.props.stateCode);
		address.push(this.props.postalCode);
		return (
			<div className="storeDetails storeDetails--tiny">
				{this.props.isMystore && (
					<strong className="storeDetails__storeHeader">
						{this.props.myStoreHeader}
					</strong>
				)}
				{!this.props.isMystore && (
					<strong className="storeDetails__storeHeader">
						{this.props.nearestStoreHeader}
					</strong>
				)}
				<address>
					<span className="storeDetails__streetName" key={address}>
						{this.props.index != null && this.props.index + 1 + ". "}
						{this.props.address1}{" "}
					</span>
					<span>{address.join(", ")}</span>
					{this.props.phone === null && (
						<span className="show-for-small-only">
							{this.props.noPhoneMessage}
						</span>
					)}
					<a className="changeCta" href={this.props.changeStoreUrl}>
						{this.props.changeStoreLabel}
					</a>
					<span className="show-for-medium storeDetails__directions__phone">
						{this.props.phone !== null && this.formatPhone()}
						{this.props.phone === null && this.props.noPhoneMessage}
					</span>
				</address>
				<a
					className="button button--flexWide"
					href={this.getDirections()}
					target="_blank"
				>
					{this.props.directionsLabel}
				</a>

				{this.props.phone !== null && (
					<a
						className="show-for-small-only button button--gray"
						href={"tel:" + this.props.phone}
					>
						Call Us
					</a>
				)}

				<AppointmentCTA
					ctaLabel={this.props.appointmentLabel}
					ctaUrl=""
					classes="storeDetails__button button--flexWide"
				/>
			</div>
		);
	}
}

StoreDetailsTiny.propTypes = StoreDetailProps;

export default StoreDetailsTiny;
