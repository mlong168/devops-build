import React from "react";
import { StoreDetailProps } from "util/Props";
import AppointmentCTA from "components/AppointmentCTA";
import StoreDetailsBase from "components/StoreDetailsBase";
import GoogleComponentHOC from "components/GoogleComponentHOC";
import { staticUrl, sanitize } from "util/helpers";
import mapMarker from "icons/mapMarkerStar.svg";
import MakeMyStore from "components/MakeMyStore";
import config from "config";
/**
	Store details should be used anytime brief store information is displayed on the page, including address,
	my store indicator and make an appointment CTA.
*/
class StoreDetailsFull extends StoreDetailsBase {
	componentDidUpdate() {
		this.props.populateMarkers([
			{
				latitude: this.props.latitude,
				longitude: this.props.longitude,
				icon: staticUrl(mapMarker),
				label: "",
				title: this.props.title,
				id: this.props.id,
				distance: this.props.distance
			}
		]);
	}
	render() {
		let address = [];
		let wrapperClassName = "storeDetails__wrapper cell";

		if (this.props.type == "full") {
			wrapperClassName += " medium-6 small-12 small-order-2 medium-order-1";
		} else {
			wrapperClassName += " small-12";
		}
		address.push(this.props.city);
		address.push(this.props.stateCode);
		address.push(this.props.postalCode);
		//full needs grid classes
		//non full types don't need it at the moment
		return (
			<div className="storeDetails storeDetails--full grid-x">
				<div className={wrapperClassName}>
					<address>
						<span className="storeDetails__streetName" key={address}>
							{this.props.index != null && this.props.index + 1 + ". "}
							{this.props.address1}{" "}
						</span>
						<span>{address.join(", ")}</span>
						{this.props.distance && (
							<span className="storeDetails__distance">
								<strong>
									{`${this.roundDistance(this.props.distance)} ${
										config.distanceMeasurement
									}`}
								</strong>
							</span>
						)}
						<a
							className="storeDetails__mobileDetailsLink"
							href={this.props.storeDetailsUrl}
							title={this.props.storeDetailsLabel}
							aria-label={this.props.storeDetailsLabel}
						/>
					</address>
					<ul className="storeDetails__contact">
						<li>
							<span className="show-for-medium">
								{this.props.phone !== null && this.formatPhone()}
								{this.props.phone === null && this.props.noPhoneMessage}
							</span>
							<a
								className="show-for-small-only button button--gray"
								href={this.getDirections()}
								target="_blank"
							>
								{this.props.directionsLabel}
							</a>
						</li>
						<li>
							<a
								className="show-for-medium"
								href={this.getDirections()}
								target="_blank"
							>
								{this.props.directionsLabel}
							</a>
							{this.props.phone !== null && (
								<a
									className="show-for-small-only button button--gray"
									href={"tel:" + this.props.phone}
								>
									Call Us
								</a>
							)}
							{this.props.phone === null && (
								<span className="show-for-small-only">
									{this.props.noPhoneMessage}
								</span>
							)}
						</li>
						{this.props.weSpeakSpanish && (
							<li className="show-for-medium">
								<span>{this.props.weSpeakSpanish}</span>
							</li>
						)}
					</ul>
					{this.props.description && (
						<span className="storeDetails__description">
							{this.props.description}
						</span>
					)}
					{/* <a
						href={this.props.storeDetailsUrl}
						className="storeDetails__detailsLink"
					>
						{this.props.storeDetailsLabel}
					</a> */}
					<AppointmentCTA
						ctaLabel={this.props.appointmentLabel}
						ctaUrl=""
						classes="storeDetails__button"
					/>
					<MakeMyStore
						id={this.props.id}
						isMystore={this.state.isMystore}
						handleMystoreAction={this.handleMystoreAction}
						myStoreLabel={this.props.myStoreLabel}
						makeMyStoreLabel={this.props.makeMyStoreLabel}
					/>
					{this.props.weSpeakSpanish && (
						<p className="show-for-small-only">{this.props.weSpeakSpanish}</p>
					)}
					<div className="storeDetails__information">
						{this.props.hours &&
							this.props.hours !== "" && (
								<section>
									<h6>{this.props.hoursLabel}</h6>
									<div dangerouslySetInnerHTML={sanitize(this.props.hours)} />
								</section>
							)}
						{this.props.manager &&
							this.props.manager !== "" && (
								<section>
									<h6>{this.props.managerLabel}</h6>
									<p>{this.props.manager}</p>
								</section>
							)}
					</div>
					{this.props.managerImage &&
						this.props.managerImage !== "" && (
							<img src={this.props.managerImage} alt={this.props.manager} />
						)}
				</div>
				<div className="cell auto small-order-1 medium-order-2">
					{!this.props.noMap && this.props.children}
					{this.props.type !== "header" &&
						this.props.storeImages &&
						this.props.storeImages.map((image, ind) => (
							<img
								src={image}
								alt={this.props.title}
								key={ind}
								className="storeDetails__storeImage"
							/>
						))}
				</div>
			</div>
		);
	}
}

StoreDetailsFull.propTypes = StoreDetailProps;

export default GoogleComponentHOC(StoreDetailsFull);
