import PropTypes from "prop-types";

export const SelectorFlowProps = {
	apiUrls: PropTypes.object
};
export const VehicleEditDialogProps = {
	category: PropTypes.string,
	apiUrls: PropTypes.object,
	storeData: PropTypes.object,
	getVehicleSelectorDataUrl: PropTypes.string,
	activeFlow: PropTypes.string
};
export const ColorSelectorProps = {
	colorSelectorHeading: PropTypes.string,
	colorCancelLabel: PropTypes.string,
	selectedColor: PropTypes.string,
	selectColor: PropTypes.func
};
export const AppointmentCTAProps = {
	componentName: PropTypes.string.isRequired,
	ctaLabel: PropTypes.string.isRequired,
	ctaUrl: PropTypes.string.isRequired,
	pid: PropTypes.string,
	qty: PropTypes.number,
	sids: PropTypes.array
};
export const WheelVisualizerProps = {
	componentName: PropTypes.string,
	vehicleRenderApiUrl: PropTypes.string,
	vehicle: PropTypes.object,
	thumbnail: PropTypes.object,
	colorSelectorHeading: PropTypes.string,
	colorCancelLabel: PropTypes.string,
	vehicleCategoryId: PropTypes.string,
	selectedColor: PropTypes.string,
	dispatch: PropTypes.func,
	logVisualizerPreview: PropTypes.func,
	myStore: PropTypes.string
};
export const VehicleInfoProps = {
	componentName: PropTypes.string,
	vehicleRenderApiUrl: PropTypes.string,
	vehicle: PropTypes.object,
	tires: PropTypes.object,
	wheels: PropTypes.object,
	vehicleDetails: PropTypes.shape({
		categoryId: PropTypes.string,
		year: PropTypes.string,
		make: PropTypes.string,
		model: PropTypes.string,
		trim: PropTypes.string,
		color: PropTypes.string
	}),
	storeData: PropTypes.object,
	tiresDetails: PropTypes.object,
	wheelsDetails: PropTypes.object,
	noVehicleMessage: PropTypes.string,
	category: PropTypes.string,
	color: PropTypes.string,
	getVehicleSelectorDataUrl: PropTypes.string
};
export const VehicleRenderProps = {
	componentName: PropTypes.string,
	vehicleRenderApiUrl: PropTypes.string,
	vehicle: PropTypes.object,
	vehicleDetails: PropTypes.shape({
		categoryId: PropTypes.string,
		year: PropTypes.string,
		make: PropTypes.string,
		model: PropTypes.string,
		trim: PropTypes.string,
		color: PropTypes.string
	}),
	wheelSku: PropTypes.string
};
export const VehicleSelectorProps = {
	addVehicle: PropTypes.func,
	addTires: PropTypes.func,
	addWheels: PropTypes.func,
	apiUrls: PropTypes.object,
	description: PropTypes.string,
	items: PropTypes.array,
	parentCategory: PropTypes.string,
	shopForTiresLabel: PropTypes.string,
	shopForTiresUrl: PropTypes.string,
	title: PropTypes.string,
	togglePartial: PropTypes.func,
	vehicles: PropTypes.array,
	viewAllLabel: PropTypes.string,
	viewAllUrl: PropTypes.string,
	isLoading: PropTypes.bool,
	logEvent: PropTypes.func,
	isCompact: PropTypes.bool,
	activeFlow: PropTypes.string,
	category: PropTypes.string,
	boltpatterns: PropTypes.array,
	toggleDropdown: PropTypes.func,
	storeData: PropTypes.object
};
export const StoreSearchFormProps = {
	componentName: PropTypes.string,
	noMap: PropTypes.bool,
	formLabel: PropTypes.string,
	postUrl: PropTypes.string,
	countryCode: PropTypes.string,
	fieldName: PropTypes.string,
	fieldPlaceholder: PropTypes.string,
	findByCurrentLocationAction: PropTypes.string,
	findByCurrentLocationTitle: PropTypes.string,
	findByAddressAction: PropTypes.string,
	findByAddressTitle: PropTypes.string,
	inputLabelText: PropTypes.string,
	latitudeField: PropTypes.string,
	longitudeField: PropTypes.string,
	stores: PropTypes.array,
	latitude: PropTypes.number,
	longitude: PropTypes.number,
	isStateSearch: PropTypes.bool
};

export const StoreDetailProps = {
	address1: PropTypes.string,
	postalCode: PropTypes.string,
	appointmentLabel: PropTypes.string,
	callUsLabel: PropTypes.string,
	changeStoreLabel: PropTypes.string,
	changeStoreUrl: PropTypes.string,
	city: PropTypes.string,
	clickedMarker: PropTypes.func,
	componentName: PropTypes.string,
	creditPageCtaLabel: PropTypes.string,
	creditPageCtaUrl: PropTypes.string,
	description: PropTypes.string,
	directionsLabel: PropTypes.string,
	distance: PropTypes.number,
	findAnotherStoreLabel: PropTypes.string,
	hours: PropTypes.string,
	hoursLabel: PropTypes.string,
	id: PropTypes.string.isRequired,
	index: PropTypes.number,
	isMystore: PropTypes.bool,
	isNearest: PropTypes.bool,
	isSelectedStore: PropTypes.bool,
	latitude: PropTypes.number,
	longitude: PropTypes.number,
	makeMyStoreLabel: PropTypes.string,
	makeMyStoreUrl: PropTypes.string,
	manager: PropTypes.string,
	managerImage: PropTypes.string,
	managerLabel: PropTypes.string,
	myStore: PropTypes.string,
	myStoreHeader: PropTypes.string,
	myStoreLabel: PropTypes.string,
	nearestStoreHeader: PropTypes.string,
	noMap: PropTypes.bool,
	phone: PropTypes.string,
	noPhoneMessage: PropTypes.string,
	selectedStore: PropTypes.string,
	setSelectedStore: PropTypes.func,
	stateCode: PropTypes.string,
	stopByForInstallation: PropTypes.string,
	storeDetailsLabel: PropTypes.string,
	storeDetailsUrl: PropTypes.string,
	storeImages: PropTypes.array,
	title: PropTypes.string,
	type: PropTypes.oneOf(["brief", "full", "extended", "tiny", "header", "PDP"]),
	walkinsText: PropTypes.string,
	weSpeakSpanish: PropTypes.string
};
