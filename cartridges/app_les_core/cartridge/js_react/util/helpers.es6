import sanitizeHtml from "sanitize-html-react";
import config from "config";

const sanitize = dirty => {
	return {
		__html: sanitizeHtml(dirty, {
			allowedTags: config.allowedTags,
			allowedAttributes: config.allowedAttributes
		})
	};
};

//export default sanitize;

const staticUrl = url => {
	return window.__INITIAL__.staticLibraryURL
		? window.__INITIAL__.staticLibraryURL + "/" + url
		: url;
};
export { staticUrl, sanitize };
