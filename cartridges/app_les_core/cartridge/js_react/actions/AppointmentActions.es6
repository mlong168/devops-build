import types from "./Types";
import config from "config";
import "isomorphic-fetch";

export const nextStep = () => ({ type: types.NEXT_STEP });
export const prevStep = () => ({ type: types.PREV_STEP });
export const phoneClicked = actionLocation => ({
	type: types.PHONE_CLICKED,
	location: actionLocation
});

export const submitRequest = () => ({
	type: types.SUBMIT_REQUEST
});

export const errorEncountered = error => ({
	type: types.ERROR_ENCOUNTERED,
	error: error
});
export const validationError = error => ({
	type: types.VALIDATION_ERROR,
	error: error
});
export const virtualPageView = payload => ({
	type: types.PAGE_VIEW,
	data: payload
});

//Contact step actions
export const receiveAppointmentConfirmation = json => ({
	type: types.RECEIVE_APPOINTMENT_CONFIRMATION,
	data: json
});

export const postAppointmentRequest = userInfo => (dispatch, getState) => {
	dispatch(submitRequest());
	const currentState = getState();
	let requestBody = {
		storeId: currentState.servicesState.storeInfo.storeId,
		services: currentState.servicesState.selectedServices,
		additionalInfo: currentState.servicesState.additionalInfo,
		date: currentState.dateState.date,
		time: currentState.dateState.time,
		userInfo: {
			firstName: userInfo.firstName.state.value,
			lastName: userInfo.lastName.state.value,
			emailAddress: userInfo.email.state.value,
			phoneNumber: userInfo.phoneNumber.state.value
		}
	};
	if (currentState.servicesState.vehicleDescription) {
		Object.assign(requestBody, {
			vehicle: {
				vehicleDescription: currentState.servicesState.vehicleDescription
			}
		});
	}
	if (currentState.servicesState.tireSize) {
		requestBody = Object.assign({}, requestBody, {
			vehicle: {
				...requestBody.vehicle,
				tireSize: currentState.servicesState.tireSize
			}
		});
	}

	return fetch(`${config.submit_api}`, {
		method: "POST",
		mode: "cors",
		headers: new Headers(config.api_headers),
		body: JSON.stringify(requestBody)
	})
		.then(response => {
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json();
		})
		.then(json => dispatch(receiveAppointmentConfirmation(json)))
		.catch(error => dispatch(errorEncountered(error.message)));
};

//Date step actions
export const selectDate = date => ({
	type: types.DATE_SELECTED,
	date: date
});

export const fetchTimesForADate = date => (dispatch, getState) => {
	dispatch(requestTimes());
	const currentState = getState();
	if (currentState.dateState.timesMatrix[date]) {
		//TODO: check this usecase/dispatch logic
		dispatch(receiveTimes(date, currentState.dateState.timesMatrix[date]));
		return null;
	}
	return fetch(
		`${config.times_api}/${
			currentState.servicesState.storeInfo.storeId
		}/${date}`,
		{
			method: "GET",
			mode: "cors",
			headers: new Headers(config.api_headers)
		}
	)
		.then(response => {
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json();
		})
		.then(json => dispatch(receiveTimes(date, json)))
		.catch(error => dispatch(errorEncountered(error.message)));
};

export const requestTimes = () => ({
	type: types.TIMES_REQUESTED
});
export const receiveTimes = (date, times) => ({
	type: types.TIMES_RECEIVED,
	date: date,
	times: times
});
export const selectTime = time => ({
	type: types.TIME_SELECTED,
	time: time
});
export const toggleHeader = () => ({
	type: types.TOGGLE_HEADER
});

//Service step actions
export const userSelectedServices = (selectedServices, additionalInfo) => ({
	type: types.USER_SELECTED_SERVICES,
	selectedServices: selectedServices,
	additionalInfo: additionalInfo
});

export const loadURLParams = (vehicleDescription, tireSize) => ({
	type: types.LOAD_URL_PARAMS,
	vehicleDescription: vehicleDescription,
	tireSize: tireSize
});

// export const receiveStoreInfo = json => ({
// 	type: types.RECEIVE_STORE_INFO,
// 	storeInfo: json
// });
// export const fetchStoreInfo = storeId => (dispatch, getState) => {
// 	const currentState = getState();
// 	if (storeId === currentState.servicesState.storeInfo.storeId) {
// 		dispatch(errorEncountered("StoreId is missing"));
// 		return null;
// 	}
// 	dispatch(submitRequest());
//
// 	return fetch(`${config.store_api}/${storeId}`, {
// 		method: "GET",
// 		mode: "cors",
// 		headers: new Headers(config.api_headers)
// 	})
// 		.then(response => {
// 			if (!response.ok) {
// 				throw Error(response.statusText);
// 			}
// 			return response.json();
// 		})
// 		.then(json => dispatch(receiveStoreInfo(json)))
// 		.catch(error => dispatch(errorEncountered(error.message)));
// };
