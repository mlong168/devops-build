import types from "./Types";
import moment from "moment";

export const logSelectorEvent = data => ({
	type: types.SELECTOR,
	eventData: data
});

export const logSearchEvent = (action, query) => ({
	type: types.GLOBAL_SEARCH,
	action: action,
	query: query
});

export const logStoreSearchEvent = (action, label) => ({
	type: types.STORE_SEARCH,
	action: action,
	label: label
});

export const logVisualizerPreview = data => ({
	type: types.VISUALIZER_PREVIEW,
	data: data
});

//appointment scheduler actions
const dataLayer = window.dataLayer || [];

export const logger = store => next => action => {
	const state = store.getState();
	switch (action.type) {
		//state.page is 0 based, but analytics expects 1 based...
		case types.ERROR_ENCOUNTERED:
		case types.VALIDATION_ERROR:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Error",
				action: action.error,
				label: ""
			});
			break;
		case types.NEXT_STEP:
			dataLayer.push(
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page + 1,
					label: "Complete"
				},
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page + 2,
					label: "Start"
				}
			);
			if (state.appState.page === 1) {
				dataLayer.push({
					event: "MakeAppt",
					category: "Appt Date",
					action: Math.abs(
						moment(
							state.dateState.date + state.dateState.time,
							"YYYY-MM-DDHmm"
						).diff(moment(), "hours")
					),
					label: state.dateState.time
				});
			}
			break;
		case types.PREV_STEP:
			dataLayer.push(
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page + 1,
					label: "Complete"
				},
				{
					event: "MakeAppt",
					category: "Appt Step",
					action: state.appState.page,
					label: "Start"
				}
			);
			break;
		case types.PHONE_CLICKED:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Call",
				action: action.location,
				label: ""
			});
			break;
		case types.USER_SELECTED_SERVICES:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Category",
				action: action.selectedServices,
				label: ""
			});
			break;
		// case types.RECEIVE_STORE_INFO:
		// 	dataLayer.push({
		// 		event: "MakeAppt",
		// 		category: "Appt Step",
		// 		StoreIDVar: action.storeInfo.storeId,
		// 		action: 1,
		// 		label: "Start"
		// 	});
		// 	break;
		case types.PAGE_VIEW:
			dataLayer.push(action.data);
			break;
		case types.RECEIVE_APPOINTMENT_CONFIRMATION:
			dataLayer.push({
				event: "MakeAppt",
				category: "Appt Step",
				action: 3,
				label: "Complete"
			});
			try {
				window.usabilla_live("trigger", "appointment trigger");
			} catch (a) {
				break;
			}
			break;
	}
	return next(action);
};
