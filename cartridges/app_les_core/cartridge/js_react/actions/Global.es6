//import config from "config";
import types from "./Types";

export const submitRequest = () => ({
	type: types.SUBMIT_REQUEST
});

export const completeRequest = () => ({
	type: types.COMPLETE_REQUEST
});

export const setUserLocation = location => ({
	type: types.SET_USER_LOCATION,
	uLat: location.uLat,
	uLng: location.uLng
});
export const selectColor = color => ({
	type: types.SELECT_COLOR,
	color: color
});
export const addVehicle = details => ({
	type: types.ADD_VEHICLE,
	vehicle: details
});

export const addTires = details => ({
	type: types.ADD_TIRES,
	tires: details
});

export const addWheels = details => ({
	type: types.ADD_WHEELS,
	wheels: details
});

export const errorEncountered = (error, location) => ({
	type: types.ERROR_ENCOUNTERED,
	error: error,
	location: location
});
//toggles greedyComponent property setting it to either a new component name or null
export const toggleGreedyComponent = greedyComponent => ({
	type: types.TOGGLE_GREEDY_COMPONENT,
	greedyComponent: greedyComponent
});
//stores "store" details in redux
export const addStoreDetails = details => ({
	type: types.ADD_STORE_DETAILS,
	store: details
});

//sets mystore id
export const setMyStore = (id, isNoLog) => ({
	type: types.SET_MY_STORE,
	storeId: id,
	isNoLog: isNoLog || false
});
export const setSelectedStore = id => ({
	type: types.SET_SELECTED_STORE,
	id: id
});
export const setBoltpatterns = boltpatterns => ({
	type: types.SET_BOLTPATTERNS,
	boltpatterns: boltpatterns
});

//Fetch boltpatterns from service API
export const getBoltpatterns = url => (dispatch, getState) => {
	const currentState = getState();
	if (currentState.globalState.boltpatterns !== null) {
		return currentState.globalState.boltpatterns;
	}

	dispatch(submitRequest());
	return fetch(url, {
		method: "GET",
		credentials: "same-origin" //must have to send cookies
	})
		.then(response => {
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response.json();
		})
		.then(json => dispatch(setBoltpatterns(json.BoltPatterns)))
		.catch(error =>
			dispatch(errorEncountered(error.message, "getBoltpatterns"))
		);
};
//posts mystore id to the commerce cloud
export const postMyStore = (url, id, isNoLog) => (dispatch, getState) => {
	const currentState = getState();
	if (currentState.myStore === id) {
		return true;
	}
	dispatch(submitRequest());
	return fetch(url, {
		method: "POST",
		credentials: "same-origin", //must have to send cookies
		body: JSON.stringify({ storeId: id })
	})
		.then(response => {
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response;
		})
		.then(() => dispatch(setMyStore(id, isNoLog)))
		.catch(error => dispatch(errorEncountered(error.message, "postMyStore")));
};
//posts vehicle details to the commerce cloud
export const postVehicleDetails = (url, details) => dispatch => {
	//set loading state
	dispatch(submitRequest());
	//post data to backend
	return fetch(url, {
		method: "POST",
		credentials: "same-origin", //must have to send cookies
		body: JSON.stringify(details)
	})
		.then(response => {
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response;
		})
		.then(() => dispatch(completeRequest()))
		.catch(error =>
			dispatch(errorEncountered(error.message, "postVehicleDetails"))
		);
};

//posts mystore id to the commerce cloud
export const postUserLocation = (url, location) => dispatch => {
	dispatch(setUserLocation(location));
	dispatch(submitRequest());
	return fetch(url, {
		method: "POST",
		credentials: "same-origin", //must have to send cookies
		body: JSON.stringify({ uLat: location.uLat, uLng: location.uLng })
	})
		.then(response => {
			if (!response.ok) {
				throw Error(response.statusText);
			}
			return response;
		})
		.then(() => dispatch(completeRequest()))
		.catch(error =>
			dispatch(errorEncountered(error.message, "postUserLocation"))
		);
};
