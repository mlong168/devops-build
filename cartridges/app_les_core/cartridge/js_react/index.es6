/**
 *	Application entry file. This file is responsible for:
 *	* initializing redux store used by all application components
 *	* searching DOM for react component target nodes of the format `<div class="render" data-json="{componentName:'componentA'}"...></div>`
 *	* for each of the found target nodes, dynamically resolving module dependency
 *	* initializing resolved component and rendering into corresponding target node
 */
import React from "react";
import PropTypes from "prop-types";
import config from "config";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import { rootReducer, analytics } from "./reducers";
import thunk from "redux-thunk";

import css from "../scss/default/_master.scss";

//enabling Redux development tools in non-production environments
const composeEnhancers = config.isProd
	? compose
	: window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const initialState = {
	globalState: {
		//hold name of the currently active "greedy" component - similar to modal behavior,
		//when only one is allowed to be open at the given moment
		greedyComponent: null,
		isLoading: false,
		vehicles: [],
		tires: null,
		wheels: null,
		//contains list of stores used by store locator to populate map markers
		stores: [],
		myStore: null,
		selectedStore: null,
		color: null,
		boltpatterns: null,
		uLat: null,
		uLng: null
	}
};
(() => {
	const cachedState = localStorage.getItem(config.localStorageId);
	if (cachedState) {
		const cachedData = JSON.parse(cachedState);
		initialState.globalState = {
			...initialState.globalState,
			...window.__INITIAL__,
			myStore: cachedData.myStore || null,
			vehicles: cachedData.vehicles || [],
			tires: cachedData.tires || null,
			wheels: cachedData.wheels || null,
			color: cachedData.color || null,
			boltpatterns: cachedData.boltpatterns || null,
			uLat: cachedData.uLat || null,
			uLng: cachedData.uLng || null
		};
	}
})();
//creating redux store shared between all components
const store = createStore(
	rootReducer,
	initialState, //TODO: integrate initial state with default/localStorage state
	composeEnhancers(applyMiddleware(thunk, analytics))
);

//creating context with all components from `containers` directory
const allComponents = require.context("./containers/", false, /\.es6$/);
//resolve component dependency based on provided list of components found on the page
const resolveDependencies = components => {
	return Promise.all(
		components.map(
			component =>
				new Promise(resolve => {
					try {
						resolve({
							func: allComponents(`./${component.name}.es6`).default,
							...component
						});
					} catch (e) {
						console.warn("Cannot resolve module: ", component.name, "\n", e);
						resolve({ error: e, ...component });
					}
				})
		)
	);
};

const initializeComponents = () => {
	// window.requestAnimationFrame(() => {
	//find all react components on the page
	let toRender = document.querySelectorAll(".render:not(.processed)");
	//parse HTMLCollection extracting data JSON and preserving reference to DOM element for further rendering
	let components = Array.prototype.reduce.call(
		toRender,
		(result, element) => {
			try {
				let data = JSON.parse(element.getAttribute("data-json"));
				result.push({
					name: data.componentName,
					data: data,
					target: element
				});
				element.classList.add("processed");
				return result;
			} catch (e) {
				console.warn("Error parsing component data - skipping: ", element);
				return result;
			}
		},
		[]
	);
	//resolve module dependency for all found components
	resolveDependencies(components)
		.then(components => {
			components.map((component, i) => {
				//handle the case when some of the components might have not been found
				if (!component.error) {
					//by convention component name has to be capitalized, otherwise JSX treats it as html tag
					let Comp = component.func;
					//wrap each component in Provider to give it access to store and render into target DOM
					render(
						<Provider store={store} {...css} key={i}>
							<Comp {...component.data} />
						</Provider>,
						component.target
					);
				} else {
					if (!config.isProd) {
						const Error = props => {
							let style = {
								background: "rgba(255,200,200,0.9)",
								fontWeight: "bold",
								color: "#FF0000"
							};
							return (
								<div className="error-component" style={style}>
									<strong>{props.name}: </strong>
									{props.error.message}
								</div>
							);
						};
						Error.propTypes = {
							name: PropTypes.string,
							error: PropTypes.string
						};
						render(<Error {...component} />, component.target);
					}
				}
			});
		})
		.catch(e => console.error(e)); // eslint-disable-line no-console
};

const observer = new MutationObserver(mutationsList => {
	for (var mutation of mutationsList) {
		if (mutation.type == "childList" && mutation.addedNodes.length > 0) {
			initializeComponents();
		}
	}
});
let node = document.querySelector(".search__content");
if (node) {
	observer.observe(node, {
		childList: true,
		subtree: true
	});
}
document.addEventListener("DOMContentLoaded", initializeComponents);
