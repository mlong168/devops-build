<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules" />
<isscript>
	importScript( "util/ViewHelpers.ds" );
</isscript>
<isset name="Product" value="${pdict.Product}" scope="page" />
<iscomment>In the product pipeline, if a product was not found for the selected attributes, we need to refresh the ProductVariationModel using the default selected variant</iscomment>
<isset name="ProductMainCat" value="${pdict.Product.variant ? pdict.Product.masterProduct.getClassificationCategory() : pdict.Product.getClassificationCategory()}" scope="pdict" />
<isset name="ProductMainCat" value="${pdict.ProductMainCat}" scope="page" />
<isset name="isQuickView" value="${pdict.CurrentHttpParameterMap.source.stringValue == 'quickview' || pdict.CurrentHttpParameterMap.source.stringValue == 'cart' || pdict.CurrentHttpParameterMap.source.stringValue == 'giftregistry' || pdict.CurrentHttpParameterMap.source.stringValue == 'wishlist'}" scope="page"/>

<div class="grid-container">

	<isinclude template="components/productbreadcrumbs"/>

	<div class="render" data-json="<isinclude url=${URLUtils.url('Account-ShowEntryData', 'category', ProductMainCat.ID)} sf-toolkit='off'/>"></div>
	<hr class="hide-for-small-only"/>

	<section class="product__brief grid-x grid-margin-x show-for-small-only">
		<isinclude template="product/productbrief"/>
	</section>

	<section class="grid-x grid-margin-x">
		<div class="cell small-12 medium-6">

			<isinclude template="product/components/productimages"/>
			<iscomment>List of features and benefits</iscomment>
			<isif condition="${!empty(Product.custom.benefitContent)}">
				<section class="productFeatures hide-for-small-only">
					<h6>${Resource.msg('product.features.header','product',null)}</h6>
					<ul class="styled-list">
						<isloop items="${Product.custom.benefitContent}" var="benefitContent" >
							<li>${benefitContent}</li>
						</isloop>
					</ul>
				</section>
			</isif>
			<iscomment><isprint value="${Product.shortDescription}" encoding="off"/></iscomment>
		</div>

		<div class="cell small-12 medium-5 medium-offset-1 product-detail">
			<iscomment>
				<h1 class="product-name" itemprop="name"><isprint value="${Product.name}"/></h1>
			</iscomment>
			<div id="product-content">
				<isinclude template="product/productcontent"/>
			</div>
			<div class="render product__store" data-json="<isinclude url=${URLUtils.url('ReactComponent-DynamicShow', 'type', 'confirmStore', 'view', 'PDP')}/>"></div>
			<iscontentasset aid="${'pdp-'+ProductMainCat.ID+'-free-service'}"/>

			<iscomment>List of features and benefits</iscomment>
			<isif condition="${!empty(Product.custom.benefitContent)}">
				<section class="productFeatures show-for-small-only">
					<h6>${Resource.msg('product.features.header','product',null)}</h6>
					<ul class="styled-list">
						<isloop items="${Product.custom.benefitContent}" var="benefitContent" >
							<li>${benefitContent}</li>
						</isloop>
					</ul>
				</section>
			</isif>
		</div>
	</section>
</div>
<isif condition="${!isQuickView}">
		<section class="productInfo">
			<div class="grid-container">
				<h6 class="header--large header--sectionHeader">${Resource.msgf('product.tab.typedetails', 'product', null,ProductMainCat.getDisplayName())}</h6>
				<hr/>
				<div class="grid-x grid-margin-x grid-margin-y">
					<div class="cell small-12 medium-8">

						<isif condition="${!empty(Product.name)}">
							<h2 class="product-name" itemprop="name"><isprint value="${Product.name}"/></h2>
						</isif>


						<isif condition="${ProductMainCat.ID == 'wheel'}">
							<p class="product-finish"><isprint value="${Product.custom.finishDescription}"/></p>
						</isif>

						<isif condition="${!empty(Product.longDescription) && Product.longDescription.markup.length > 0}">
							<isprint value="${Product.longDescription}" encoding="off"/>
						</isif>

						<isinclude template="product/components/group"/>
						<isif condition="${ProductMainCat.ID == 'wheel'}">
						<br>
							<div class="see-other-links">
								<p>
									<a href="${URLUtils.url('Search-Show','cgid','wheels')}" alt="${Resource.msg('product.wheels.seeother','product',null)}">${Resource.msg('product.wheels.seeother','product',null)}</a>
								</p>
							</div>
						</isif>
					</div>
					<aside class="cell small-12 medium-3 medium-offset-1">
						<isset name="WarrantyValue" value="${null}" scope="page" />
						<isif condition="${ProductMainCat.ID == 'tire'}">
							<isif condition="${!empty(Product.custom.warranty) && (Product.custom.warranty > 0) }">
								<isset name="WarrantyValue" value="${Resource.msgf('product.warranty.tire.miles','product',null,dw.util.StringUtils.formatNumber(Product.custom.warranty,'#,###'))}" scope="page" />
							</isif>
							<isset name="warrantyContent" value="${getWarrantyContentWithValue('pdp-'+ProductMainCat.ID+'-warranty',WarrantyValue)}" scope="page" />
						<iselseif condition="${ProductMainCat.ID == 'wheel'}">
							<isif condition="${!empty(Product.custom.warranty) && (Product.custom.warranty > 0)}">
								<isset name="WarrantyValue" value="${Resource.msgf('product.warranty.wheels.year','product',null)}" scope="page" />
							</isif>
							<isset name="warrantyContent" value="${getWarrantyContentWithValue('pdp-'+ProductMainCat.ID+'-warranty',WarrantyValue)}" scope="page" />
						</isif>
						<isif condition="${!empty(warrantyContent)}">
							<isprint value="${warrantyContent}" encoding="off" />
						</isif>
					</aside>
					<isif condition="${ProductMainCat.ID == 'wheel'}">
						<div class="cell small-12">
							<div class="productInfo__carRendering">
								<iscomment>react component to render vehicle with stored car && this wheel ID(SKU)</iscomment>
								<div class="render" data-json="<isinclude url=${URLUtils.url('ReactComponent-VehicleRenderWheelPDP', 'sku', Product.ID)} sf-toolkit='off'/>"></div>
							</div>
						</div>
					</isif>
				</div>
			</div>
		</section>

<div class="grid-container">
    <isset name="bestValueContent" value="${getBestValueContent('pdp-'+ProductMainCat.ID+'-best-value')}" scope="page" />
    <isif condition="${!empty(bestValueContent)}">
	    <section class="productBestValue">
			<h6 class="header--large header--sectionHeader">${Resource.msgf('product.best.value.promise','product',null,ProductMainCat.getDisplayName())}</h6>
			<hr/>
	    	<isprint value="${bestValueContent}" encoding="off" />
	    </section>
    </isif>

	<isif condition="${!empty(pdict.fees) && ProductMainCat.ID != 'wheel'}">
		<isinclude template="product/components/serviceprice"/>
	</isif>
</div>
     <iscontentasset aid="${'pdp-'+ProductMainCat.ID+'-our-promise'}"/>
<div class="grid-container">
    <iscomment>
    	<isinclude template="product/components/reviews"/></div>
    </iscomment>
 	<isscript>
		// get all orderable cross sell recommendations (1 = cross sell)
		var recommendations = Product.getOrderableRecommendations(1).iterator();
		var recProducts = new dw.util.ArrayList();
		var counter = 0;
		// display 20 recommendations at maximum
		while (recommendations.hasNext() && counter < 20) {
			var recommendedProduct = recommendations.next().getRecommendedItem();
			recProducts.add(recommendedProduct);
			counter++;
		}
	</isscript>

	<isif condition="${recProducts.size() > 0}">
		<div class="productRecommendations contentSection grid-x grid-margin-x grid-margin-y">
			<header class="cell small-12">
				<h6 class="header--large header--sectionHeader">
					<isinclude url="${URLUtils.url('Product-GetVehicleInformation', 'categoryID', ProductMainCat.ID)}"/>
				</h6>
				<hr/>
			</header>
		 	<iscomment>@TODO : Insert the checks so that we can populate More Tires, More Wheels, etc...</iscomment>
			<isinclude template="product/components/recommendations"/>
		</div>
	</isif>
	<isif condition="${ProductMainCat.ID == 'wheel'}">
		<iscontentasset aid="pdp-wheel-disclaimer"/>
	<iselse/>
		<iscontentasset aid="pdp-tire-disclaimer"/>
	</isif>
</div>
	<isset name="categoryID" value="${ProductMainCat.ID}" scope="page" />
</isif>
