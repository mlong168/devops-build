<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules"/>
<iscomment>
The <!—BEGIN/END… comments are control statements for the build cartridge which can be found in xChange https://xchange.demandware.com/docs/DOC-5728 or checked out from SVN at https://svn2.hosted-projects.com/cs_europe/DWTechRepository/cartridges/build_cs
If you are not using the build cartridge the comments can be safely removed.
</iscomment>

<meta charset=UTF-8>

<iscomment>See https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md#x-ua-compatible</iscomment>
<meta http-equiv="x-ua-compatible" content="ie=edge">

<iscomment>See https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md#mobile-viewport</iscomment>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script type="text/javascript">
    /*Global function triggered by googleApi once it's loaded - generates global event to notify listeners*/
    window.googleLoaded = function() {
    	window.googleHasLoaded = true;
      var event = document.createEvent("Event");
      event.initEvent("googleApiLoaded", false, true); //needed to support IE11.
    	document.dispatchEvent(event);
    };
</script>

<iscomment>the page title calculated by the app </iscomment>

<isif condition="${dw.system.System.getInstanceType() != dw.system.System.PRODUCTION_SYSTEM}">
	<title><isif condition="${!empty(pdict.CurrentPageMetaData.title)}"> ${pdict.CurrentPageMetaData.title}<iselse> ${Resource.msg('global.default.page.title', 'locale', null)}</isif> </title>
<iselse/>
  <title><isprint value="${pdict.CurrentPageMetaData.title}" encoding="off" /></title>
</isif>

<iscomment>FAVICON ICON: (website icon, a page icon or an urlicon) 16x16 pixel image icon for website</iscomment>
<link href="${URLUtils.staticURL('/favicon.png')}" rel="shortcut icon" />

<iscomment>include all meta tags</iscomment>
<iscomment>
	This Content-Type setting is optional as long as the webserver transfers
	the Content-Type in the http header correctly. But because some browsers or
	proxies might not deal with this setting in the http header correctly, a
	second setting can help to keep everything just fine.
</iscomment>

<iscomment>Automatic generation for meta tags.</iscomment>
<meta name="description" content=" <isif condition="${!empty(pdict.CurrentPageMetaData.description)}">${pdict.CurrentPageMetaData.description}</isif>"/>
<meta name="keywords" content=" <isif condition="${!empty(pdict.CurrentPageMetaData.keywords)}">${pdict.CurrentPageMetaData.keywords}</isif>"/>


<isinclude template="components/header/htmlhead_UI"/>

<!--  UI -->
<link rel="stylesheet" href="${URLUtils.staticURL('/css/style.css')}" />

<iscomment>Gather device-aware scripts</iscomment>
<isinclude url="${URLUtils.url('Home-SetLayout')}"/>

<iscomment>### Data Layer Global Elements ###</iscomment>
<isdatalayer pageid="global"/>
<isdltag>

<script>
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push(window.universal_variable);
</script>

<iscomment>GTM head script include </iscomment>
<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('dataLayerEnabled')}">
	<isprint value="${dw.system.Site.getCurrent().getCustomPreferenceValue('dataLayerGTMScript')}" encoding="off" />
</isif>

<iscomment>React initial state</iscomment>
<isscript>
    importScript( "util/ViewHelpers.ds" );
</isscript>
<script>
    var initial = "<isprint encoding="jsblock" value="${ViewHelpers.getInitialState()} "/>";
    window.__INITIAL__ = JSON.parse(initial);
</script>
