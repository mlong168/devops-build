import common from "./config.common";

const ENV_BASE_URL = "http://localhost:9000";
const commonConfig = common(ENV_BASE_URL);
const config = {
	...commonConfig,
	isProd: true
};
export default config;
