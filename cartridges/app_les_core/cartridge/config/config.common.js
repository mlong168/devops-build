const config = () => ({
	isProd: false,
	allowedTags: ["strong", "a", "i", "b", "em", "br", "sup", "p"],
	allowedAttributes: {
		a: ["href", "name", "target"],
		img: ["src"]
	},
	maxLoadIndex: 500,
	localStorageId: "lesschwab",
	disallowedLocationTypes: ["intersection", "park", "natural_feature"],
	vehicleDefaultColor: "BB1919",
	availableColors: [
		"0B2D78",
		"227544",
		"000000",
		"8FADD8",
		"BB1919",
		"DE9832",
		"DDDDDD",
		"FFFFFF"
	],
	fetchErrorMessage: "Something went wrong.",
	fetchErrorHeading: "Vehicle Entry Not Available",
	fetchErrorDetail:
		"Sorry about that! We\u0027ll be up and running shortly. Please come by soon.",
	breakpoints: {
		maxMobile: 768,
		maxTablet: 1024,
		maxMobilePx: "768px"
	},
	geolocationClientOptions: {
		timeout: 40000,
		enableHighAccuracy: false
	},
	googleMaps: {
		url: "https://www.google.com/maps/dir/",
		geocodeBounds: {
			west: -134.5728,
			east: -100.0,
			north: 58.3592,
			south: 33.4849
		},
		boundsPadding: 5,
		componentRestrictions: {
			country: "US"
		},
		mapOptions: {
			zoom: 8, //15,
			scrollwheel: false,
			scaleControl: false,
			fullscreenControl: false,
			mapTypeControl: false,
			streetViewControl: false
		},
		defaultPosition: {
			latitude: 44.284381,
			longitude: -121.175054
		}
	},
	distanceMeasurement: "mi", //used to display "mi" after the number of miles.
	navMouseHoverTimeout: 500 //millseconds
});
export default config;
