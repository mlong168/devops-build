import common from "./config.common";

const ENV_BASE_URL = "";
const commonConfig = common(ENV_BASE_URL);
const config = {
	...commonConfig,
	isProd: true
};
export default config;
