"use strict";

/**
 *	Provides functions to make work around the search refinement filters
 *	@namespace
 *
 */

var URLUtils = require("dw/web/URLUtils");

/*
* Build the URL used for the refinement to include refinement values.
* @param refinementAttributeID {string} the product search hit, given by the Scripting API
*/
function buildRefinementValuesURL(refinementAttributeID) {
	var params = request.httpParameterMap;
	var refinementValuesURL = URLUtils.url("Search-RefinementValues");

	var parameterNames = params.getParameterNames().toArray();
	for (i = 0; i < params.getParameterCount(); i++) {
		refinementValuesURL.append(
			parameterNames[i],
			params.get(parameterNames[i])
		);
	}
	refinementValuesURL.append("RefinementAttributeID", refinementAttributeID);
	return refinementValuesURL;
}

exports.buildRefinementValuesURL = buildRefinementValuesURL;
