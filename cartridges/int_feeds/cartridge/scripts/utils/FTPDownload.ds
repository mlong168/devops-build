/**
 * Demandware Script File
 * 
 *
 * Copies files from a remote FTP-Location
 *
 *   @input HostURL 		: String (S)FTP-Service-URL.
 *   @input Port 			: String Port number.
 *   @input UserID 			: String The User-ID.
 *   @input Password 		: String The Password.
 *   @input FilePattern 	: String Input File pattern to search in remote folder(default is  "^[\\w\-]{1,}\\.xml$" (*.xml)).
 *   @input SourceFolder 	: String Remote folder of FTP Server.
 *   @input TargetFolder 	: String Local folder in which will placed files, relatively to IMPEX/.
 *   @input SecureFtp 		: String Type of connection (FTP / SFTP).
 *   @input DeleteFile 		: Boolean When file is uploaded, delete or keep it? ("Delete" / "Keep on server").
 *   @input Timeout 		: Number The timeout for this client, in milliseconds.
 *   @output ErrorMsg 		: String An Error message
 *
 */
importPackage(dw.system);
importPackage(dw.net);
importPackage(dw.io);
importPackage(dw.util);


/**
 * The main function.
 * 
 * @param pdict : PipelineDictionary The pipeline dictionary.
 * 
 * @returns Number If the given directory or the result is empty, PIPELET_ERROR is returned. Otherwise PIPELET_NEXT.
 */
function execute(pdict: PipelineDictionary): Number {
	
    var filename : String = pdict.FilePattern;
	
    var _hostURL : String = pdict.HostURL;
	var _port : Number = !empty(pdict.Port) ? new Number(pdict.Port) : null;
	var _userID : String = pdict.UserID;
	var _password : String = pdict.Password;
	var _filePattern : String = empty(filename) ? "^[\\w\-]{1,}\\.xml$" : filename;
	var _sourceFolder : String = pdict.SourceFolder;
	var _targetFolder : String = pdict.TargetFolder;
	var _secureFtp : String = pdict.SecureFtp;
	var _deleteFile : Boolean = pdict.DeleteFile;
	var _timeout : Number = pdict.Timeout;
    var ftpClient: Object;
    var copyResult: Boolean = true;
    var sftp: Boolean = false;

    //Check for mandatory fields
	try{
	    if (!empty(_hostURL) && !empty(_secureFtp) && !empty(_deleteFile) && !empty(_sourceFolder) && !empty(_targetFolder)) {
			
			//Check for connection type
	        var sftp: boolean = false;
	        if ("FTP".equals(_secureFtp)) {
	            ftpClient = new FTPClient();
	        } else {
	            ftpClient = new SFTPClient();
	            sftp = true;
	        }
	        //set connection timeout
	        if (ftpClient != null && _timeout != null && _timeout > 0) {
	            ftpClient.setTimeout(_timeout);
	        }
	
	        // Try to connect with or without port number
	        try{
		        if (!empty(_userID) && !empty(_password)) {
		            if (!empty(_port)) {
		                ftpClient.connect(_hostURL, _port, _userID, _password);
		            } else {
		                // use default port if port is not provided in parameter
		                ftpClient.connect(_hostURL, _userID, _password);
		            }
		        } else {
		            if (sftp) {
		                pdict.ErrorMsg = "User-ID and Password are manadatory for SFTP-Connection.";
		                return PIPELET_ERROR;
		            } else {
		                ftpClient.connect(_hostURL);
		            }
		        }
		   	}catch(e){
	        	Logger.getLogger("CustomJobs", "CustomJobs").error("FTPUpload.ds :: Error while connecting to the server - "+ e.message());
	        }
	        if (ftpClient.connected) {
	             //Transfer file(s) to location from (FTP or SFTP) and delete file
	            try{
	            	copyResult = copyFilesToTarget(ftpClient, _sourceFolder, _filePattern, _targetFolder, _deleteFile);
	            }catch(e){
	            	Logger.getLogger("CustomJobs", "CustomJobs").error("FTPUpload.ds :: Can't trasfer file - "+ e.message());
	            }
	        } else {
	            pdict.ErrorMsg = "The connection couldn't be established.";
	            return PIPELET_ERROR;
	        }
	    } else {
	        pdict.ErrorMsg = "One or more mandatory parameters are missing.";
	        return PIPELET_ERROR;
	    }
	}catch(e){
		Logger.getLogger("CustomJobs", "CustomJobs").error("FTPUpload.ds :: An Exception occured - "+ e.message());
	}finally{
        ftpClient.disconnect();
    }

    if (!copyResult) {
        pdict.ErrorMsg = "File-List was empty.";
        return PIPELET_ERROR;
    }
    
    return PIPELET_NEXT;
}
/**
 *	Copy (and delete) files from a remote FTP-Folder locally
 *	@param ftpClient 	: Object 	FTP Client used
 *	@param sourceFolder : String 	source Folder
 *	@param filePattern 	: String 	The pattern for the filenames
 *	@param targetFolder : String 	target FTP Folder
 *	@param deleteFile 	: Boolean 	Flag if files should be deleted after successful copying 
 *	
 *	@returns Boolean If files were found at the specified location.
 **/
function copyFilesToTarget(ftpClient: Object,
    sourceFolder: string,
    filePattern: string,
    targetFolder: string,
    deleteFile: boolean): boolean {
    var cd: boolean = ftpClient.cd(sourceFolder);
    var regExp: RegExp = new RegExp(filePattern);
    var fileInfoList: Array = ftpClient.list();
	
	Logger.getLogger("CustomJobs", "CustomJobs").debug("FTPDownload.ds :: Total number of file found on FTP - "+ filePattern + "--"+ fileInfoList.length);
	//Files or folder in the location process each and transfer file to location based on RegEx
    if (fileInfoList != null && fileInfoList.length > 0) {
    	var totalFiles : Number = 0;
        for (var i: Number = 0; i < fileInfoList.length; i++) {
            var fileInfo: FTPFileInfo = fileInfoList[i];
            if (regExp.test(fileInfo.name)) {
            	Logger.getLogger("CustomJobs", "CustomJobs").debug("FTPDownload.ds :: Current Downloading file - "+ fileInfo.name);
            	//Transfer file to target location
                copyFileToTargetFolder(ftpClient, targetFolder, fileInfo.name, deleteFile);
                Logger.getLogger("CustomJobs", "CustomJobs").debug("FTPDownload.ds :: Successfully Downloading file - "+ fileInfo.name);
                totalFiles = i+1;
            }
        }
        Logger.getLogger("CustomJobs", "CustomJobs").debug("FTPDownload.ds :: Total number of file downloaded - "+ totalFiles);
        return true;
    }
	Logger.getLogger("CustomJobs", "CustomJobs").debug("FTPUpload.ds :: No file found on Remote file Location");
    return true;
}
/**
 *	Copy (and delete) a file from a remote FTP-Folder locally
 *	@param ftpClient 	: Object 	FTP Client used
 *	@param targetFolder : String 	target FTP Folder
 *	@param fileName 	: String 	The file to copy
 *	@param deleteFile 	: Boolean	Flag if files should be deleted after successful copying 
 *
 **/
function copyFileToTargetFolder(ftpClient: Object, targetFolder: string, fileName: string, deleteFile: boolean) {
    var targetDirStr: string = targetFolder.charAt(0).equals("/") ? targetFolder : "/" + targetFolder;
    var theDir: File = new File("IMPEX" + targetDirStr);
    theDir.mkdirs();
	try{
	    var theFile: File = new File(theDir.fullPath + "/" + fileName);
	    ftpClient.getBinary(fileName, theFile);
	    theFile.createNewFile();
	}catch(e){
		Logger.getLogger("CustomJobs", "CustomJobs").error("FTPUpload.ds :: An Exception occured during local file creation - "+ e.message());
	}
	
	try{
		if (deleteFile) {
        	ftpClient.del(fileName);
   		}
	}catch(e){
		Logger.getLogger("CustomJobs", "CustomJobs").error("FTPUpload.ds :: An Exception occured during remote file delete - "+ e.message());
	}

}