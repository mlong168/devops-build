'use strict';

/**
 * Model for search functionality.
 *
 * @module models/SearchModel
 */

/* API Includes */
var CatalogMgr = require('dw/catalog/CatalogMgr');
var ContentSearchModel = require('dw/content/ContentSearchModel');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var Resource = require('dw/web/Resource');
var app = require('~/cartridge/scripts/app');

var Stores = require('~/cartridge/controllers/Stores');

/**
 * Search helper class providing enhanced search functionality.
 * @class module:models/SearchModel~SearchModel
 */
var SearchModel = ({

});

/**
 * Initializes the given search model using the HttpParameterMap.
 *
 * @param {dw.catalog.SearchModel} searchModel SearchModel to initialize.
 * @param {dw.web.HttpParameterMap} httpParameterMap HttpParameterMap to read common search parameters from.
 * @returns {dw.catalog.SearchModel} Search model.
 */
SearchModel.initializeSearchModel = function (searchModel, httpParameterMap) {

    if (searchModel) {
        if (httpParameterMap.q.submitted) {
            searchModel.setSearchPhrase(httpParameterMap.q.value);
        }

        if (httpParameterMap.psortb1.submitted && httpParameterMap.psortd1.submitted) {
            searchModel.setSortingCondition(httpParameterMap.psortb1.value, httpParameterMap.psortd1.intValue);
        }

        if (httpParameterMap.psortb2.submitted && httpParameterMap.psortd2.submitted) {
            searchModel.setSortingCondition(httpParameterMap.psortb2.value, httpParameterMap.psortd2.intValue);
        }

        if (httpParameterMap.psortb3.submitted && httpParameterMap.psortd3.submitted) {
            searchModel.setSortingCondition(httpParameterMap.psortb3.value, httpParameterMap.psortd3.intValue);
        }

                
        var nameMap = httpParameterMap.getParameterMap('prefn');
        var valueMap = httpParameterMap.getParameterMap('prefv');
        var minValueMap = httpParameterMap.getParameterMap('prefmin');
        var maxValueMap = httpParameterMap.getParameterMap('prefmax');

    	for (var i in nameMap) {
            if (valueMap[i]) {
	            searchModel.addRefinementValues(nameMap[i], valueMap[i]);
    	    }
            else{
            	if(minValueMap[i] && maxValueMap[i]){
            		searchModel.setRefinementValueRange(nameMap[i], minValueMap[i], maxValueMap[i]);
            	}
            }
    	}
    }
    return searchModel;
};

/**
 * Creates and initializes a {dw.catalog.ProductSearchModel} based on the given HTTP parameters.
 *
 * @param {dw.web.HttpParameterMap} httpParameterMap HttpParameterMap to read product search parameters from.
 * @returns {dw.catalog.ProductSearchModel} Created and initialized product serach model.
 */
SearchModel.initializeProductSearchModel = function (httpParameterMap) {
    var productSearchModel = this.initializeSearchModel(new ProductSearchModel(), httpParameterMap);

    productSearchModel.setRecursiveCategorySearch(true);

    if (httpParameterMap.pid.submitted) {
        productSearchModel.setProductID(httpParameterMap.pid.value);
    }

    if (httpParameterMap.pmin.submitted && httpParameterMap.pmin.doubleValue >=0.1) {
        productSearchModel.setPriceMin(httpParameterMap.pmin.doubleValue);
    }else if(httpParameterMap.cgid.submitted && dw.system.Site.getCurrent().getCustomPreferenceValue('categoryHasPrice').indexOf(httpParameterMap.cgid.value) >= 0){
    	// Add Price Refinement By Default for All Searches
       	productSearchModel.setPriceMin(0.1);
    }
    if (httpParameterMap.pmax.submitted) {
        productSearchModel.setPriceMax(httpParameterMap.pmax.doubleValue);
    }

    var sortingRule = httpParameterMap.srule.submitted ? CatalogMgr.getSortingRule(httpParameterMap.srule.value) : null;
    if (sortingRule) {
        productSearchModel.setSortingRule(sortingRule);
    }

    // only add category to search model if the category is online
    if (httpParameterMap.cgid.submitted) {
        var category = CatalogMgr.getCategory(httpParameterMap.cgid.value);
        if (category && category.isOnline() && productSearchModel) {
            productSearchModel.setCategoryID(category.getID());
        }

    }
   	

    return productSearchModel;
};

/**
 * Creates and initializes a {dw.content.ContentSearchModel} based on the given HTTP parameters.
 *
 * @param {dw.web.HttpParameterMap} httpParameterMap HttpParameterMap to read content search parameters from.
 * @returns {dw.content.ContentSearchModel} Created and initialized product serach model.
 */
SearchModel.initializeContentSearchModel = function (httpParameterMap) {
    var contentSearchModel = this.initializeSearchModel(new ContentSearchModel(), httpParameterMap);
    
    // Setting display order of assets based on 'displayOrder' in ASCENDING ORDER
    contentSearchModel.setSortingCondition('displayOrder', 1);
    
    contentSearchModel.setRecursiveFolderSearch(true);
    if (httpParameterMap.cid.submitted) {
        contentSearchModel.setContentID(httpParameterMap.cid.value);
    }

    if (httpParameterMap.fdid.submitted) {
        contentSearchModel.setFolderID(httpParameterMap.fdid.value);
    }
    return contentSearchModel;
};


/**
 * Returns the refinement values for given attributeID by applying provided refinements
 */
SearchModel.getRefinementValues = function (cgid, refinements, attributeID) {
	var refinementValues = [], Refinements;
	var productSearchModel = new ProductSearchModel();
	
	productSearchModel.setPriceMin(0.1);

	// only add category to search model if the category is online
    if (!empty(cgid)) {
        var category = CatalogMgr.getCategory(cgid);
        if (category && category.isOnline() && productSearchModel) {
            productSearchModel.setCategoryID(category.getID());
        }
    }
    
    if (!empty(refinements)) {
    	//do something
    	for (var i =0; i< refinements.length; i++) {
    		productSearchModel.addRefinementValues(refinements[i].key, refinements[i].value);
    	}
    }
    //search with cgid and applied refinements
    productSearchModel.search();
    
    Refinements = productSearchModel.refinements;
    
    if (!empty(Refinements) && !empty(Refinements.refinementDefinitions)) {
	    for each (var RefinementDefinition in Refinements.refinementDefinitions) {
			if (RefinementDefinition.attributeID == attributeID && RefinementDefinition.isAttributeRefinement()) {
				
				for each (var RefinementValue in Refinements.getRefinementValues(RefinementDefinition)) {
					refinementValues.push(RefinementValue);
				}
				return refinementValues;
			}
		}
    }
    return refinementValues;
};

SearchModel.initializeProductSearchModelByCustomParams = function (customParam) {
	var refinementValues = [], Refinements;
	var productSearchModel = new ProductSearchModel();
	
	productSearchModel.setRecursiveCategorySearch(true);
	
	// Add Price Refinement By Default for All Searches
   	productSearchModel.setPriceMin(0.1);
	// only add category to search model if the category is online
    if ('cgid' in customParam && !empty(customParam.cgid)) {
        var category = CatalogMgr.getCategory(customParam.cgid);
        if (category && category.isOnline() && productSearchModel) {
            productSearchModel.setCategoryID(category.getID());
        }
    }
    
    if (customParam.refinements && !empty(customParam.refinements)) {
    	//do something
    	var refinements = customParam.refinements;
    	for (var i =0; i< refinements.length; i++) {
    		productSearchModel.addRefinementValues(refinements[i].key, refinements[i].value);
    	}
    }
    
    return productSearchModel;
}

SearchModel.getRecommendedProducts = function getRecommendedProducts(categoryId,params){
	var recommendations = [];
	var productCount = 1;
	// Fill Spot 1 with 1 GOOD Product
	recommendations = recommendations.concat(getGoodProduct(categoryId,1,params));
	var count = 1;
	
	var good = getGoodProduct(categoryId,3,params);
	
	var better = getBestOrBetterProduct(categoryId,Resource.msg('search.recommended.better', 'search', null),3,params);
	
	var best = getBestOrBetterProduct(categoryId,Resource.msg('search.recommended.best', 'search', null),3,params);
	
	// G-BT-BS or G-BT-BT
	if(!empty(better)){
		if(better.length > 0){
			if(!empty(best) && best.length>0){
				for(var product in better){
					var pos = recommendations.map(function(e) { return e.ID; }).indexOf(better[product].ID);
					if(pos < 0){
						recommendations.push(better[product]);
						break;
					}
				}
				
				for(var product in best){
					var pos = recommendations.map(function(e) { return e.ID; }).indexOf(best[product].ID);
					if(pos < 0){
						recommendations.push(best[product]);
						break;
					}
				}
				
				// IDEAL 1-1-1
				return recommendations;
			}
			else if (better.length >= 2){
				var pos = recommendations.map(function(e) { return e.ID; }).indexOf(better[0].ID);
				(recommendations.map(function(e) { return e.ID; }).indexOf(better[1].ID) < 0 ) ?recommendations.push(better[1]): ''; 
				(recommendations.map(function(e) { return e.ID; }).indexOf(better[0].ID) < 0 ) ?recommendations.push(better[0]): '';
			}
			else{
				(recommendations.map(function(e) { return e.ID; }).indexOf(better[0].ID) < 0 ) ?recommendations.push(better[0]): ''; 
			}
		}	
	}
	
	// G-BT-BS or G-BS-BS
	if(recommendations.length == 3){
		return recommendations;
	}
	else{
		var neededBestCount = 0;
		if(recommendations.length == 2){
			neededBestCount = 1; 
		}
		else{
			neededBestCount = 2;
		}
		for(var product in best){
			var pos = recommendations.map(function(e) { return e.ID; }).indexOf(best[product].ID);
			if(pos < 0){
				recommendations.push(best[product]);
				neededBestCount-- ;
				if(neededBestCount == 0){
					return recommendations;
				}
			}
		}
	}
	
	return recommendations;
}

function getGoodProduct(categoryID,count,params){
	
	var productSearchModel;
	// Get GOOD Product on Sale Using Product Search Model
	// Constructs the search based on the HTTP params and sets the categoryID.
	var Search = app.getModel('Search');
	if(params!==null){
		productSearchModel = Search.initializeProductSearchModel(params);
	}
	else{
		productSearchModel = new ProductSearchModel();
	}
    
    var PriceBookMgr =  require('dw/catalog/PriceBookMgr');
    var storeID = !empty(Stores.GetPreferredOrNearestStore().store)?Stores.GetPreferredOrNearestStore().store.ID:"";
    var salePriceBookName = storeID + '-'+session.currency.currencyCode.toLowerCase()+'-sale-prices';
   	// GET ALL THE GOOD PRODUCTS FROM SALE PRICE BOOK
    productSearchModel.setCategoryID(categoryID);
   	productSearchModel.addRefinementValues(Resource.msg('search.recommended.valuestep', 'search', null),Resource.msg('search.recommended.good', 'search', null));
   	var sortingRule = dw.catalog.CatalogMgr.getSortingRule(Resource.msg('search.recommended.unitssold', 'search', null));
   	if(sortingRule !== null){
   		productSearchModel.setSortingRule(sortingRule);
   	}
    
   	productSearchModel.search();
   	var iter = productSearchModel.getProducts();
   	var goodProducts = [];
   	var counter = 0;
   	while(iter.hasNext()){
   		if(counter >= count){
				break;
			}
   		var product = iter.next();
   		if (product.getPriceModel()!==null && product.getPriceModel().getPriceBookPrice(salePriceBookName).available == true){
			counter++;
			goodProducts.push(product);
   		}
   	}

   	if(!empty(goodProducts) && goodProducts.length >= count){
   		// Return if "count" number of GOOD Products added
   		return goodProducts;
   	}
   	
   	// Return Lowest Price Products if "count" number of GOOD Products not found
   	productSearchModel.removeRefinementValues(Resource.msg('search.recommended.valuestep', 'search', null),null);
   	sortingRule = dw.catalog.CatalogMgr.getSortingRule(Resource.msg('search.recommended.pricelowtohigh', 'search', null));
   	if(sortingRule !== null){
   		productSearchModel.setSortingRule(sortingRule);
   	}
    
   	productSearchModel.search();
   	
   	var iterator = productSearchModel.getProducts();
   	
   	while(iterator.hasNext()){
   		if(counter >= count){
				break;
			}
   		var product = iterator.next();
   		if (product.getPriceModel()!==null && product.getPriceModel().price.available!==false){
				counter++;
				goodProducts.push(product);
   		}
   	}
   	return goodProducts;
}

/*
 * Calculate the BETTER / BEST for recommended sort 
 */


function getBestOrBetterProduct(categoryID,valueStep, count,params){

   	var ownedNotOnSale = [];
   	var notOwnedNotOnSale = [];
   	var notOwnedOnSale = [];
   	var ownedOnSale = [];
   	
	var productSearchModel;
	var Search = app.getModel('Search');
	if(!empty(params)){
		productSearchModel = Search.initializeProductSearchModel(params);
	}
	else{
		productSearchModel = new ProductSearchModel();
	}
	
    var PriceBookMgr =  require('dw/catalog/PriceBookMgr');
    
   	productSearchModel.setCategoryID(categoryID);
   	
   	var sortingRuleUnitSold = dw.catalog.CatalogMgr.getSortingRule(Resource.msg('search.recommended.unitssold', 'search', null));
   	if(sortingRuleUnitSold !== null){
   		productSearchModel.setSortingRule(sortingRuleUnitSold);
   	}
    
	
	var storeID = !empty(Stores.GetPreferredOrNearestStore().store)?Stores.GetPreferredOrNearestStore().store.ID:"";
	var salePriceBookName = storeID + '-'+session.currency.currencyCode.toLowerCase()+'-sale-prices';
	productSearchModel.addRefinementValues(Resource.msg('search.recommended.valuestep', 'search', null),valueStep);
	productSearchModel.search();
   	
	// Search For BEST/BETTER (valueStep)
   	if(productSearchModel.count > 0	){
   		var noOfProducts = (productSearchModel.count > count) ? count : productSearchModel.count; 
   		for(var i =0 ; i <noOfProducts ; i ++){
   			// RECORD -- Get "valueStep" Selling BEST Product -- Regardless of OWNED and unitsSold
   			if(productSearchModel.getProducts().asList().length >= i){
   				notOwnedNotOnSale.push(productSearchModel.getProducts().asList().get(i));
   			}
   		}
   		
   		var iterator = productSearchModel.getProducts();
   		var counter =0;
   		while(iterator.hasNext()){
   			if(counter >= count){
   					break;
   				}
   			var product = iterator.next();
   			if (product.getPriceModel()!==null && product.getPriceModel().getPriceBookPrice(salePriceBookName).available == true){
   					counter ++;
   					// RECORD "valueStep" Product, Not OWNED and on SALE
   					notOwnedOnSale.push(product);
   			}
   		}
   		
   		// Search for "valueStep" and OWNED  
   		productSearchModel.addRefinementValues(Resource.msg('search.recommended.owned', 'search', null),"Y");
   		//productSearchModel.setSortingRule(sortingRuleUnitSold);
   		productSearchModel.search();
   		
   		if(productSearchModel.count>0){
   			var iterator = productSearchModel.getProducts();
   			counter =0;
   	   		while(iterator.hasNext()){
   	   			if(counter >= count){
						break;
					}
   	   			var product = iterator.next();
   	   			if (product.getPriceModel()!==null && product.getPriceModel().getPriceBookPrice(salePriceBookName).available == true){
		   				counter ++;
   	   					// RECORD "valueStep", OWNED AND ON SALE
   	   					ownedOnSale.push(product);
   	   			}
   	   		}
   			
   			var noOfProducts = (productSearchModel.count > count) ? count : productSearchModel.count; 
   			for(var i =0 ; i <noOfProducts ; i ++){
   				// RECORD valueStep OWNED Product not on sale
   				if(productSearchModel.getProducts().asList().length >= i){
   					ownedNotOnSale.push(productSearchModel.getProducts().asList().get(i));
   				}
   			}
   			
   		}
   		
   		var productToBeReturned = [];
   		
   		for(var index in notOwnedOnSale){
   			productToBeReturned.push(notOwnedOnSale[index]);
   		}
   		
   		if(productToBeReturned.length < count){
   			for(var index in ownedOnSale){
   				var pos = productToBeReturned.map(function(e) { return e.ID; }).indexOf(ownedOnSale[index].ID);
   				if(productToBeReturned.length < count && pos < 0){
   					productToBeReturned.push(ownedOnSale[index]);
   				}
   	   		}
   		}
   		
   		if(productToBeReturned.length < count){
   			for(var index in ownedNotOnSale){
   				var pos = productToBeReturned.map(function(e) { return e.ID; }).indexOf(ownedNotOnSale[index].ID);
   				if(productToBeReturned.length < count && pos < 0){
   					productToBeReturned.push(ownedNotOnSale[index]);
   				}
   	   		}
   		}
   		
   		if(productToBeReturned.length < count){
   			for(var index in notOwnedNotOnSale){
   				var pos = productToBeReturned.map(function(e) { return e.ID; }).indexOf(notOwnedNotOnSale[index].ID);
   				if(productToBeReturned.length < count && pos < 0){
   					productToBeReturned.push(notOwnedNotOnSale[index]);
   				}
   	   		}
   		}
   		/*
   		if(!empty(ownedOnSale)){
   			return ownedOnSale.sort(comparePrice);
   		}
   		if(!empty(ownedNotOnSale)){
   			return ownedNotOnSale.sort(comparePrice);
   		}
   		if(!empty(notOwnedOnSale)){
   			return notOwnedOnSale.sort(comparePrice);
   		}
   		if(!empty(notOwnedNotOnSale)){
			return notOwnedNotOnSale.sort(comparePrice);
   		}*/
   		return productToBeReturned;
   	}
	// No BEST or BETTER Product Found
	return null;
}

//Used in sorting of products by price
function comparePrice(prod1 , prod2){
	
	if(!empty(prod1) && !empty(prod2) && prod1.getPriceModel().price.available && prod2.getPriceModel().price.available){
		return prod1.getPriceModel().price.compareTo(prod2.getPriceModel().price);
	}
	return 0;
}

module.exports = SearchModel;
