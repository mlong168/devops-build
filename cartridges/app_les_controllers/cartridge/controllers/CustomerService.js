'use strict';

/**
 * This controller handles customer service related pages, such as the contact us form.
 *
 * @module controllers/CustomerService
 */

/* API Includes */
var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Site = require('dw/system/Site');

/**
 * Renders the customer service overview page.
 */
function show() {
	var metaParam = {
		pageTitle : Resource.msg('customer.service.page.title', 'email', null)
	}
	require('~/cartridge/scripts/meta').update(metaParam);
    app.getView().render('content/customerservice');
}

/**
 * Provides a contact us form which sends an email to the configured customer service email address.
 */
function contactUs() {
    app.getForm('contactus').clear();
	var metaParam = {
		pageTitle : Resource.msg('contact.service.page.title', 'email', null)
	}
	require('~/cartridge/scripts/meta').update(metaParam);
    app.getView('CustomerService').render('content/contactus');
}

/**
 * The form handler for the contactus form.
 */
function submit() {
    var contactUsForm = app.getForm('contactus');

    var contactUsResult = contactUsForm.handleAction({
        send: function (formgroup) {
        	var subject, toEmail, fromEmail, toName, timeStamp, custName, custPhone, custEmail, custComment, templateID, result;
        	
        	subject = Resource.msg('contact.customerservice.subject', 'email', null);
        	toEmail = Site.getCurrent().getCustomPreferenceValue('customerServiceEmail');
        	toName = Resource.msg('contact.customerservice.name', 'email', null);
        	fromEmail = formgroup.email.value;
        	timeStamp = Site.getCurrent().calendar.getTime();
        	custName = formgroup.name.value;
        	custPhone = formgroup.phone.value;
        	custEmail = formgroup.email.value;
        	custComment = formgroup.comment.value;
        	templateID = Site.getCurrent().getCustomPreferenceValue('sendGridContactUsTemplateID');
        	
        	result = app.getController('SendGrid').SendMail(subject, toEmail, fromEmail, toName, custName, timeStamp, custName, custPhone, custEmail, custComment, templateID);
        	
            return result;
        },
        error: function () {
            // No special error handling if the form is invalid.
            return null;
        }
    });

    if (contactUsResult && (contactUsResult == 'OK')) {
        app.getView('CustomerService', {
            ConfirmationMessage: true
        }).render('content/contactus');
    } else {
    	var phone = app.getController('Stores').StorePhone();
        app.getView('CustomerService', {
        	IsError: true,
        	Error: Resource.msgf('contactus.generalerror','forms',null,phone)
        }).render('content/contactus');
    }
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/CustomerService~show */
exports.Show = guard.ensure(['get'], show);
/** @see module:controllers/CustomerService~contactUs */
exports.ContactUs = guard.ensure(['get', 'https'], contactUs);
/** @see module:controllers/CustomerService~submit */
exports.Submit = guard.ensure(['csrf', 'post', 'https'], submit);