'use strict';

/**
 * Controller handling search, category, and suggestion pages.
 *
 * @module controllers/Search
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var PagingModel = require('dw/web/PagingModel');
var URLUtils = require('dw/web/URLUtils');
var ContentMgr = require('dw/content/ContentMgr');
var SearchModel = require('dw/catalog/SearchModel');
var Site = require('dw/system/Site');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var CatalogMgr = require('dw/catalog/CatalogMgr');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Stores = require('~/cartridge/controllers/Stores');
var pageMeta = require('~/cartridge/scripts/meta');

/**
 * Renders a full-featured product search result page.
 * If the httpParameterMao format parameter is set to "ajax" only the product grid is rendered instead of the full page.
 *
 * Checks for search redirects configured in Business Manager based on the query parameters in the
 * httpParameterMap. If a search redirect is found, renders the redirect (util/redirect template).
 * Constructs the search based on the HTTP params and sets the categoryID. Executes the product search and then the
 * content asset search.
 *
 * If no search term, search parameter or refinement was specified for the search and redirects
 * to the Home controller Show function. If there are any product search results
 * for a simple category search, it dynamically renders the category page for the category searched.
 *
 * If the search query included category refinements, or is a keyword search it renders a product hits page for the category
 * (rendering/category/categoryproducthits template).
 * If only one product is found, renders the product detail page for that product.
 * If there are no product results found, renders the nohits page (search/nohits template).
 * @see {@link module:controllers/Search~showProductGrid|showProductGrid} function}.
 */
function show(jsonObjectData) {
	var breadcrumData = null,
		customParam = null,
		productSearchModel = null, 
		contentSearchModel = null,
		recommendedProducts = [],
		params = request.httpParameterMap,
		entry = null;

    if (params.format.stringValue === 'ajax' || params.format.stringValue === 'page-element') {

        showProductGrid();
        return;
    }

    var redirectUrl = SearchModel.getSearchRedirect(params.q.value);

    if (redirectUrl){
        app.getView({
            Location: redirectUrl.location,
            CacheTag: true
        }).render('util/redirect');
        return;
    }

    if (!empty(jsonObjectData) && 'breadcrumb' in jsonObjectData) {
    	breadcrumData = jsonObjectData.breadcrumb;
    }
    var category = dw.catalog.CatalogMgr.getCategory(params.cgid.value);
    var pageDescription = '';
	var pageKeywords = '';
    if(!empty(category)){
    	pageDescription = category.pageDescription;
    	pageKeywords = category.pageKeywords ;
    }
    var Search = app.getModel('Search');
	// Get Recommended - GOOD, BETTER, BEST - Products
	if(params.cgid.submitted && params.cgid.value == 'tire' && ((params.srule.submitted && params.srule.value == Resource.msg('search.sorting.recommended', 'search', null)) || (!params.srule.submitted && category.getDefaultSortingRule() !=null && category.getDefaultSortingRule().ID == Resource.msg('search.sorting.recommended', 'search', null)))){
		recommendedProducts = Search.getRecommendedProducts(params.cgid.value,params);
	}
	
	if (!empty(jsonObjectData) && 'params' in jsonObjectData) {
		// Constructs the search based on jsonObjectData.params and sets the categoryID.
		customParam = jsonObjectData.params;
    	productSearchModel = Search.initializeProductSearchModelByCustomParams(customParam);
    	contentSearchModel = Search.initializeContentSearchModel(params);
    } else {
        // Constructs the search based on the HTTP params and sets the categoryID.
        productSearchModel = Search.initializeProductSearchModel(params);
        contentSearchModel = Search.initializeContentSearchModel(params);
    }
    
    // execute the product search
    productSearchModel.search();
    contentSearchModel.search();
    
	var	pageTitle : String = ''; 
	// Update Page Meta Based on Search Entry
   	if(params.source.submitted) {
   		pageTitle = constructPageTitle(params, productSearchModel);
		var metaParams = {
			pageTitle: pageTitle,
			pageDescription : pageDescription,
			pageKeywords : pageKeywords
		}
		pageMeta.update(metaParams);
   	}
    
   	// Parameter only comes when we arrive from Directory Pages
   	if(params.entry.submitted) {
   		entry = params.entry.value;
   	}
    
    var sortingCondition = productSearchModel.getSortingRule();
    
    var datalayerProductPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    var datalayerCurrentPageModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    
    // Placement for categorylanding.isml, categorylanding2.isml and tirelanding.isml
    var contentFolder = ContentMgr.getFolder('cat-' + productSearchModel.categoryID);
    
    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.httpsHome());
    } else if (productSearchModel.count > 0) {

        if ((productSearchModel.count > 1) || productSearchModel.refinedSearch && empty(productSearchModel.getSearchPhrase()) || (contentSearchModel.count > 0)) {
            var productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
            
            if (params.start.submitted) {
                productPagingModel.setStart(params.start.intValue);
                datalayerProductPagingModel.setStart(0);
                datalayerCurrentPageModel.setStart(0);
            }

            if (params.sz.submitted && request.httpParameterMap.sz.intValue <= 60) {
                productPagingModel.setPageSize(params.sz.intValue);
                datalayerCurrentPageModel.setPageSize(params.sz.intValue);
                datalayerProductPagingModel.setPageSize(productPagingModel.currentPage + 1 * params.sz.intValue);
            } else {
                productPagingModel.setPageSize(12);
                datalayerCurrentPageModel.setPageSize(12);
                datalayerProductPagingModel.setPageSize(productPagingModel.currentPage + 1 * 12);
            }
            
            if (productSearchModel.category && empty(pageTitle)) {
                pageMeta.update(productSearchModel.category);
            }

            // This block is for categorylanding2.isml
            if (productSearchModel.categorySearch && productSearchModel.category.template && productSearchModel.category.template.indexOf('categorylanding2') > 0) {

            	var catDefaultRefinement = null, catDefaultRefinementValue=null, isBreak = false;
            	var Refinements = productSearchModel.refinements;

            	for each (var RefinementDefinition in Refinements.refinementDefinitions) {
        			if (RefinementDefinition.attributeID !== 'vehicleType') {
        				continue;
        			}
    				if (RefinementDefinition.isAttributeRefinement()) {
    					for each (var RefinementValue in Refinements.getAllRefinementValues(RefinementDefinition)) {
    						catDefaultRefinement = RefinementDefinition;
    						catDefaultRefinementValue = RefinementValue;
    						isBreak = true;
    						break;
    					}
    				}
    				if (isBreak) {
    					break;
    				}
            	}
            	
            	// These are refinements added for categorylanding2.isml
            	if(catDefaultRefinement && catDefaultRefinementValue){
                	productSearchModel.addRefinementValues(catDefaultRefinement.attributeID,catDefaultRefinementValue.value);
                	productSearchModel.search();
                	productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
                    if (params.start.submitted) {
                        productPagingModel.setStart(params.start.intValue);
                    }
                    if (params.sz.submitted && params.sz.intValue <= 60) {
    	    	        productPagingModel.setPageSize(params.sz.intValue);
    	    	    } else if (!empty(contentFolder) && !empty(contentFolder.custom.pageSize)) {
    	    			productPagingModel.setPageSize(contentFolder.custom.pageSize);
    	    		} else {
    	    	        productPagingModel.setPageSize(12);
    	    	    }
            	}
                
                // Renders a dynamic template.
                app.getView({
                    ProductSearchResult: productSearchModel,
                    ContentSearchResult: contentSearchModel,
                    ProductPagingModel: productPagingModel,
                    DatalayerProductPagingModel: datalayerProductPagingModel,
                    DatalayerCurrentPageModel: datalayerCurrentPageModel,
                    ContentFolder: contentFolder,
                    breadcrumData: breadcrumData
                }).render(productSearchModel.category.template);
            } else {
                //SearchPromo - for displaying search driven banners above the product grid, provided there is a q parameter in the httpParameterMap
                var searchPromo;
                if (params.q.value) {
                    searchPromo = ContentMgr.getContent('keyword_' + params.q.value.toLowerCase());
                }
                
                // This block can apply for tirelanding.isml or categorylanding.isml
                if (productSearchModel.categorySearch && !empty(productSearchModel.category.template)) {
	                app.getView({
	                    ProductSearchResult: productSearchModel,
	                    ContentSearchResult: contentSearchModel,
	                    ProductPagingModel: productPagingModel,
	                    DatalayerProductPagingModel: datalayerProductPagingModel,
	                    DatalayerCurrentPageModel: datalayerCurrentPageModel,
	                    ContentFolder: contentFolder,
	                    breadcrumData: breadcrumData
	                }).render(productSearchModel.category.template);
                } else {
	                app.getView({
	                    ProductSearchResult: productSearchModel,
	                    ContentSearchResult: contentSearchModel,
	                    ProductPagingModel: productPagingModel,
	                    DatalayerProductPagingModel: datalayerProductPagingModel,
	                    DatalayerCurrentPageModel: datalayerCurrentPageModel,
	                    SearchPromo: searchPromo,
	                    recommendedProducts : recommendedProducts,
	                    breadcrumData: breadcrumData,
	                    Entry: entry
	                }).render('rendering/category/categoryproducthits');
                }
            }
        } else {
            var targetProduct = productSearchModel.getProducts().next();
            var productID = targetProduct.getID();

            // If the target was not a master, simply use the product ID.
            if (targetProduct.isMaster()) {

                // In the case of a variation master, the master is the representative for
                // all its variants. If there is only one variant, return the variant's
                // product ID.
                var iter = productSearchModel.getProductSearchHits();
                if (iter.hasNext()) {
                    var productSearchHit = iter.next();
                    if (productSearchHit.getRepresentedProducts().size() === 1) {
                        productID = productSearchHit.getFirstRepresentedProductID();
                    }
                }
            }
            app.getView({
                Location: URLUtils.https('Product-Show', 'pid', productID)
            }).render('util/redirect');
        }
    }
    else {
    	
    	if (productSearchModel.categorySearch && productSearchModel.category.template) {
    		// This block will apply for categorylanding.isml
    		if (productSearchModel.category) {
                pageMeta.update(productSearchModel.category);
            }
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel,
                DatalayerProductPagingModel: datalayerProductPagingModel,
                DatalayerCurrentPageModel: datalayerCurrentPageModel,
                ContentFolder: contentFolder,
                breadcrumData: breadcrumData
            }).render(productSearchModel.category.template);
        }
    	else if(contentSearchModel.count > 0){
    		if (contentSearchModel.folder) {
         	   pageMeta.update(contentSearchModel.folder);
        	}
        	else{
        		pageMeta.update({
        			pageTitle : Resource.msg('search.results.label', 'global', null),
					pageDescription : pageDescription,
					pageKeywords : pageKeywords
				});
        	}
    		app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel
            }).render('rendering/category/categoryproducthits');
        }
    	else {
    		metaParams = {
    			pageTitle : Resource.msg('noresults.label', 'global', null),
				pageDescription : pageDescription,
				pageKeywords : pageKeywords
    		}
        	pageMeta.update(metaParams);
        	app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel
            }).render('search/nohits');
        }
    }
}


function constructPageTitle(params, productSearchModel){
		
    	var metaParams =null ;
		var metaDetails;
		var pageTitle : String = '';
   	
	   	switch (params.source.value){
	   		case 'tireSizeFlow':
	   			metaDetails = session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).tires;
	   			var pageTitleUpdated = false;
				if(!empty(metaDetails)){
					for(var key in metaDetails){
						if(!empty(metaDetails[key])){
							if(key == 'rear'){
								pageTitle = pageTitle.concat(Resource.msg('front.label', 'global', 'null') + ' | ');
							}
							// All three values submitted
							if(!empty(metaDetails[key].rim) && !empty(metaDetails[key].width) && !empty(metaDetails[key].ratio)){
								if(key == 'front'){
									pageTitle = pageTitle.concat(metaDetails[key].rim + ' ' + Resource.msg('tires.inch', 'global', 'null') + ' | ');
								}
								pageTitle = pageTitle.concat(metaDetails[key].width + '/' + metaDetails[key].ratio +'R'+ '-' + metaDetails[key].rim + ' ');
								pageTitleUpdated = true;
							}
							if(key == 'rear'){
								pageTitle = pageTitle.concat(Resource.msg('back.label', 'global', 'null') + ' ');
							}
							if(!pageTitleUpdated){
								// Only Rim Size and/or Width
								pageTitle = pageTitle.concat(!empty(metaDetails[key].rim)?metaDetails[key].rim + ' ' + Resource.msg('tires.inch', 'global', 'null') + ' | ':''); 
								pageTitle = pageTitle.concat(!empty(metaDetails[key].width)?metaDetails[key].width + ' ' + Resource.msg('tires.width', 'global', 'null') :'');
							}
						}
					}
					if(pageTitleUpdated){
						pageTitle = pageTitle.concat('| ' +Resource.msg('shop.by.tire.size.page.title', 'global', 'null') + ' ');
						pageTitle = pageTitle.concat(' - ' + Resource.msg('tires.by.vehicle.title', 'global', 'null'));
						pageTitleUpdated = false;
					}
				} else if(!empty(params.prefv1) && !empty(params.prefv2) && !empty(params.prefv3)){
					pageTitle = params.prefv2.value+' Inch Tires | '+params.prefv1.value+'/'+params.prefv3.value+'R-'+params.prefv2.value+' | Shop by Tire Size - Les Schwab';
				}
			break;
	   		case 'wheelSizeFlow':
	   			metaDetails = session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).wheels;
	   			var pageTitleUpdated = false;
				if(!empty(metaDetails)){
					for(var key in metaDetails){
						if(!empty(metaDetails[key])){
							if(key == 'rear'){
								pageTitle = pageTitle.concat(Resource.msg('front.label', 'global', 'null') + ' | ');
							}
							// All three values submitted
							if(!empty(metaDetails[key].diameter)){
								if(key == 'front'){
									pageTitle = pageTitle.concat(new Number(metaDetails[key].diameter) + ' ' + Resource.msg('wheels.inch', 'global', 'null') + ' | ');
								}
								var boltPattern = '';
								if(!empty(metaDetails[key].boltPattern)) {
									boltPattern = metaDetails[key].boltPattern;
								} else {
									boltPattern = metaDetails[key].boltpattern;
								}
								pageTitle = pageTitle.concat(boltPattern + ' ' +  Resource.msg('wheels.boltpatterns', 'global', 'null') + ' & ');
								pageTitle = pageTitle.concat(new Number(metaDetails[key].diameter) + ' ' +  Resource.msg('inch.rims', 'global', 'null') + ' ');
								pageTitleUpdated = true;
							}
							if(key == 'rear'){
								pageTitle = pageTitle.concat(Resource.msg('back.label', 'global', 'null') + ' ');
							}
							if(!pageTitleUpdated){
								pageTitle = pageTitle.concat(!empty(metaDetails[key].diameter)?new Number(metaDetails[key].diameter)+ ' ' + Resource.msg('tires.inch', 'global', 'null') + ' | ':''); 
							}
						}
					}
					if(pageTitleUpdated){
						pageTitle = pageTitle.concat(' - ' + Resource.msg('tires.by.vehicle.title', 'global', 'null'));
						pageTitleUpdated = false;
					}
				} else if(!empty(params.prefv1) && !empty(params.prefv2)) {
					pageTitle = params.prefv2.value+' Inch Wheels | '+params.prefv1.value+' Bolt Pattern & '+params.prefv2.value+' Inch Rims - Les Schwab';					
				}
	   		break;
	   		case 'tireVehicleFlow':
	   			metaDetails = session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).vehicle;
				if(!empty(metaDetails)){
					var widthDetails = productSearchModel.getRefinementValues('width');
					var rimSizeDetails = productSearchModel.getRefinementValues('rimSize');
					var aspectRatioDetails = productSearchModel.getRefinementValues('aspectRatio');
					var pageTitleUpdated = false;
					var rimSizeMeta = '';
					
					pageTitle = pageTitle.concat(!empty(metaDetails.year)?metaDetails.year + ' ' : '');
					pageTitle = pageTitle.concat(!empty(metaDetails.make)?metaDetails.make + ' ' : '');
					pageTitle = pageTitle.concat(!empty(metaDetails.model)?metaDetails.model + ' | ' : '');				
					
					pageTitle = pageTitle.concat(pageTitleUpdated? Resource.msg('tires.pagetitle', 'global', 'null') : ''); 
					pageTitleUpdated = false;
					
					for(var index in rimSizeDetails){
						pageTitle = pageTitle.concat(index > 0 ? '-'+rimSizeDetails[index] : rimSizeDetails[index]);
						rimSizeMeta = rimSizeMeta.concat(index > 0 ? '-'+rimSizeDetails[index] : rimSizeDetails[index]);
						pageTitleUpdated = true; 
					}
					
					pageTitle = pageTitle.concat(pageTitleUpdated? ' '+Resource.msg('inches.label', 'global', 'null') + ' | ' : '');
					pageTitleUpdated = false;
					
					for(var index in widthDetails){
						pageTitle = pageTitle.concat(index > 0 ? '-'+widthDetails[index] : widthDetails[index]);
						pageTitleUpdated = true; 
					}
					
					pageTitle = pageTitle.concat(pageTitleUpdated? '/' : '');
					pageTitleUpdated = false; 
					
					for(var index in aspectRatioDetails){
						pageTitle = pageTitle.concat(index > 0 ? '-'+aspectRatioDetails[index] : aspectRatioDetails[index]);
						pageTitleUpdated = true; 
					}
					
					pageTitle = pageTitle.concat(pageTitleUpdated? 'R': '');
					pageTitle = pageTitle.concat(!empty(rimSizeMeta)? rimSizeMeta + ' | ': '');
					pageTitle = pageTitle.concat(Resource.msg('tires.by.vehicle.title', 'global', 'null'));					
					
					pageTitleUpdated = false; 
				} else if(!empty(params.prefv1) && !empty(params.prefv2) && !empty(params.prefv3)) {
					if(params.year.submitted && params.makeName.submitted && params.modelName.submitted) {
						pageTitle = params.year.value+' '+params.makeName.value+' '+params.modelName.value+' | '+params.prefv2.value+' Inches | '+params.prefv1.value+'/'+params.prefv3.value+'R'+params.prefv2.value+' | Les Schwab';
					} else {
						pageTitle = params.prefv3.value+' Inches | '+params.prefv4.value+'/'+params.prefv1.value+'R-'+params.prefv3.value+' | Les Schwab';	
					}
				}
			break;
			case 'wheelVehicleFlow':
			// TODO Pune Team to update this after Jacob's confirmation
				metaDetails = session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).vehicle;
				if(!empty(metaDetails)){
					var diameterDetails  = productSearchModel.getRefinementValues('diameter');
					if(empty(diameterDetails)){
						diameterDetails = [];
						// Get Min and Max Value
						diameterDetails[0]=productSearchModel.getRefinementMinValue('diameter');
						diameterDetails[1]=productSearchModel.getRefinementMaxValue('diameter');
					}
					var pageTitleUpdated = false;
										
					pageTitle = pageTitle.concat(metaDetails.year + ' ');
					pageTitle = pageTitle.concat(metaDetails.make + ' ');
					pageTitle = pageTitle.concat(metaDetails.model + ' ');
					pageTitle = pageTitle.concat(' Wheels ');
					
					for(var index in diameterDetails){
						pageTitle = pageTitle.concat(index > 0 ? '-'+diameterDetails[index] :'| ' + diameterDetails[index]);
						pageTitleUpdated = true; 
					}
					pageTitle = pageTitle.concat(pageTitleUpdated? ' '+Resource.msg('inch.diameter', 'global', 'null') : '');
					pageTitle = pageTitle.concat(' - ' + Resource.msg('tires.by.vehicle.title', 'global', 'null'));
					pageTitleUpdated = false; 
				} else if(!empty(params.prefmin3) && !empty(params.prefmax3)) {
					if(params.year.submitted && params.makeName.submitted && params.modelName.submitted) {
						pageTitle = params.year.value+' '+params.makeName.value+' '+params.modelName.value+' Wheels | '+params.prefmin3.value+'-'+params.prefmax3.value+' Inch Diameter - Les Schwab';
					} else {
						pageTitle = params.prefmin3.value+'-'+params.prefmax3.value+' Inch Diameter - Les Schwab';	
					}
				}
			break;
			case 'all-season-tires':
				pageTitle = Resource.msg('page.title.allseasontires', 'global', 'null');
			break;
			case 'winter-tires':
				pageTitle = Resource.msg('page.title.wintertires', 'global', 'null');
			break;
			case 'performance-tires':
				pageTitle = Resource.msg('page.title.performancetires', 'global', 'null');
			break;
			case 'traction-tires':
				pageTitle = Resource.msg('page.title.tractiontires', 'global', 'null');
			break;
	   	}
	   	
	   	return pageTitle;
}

/**
 * Renders a full-featured content search result page.
 *
 * Constructs the search based on the httpParameterMap params and executes the product search and then the
 * content asset search.
 *
 * If no search term, search parameter or refinement was specified for the search, it redirects
 * to the Home controller Show function. If there are any content search results
 * for a simple folder search, it dynamically renders the content asset page for the folder searched.
 * If the search included folder refinements, it renders a folder hits page for the folder
 * (rendering/folder/foldercontenthits template).
 *
 * If there are no product results found, renders the nohits page (search/nohits template).
 */
function showContent() {

	var params = request.httpParameterMap;

	var Search = app.getModel('Search');
	var productSearchModel = Search.initializeProductSearchModel(params);
	var contentSearchModel = Search.initializeContentSearchModel(params);

	// Executes the product search.
	productSearchModel.search();
	contentSearchModel.search();

    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.httpsHome());
    } else if (contentSearchModel.count > 0 || contentSearchModel.folder.onlineContent.size() > 0) {
    	// Updating Page Meta
    	if (contentSearchModel.folder) {
        	pageMeta.update(contentSearchModel.folder);
        }
		var contentPagingModel = new PagingModel(contentSearchModel.content, contentSearchModel.count);
		var pageSize = Site.getCurrent().getCustomPreferenceValue('articlePageSize');
		contentPagingModel.setPageSize(pageSize);

		if (params.start.submitted) {
		    contentPagingModel.setStart(params.start.intValue);
		}

		var showOnlyContent = false;
		if (params.ajax.submitted) {
			showOnlyContent = true;
		}

		// Loop for localized articles
		if(params.format.submitted && params.format.stringValue === 'page-articles'){
			if(params.sz.submitted){
				contentPagingModel.setPageSize(params.sz.intValue);
			}
			var template = "rendering/folder/localizedgrid_nohits";
			if(params.q.submitted && !empty(params.q.value)){
				template = "rendering/folder/topcontent_nohits";
			}
			app.getView({
			     ProductSearchResult: productSearchModel,
			     ContentSearchResult: contentSearchModel,
			     ContentPagingModel: contentPagingModel,
			     ShowContentOnly : showOnlyContent
			 }).render(template);
		} else if (contentSearchModel.folderSearch && !contentSearchModel.refinedFolderSearch && contentSearchModel.folder.template) {
			// Loop for Folders with Templates
			// Renders a dynamic template
			app.getView({
			    ProductSearchResult: productSearchModel,
			    ContentSearchResult: contentSearchModel,
			    ContentPagingModel: contentPagingModel,
			    ShowContentOnly : showOnlyContent
			}).render(contentSearchModel.folder.template);
		} else {
			app.getView({
			    ProductSearchResult: productSearchModel,
			    ContentSearchResult: contentSearchModel,
			    ContentPagingModel: contentPagingModel
			}).render('rendering/folder/foldercontenthits');
		}
    } else {
        app.getView({
            ProductSearchResult: productSearchModel,
            ContentSearchResult: contentSearchModel
        }).render('search/nohits');
    }
}

/**
 * Renders the search suggestion page (search/suggestions template).
 */
function getSuggestions() {

    app.getView().render('search/suggestions');
}

/**
 * Renders the partial content of the product grid of a search result as rich HTML.
 *
 * Constructs the search based on the httpParameterMap parameters and executes the product search and then the
 * content asset search. Constructs a paging model and determines whether the infinite scrolling feature is enabled.
 *
 * If there are any product search results for a simple category search, it dynamically renders the category page
 * for the category searched.
 *
 * If the search query included category refinements or is a keyword search, it renders a product hits page for the category
 * (rendering/category/categoryproducthits template).
 */
function showProductGrid() {
	
    var params = request.httpParameterMap;
    
    var recommendedProducts=[];
    var Search = app.getModel('Search');
    var category = dw.catalog.CatalogMgr.getCategory(params.cgid.value);
	// Get Recommended - GOOD, BETTER, BEST - Products
	if(params.cgid.submitted && params.cgid.value == 'tire' && ((params.srule.submitted && params.srule.value == Resource.msg('search.sorting.recommended', 'search', null)) || (!params.srule.submitted && category.getDefaultSortingRule() !=null && category.getDefaultSortingRule().ID == Resource.msg('search.sorting.recommended', 'search', null)))){
		recommendedProducts = Search.getRecommendedProducts(params.cgid.value,params);
	}
    
    // Constructs the search based on the HTTP params and sets the categoryID.
    
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);
    var sortingCondition = productSearchModel.getSortingRule();

    // Executes the product search.
    productSearchModel.search();
    contentSearchModel.search();

    var productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    var datalayerProductPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    var datalayerCurrentPageModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
   
    if (params.start.submitted) {
        productPagingModel.setStart(params.start.intValue);
        datalayerCurrentPageModel.setStart(params.start.intValue);
        datalayerProductPagingModel.setStart(0);
    }

    if (params.sz.submitted && params.sz.intValue <= 60) {
        productPagingModel.setPageSize(params.sz.intValue);
        datalayerCurrentPageModel.setPageSize(params.sz.intValue);
        datalayerProductPagingModel.setPageSize((productPagingModel.currentPage + 1) * params.sz.intValue);
    } else {
        productPagingModel.setPageSize(12);
        datalayerCurrentPageModel.setPageSize(12);
        datalayerProductPagingModel.setPageSize((productPagingModel.currentPage + 1) * 12);
    }

    if (params.format.stringValue === 'page-element') {
        app.getView({
            ProductSearchResult: productSearchModel,
            ProductPagingModel: productPagingModel,
            DatalayerProductPagingModel: datalayerProductPagingModel,
            DatalayerCurrentPageModel: datalayerCurrentPageModel,
            recommendedProducts : recommendedProducts,
            hideRecommendedProducts : 'true'
        }).render('search/productgridwrapper');
    } else {
    	if (productSearchModel.categorySearch && productSearchModel.category.template && productSearchModel.category.template.indexOf('categorylanding2') > 0) {
            // Renders a dynamic template.
    		var contentFolder = ContentMgr.getFolder('cat-' + productSearchModel.categoryID);

    		if (!empty(contentFolder)) {
	    		if (params.sz.submitted && params.sz.intValue <= 60) {
	    	        productPagingModel.setPageSize(params.sz.intValue);
	    	    }else if (!empty(contentFolder) && !empty(contentFolder.custom.pageSize)) {
	    			productPagingModel.setPageSize(contentFolder.custom.pageSize);
	    		} else {
	    	        productPagingModel.setPageSize(12);
	    	    }
    		}
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel,
                DatalayerProductPagingModel: datalayerProductPagingModel,
                DatalayerCurrentPageModel: datalayerCurrentPageModel,
            }).render('rendering/category/categoryproducthits2');
        } else if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
            // categoryproducthits2 template.
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel,
                DatalayerProductPagingModel: datalayerProductPagingModel,
                DatalayerCurrentPageModel: datalayerCurrentPageModel,
            }).render(productSearchModel.category.template);
        } else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel,
                DatalayerProductPagingModel: datalayerProductPagingModel,
                DatalayerCurrentPageModel: datalayerCurrentPageModel,
                recommendedProducts : recommendedProducts
            }).render('rendering/category/categoryproducthits');
        }
    }

}

function getTireAspectRatio() {
	var aspectRatio, width = request.httpParameterMap.width.stringValue;
	var aspectRatios = JSON.parse(Site.getCurrent().getCustomPreferenceValue('tireAspectRatio'));
    for (var i = 0; i < aspectRatios.length; i++) {
        if (width === aspectRatios[i].width) {
        	aspectRatio = aspectRatios[i].aspectratio;
            break;
        }
    }
    app.getView({jsonData: JSON.stringify({aspectratios: aspectRatio})}).render('react/apiresponse');
}

function getTireRimSize() {
	var rimSize, aspectratio = request.httpParameterMap.aspectratio.stringValue;
	var rimSizes = JSON.parse(Site.getCurrent().getCustomPreferenceValue('tireRimSize'));
    for (var i = 0; i < rimSizes.length; i++) {
        if (aspectratio === rimSizes[i].aspectratio) {
        	rimSize = rimSizes[i].rimsize;
            break;
        }
    }
    app.getView({jsonData: JSON.stringify({rimsizes: rimSize})}).render('react/apiresponse');
}

function getWheelDiameter() {
	var diameter=[], boltpattern = request.httpParameterMap.boltpattern.stringValue;;
	var diameters = JSON.parse(Site.getCurrent().getCustomPreferenceValue('wheelDiameter'));
    for (var i = 0; i < diameters.length; i++) {
        if (boltpattern === diameters[i].boltpattern) {
        	diameter = diameters[i].diameter;
            break;
        }
    }
    app.getView({jsonData: JSON.stringify({diameters: diameter})}).render('react/apiresponse');
}

function storeURLData() {
	var param = request.httpParameterMap;
	var clearURL = param.clearURL.stringValue;
	var rangeFilter = param.rangeFilter.stringValue;
	
	require('dw/system/Transaction').wrap(function(){
		session.custom.clearURL = clearURL;
		session.custom.rangeFilter = rangeFilter;
	});
	app.getView({jsonData:'true'}).render('react/apiresponse');
}

function getClearURL() {
	var param = request.httpParameterMap;
	var cgid= param.catID.value;
	if (!empty(session.custom.clearURL)) {
		var jsonData = JSON.parse(session.custom.clearURL);
		if (cgid in jsonData) {
			app.getView({jsonData:jsonData[cgid]}).render('react/apiresponse');
		} else {
			app.getView({jsonData:''}).render('react/apiresponse');
		}
	} else {
		app.getView({jsonData:''}).render('react/apiresponse');
	}
}

function getRangeFilterRelaxURL() {
	var param = request.httpParameterMap;
	if (!empty(session.custom.rangeFilter)) {
		var filterData = JSON.parse(session.custom.rangeFilter);
		
		
		if (!empty(filterData)) {
			var catRangeFilter = filterData[param.catID.value];
			var rangeFilterID =param.filterAttrId.value;
			var relaxURL = '';
			if (!empty(catRangeFilter) && !empty(catRangeFilter[rangeFilterID]) && 'min' in catRangeFilter[rangeFilterID] && 'max' in catRangeFilter[rangeFilterID]) {
				var rangeFilterValueMin = catRangeFilter[rangeFilterID].min;
				var rangeFilterValueMax = catRangeFilter[rangeFilterID].max;
				var relaxURL = param.rangeRelaxURL.stringValue;
				
				if (relaxURL.indexOf('###'+rangeFilterID+'###')> 0) {
					relaxURL = relaxURL.replace('##'+rangeFilterID+'##',rangeFilterValueMax);
				} else if (relaxURL.indexOf('%23%23%23'+rangeFilterID+'%23%23%23')> 0) {
					relaxURL = relaxURL.replace('%23%23%23'+rangeFilterID+'%23%23%23',rangeFilterValueMax);
				}
				
				if (relaxURL.indexOf('##'+rangeFilterID+'##')> 0) {
					relaxURL = relaxURL.replace('##'+rangeFilterID+'##',rangeFilterValueMin);
				} else if (relaxURL.indexOf('%23%23'+rangeFilterID+'%23%23')> 0) {
					relaxURL = relaxURL.replace('%23%23'+rangeFilterID+'%23%23',rangeFilterValueMin);
				}
			} else if (!empty(catRangeFilter) && !empty(catRangeFilter[rangeFilterID]) && 'value' in catRangeFilter[rangeFilterID]) {
				var val = catRangeFilter[rangeFilterID].value;
				relaxURL = param.relaxURL.stringValue;
				if (relaxURL.indexOf('##'+rangeFilterID+'##')> 0) {
					relaxURL = relaxURL.replace('##'+rangeFilterID+'##',val);
				} else {
					relaxURL = param.relaxURL.stringValue;
				}	
			} else {
				relaxURL = param.relaxURL.stringValue;
			}
			app.getView({jsonData:relaxURL}).render('react/apiresponse');
		} else {
			app.getView({jsonData:param.rangeRelaxURL.stringValue}).render('react/apiresponse');
		}
	} else {
		app.getView({jsonData:param.rangeRelaxURL.stringValue}).render('react/apiresponse');
	}
}

function refinementValues() {
	var params = request.httpParameterMap;
	var Search = app.getModel('Search');
	var RefinementAttributeID = params.RefinementAttributeID.value;
	var productSearchModel = Search.initializeProductSearchModel(params);
	// execute the product search
	productSearchModel.search();
	 // Renders a dynamic template.
    app.getView({
        ProductSearchResult: productSearchModel,
        RefinementAttributeID: RefinementAttributeID
    }).render('search/components/customrefinementvalues');
}

function directoryEntryData() {
	var params = request.httpParameterMap;
	var entry = params.entry.value, vehicleDetails, tiresDetails, wheelsDetails, sourceType;
	sourceType = params.source.value;
	
	if(!empty(entry)) {
		if(entry == 'wheels' || entry == 'tires') {
			vehicleDetails = {
					'year': params.year.value,
					'make': params.makeName.value,
					'model': params.modelName.value,
					'trim': params.trimName.value
			};
	        tiresDetails = {
	        		'front': {
		        		'width':params.prefv1.value,
		        		'ratio':params.prefv3.value,
		        		'rim':params.prefv2.value
	        		}
	        };
	        wheelsDetails = {
	        		'front': {
		        		'boltPattern':params.prefv1.value,
		        		'diameter':params.prefv2.value
	        		}
    		};
		}
		app.getView({
	   		vehicleDetails: vehicleDetails,
	   		tiresDetails: tiresDetails,
	   		wheelsDetails: wheelsDetails,
	   		entry: entry,
	   		source: sourceType
	   	}).render('search/productsearchresult_header');
	}
}

/*
 * Web exposed methods
 */
exports.GetTireAspectRatio = guard.ensure(['get'], getTireAspectRatio);
exports.GetTireRimSize = guard.ensure(['get'], getTireRimSize);
exports.GetWheelDiameter = guard.ensure(['get'], getWheelDiameter);

/** Renders a full featured product search result page.
 * @see module:controllers/Search~show
 * */
exports.Show            = guard.ensure(['get'], show);

/** Renders a full featured content search result page.
 * @see module:controllers/Search~showContent
 * */
exports.ShowContent     = guard.ensure(['get'], showContent);

/** Determines search suggestions based on a given input and renders the JSON response for the list of suggestions.
 * @see module:controllers/Search~getSuggestions */
exports.GetSuggestions = guard.ensure(['get'], getSuggestions);

exports.GetClearURL = guard.ensure(['include'], getClearURL);
exports.GetRangeFilterRelaxURL = guard.ensure(['include'], getRangeFilterRelaxURL);
exports.StoreURLData = guard.ensure(['include'], storeURLData);
exports.RefinementValues = guard.ensure(['include'], refinementValues);
exports.DirectoryEntryData = guard.ensure(['include'], directoryEntryData);