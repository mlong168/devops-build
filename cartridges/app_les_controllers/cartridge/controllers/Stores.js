'use strict';

/**
 * Controller that renders the store finder and store detail pages.
 *
 * @module controllers/Stores
 */

/* API Includes */
var StoreMgr = require('dw/catalog/StoreMgr');
var ContentMgr = require('dw/content/ContentMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var LinkedHashMap = require('dw/util/LinkedHashMap');
var PriceBookMgr = require('dw/catalog/PriceBookMgr');
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var viewHelpers = require('app_les_core/cartridge/scripts/util/ViewHelpers');
var pageMeta = require('~/cartridge/scripts/meta');

/**
* Function exposed for Other Controllers to access Preferred Store
*/
function getUserPreferredStore() {
	var storeID;
    if (customer.registered) {
    	storeID = customer.profile.custom.storeId;
    } else {
    	storeID = session.privacy.preferredstoreId;
    }
    if(!empty(storeID)) {
    	return StoreMgr.getStore(storeID);
    } else {
    	return null;
    }
}

/**
* Function exposed for to Update Users Preferred Store
*/
function updatePreferredStore() {
	var requestBody = request.httpParameterMap.requestBodyAsString;

	var storeID = JSON.parse(requestBody).storeId;
    if (customer.registered && customer.authenticated) {
    	var Customer = app.getModel('Customer');
    	Customer.updatePreferredStore(storeID);
    } else {
    	session.privacy.preferredstoreId = storeID;
    }

    var store = StoreMgr.getStore(storeID);
    setPricebook(store);

    return true;
}

/**
 * Find Stores by CurrentLocation / ZIP / Sate, City
 * Renders the store information page (storelocator/storelocator template).
 */
function showStores() {
    var stores, geoLocation, Stores, searchKey, stateCode, stateCodeMappings, 
    	queryState = false, isStateSearch = false, cityName, Cities, isCitySearch = false,
    	sessionUserLocation = null,
		metaParam= {
    		pageTitle :  Resource.msg('find.stores.near.you', 'storelocator', null)
    	},
    	params = request.httpParameterMap;
    var isLocationSearch = params.parameterCount == 0;
    
	pageMeta.update(metaParam);
	
	if(session.privacy.custUserLocation){
		sessionUserLocation =  JSON.parse(session.privacy.custUserLocation);
	}
	
	if(params.uLat && params.uLng && params.uLat.value && params.uLng.value ){
		isLocationSearch = true;
		geoLocation = {
			latitude: Number(params.uLat.value),
			longitude: Number(params.uLng.value),
		}
	} else if(isLocationSearch && sessionUserLocation && sessionUserLocation.uLat && sessionUserLocation.uLng){
		geoLocation = {
			latitude: Number(sessionUserLocation.uLat),
			longitude: Number(sessionUserLocation.uLng)
		};
	} else if (isLocationSearch){
		geoLocation = request.getGeolocation();
	}
	
	if (isLocationSearch) {
   		stores = queryStoresByCoordinates(geoLocation.latitude, geoLocation.longitude);
   		Stores = JSON.stringify(getStoresJSON(stores, true));
	}
    
    if (params.sq.submitted && !empty(params.sq.value)) {
    	var latitude, longitude;
			searchKey = params.sq.stringValue;
		stateCodeMappings = JSON.parse(Site.getCurrent().getCustomPreferenceValue('searchByStateCodeMappings'));
    
		if (searchKey.length >= 2) {
    		for(var i=0; i<stateCodeMappings.length;i++) {
    			if(stateCodeMappings[i].name.equalsIgnoreCase(searchKey) || stateCodeMappings[i].code.equalsIgnoreCase(searchKey)) {
    				stateCode = stateCodeMappings[i].code;
    				queryState = true;
    				break;
    			}
    		}
    	}
    	if (!queryState) {
   			// GET Latitude and Longitude from Geocoder service
    		latitude = Number(params.latitude.value);
   			longitude = Number(params.longitude.value);
   			geoLocation = {
   				latitude: latitude,
   				longitude: longitude
   			}
   			stores = queryStoresByCoordinates(latitude, longitude);
   			Stores = JSON.stringify(getStoresJSON(stores, true));
   		}
   	}
    // Added for Search By Query String (state/city)
    if (empty(stores) || queryState) {
    	isStateSearch = true;
    	var stateCodeMappings = JSON.parse(Site.getCurrent().getCustomPreferenceValue('searchByStateCodeMappings'));
    	if (params.city.submitted && !empty(params.city.value) && params.state.submitted && !empty(params.state.value)) {
    		cityName = params.city.value;
    		stateCode = params.state.value;
    		var storeCityIterator = SystemObjectMgr.querySystemObjects('Store', 'stateCode ILIKE {0} AND city ILIKE {1}', 'stateCode asc', stateCode, cityName);
    		if(storeCityIterator.hasNext()) {
    			stores = storeCityIterator.asList();
    			Stores = JSON.stringify(getStoresJSON(stores, false));
    		}
       		var storeStateIterator = SystemObjectMgr.querySystemObjects('Store', 'stateCode ILIKE {0}', 'stateCode asc', stateCode);
    		if(storeStateIterator.hasNext()) {
    			Cities = getCitiesJSON(storeStateIterator.asList(), stateCode);
    		}
    		isCitySearch = true;
    		// Page Title for City Search
    		var stateName='';
    		for(var i=0; i<stateCodeMappings.length;i++) {
    			if(stateCodeMappings[i].code.equalsIgnoreCase(stateCode)) {
    				stateName = stateCodeMappings[i].name;
    				break;
    			}
    		} 
    	    var pageTitle = Resource.msgf('storelocator.city.state.search.title', 'storelocator', null, cityName, stateCode, stateName); 
    	    var metaParam={
    	    	pageTitle :  pageTitle
    	    }
    	    pageMeta.update(metaParam);
    	} else if((params.state.submitted && !empty(params.state.value) || queryState)) {
    		if(params.state.submitted && !empty(params.state.value) && !queryState) {
    			stateCode = params.state.value;
    			// Page Title for State Search
    			var stateName='';
        		for(var i=0; i<stateCodeMappings.length;i++) {
        			if(stateCodeMappings[i].code.equalsIgnoreCase(stateCode)) {
        				stateName = stateCodeMappings[i].name;
        				break;
        			}
        		} 
        	    var pageTitle = Resource.msgf('storelocator.state.search.title', 'storelocator', null, stateCode, stateName);
        	    var metaParam={
        	    	pageTitle :  pageTitle
        	    }
        	    pageMeta.update(metaParam);
    		}
       		var storeIterator = SystemObjectMgr.querySystemObjects('Store', 'stateCode ILIKE {0}', 'stateCode asc', stateCode);
    		if(storeIterator.hasNext()) {
    			stores = storeIterator.asList();
    			Stores = JSON.stringify(getStoresJSON(stores, false));
    			if(params.state.submitted && !empty(params.state.value)) {
    				Cities = getCitiesJSON(stores, stateCode);
    			}
    		}
    	} 
    }
    app.getView({
    	Stores : Stores,
    	StoreSearchForm: JSON.stringify(app.getController('ReactComponent').getStoreSearchForm(false, geoLocation, isStateSearch)),
    	Cities : Cities,
    	CitySearchPerformed : isCitySearch
    }).render('storelocator/storelocator');
}


function getCitiesJSON(stores, stateCode) {
	var cityList = [], res = [];
	for(var store in stores) { 
		if(cityList.indexOf(stores[store].city) < 0){
			cityList.push(stores[store].city);
			cityList.sort();
		}
	}
	return {
		StateCode : stateCode,
		CityList : cityList
	};
}

/**
 * Returns JSON object of Stores
 */
function getStoresJSON(stores, isDistanceAvail) {
	var res = [], storesArray = [];
	if (stores && stores.toArray) {

		var nearestStoreData = getClosestStore();
		var nearestStore = nearestStoreData.store;
		var preferredStore = getUserPreferredStore();

		storesArray = stores.toArray();

		if(isDistanceAvail) {
			res = storesArray.map(function(store){
				return getStoreJSONObject(store.key, "brief", store.value, preferredStore, nearestStore);
			});
		} else {
			res = storesArray.map(function(store){
				return getStoreJSONObject(store, "brief", null, preferredStore, nearestStore);
			});
		}
	}
	return {
		componentName: "StoreList",
		noResultsMessage: Resource.msgf('storelocator.noResults.message', 'storelocator', null, Site.getCurrent().getCustomPreferenceValue('storeLookupMaxDistance').value),
		noResultsSubMessage:  Resource.msg('storelocator.noResults.subMessage', 'storelocator', null),
    storeList: res
	};
}

/**
 * Returns JSON object based on the provide params and Store object
 */

function getStoreJSONObject(store, type, storeDistance, prefStore, nearestStore) {
	var storeID = store.ID;
	var currency = session.getCurrency().getCurrencyCode().toLowerCase();
	var priceBook = !empty(PriceBookMgr.getPriceBook(storeID+'-'+currency+'-sale-prices'))?PriceBookMgr.getPriceBook(storeID+'-'+currency+'-sale-prices') : PriceBookMgr.getPriceBook(storeID+'-'+currency+'-list-prices')
	
	return {
		address1: store.address1,
		makeMyStoreUrl: URLUtils.url("Stores-UpdatePreferredStore").toString(),
		stopByForInstallation:Resource.msg('pdp.stopby', 'storelocator', null),
		creditPageCtaLabel:Resource.msg('storelocator.credit.options', 'storelocator', null),
		creditPageCtaUrl: URLUtils.url("Page-Show","cid", "credit").toString(),
		type: type || "brief",
        appointmentLabel: Resource.msg('storelocator.appointment.label', 'storelocator', null),
        callUsLabel: Resource.msg('storelocator.callus', 'storelocator', null),
        myStoreHeader: Resource.msg('mystore.header', 'storelocator', null),
        nearestStoreHeader: Resource.msg('neareststore.header', 'storelocator', null),
        changeStoreLabel: Resource.msg('storelocator.change.store', 'storelocator', null),
		changeStoreUrl: URLUtils.url("Stores-ShowStores","dwfrm_storelocator_findByCurrentLocation","dwfrm_storelocator_findByCurrentLocation").toString(),
        city: store.city,
        componentName: "StoreDetails",
        description: !empty(store.custom.description) ? store.custom.description : "",
        directionsLabel: Resource.msg('storelocator.getdirections.label', 'storelocator', null),
        distance: storeDistance,
        findAnotherStoreLabel: Resource.msg('mystore.find.another.store.label', 'storelocator', null),
        hours: !empty(store.storeHours) ? store.storeHours.toString():'',
        hoursLabel: Resource.msg('storelocator.hours','storelocator',null),
        id: store.ID,
        isMystore: !empty(prefStore) ? prefStore.ID == store.ID : false,
		isNearest: typeof nearestStore === "boolean" ? nearestStore: !empty(nearestStore) ? nearestStore.ID == store.ID : false,
        latitude: store.latitude,
        longitude: store.longitude,
        makeMyStoreLabel: Resource.msg('storelocator.makemystore.label', 'storelocator', null),
        manager: store.custom.manager,
        managerImage: !empty(store.custom.managerImage)?store.custom.managerImage.getURL().toString():"",
        managerLabel: Resource.msg('storelocator.manager','storelocator',null),
        myStoreLabel: Resource.msg('storelocator.mystore.label', 'storelocator', null),
        phone: store.phone,
        stateCode: store.stateCode,
        storeDetailsLabel: Resource.msg('storelocator.storedetails.label', 'storelocator', null),
        storeDetailsUrl: URLUtils.url('Stores-ShowStoreDetails', 'storeId', store.ID, 'distance', storeDistance).toString(),
        storeImages: !empty(store.image) ? [store.image.getURL().toString()] : [],
        title: store.address1 + Resource.msg('storelocator.storeheading','storelocator',null),
        weSpeakSpanish: store.custom.spanishSpoken?Resource.msg('storelocator.spanishSpoken','storelocator',null):"",
        name: store.name,
        email: store.email,
        address2: store.address2,
        postalCode:store.postalCode,
        countryCode: store.countryCode,
        inventoryListID: store.inventoryList?store.inventoryList.ID:"",
        posEnabled: store.posEnabled,
        inventory_list_id: store.custom.inventoryListId,
        priceBookId: priceBook?priceBook.ID: null,
        storeLocatorFlag: store.storeLocatorEnabled,
        communityInvolvement: store.custom.communityInvolvement.toString(),
        services: store.custom.additionalServices.toString(),
        noPhoneMessage:  Resource.msg('storelocator.noPhoneMessage', 'storelocator', null),
        enableAppointment: !empty(store.custom.enableAppointment)?store.custom.enableAppointment:false
	};
}

/**
 * Show store in header for registered user(preferred store or closest store) and guest user (closest store).
 * Renders the store information page (storelocator/mystore template).
 */
function getPreferredOrNearestStore() {
	var store,
		nearestStore = false,
		distance = null;

	store = getUserPreferredStore();

    if(empty(store)) {
		var storeData = getClosestStore();
		store = storeData.store;
		nearestStore = storeData.isNearest;
		distance = storeData.distance;
    }
   	setPricebook(store);

	return {store: store, isNearest: nearestStore, distance: distance}
}

function getClosestStore() {
	var stores,
		result = {distance: null, store: null, isNearest: false},
		sessionUserLocation = null,
		geoLocation = null;

	if(session.privacy.custUserLocation){
		sessionUserLocation =  JSON.parse(session.privacy.custUserLocation);
	};

	if(sessionUserLocation && sessionUserLocation.uLat && sessionUserLocation.uLng){
		geoLocation = {
			latitude: Number(sessionUserLocation.uLat),
			longitude: Number(sessionUserLocation.uLng)
		};
	}else{
		geoLocation = request.getGeolocation();
	}

	if(!empty(geoLocation)) {
		stores = StoreMgr.searchStoresByCoordinates(geoLocation.latitude, geoLocation.longitude,
				Site.getCurrent().getCustomPreferenceValue('storeLookupUnit').value, null);
		if (stores.getLength() > 0) {
			result.store = stores.keySet().iterator().next();
			result.distance = stores.entrySet().toArray().shift().value;
			result.isNearest = true;
		}
	}
	return result;
}


/*
  Function to query stores by latitude and longitude
 */
function queryStoresByCoordinates(latitude, longitude){
	var storeMgrResult = StoreMgr.searchStoresByCoordinates(latitude,longitude,
			Site.getCurrent().getCustomPreferenceValue('storeLookupUnit').value,
			Site.getCurrent().getCustomPreferenceValue('storeLookupMaxDistance').value);

	return storeMgrResult.entrySet();
}

/*
 * Render the store details page.
 *
 * Gets the store ID from the httpParameterMap.
 */
function showStoreDetails() {

    var storeID = request.httpParameterMap.storeId.value;
    var store = StoreMgr.getStore(storeID);
    var backUrl;
	// find the last store results related click
	var list = session.clickStream.clicks;
	for( var i=list.size()-1; i >= 0; i-- )
	{
		var click = list[i];
		if(click.pipelineName == 'Stores-ShowStores') {
			backUrl = 'http://' + click.host + click.url.replace(/source=/g, "src=").replace(/format=/g, "frmt=");
			break;
		}
	}
	
	// Update Page Meta in the form of  : "City", "State Abbreviation" | "Zip Code" | "Street Address" | "State" Tire Shop - Les Schwab
    var pageTitle : String = "";
    var stateMappings = JSON.parse(Site.getCurrent().getCustomPreferenceValue('stateCodeMapping'));
    pageTitle = pageTitle.concat(store.city?store.city + ', ' : '');
    pageTitle = pageTitle.concat(store.stateCode?store.stateCode + ' ' : '');
    pageTitle = pageTitle.concat(Resource.msg('tire.shop', 'global', null) + ' | ');
    pageTitle = pageTitle.concat(store.postalCode?store.postalCode+ ' | ' : '');
    pageTitle = pageTitle.concat(store.address1?store.address1+ ' ' : '');
    pageTitle = pageTitle.concat(Resource.msg('title.lesschwab', 'global', null));
    
    var metaParam={
    	pageTitle :  pageTitle
    }

    pageMeta.update(metaParam);
    app.getView({
    	Store: store,
    	Back: backUrl
    }).render('storelocator/storedetails');
}

/**
 * @returns Store city
 */
function personalizeCity() {

	var store = getPreferredOrNearestStore().store;

	if(!empty(store)){
		var personalizeData = !empty(store.custom.community) ? store.custom.community : store.city;
		var folder = ContentMgr.getFolder(personalizeData);
		if(!empty(folder)){
			personalizeData = folder.displayName;
		}
		app.getView({
			personalizeData: personalizeData
		}).render('/content/rendercontent');
	}

}

/**
 * @returns Store phone
 */
function personalizePhone() {

	var store = getPreferredOrNearestStore().store;

	if(!empty(store)){
		var personalizeData = !empty(store.phone) ? store.phone : "";
    personalizeData = viewHelpers.formatPhoneNumber(personalizeData);
    app.getView({
			personalizeData: personalizeData
		}).render('/content/rendercontent');
	}

}

/**
 * @returns Store phone
 */
function storePhone() {
	var store = getPreferredOrNearestStore().store;
	if(!empty(store)){
		var phone = !empty(store.phone) ? store.phone : "";
		return viewHelpers.formatPhoneNumber(phone);
	}
}

/**
 * @returns Store manager image
 */
function personalizeManagerImage() {

	var store = getPreferredOrNearestStore().store;

	if(!empty(store)){
		var personalizeData = !empty(store.custom.managerImage) ? store.custom.managerImage.getURL().toString() : "";
		app.getView({
			personalizeData: personalizeData
		}).render('/content/rendercontent');
	}

}

/**
 * @returns Store manager name
 */
function personalizeManagerName() {

	var store = getPreferredOrNearestStore().store;

	if(!empty(store)){
		var personalizeData = !empty(store.custom.manager) ? store.custom.manager : "";
		app.getView({
			personalizeData: personalizeData
		}).render('/content/rendercontent');
	}

}

/**
 * Creating Pricebook Id dynamically as per Preferred Store and setting it at store level.
 *
 */
function setPricebook(store) {
	var currency = session.getCurrency().getCurrencyCode().toLowerCase();
	if (!empty(store)) {
		var storeID = store.ID;

		var listPriceBookName = storeID + "-" + currency + "-list-prices";
		var salesPriceBookName = storeID + "-" + currency + "-sale-prices";

		var salePriceBook = PriceBookMgr.getPriceBook(salesPriceBookName);
		var listPriceBook = PriceBookMgr.getPriceBook(listPriceBookName);
		var bothPriceBookAvailable = (!empty(salePriceBook) && !empty(listPriceBook))?true:false;

		if(bothPriceBookAvailable){
			PriceBookMgr.setApplicablePriceBooks(salePriceBook,listPriceBook);
		}
		else{
			var priceBook = !empty(salePriceBook)?salePriceBook:!empty(listPriceBook)?listPriceBook:null;
			if(priceBook != null){
				PriceBookMgr.setApplicablePriceBooks(priceBook);
			}
		}
	}
}

function getStoresForSiteMap(){
	var states = Site.getCurrent().getCustomPreferenceValue('storeStatesList');
	var storesList = [];
	var stateMappings = JSON.parse(Site.getCurrent().getCustomPreferenceValue('stateCodeMapping'));
	for(var state in states){
		var storeCount = 0;
		var storesInState = SystemObjectMgr.querySystemObjects('Store', 'stateCode = {0}', 'stateCode desc', states[state]).asList();
		var cityList = [];
		var cityStoresList = [];
		for(var store in storesInState){
			if(cityList.indexOf(storesInState[store].city) < 0){
				cityList.push(storesInState[store].city);
				cityList.sort();
			}
		}
		for(var city in cityList){
			var cityStoresJson = {};
			cityStoresJson.city = cityList[city];
			cityStoresJson.stores= SystemObjectMgr.querySystemObjects('Store', 'city = {0}', 'city asc', cityList[city]).asList();
			if(!cityStoresJson.stores.empty){
				storeCount += cityStoresJson.stores.length;
				cityStoresList.push(cityStoresJson);
				cityStoresList.sort();
			}
		}
		storesList.push({stateCode: states[state] , stateName : stateMappings[states[state]], stores : cityStoresList, storeCount : storeCount.toString() + ((storeCount>1)?' Stores':' Store')});
	}
	return storesList;
}

function saveUserLocation(){
	var requestBody = request.httpParameterMap.requestBodyAsString, jsonData = JSON.parse(requestBody), data = {};

    for(var key in jsonData){
        switch(key){
            case "uLat":
            case 'uLng':
                data[key]=jsonData[key];
        }
    }
	// Add object to session to be displayed
	session.privacy.custUserLocation = JSON.stringify(data);
    let r = require("~/cartridge/scripts/util/Response");
    r.renderJSON({
        success: true
    });
}
/** Saes user geo location
 * @see module:controllers/Stores~saveUserLocation */
exports.SaveUserLocation = guard.ensure(['post', 'https'], saveUserLocation);

/*
 * Exposed web methods
 */

/** Returns closest Store
 * @see module:controllers/Stores~getClosestStore */
exports.GetClosestStore = guard.ensure(['get'], getClosestStore);
/** Returns users preferred Store
 * @see module:controllers/Stores~getUserPreferredStore */
exports.GetUserPreferredStore = guard.ensure(['get'], getUserPreferredStore);
/** Updates the user's store.
 * @see module:controllers/Stores~updatePreferredStore */
exports.UpdatePreferredStore = guard.ensure(['post'], updatePreferredStore);
/** Renders the Store in Header.
 * @see module:controllers/Stores~getPreferredOrNearestStore */
exports.GetPreferredOrNearestStore = guard.ensure(['get'], getPreferredOrNearestStore);
/** Renders the List of Stores on Store Locator Page.
 * @see module:controllers/Stores~showStores */
exports.ShowStores = guard.ensure(['get'], showStores);
/** Renders the details of a store.
 * @see module:controllers/Stores~showStoreDetails */
exports.ShowStoreDetails = guard.ensure(['get'], showStoreDetails);
/** Renders the region of the store.
 * @see module:controllers/Stores~personalizeContent*/
exports.PersonalizeCity = guard.ensure(['get'], personalizeCity);
/** Renders the phone of the store.
 * @see module:controllers/Stores~personalizePhone*/
exports.PersonalizePhone = guard.ensure(['get'], personalizePhone);
/** @see module:controllers/Stores~currentStorePhone*/
exports.StorePhone = guard.ensure(['post'], storePhone);
/** Renders the manager image of the store.
 * @see module:controllers/Stores~personalizeContent*/
exports.PersonalizeManagerImage = guard.ensure(['get'], personalizeManagerImage);
/** Renders the manager of the store.
 * @see module:controllers/Stores~personalizeManagerName*/
exports.PersonalizeManagerName = guard.ensure(['get'], personalizeManagerName);
/** Returns All The Stores To Be Shown on SiteMap.
 * @see module:controllers/Stores~getStoreJSONObject*/
exports.GetStoresForSiteMap = guard.ensure(['get'], getStoresForSiteMap);
