'use strict';

/**
 * Controller that calls riderstyler webservices to get the response for searching vehicle by type
 *
 * @module controllers/Vehicle
 */

var params = request.httpParameterMap;

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var rideStyler = require('int_ridestyler/cartridge/scripts/services/libRideStylerServices');
var pageMeta = require('~/cartridge/scripts/meta');

/* API Modules */
var Resource = require('dw/web/Resource');
var Result = require('dw/svc/Result');
var URLUtils = require('dw/web/URLUtils');


function invokeWheelSearch(params, vehicleObj, service) {

	// Fetch all bolt-patterns from ridestyler  
	var boltPatterService = rideStyler.fetchBoltpatternsService();
	var boltPatternResult = boltPatterService.call();
	var boltPatterns, requiredBoltPatternObj, fitments, fitment, VehicleFitmentBoltPatternID,
		diameterMin, diameterMax, offsetMin,offsetMax,widthMin,widthMax ;
	if(boltPatternResult.status == Resource.msg('service.response.ok','service',null)) {
		boltPatterns = JSON.parse(boltPatternResult.getObject()).BoltPatterns;
		
		// Fetch fitments for wheels from ridestyler
		addServiceParameters(service);
		var result = service.call();
		if(result.status == Resource.msg('service.response.ok','service',null)) {
			fitments = JSON.parse(result.getObject()).Fitments;
			fitment = fitments[0];
 			VehicleFitmentBoltPatternID = fitment.VehicleFitment_BoltPatternID;
 			
 			diameterMin = fitment.VehicleFitmentDiameterMin,
			diameterMax = fitment.VehicleFitmentDiameterMax,
			offsetMin = fitment.VehicleFitmentOffsetMin,
			offsetMax = fitment.VehicleFitmentOffsetMax,
			widthMin = fitment.VehicleFitmentWidthMin,
			widthMax = fitment.VehicleFitmentWidthMax;
			
			for (var i = 0; i < boltPatterns.length ; i++) {
				if(VehicleFitmentBoltPatternID == boltPatterns[i].BoltPatternID) {
					requiredBoltPatternObj = boltPatterns[i];
					break;
				}
			}
			
			// calculate min-value and max-value for range filters
			for (var i = 0; i < fitments.length ; i++) {
				diameterMin = Math.max(diameterMin, fitments[i].VehicleFitmentDiameterMin);
				diameterMax = Math.min(diameterMax, fitments[i].VehicleFitmentDiameterMax);
				offsetMin = Math.max(offsetMin, fitments[i].VehicleFitmentOffsetMin);
				offsetMax = Math.min(offsetMax, fitments[i].VehicleFitmentOffsetMax);
				widthMin = Math.max(widthMin, fitments[i].VehicleFitmentWidthMin);
				widthMax = Math.min(widthMax, fitments[i].VehicleFitmentWidthMax);
			}

			var boltPattern = requiredBoltPatternObj.BoltPatternBoltCount+'-'+requiredBoltPatternObj.BoltPatternSpacingMM+'|'+requiredBoltPatternObj.BoltPatternBoltCount+'-'+requiredBoltPatternObj.BoltPatternSpacingIN;

	    	//call the search
	    	var custEntryData = JSON.parse(session.privacy.custEntryData) || {};
	    	custEntryData.vehicle = {year: vehicleObj.year, make: vehicleObj.make, model: vehicleObj.model, trim: vehicleObj.trim, color: "BB1919"};
	    	session.privacy.custEntryData = JSON.stringify(custEntryData);

	    	//Create Searh Params Object
	    	var searchParams = [{
	    	    "id": "boltPattern",
	    	    "value": boltPattern
	    	}, 
	    	{
	    	    "id": "wheelWidth",
	    	    "range": [
	    	    	widthMin,
	    	    	widthMax
	    		]
	    	},
	    	{
	    	    "id": "diameter",
	    	    "range": [
	    	    	diameterMin,
	    	    	diameterMax
	    		]
	    	},
	    	{
	    	    "id": "offset",
	    	    "range": [
	    	    	offsetMin,
	    	    	offsetMax
	    		]
	    	}];	
	    	
	    	// Redirect to Search Show
	    	var searchURL = URLUtils.https('Search-Show','cgid','wheel').toString();
	    	var paramsArr =[], paramsStr;
	    	for(var i=0; i<searchParams.length;i++) {
	    		var j=i+1;
	    		if('boltPattern' == searchParams[i].id) {
	    			paramsArr.push('prefn' + j + '='+ searchParams[i].id);
	    			paramsArr.push('prefv' + j + '='+ searchParams[i].value);
	    		} else {
	    			paramsArr.push('prefn' + j + '='+ searchParams[i].id);
	    			paramsArr.push('prefmin' + j + '='+ searchParams[i].range[0]);
	    			paramsArr.push('prefmax' + j + '='+ searchParams[i].range[1]);
	    		}
	    	}
	    	paramsArr.push('source=wheelVehicleFlow');
	    	paramsArr.push('entry=wheels');
	    	
	    	paramsArr.push('year='+vehicleObj.year);
	    	paramsArr.push('make='+vehicleObj.makeId);
	    	paramsArr.push('model='+vehicleObj.modelId);
	    	paramsArr.push('VehicleConfiguration='+vehicleObj.trimId);
	    	
	    	paramsArr.push('makeName='+vehicleObj.make);
	    	paramsArr.push('modelName='+vehicleObj.model);
	    	paramsArr.push('trimName='+vehicleObj.trim);
	    	
	    	
	    	if (searchURL.indexOf('?') == -1) {
	    		searchURL = searchURL.concat('?');
	    	} else {
	    		searchURL = searchURL.concat('&');
	    	}
	    	paramsStr = paramsArr.join('&');
	    	response.redirect(searchURL + paramsStr);
	    	return;
		}
	} 
	return null; 
}

/**
 * Vehicle information flow start
 */
function show() {
	const START = 0,
		  COUNT = 100;
	var service, serviceType = null, metaParam;
	var vehicleObj = JSON.parse(session.privacy.vehicleObj) || {}, result, jsonObj, assetJsonObj;
	var breadcrumb = [];

	if (params.width.submitted && params.aspectRatio.submitted && params.loadIndex.submitted && params.rimSize.submitted) {
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid', params.entry.value), displayName: Resource.msg(params.entry.value + '.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show'), displayName: vehicleObj.year});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','year',vehicleObj.year), displayName: vehicleObj.make});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','year',vehicleObj.year,'make',vehicleObj.makeId), displayName: vehicleObj.model});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','year',vehicleObj.year,'make',vehicleObj.makeId,'model',vehicleObj.modelId), displayName: vehicleObj.trim});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','year',vehicleObj.year,'make',vehicleObj.makeId,'model',vehicleObj.modelId, 'VehicleConfiguration',vehicleObj.trimId), displayName: params.width+'/'+params.aspectRatio+'R-'+params.rimSize.value});
    	breadcrumb.push({url: '',displayName: Resource.msg('results.label','seo',null)});
    	
    	// Add Vehicle Data in Session for Tires
    	var custEntryData = JSON.parse(session.privacy.custEntryData) || {};
    	custEntryData.vehicle = {year: vehicleObj.year, make: vehicleObj.make, model: vehicleObj.model, trim: vehicleObj.trim, color: "BB1919"};
    	session.privacy.custEntryData = JSON.stringify(custEntryData);

    	//Create Searh Params Object
    	var searchParams = [{
    	    "id": "width",
    	    "value": params.width.value
    	}, 
    	{
    	    "id": "rimSize",
    	    "value": params.rimSize.value
    	},
    	{
    	    "id": "aspectRatio",
    	    "value": params.aspectRatio.value
    	},
    	{
    	    "id": "loadIndex",
    	    "range": [
    			params.loadIndex.value,
    			500
    		]
    	}];	
    	
    	// Redirect to Search Show
    	var searchURL = URLUtils.https('Search-Show','cgid','tire').toString();
    	var paramsArr =[], paramsStr;
    	for(var i=0; i<searchParams.length;i++) {
    		var j=i+1;
    		if('loadIndex' == searchParams[i].id) {
    			paramsArr.push('prefn' + j + '='+ searchParams[i].id);
    			paramsArr.push('prefmin' + j + '='+ searchParams[i].range[0]);
    			paramsArr.push('prefmax' + j + '='+ searchParams[i].range[1]);
    		} else {
    			paramsArr.push('prefn' + j + '='+ searchParams[i].id);
    			paramsArr.push('prefv' + j + '='+ searchParams[i].value);
    		}
    	}
    	paramsArr.push('source=tireVehicleFlow');
    	paramsArr.push('entry=tires');
    	paramsArr.push('year='+vehicleObj.year);
    	paramsArr.push('make='+vehicleObj.makeId);
    	paramsArr.push('model='+vehicleObj.modelId);
    	paramsArr.push('VehicleConfiguration='+vehicleObj.trimId);
    	
    	paramsArr.push('makeName='+vehicleObj.make);
    	paramsArr.push('modelName='+vehicleObj.model);
    	paramsArr.push('trimName='+vehicleObj.trim);
    	
    	if (searchURL.indexOf('?') == -1) {
    		searchURL = searchURL.concat('?');
    	} else {
    		searchURL = searchURL.concat('&');
    	}
    	paramsStr = paramsArr.join('&');
    	response.redirect(searchURL + paramsStr);
    	return;

	}else if (params.VehicleConfiguration.submitted) {
		serviceType = 'config';

		vehicleObj = {};
		vehicleObj.entry = params.entry.value;
		vehicleObj.year = params.year.value;

		service = rideStyler.fetchMakesService();
		addServiceParameters(service);
		result = service.call();
		vehicleObj.makeId = params.make.value;
		vehicleObj.make = getDisplayValue(result.getObject(),'Makes', 'VehicleMakeID', params.make.value, 'VehicleMakeName');
		
		service = rideStyler.fetchModelsService();
		addServiceParameters(service);
		result = service.call();
		vehicleObj.modelId = params.model.value;
		vehicleObj.model = getDisplayValue(result.getObject(),'Models', 'VehicleModelID', params.model.value, 'VehicleModelName');
		
		service = rideStyler.fetchDescriptionService();
		service.addParam('start', START);
		service.addParam('count', COUNT);
		addServiceParameters(service);
		result = service.call();
		vehicleObj.trimId = params.VehicleConfiguration.value;
		vehicleObj.trim = getDisplayValue(result.getObject(),'Descriptions', 'ConfigurationID', params.VehicleConfiguration.value, 'TrimName');
		assetJsonObj = prepareDynamicAssetJSON(vehicleObj, 'vehicleTrim');

		breadcrumb.push({url: URLUtils.url('Search-Show','cgid', params.entry.value), displayName: Resource.msg(params.entry.value + '.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value), displayName: vehicleObj.year});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value,'year',params.year.value), displayName: vehicleObj.make});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value,'year',params.year.value,'make',params.make.value), displayName: vehicleObj.model});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value,'year',params.year.value,'make',params.make.value,'model',params.model.value), displayName: vehicleObj.trim});
		breadcrumb.push({url: '#', displayName: Resource.msg('vehicle.select.'+params.entry.value+'.size','seo',null)});

		if(params.entry.value == 'wheels') {
	    	// Init Fitments Service
			service = rideStyler.fetchFitmentsService();
			// Redirect to Wheel Search Results
			invokeWheelSearch(params, vehicleObj, service);
		} else if(params.entry.value == 'tires') {
			// Init Tire Option Details
			service = rideStyler.fetchTireOptionDetailsService();

			metaParam = {
				pageTitle : Resource.msgf('vehicle.title.select.tires.size', 'seo', null, vehicleObj.year, vehicleObj.make, vehicleObj.model, vehicleObj.trim)
			}
			pageMeta.update(metaParam);
		}
	} else if (params.year.submitted && params.make.submitted && params.model.submitted) {
		serviceType = 'trim';

		vehicleObj = {};
		vehicleObj.entry = params.entry.value;
		vehicleObj.year = params.year.value;

		service = rideStyler.fetchMakesService();
		addServiceParameters(service);
		result = service.call();
		vehicleObj.makeId = params.make.value;
		vehicleObj.make = getDisplayValue(result.getObject(),'Makes', 'VehicleMakeID', params.make.value, 'VehicleMakeName');
		
		service = rideStyler.fetchModelsService();
		addServiceParameters(service);
		result = service.call();
		vehicleObj.modelId = params.model.value;
		vehicleObj.model = getDisplayValue(result.getObject(),'Models', 'VehicleModelID', params.model.value, 'VehicleModelName');

		assetJsonObj = prepareDynamicAssetJSON(vehicleObj, 'vehicleModel');
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid', params.entry.value), displayName: Resource.msg(params.entry.value + '.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value), displayName: vehicleObj.year});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value,'year',params.year.value), displayName: vehicleObj.make});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value,'year',params.year.value,'make',params.make.value), displayName: vehicleObj.model});
		breadcrumb.push({url: '#', displayName: Resource.msg('vehicle.select.trim','seo',null)});

		service = rideStyler.fetchDescriptionService();
		service.addParam('start', START);
		service.addParam('count', COUNT);

		metaParam = {
			pageTitle : Resource.msgf('vehicle.title.select.'+params.entry.value+'.trim', 'seo', null, vehicleObj.year, vehicleObj.make, vehicleObj.model)
		}
		pageMeta.update(metaParam);
	} else if (params.year.submitted && params.make.submitted) {
		serviceType = 'model';

		vehicleObj = {};
		vehicleObj.entry = params.entry.value;
		vehicleObj.year = params.year.value;
		
		service = rideStyler.fetchMakesService();
		addServiceParameters(service);
		result = service.call();
		vehicleObj.makeId = params.make.value;
		vehicleObj.make = getDisplayValue(result.getObject(),'Makes', 'VehicleMakeID', params.make.value, 'VehicleMakeName');
		
		assetJsonObj = prepareDynamicAssetJSON(vehicleObj, 'vehicleMake');
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid', params.entry.value), displayName: Resource.msg(params.entry.value + '.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value), displayName: vehicleObj.year});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value,'year',params.year.value), displayName: vehicleObj.make});
		breadcrumb.push({url: '#', displayName: Resource.msg('vehicle.select.model','seo',null)});
		service = rideStyler.fetchModelsService();

		metaParam = {
			pageTitle : Resource.msgf('vehicle.title.select.'+params.entry.value+'.model', 'seo', null, vehicleObj.year, vehicleObj.make)
		}
		pageMeta.update(metaParam);
	} else if(params.year.submitted){
		serviceType = 'make';
		vehicleObj = {};
		vehicleObj.entry = params.entry.value;
		vehicleObj.year = params.year.value;
		assetJsonObj = prepareDynamicAssetJSON(vehicleObj, 'vehicleYear');

		breadcrumb.push({url: URLUtils.url('Search-Show','cgid', params.entry.value), displayName: Resource.msg(params.entry.value + '.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show','entry', params.entry.value), displayName: vehicleObj.year});
		breadcrumb.push({url: '#', displayName: Resource.msg('vehicle.select.make','seo',null)});
		service = rideStyler.fetchMakesService();

		metaParam = {
			pageTitle : Resource.msgf('vehicle.title.select.'+params.entry.value+'.make', 'seo', null, vehicleObj.year)
		}
		pageMeta.update(metaParam);
	} else {
		serviceType = 'year';
		vehicleObj = {};
		vehicleObj.entry = params.entry.value;
		assetJsonObj = prepareDynamicAssetJSON(vehicleObj, 'vehicle');
		
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid', params.entry.value), displayName: Resource.msg(params.entry.value + '.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-Show', 'entry', params.entry.value), displayName: Resource.msg('vehicle.select.year','seo',null)});
		service = rideStyler.fetchYearsService();

		metaParam = {
			pageTitle : Resource.msg('vehicle.title.select.'+params.entry.value+'.year', 'seo', null)
		}
		pageMeta.update(metaParam);
	}
	session.privacy.vehicleObj = JSON.stringify(vehicleObj);
	addServiceParameters(service);
	result = service.call();

	if(result.status == Resource.msg('service.response.ok','service',null)) {
		app.getView({
			breadcrumData: breadcrumb,
			serviceType: serviceType,
			jsonData: result.getObject(),
			jsonObj: vehicleObj,
			assetJsonObj: assetJsonObj
		}).render('vehicle/vehicleinfo');
	 } else {
		 app.getView({
			 	breadcrumData: breadcrumb,
			 	serviceType: serviceType,
				jsonData: Resource.msg('service.response.erroroccured','service',null)
			}).render('vehicle/vehicleinfo');
	 }
}

/**
 * Function to prepare Dynamic Asset JSON for Vehicle and Tires SEO Pages
 * @param Vehicle or Tire JSON Object.
 * @param The SEO Page Parameter Submitted
 * @returns The Dynamic Asset Object
 **/
function prepareDynamicAssetJSON(object, params) {
	var assetJsonObj = {};
	if (params != null) {
		switch (params) {
			case 'vehicle':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>', value: Resource.msg('vehicle.select.entry.h1', 'seo', null)},
					{start:'<H2-START>',end: '<H2-END>', value: Resource.msg('vehicle.select.entry.'+object.entry+'.h2', 'seo', null)},
					{start:'<P-START>',end: '<P-END>', value: Resource.msg('vehicle.select.entry.'+object.entry+'.para', 'seo', null)}
				]};
				break;
			case 'vehicleYear':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msgf('vehicle.select.year.submit.h1', 'seo', null, object.year)},
					{start:'<H2-START>',end: '<H2-END>', value: Resource.msg('vehicle.select.entry.'+object.entry+'.h2', 'seo', null)}
				]};
				if(object.entry == 'wheels') {
					assetJsonObj.items.push({start:'<P-START>',end: '<P-END>',value: Resource.msg('vehicle.select.year.submit.'+object.entry+'.para1', 'seo', null) +' '+ Resource.msgf('vehicle.select.year.submit.'+object.entry+'.para2', 'seo', null, object.year)});
				} else {
					assetJsonObj.items.push({start:'<P-START>',end: '<P-END>',value: Resource.msgf('vehicle.select.year.submit.'+object.entry+'.para', 'seo', null, object.year)});
				}
				break;
			case 'vehicleMake':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msgf('vehicle.select.make.submit.'+object.entry+'.h1', 'seo', null, object.year, object.make)},
					{start:'<H2-START>',end: '<H2-END>',value: Resource.msgf('vehicle.select.make.submit.'+object.entry+'.h2', 'seo', null, object.make)}
				]};
				if(object.entry == 'wheels') {
					assetJsonObj.items.push({start:'<P-START>',end: '<P-END>',value: Resource.msg('vehicle.select.make.submit.'+object.entry+'.para1', 'seo', null)+' '+Resource.msgf('vehicle.select.make.submit.'+object.entry+'.para2', 'seo', null, object.year, object.make)});
				} else {
					assetJsonObj.items.push({start:'<P-START>',end: '<P-END>',value: Resource.msgf('vehicle.select.make.submit.'+object.entry+'.para', 'seo', null, object.year, object.make)});
				}
				break;
			case 'vehicleModel':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msgf('vehicle.select.model.submit.'+object.entry+'.h1', 'seo', null, object.year, object.make, object.model)},
					{start:'<H2-START>',end: '<H2-END>',value: Resource.msgf('vehicle.select.make.submit.'+object.entry+'.h2', 'seo', null, object.make)},
				]};
				if(object.entry == 'wheels') {
					assetJsonObj.items.push({start:'<P-START>',end: '<P-END>',value: Resource.msg('vehicle.select.model.submit.'+object.entry+'.para1', 'seo', null)+' '+Resource.msgf('vehicle.select.model.submit.'+object.entry+'.para2', 'seo', null, object.year, object.make, object.model)});
				} else {
					assetJsonObj.items.push({start:'<P-START>',end: '<P-END>',value: Resource.msgf('vehicle.select.model.submit.'+object.entry+'.para', 'seo', null, object.year, object.make, object.model)});
				}
				break;
			case 'vehicleTrim':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msgf('vehicle.select.trim.submit.'+object.entry+'.h1', 'seo', null, object.year, object.make, object.model, object.trim)},
					{start:'<H2-START>',end: '<H2-END>',value: Resource.msgf('vehicle.select.make.submit.'+object.entry+'.h2', 'seo', null, object.make)},
					{start:'<P-START>',end: '<P-END>',value: Resource.msgf('vehicle.select.trim.submit.para', 'seo', null, object.year, object.make, object.model, object.trim)}
				]};
				break;
			case 'tire':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>', value: Resource.msg('tire.select.entry.h1', 'seo', null)}
				]};
				break;
			case 'tireWidth':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msg('tire.select.entry.h1', 'seo', null)}
				]};
				break;
			case 'tireAspectRatio':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msg('tire.select.entry.h1', 'seo', null)}
				]};
				break;
			case 'wheel':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>', value: Resource.msg('wheel.select.entry.h1', 'seo', null)}
				]};
				break;
			case 'wheelBoltPattern':
				assetJsonObj = {items:[
					{start:'<H1-START>',end: '<H1-END>',value: Resource.msgf('wheel.select.boltPattern.submit.h1', 'seo', null, object.boltPattern)}
				]};
				break;
		}
		//TODO: REWORK THIS TO BE MORE FLEXIBLE
		if(object.entry == 'tires') {
			assetJsonObj.items.push({start:'<IMG-START>',end: '<IMG-END>', value: "<img src='" + URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/seo/directories/Tire_388.png").toString() + "' alt='A single tire in an upright postion against a white backdrop.'/>"});
		} else {
			assetJsonObj.items.push({start:'<IMG-START>',end: '<IMG-END>', value: "<img src='" + URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/seo/directories/Wheel_544.png").toString() + "' alt='A single wheel in an upright postion against a white backdrop.'/>"});
		}
	}
	return assetJsonObj;
}

/**
 * this function returns display value for selected vehicle parameter
 * @param obj
 * @param mainKey
 * @param subKeyAttr
 * @param subKeyAttrValue
 * @param requestedValue
 * @returns
 */
function getDisplayValue(obj,mainKey, subKeyAttr, subKeyAttrValue, requestedValue) {
	var jsonData = JSON.parse(obj);
	jsonData = jsonData[mainKey];
	for (var i=0; i< jsonData.length; i++) {
		var jsonObj = jsonData[i];
		if (subKeyAttr in jsonObj && jsonObj[subKeyAttr] == subKeyAttrValue) {
			if (requestedValue in jsonObj && jsonObj[requestedValue]) {
				return jsonObj[requestedValue];
			} else {
				return '';
			}
		}
	}
	return '';
}


/* Common Utility Function To Add All HTTPParameters to Service Object*/
function addServiceParameters(service){
	var parameterNames = params.getParameterNames().toArray();
	for(i=0;i<params.getParameterCount();i++){
		if(parameterNames[i] == 'year') {
			service.addParam('Year',params.get(parameterNames[i]));
		} else if(parameterNames[i] == 'make') {
			service.addParam('VehicleMake',params.get(parameterNames[i]));
		} else if(parameterNames[i] == 'model') {
			service.addParam('VehicleModel',params.get(parameterNames[i]));
		} else {
			service.addParam(parameterNames[i],params.get(parameterNames[i]));
		}
	}
}

/***
 * This page handles the tire size selection flow
 */
function showTire() {
	var breadcrumb = [];
	var refinements = [];
	var stepValue = 'width';
	var Search = app.getModel('Search');
    var jsonData = null, attributeID = null, assetJsonObj, tireObj = {}, metaParam;

	metaParam = {
		pageTitle : Resource.msg('title.select.tires.size', 'seo', null)
	}
	pageMeta.update(metaParam);
    if (params.width.submitted && params.aspectRatio.submitted && params.rimSize.submitted) {
    	breadcrumb.push({url: URLUtils.url('Search-Show','cgid','tires'), displayName: Resource.msg('tires.label','seo',null)});
    	//breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire','width',params.width.value),displayName: params.width.value+ ' mm'});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire','width',params.width.value),displayName: params.width.value});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire','width',params.width.value),displayName: params.aspectRatio.value});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire','width',params.width.value,'aspectRatio',params.aspectRatio.value),displayName: params.rimSize.value + ' inch'});
    	breadcrumb.push({url: '',displayName: 'Result'});
    	
    	//Call Search
    	var custEntryData = JSON.parse(session.privacy.custEntryData) || {};
    	custEntryData.tires = {front: {width: params.width.value, ratio: params.aspectRatio.value, rim: params.rimSize.value}, rear: null};
    	session.privacy.custEntryData = JSON.stringify(custEntryData);
    	
    	//Create Searh Params Object
    	var searchParams = [{
    	    "id": "width",
    	    "value": params.width.value
    	},
    	{
    	    "id": "rimSize",
    	    "value": params.rimSize.value
    	},
    	{
    	    "id": "aspectRatio",
    	    "value": params.aspectRatio.value
    	}
    	];
    	
    	// Redirect to Search Show
    	var searchURL = URLUtils.https('Search-Show','cgid','tire');
    	for each(var i = 0; i < searchParams.length; i++) {
    		var j=i+1;
			searchURL.append('prefn'+j,searchParams[i].id);
			searchURL.append('prefv'+j,searchParams[i].value);
    	}
    	searchURL.append('source','tireSizeFlow');
    	searchURL.append('entry','tires');
    	response.redirect(searchURL);
    	return;
    	
    }else if (params.width.submitted && params.aspectRatio.submitted) {
    	refinements.push({key: 'width', value: params.width.value});
    	refinements.push({key: 'aspectRatio', value: params.aspectRatio.value});
    	attributeID = 'rimSize';
    	stepValue = 'rimSize';
		tireObj = {
			width:	params.width.value,
			aspectRatio: params.aspectRatio.value
		};
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid','tires'), displayName: Resource.msg('tires.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire'),displayName: params.width.value});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire','width',params.width.value),displayName: params.aspectRatio.value});
    	breadcrumb.push({url: '',displayName: Resource.msg('tire.select.rimSize','seo',null)});
    	jsonData = Search.getRefinementValues('tires',refinements, attributeID);
    	assetJsonObj = prepareDynamicAssetJSON(tireObj, 'tireAspectRatio');
    	
    }else if (params.width.submitted) {
    	refinements.push({key: 'width', value: params.width.value});
    	attributeID = 'aspectRatio';
    	stepValue = 'aspectRatio';
		tireObj = {
			width: params.width.value
		};
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid','tires'), displayName: Resource.msg('tires.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-ShowTire'),displayName: params.width.value});
    	breadcrumb.push({url: '',displayName: Resource.msg('tire.select.aspectRatio','seo',null)});
    	jsonData = Search.getRefinementValues('tires',refinements, attributeID);
    	assetJsonObj = prepareDynamicAssetJSON(tireObj, 'tireWidth');
    } else {
    	attributeID = 'width';
    	breadcrumb.push({url: URLUtils.url('Search-Show','cgid','tires'), displayName: Resource.msg('tires.label','seo',null)});
    	breadcrumb.push({url: '',displayName: Resource.msg('tire.select.width','seo',null)});
    	jsonData = Search.getRefinementValues('tires',refinements, attributeID);
    	assetJsonObj = prepareDynamicAssetJSON(tireObj, 'tire');
    }

	app.getView({
		stepValue: stepValue,
		breadcrumData: breadcrumb,
		jsonData: jsonData,
		assetJsonObj: assetJsonObj
	}).render('vehicle/tiresizeinfo');

}

/***
 * This page handles the wheel size selection flow
 */
function showWheel() {
	var breadcrumb = [];
	var refinements = [];
	var stepValue = 'boltPattern';
	var Search = app.getModel('Search');
    var jsonData = null, attributeID = null, assetJsonObj, wheelObj = {}, metaParam;

    if (params.boltPattern.submitted && params.diameter.submitted) {
    	breadcrumb.push({url: URLUtils.url('Search-Show','cgid','wheels'), displayName: Resource.msg('wheels.label','seo',null)});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowWheel','boltPattern',params.boltPattern.value),displayName: params.boltPattern.value});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowWheel','boltPattern',params.boltPattern.value),displayName: params.diameter.value});
    	breadcrumb.push({url: URLUtils.url('Vehicle-ShowWheel','boltPattern',params.boltPattern.value,'diameter',params.diameter.value),displayName: params.diameter.value + ' inch'});
    	breadcrumb.push({url: '',displayName: 'Result'});

    	//call the search
    	var custEntryData = JSON.parse(session.privacy.custEntryData) || {};
    	custEntryData.wheels = {front: {boltPattern: params.boltPattern.value, diameter: params.diameter.value}, rear: null};
    	session.privacy.custEntryData = JSON.stringify(custEntryData);

    	//Create Searh Params Object
    	var searchParams = [{
    	    "id": "boltPattern",
    	    "value": params.boltPattern.value
    	}, 
    	{
    	    "id": "diameter",
    	    "value": params.diameter.value    	
    	}];	
    	
    	// Redirect to Search Show
    	var searchURL = URLUtils.https('Search-Show','cgid','wheel');
    	for each(var i = 0; i < searchParams.length; i++) {
    		var j=i+1;
			searchURL.append('prefn'+j,searchParams[i].id);
			searchURL.append('prefv'+j,searchParams[i].value);
    	}
    	searchURL.append('source','wheelSizeFlow');
    	searchURL.append('entry','wheels');
    	response.redirect(searchURL);
    	return;
    	
    } else if (params.boltPattern.submitted) {
    	refinements.push({key: 'boltPattern', value: params.boltPattern.value});
    	attributeID = 'diameter';
    	stepValue = 'diameter';
		wheelObj = {
			boltPattern: params.boltPattern.value
		};
		breadcrumb.push({url: URLUtils.url('Search-Show','cgid','wheels'), displayName: Resource.msg('wheels.label','seo',null)});
		breadcrumb.push({url: URLUtils.url('Vehicle-ShowWheel','width', params.boltPattern.value),displayName: params.boltPattern.value});
    	breadcrumb.push({url: '',displayName: Resource.msg('wheel.select.diameter','seo',null)});
    	jsonData = Search.getRefinementValues('wheels',refinements, attributeID);
    	assetJsonObj = prepareDynamicAssetJSON(wheelObj, 'wheelBoltPattern');
    	metaParam = {
   			pageTitle : Resource.msgf('title.select.wheels.boltPattern', 'seo', null, params.boltPattern.value)
   		}
   		pageMeta.update(metaParam);
    } else {
    	attributeID = 'boltPattern';
    	breadcrumb.push({url: URLUtils.url('Search-Show','cgid','wheels'), displayName: Resource.msg('wheels.label','seo',null)});
    	breadcrumb.push({url: '',displayName: Resource.msg('wheel.select.boltPattern','seo',null)});
    	jsonData = Search.getRefinementValues('wheels',refinements, attributeID);
    	assetJsonObj = prepareDynamicAssetJSON(wheelObj, 'wheel');
    	metaParam = {
   			pageTitle : Resource.msg('title.select.wheels.size', 'seo', null)
   		}
   		pageMeta.update(metaParam);
    }

	app.getView({
		stepValue: stepValue,
		breadcrumData: breadcrumb,
		jsonData: jsonData,
		assetJsonObj: assetJsonObj
	}).render('vehicle/wheelsizeinfo');

}

exports.Show = guard.ensure(['get'],show);
exports.ShowTire = guard.ensure(['get'],showTire);
exports.ShowWheel = guard.ensure(['get'],showWheel);