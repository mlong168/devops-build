/**
* This controller will handle and process Appointment Schedule requests
*
* @module  controllers/Appointment
*/

'use strict';

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var Response = require('~/cartridge/scripts/util/Response');

/**
* Returns JSON string representing appoitment CTA component
* @param
* @return {String} The JSON string for appointemnt CTA component
*/
 var appointmentCTA = function(params){
     //TODO: Pankaj, please populate pid, qty and sids(array of service ids) here
     var jsonData = {
         componentName: "AppointmentCTA",
         ctaUrl: URLUtils.url('Appointment-Start').toString(),
         ctaLabel: Resource.msg('global.scheduleinstallation', 'locale', null),
         pid: !empty(params.pid)?params.pid:null,
         qty: !empty(params.qty)?params.qty.toString():null,
         sids: !empty(params.sids)?params.sids:null
     };
     return jsonData;
// 	app.getView({jsonData: JSON.stringify(jsonData)}).render('react/staticshow');
 }

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
 var start = function(){
     var storeID;
	 var storeService = null;
	 var nearestStore = app.getController('Stores').GetPreferredOrNearestStore();
	 if (!empty(nearestStore)) {
		 storeID = nearestStore.store.ID;
	 }
	 try {
	 var locationData = app.getController('AppointmentPlus').GetLocations(storeID);
	 var locationID = locationData.data[0].location_id;
	 var result = app.getController('AppointmentPlus').GetAssignedServices(locationID);

	 if (result) {
		var serviceData =  result.data;
		var storeArr = [];
		 for (var i=0; i<serviceData.length; i++) {
			 storeArr.push(serviceData[i].service_id);
		 }
		 storeService = storeArr;
	 }
	 } catch (e) {

	}
     var jsonData = {
         componentName: "AppointmentScheduler",
         services: storeService,
         //userStore: app.getController('ReactComponent').PopulateStoreInformation(nearestStore)
     };

 	app.getView({jsonData: JSON.stringify(jsonData)}).render('appointment/services');
 }

 var slots = function(){
     try {
 		 var openSlots = null; var openSlotsArray = [], slot = null;
 		 var params = buildParamsToRetrieveOpenSlots();
 		 var getOpenSlotResults = app.getController('AppointmentPlus').GetOpenSlots(params);
 		 if(!empty(getOpenSlotResults) && getOpenSlotResults.result == 'success') {
 			 var resultObj = getOpenSlotResults.data;
 			 for (var i=0; i<resultObj.length; i++) {
 				 slot = {};
 				 slot.startTimestamp = resultObj[i].startTimestamp;
 				 slot.endTimestamp = resultObj[i].endTimestamp;
 				 openSlotsArray.push(slot);
 			 }
 			 openSlots = {};
 			 openSlots.slots = openSlotsArray;
 		 }
 	 }catch (e) {
 		 dw.system.Logger.error(e);
 	 }
 	 Response.renderJSON(openSlots);
 }

 var contact = function(){
     app.getView({
    	 Step : '3',
    	 showVehicle : false
     }).render('appointment/contact');
 }

 var review = function(){
     app.getView({
    	 Step : '4',
    	 showVehicle : false
     }).render('appointment/review');
 }

 var submit = function(){
     showConfirmation();
 }

 var editAppointment = function(){
	 showConfirmation();//TODO: BE needs to readdress it
 }

 var showConfirmation = function() {
	 app.getView().render('appointment/confirmation');
 }


 var buildParamsToRetrieveOpenSlots = function(){

	 var params = {}; var appointmentServices={};

	 // Temporary code just for service calling/response testing.

	 params.locationId = "21";
	 params.startTimestamp = "2018-05-11T00:00:00Z";
	 params.endTimestamp = "2018-06-11T00:00:00Z";
	 appointmentServices.serviceId = "102";
	 appointmentServices.locationId = "21";
	 appointmentServices.spots = "1";
	 params.appointmentService = appointmentServices;

	 return JSON.stringify(params);
 }
 var buildParamsToCreateCustomers = function(){
	 var params = {};
	 // Temporary code just for service calling/response testing.
	 params.locationId = "21";
	 params.first_name = "Test";
	 params.last_name = "Customer";
	 params.day_phone = "333-3333-3333";
	 params.email = "test.customer@isobar.com"
	 return params;
 }

/* Exports of the controller */
 /**
 * @see {@link module:controllers/Appointment~appointmentCTA} */
exports.AppointmentCTA = guard.ensure(['https','get'], appointmentCTA);
 /**
 * @see {@link module:controllers/Appointment~start} */
exports.Start = guard.ensure(['https','get'], start);
/**
* @see {@link module:controllers/Appointment~review} */
exports.Review = guard.ensure(['get','https'], review);
/**
* @see {@link module:controllers/Appointment~review} */
exports.Slots = guard.ensure(['get','https'], slots);
/**
* @see {@link module:controllers/Appointment~review} */
exports.Contact = guard.ensure(['get','https'], contact);
/**
* @see {@link module:controllers/Appointment~review} */
exports.Submit = guard.ensure(['post','https'], submit);

/**
* @see {@link module:controllers/Appointment~review} */
exports.Edit = guard.ensure(['post','https'], editAppointment);
/*
 * Local method
 */
