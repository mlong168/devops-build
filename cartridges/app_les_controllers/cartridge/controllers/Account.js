'use strict';

/**
 * Controller that renders the account overview, manages customer registration and password reset,
 * and edits customer profile information.
 *
 * @module controllers/Account
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Form = require('~/cartridge/scripts/models/FormModel');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var UUIDUtils = require('dw/util/UUIDUtils');
var Site = require("dw/system/Site");

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var guard = require('~/cartridge/scripts/guard');

var j=1;

/**
 * Gets a ContentModel object that wraps the myaccount-home content asset,
 * updates the page metadata, and renders the account/accountoverview template.
 */
function show() {
    var accountHomeAsset, pageMeta, Content, vehicles, wheels, tires;

    Content = app.getModel('Content');
    accountHomeAsset = Content.get('myaccount-home');

    if(customer.authenticated){
    	vehicles = JSON.parse(customer.profile.custom.vehiclesJson);
    	wheels = JSON.parse(customer.profile.custom.wheelsJson);
    	tires = JSON.parse(customer.profile.custom.tiresJson);
    }

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountHomeAsset);

    app.getView({
    	"vehicles":vehicles,
    	"wheels":wheels,
    	"tires":tires
    	}).render('account/accountoverview');
}

function addAddressFields() {
     app.getView().render('account/user/addressfields');
}

/**
 * Clears the profile form and copies customer profile information from the customer global variable
 * to the form. Gets a ContentModel object that wraps the myaccount-personaldata content asset, and updates the page
 * meta data. Renders the account/user/registration template using an anonymous view.
 */
function editProfile() {
    var pageMeta;
    var accountPersonalDataAsset;
    var Content = app.getModel('Content');

        app.getForm('profile').clear();

        app.getForm('profile.customer').copyFrom(customer.profile);
        app.getForm('profile.login').copyFrom(customer.profile.credentials);

    	if(!empty(session.forms.profile.customer.phonemobile.value)) {
    		session.forms.profile.customer.phonetype.value = 'mobile';
    	} else if (!empty(session.forms.profile.customer.phonework.value)) {
    		session.forms.profile.customer.phonetype.value = 'work';
    	} else if(!empty(session.forms.profile.customer.phonehome.value)) {
    		session.forms.profile.customer.phonetype.value = 'home';
    	} else {
    		session.forms.profile.customer.phonetype.value = '';
    	}

    	if(!empty(customer.profile.addressBook.addresses)) {
            // Gets user address to be edited.
            var address = customer.profile.addressBook.addresses[0];

            app.getForm('profile.address').copyFrom(address);
            app.getForm('profile.address.states').copyFrom(address);
        }
    accountPersonalDataAsset = Content.get('myaccount-personaldata');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountPersonalDataAsset);
    // @FIXME bctext2 should generate out of pagemeta - also action?!
    app.getView({
        bctext2: Resource.msg('account.user.registration.editaccount', 'account', null),
        Action: 'edit',
        ContinueURL: URLUtils.https('Account-EditForm')
    }).render('account/user/personalinfoform');
}

/**
 * Handles the form submission on profile update of edit profile. Handles cancel and confirm actions.
 *  - cancel - clears the profile form and redirects to the Account-Show controller function.
 *  - confirm - gets a CustomerModel object that wraps the current customer. Validates several form fields.
 * If any of the profile validation conditions fail, the user is redirected to the Account-EditProfile controller function. If the profile is valid, the user is redirected to the Account-Show controller function.
 */
function editForm() {
    app.getForm('profile').handleAction({
        cancel: function () {
            app.getForm('profile').clear();
            response.redirect(URLUtils.https('Account-Show'));
        },
        confirm: function () {
            var isProfileUpdateValid = true;
            var hasEditSucceeded = false;
            var isAjax = request.httpParameterMap.format.submitted && request.httpParameterMap.format.value == 'ajax' ? true : false;
            var isAddressSave = request.httpParameterMap.isSaveAddress;
            var Customer = app.getModel('Customer');
            var Address = app.getModel('Address');

            if (!Customer.checkUserName()) {
                app.getForm('profile.customer.email').invalidate();
                isProfileUpdateValid = false;
            }

            // Updating PhoneNumber in Form
            var phoneType = app.getForm('profile.customer.phonetype').value();
            if(!empty(phoneType)) {
                var phoneNumber = app.getForm('profile.customer.phonetemp').value();
            	if (phoneType == "mobile") {
        			session.forms.profile.customer.phonemobile.value = !empty(phoneNumber) ? phoneNumber : '';
        		} else if (phoneType == "work") {
        			session.forms.profile.customer.phonework.value = !empty(phoneNumber) ? phoneNumber : '';
        		} else if (phoneType == "home") {
        			session.forms.profile.customer.phonehome.value = !empty(phoneNumber) ? phoneNumber : '';
        		}
            }

            if (isProfileUpdateValid) {

            	// Updating Customer Address
            	if(isAddressSave.booleanValue) {
                    if(!empty(customer.profile.addressBook.addresses)) {
                    	session.forms.profile.address.country.value = 'US';
                    	try {
                    		Address.update(session.forms.profile.address.addressid.value, session.forms.profile.address);
                    		success = true;
                        } catch (e) {
                        	success = false;
                        	response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
                        }
                    } else {
                    	session.forms.profile.address.addressid.value = 'default-address';
                    	if(!Address.create(session.forms.profile.address)) {
                			response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
                		}
                    }
            	}

            	hasEditSucceeded = Customer.updateAccountProfile(app.getForm('profile.customer.email').value(), app.getForm('profile'));

                if (!hasEditSucceeded) {
                    isProfileUpdateValid = false;
                }
            }

            if (isAjax) {
            	 let r = require('~/cartridge/scripts/util/Response');

                 r.renderJSON({
                     success: isProfileUpdateValid,
                 });
            } else {
	            if (isProfileUpdateValid && hasEditSucceeded) {
	                response.redirect(URLUtils.https('Account-Show'));
	            } else {
	                response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
	            }
            }
        },
        changepassword: function () {
            var isProfileUpdateValid = true;
            var hasEditSucceeded = false;
            var Customer = app.getModel('Customer');

            if (!Customer.checkUserName()) {
                app.getForm('profile.customer.email').invalidate();
                isProfileUpdateValid = false;
            }

            if (!app.getForm('profile.login.currentpassword').value()) {
                app.getForm('profile.login.currentpassword').invalidate();
                isProfileUpdateValid = false;
            }

            if (app.getForm('profile.login.newpassword').value() !== app.getForm('profile.login.newpasswordconfirm').value()) {
                app.getForm('profile.login.newpasswordconfirm').invalidate();
                isProfileUpdateValid = false;
            }

            if (isProfileUpdateValid) {
                hasEditSucceeded = Customer.editAccount(app.getForm('profile.customer.email').value(), app.getForm('profile.login.newpassword').value(), app.getForm('profile.login.currentpassword').value(), app.getForm('profile'));
                if (!hasEditSucceeded) {
                    app.getForm('profile.login.currentpassword').invalidate();
                }
            }

            if (isProfileUpdateValid && hasEditSucceeded) {
                response.redirect(URLUtils.https('Account-Show'));
            } else {
                response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
            }
        },
        error: function () {
            response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
        }
    });
}

/**
 * Gets the requestpassword form and renders the requestpasswordreset template. This is similar to the password reset
 * dialog, but has a screen-based interaction instead of a popup interaction.
 */
function passwordReset() {
    app.getForm('requestpassword').clear();
    app.getView({
        ContinueURL: URLUtils.https('Account-PasswordResetForm')
    }).render('account/password/requestpasswordreset');
}

/**
 * Handles form submission from dialog and full page password reset. Handles cancel, send, and error actions.
 *  - cancel - renders the given template.
 *  - send - gets a CustomerModel object that wraps the current customer. Gets an EmailModel object that wraps an Email object.
 * Checks whether the customer requested the their login password be reset.
 * If the customer wants to reset, a password reset token is generated and an email is sent to the customer using the mail/resetpasswordemail template.
 * Then the account/password/requestpasswordreset_confirm template is rendered.
 *  - error - the given template is rendered and passed an error code.
 */
function passwordResetFormHandler(templateName, continueURL) {
    var resetPasswordToken, passwordemail;

    app.getForm('profile').handleAction({
        cancel: function () {
            app.getView({
                ContinueURL: continueURL
            }).render(templateName);
        },
        send: function () {
            var Customer, resettingCustomer, Email;
            Customer = app.getModel('Customer');
            Email = app.getModel('Email');
            var requestForm = Form.get('requestpassword').object.email.htmlValue;
            resettingCustomer = Customer.retrieveCustomerByLogin(requestForm);

            if (!empty(resettingCustomer)) {
                resetPasswordToken = resettingCustomer.generatePasswordResetToken();

                passwordemail = Email.get('mail/resetpasswordemail', resettingCustomer.object.profile.email);
                passwordemail.setSubject(Resource.msg('resource.passwordassistance', 'email', null));
                passwordemail.send({
                    ResetPasswordToken: resetPasswordToken,
                    Customer: resettingCustomer.object.profile.customer
                });
            }

            //for security reasons the same message will be shown for a valid reset password request as for a invalid one
            app.getView({
                ErrorCode: null,
                ShowContinue: true,
                ContinueURL: continueURL,
                PasswordReset: true,
            }).render('account/login/accountlogin');
        },
        error: function () {
            app.getView({
                ErrorCode: 'formnotvalid',
                ContinueURL: continueURL
            }).render(templateName);
        }
    });
}

/**
 * The form handler for password resets.
 */
function passwordResetForm() {
    passwordResetFormHandler('account/password/requestpasswordreset', URLUtils.https('Login-Show'));
}

/**
 * Clears the requestpassword form and renders the account/password/requestpasswordresetdialog template.
 */
function passwordResetDialog() {
    // @FIXME reimplement using dialogify
    app.getForm('requestpassword').clear();
    app.getView({
        ContinueURL: URLUtils.https('Account-PasswordResetDialogForm')
    }).render('account/password/requestpasswordresetdialog');
}

/**
 * Handles the password reset form.
 */
function passwordResetDialogForm() {
    // @FIXME reimplement using dialogify
    passwordResetFormHandler('account/password/requestpasswordresetdialog', URLUtils.https('Login-Show'));
}

/**
 * Gets a CustomerModel wrapping the current customer. Clears the resetpassword form. Checks if the customer wants to reset their password.
 * If there is no reset token, redirects to the Account-PasswordReset controller function. If there is a reset token,
 * renders the screen for setting a new password.
 */
function setNewPassword() {
    var Customer, resettingCustomer;
    Customer = app.getModel('Customer');

    app.getForm('resetpassword').clear();
    resettingCustomer = Customer.getByPasswordResetToken(request.httpParameterMap.Token.getStringValue());

    if (empty(resettingCustomer)) {
        response.redirect(URLUtils.https('Account-PasswordReset'));
    } else {
        app.getView({
            ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
        }).render('account/password/setnewpassword');
    }
}

/**
 * Gets a profile form and handles the cancel and send actions.
 *  - cancel - renders the setnewpassword template.
 *  - send - gets a CustomerModel object that wraps the current customer and gets an EmailModel object that wraps an Email object.
 * Checks whether the customer can be retrieved using a reset password token.
 * If the customer does not have a valid token, the controller redirects to the Account-PasswordReset controller function.
 * If they do, then an email is sent to the customer using the mail/setpasswordemail template and the setnewpassword_confirm template is rendered.
 * */
function setNewPasswordForm() {

    app.getForm('profile').handleAction({
        cancel: function () {
            app.getView({
                ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
            }).render('account/password/setnewpassword');
            return;
        },
        send: function () {
            var Customer;
            var Email;
            var passwordChangedMail;
            var resettingCustomer;
            var success;

            Customer = app.getModel('Customer');
            Email = app.getModel('Email');
            resettingCustomer = Customer.getByPasswordResetToken(request.httpParameterMap.Token.getStringValue());

            if (!resettingCustomer) {
                response.redirect(URLUtils.https('Account-PasswordReset'));
            }

            if (app.getForm('resetpassword.password').value() !== app.getForm('resetpassword.passwordconfirm').value()) {
                app.getForm('resetpassword.passwordconfirm').invalidate();
                app.getView({
                    ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
                }).render('account/password/setnewpassword');
            } else {

                success = resettingCustomer.resetPasswordByToken(request.httpParameterMap.Token.getStringValue(), app.getForm('resetpassword.password').value());
                if (!success) {
                    app.getView({
                        ErrorCode: 'formnotvalid',
                        ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
                    }).render('account/password/setnewpassword');
                } else {
                    passwordChangedMail = Email.get('mail/passwordchangedemail', resettingCustomer.object.profile.email);
                    passwordChangedMail.setSubject(Resource.msg('resource.passwordassistance', 'email', null));
                    passwordChangedMail.send({
                        Customer: resettingCustomer.object
                    });

                    // User Login
                    Customer.login(resettingCustomer.object.profile.email, app.getForm('resetpassword.password').value(), null);

                    //app.getView().render('account/password/setnewpassword_confirm');
                    response.redirect(URLUtils.httpsHome());
                }
            }
        }
    });
}

/** Clears the profile form, adds the email address from login as the profile email address,
 *  and renders customer registration page.
 */
function startRegister() {

    app.getForm('profile').clear();

    app.getForm('oauthlogin').clear();


    if (app.getForm('login.username').value() !== null) {
        app.getForm('profile.customer.email').object.value = app.getForm('login.username').object.value;
    }

    app.getView({
        ContinueURL: URLUtils.https('Account-RegistrationForm')
    }).render('account/user/registration');
}

/**
 * Gets a CustomerModel object wrapping the current customer.
 * Gets a profile form and handles the confirm action.
 *  confirm - validates the profile by checking  that the email and password fields:
 *  - match the emailconfirm and passwordconfirm fields
 *  - are not duplicates of existing username and password fields for the profile
 * If the fields are not valid, the registration template is rendered.
 * If the fields are valid, a new customer account is created, the profile form is cleared and
 * the customer is redirected to the Account-Show controller function.
 */
function registrationForm() {
    app.getForm('profile').handleAction({
        confirm: function () {
            var email, emailConfirmation, orderNo, profileValidation, password, passwordConfirmation, existingCustomer, Customer, target;

            Customer = app.getModel('Customer');
            email = app.getForm('profile.customer.email').value();
            emailConfirmation = app.getForm('profile.customer.emailconfirm').value();
            orderNo =  app.getForm('profile.customer.orderNo').value();
            profileValidation = true;

            password = app.getForm('profile.login.password').value();
            passwordConfirmation = app.getForm('profile.login.passwordconfirm').value();

            if (password !== passwordConfirmation) {
                app.getForm('profile.login.passwordconfirm').invalidate();
                profileValidation = false;
            }

            // Checks if login is already taken.
            existingCustomer = Customer.retrieveCustomerByLogin(email);
            if (existingCustomer !== null) {
                app.getForm('profile.customer.email').invalidate();
                profileValidation = false;
            }

            if (profileValidation) {
                profileValidation = Customer.createAccount(email, password, app.getForm('profile'));

                if (orderNo) {
                    var orders = OrderMgr.searchOrders('orderNo={0} AND status!={1}', 'creationDate desc', orderNo,
                            dw.order.Order.ORDER_STATUS_REPLACED);
                    if (orders) {
                        var foundOrder = orders.next();
                        Transaction.wrap(function(){
                            foundOrder.customer = profileValidation;
                        })
                        session.custom.TargetLocation = URLUtils.https('Account-Show','Registration','true');
                    }
                }
            }

            if (!profileValidation) {

                app.getView({
                    ContinueURL: URLUtils.https('Account-RegistrationForm')
                }).render('account/user/registration');
            } else {
                app.getForm('profile').clear();
                target = session.custom.TargetLocation;
                if (target) {
                    delete session.custom.TargetLocation;
                    //@TODO make sure only path, no hosts are allowed as redirect target
                    dw.system.Logger.info('Redirecting to "{0}" after successful login', target);
                    response.redirect(target);
                } else {
                    response.redirect(URLUtils.https('Account-Show', 'registration', 'true'));
                }
            }
        }
    });
}

/**
 * Determines whether the request has an OAuth provider set. If it does, calls the
 * {@link module:controllers/Login~handleOAuthLoginForm|handleOAuthLoginForm} function,
 * if not, calls the {@link module:controllers/Login~handleLoginForm|handleLoginForm} function.
 */
function processRegisterForm () {
    if (request.httpParameterMap.OAuthProvider.stringValue) {
    	app.getController('Login').OAuthLoginForm();
    } else {
    	registrationForm();
    }
}

/**
 * Renders the accountnavigation template.
 */
function includeNavigation() {
    app.getView().render('account/accountnavigation');
}

/**
 * Renders the settings template.
 */
function settings() {
    app.getForm('profile').clear();
    app.getForm('profile.customer').copyFrom(customer.profile);

    var billpay = customer.profile.custom.billPayReminder;
    var appointment = customer.profile.custom.appointmentReminder;
    var maintenance = customer.profile.custom.maintenanceReminder;
    var maintenancetext = false, maintenanceemail = false;
    if(!empty(maintenance)) {
    	if(maintenance.length == 1) {
    		if(maintenance[0] == 'email') {
    			maintenanceemail = true;
    		} else {
    			maintenancetext = true;
    		}
    	} else if (maintenance.length == 2) {
    		maintenanceemail = true;
    		maintenancetext = true;
    	}
    }
    var generalpreference = customer.profile.custom.generalPreference;

    app.getView({
    	ContinueURL: URLUtils.https('Account-SettingsForm'),
    	billpay: billpay,
    	appointment: appointment,
    	maintenancetext: maintenancetext,
    	maintenanceemail: maintenanceemail,
    	generalpreference: generalpreference
    }).render('account/settings');
}

function settingsForm() {
	var Customer = app.getModel('Customer');
	app.getForm('profile').handleAction({
		changepreference: function () {
			var maintenance = [];
			var preferenceView = {};

			if(!empty(request.httpParameterMap.maintenanceemail.value)) {
				maintenance.push('email');
			}

			if(!empty(request.httpParameterMap.maintenancetext.value)) {
	    		maintenance.push('text');
	    	}
    		preferenceView.maintenance = maintenance;
			preferenceView.billpay = !empty(request.httpParameterMap.billpay.value) ? request.httpParameterMap.billpay.value : '';
			preferenceView.appointment = !empty(request.httpParameterMap.appointment.value) ? request.httpParameterMap.appointment.value : '';
			preferenceView.generalpreference = !empty(request.httpParameterMap.generalpreference.value) ? 'email' : '';

			Customer.updateSettingsPreferences(preferenceView);

	    	response.redirect(URLUtils.https('Account-Settings'));
	    },
	    changepassword: function () {
	        var isProfileUpdateValid = true;
	        var hasEditSucceeded = false;

	        if (!Customer.checkUserName()) {
	            app.getForm('profile.customer.email').invalidate();
	            isProfileUpdateValid = false;
	        }

	        if (!app.getForm('profile.login.currentpassword').value()) {
	            app.getForm('profile.login.currentpassword').invalidate();
	            isProfileUpdateValid = false;
	        }

	        if (app.getForm('profile.login.newpassword').value() !== app.getForm('profile.login.newpasswordconfirm').value()) {
	            app.getForm('profile.login.newpasswordconfirm').invalidate();
	            isProfileUpdateValid = false;
	        }

	        if (isProfileUpdateValid) {
	            hasEditSucceeded = Customer.editAccount(app.getForm('profile.customer.email').value(), app.getForm('profile.login.newpassword').value(), app.getForm('profile.login.currentpassword').value(), app.getForm('profile'));
	            if (!hasEditSucceeded) {
	                app.getForm('profile.login.currentpassword').invalidate();
	            }
	        }

	        if (isProfileUpdateValid && hasEditSucceeded) {
	            response.redirect(URLUtils.https('Account-Settings'));
	        } else {
	            response.redirect(URLUtils.https('Account-Settings', 'invalid', 'true'));
	        }
	    },
	    error: function () {
	        response.redirect(URLUtils.https('Account-Settings', 'invalid', 'true'));
	    }
	});
}

function saveTextSettings() {
	var phoneNumber = request.httpParameterMap.phoneNumber.value;
	var prefName = request.httpParameterMap.prefName.value;
	var Customer = app.getModel('Customer');
	Customer.updateTextPreferences(phoneNumber, prefName);

	return true;
}

/**
 * Renders the User information summary
 *
 */
function showUserInfo() {
    var accountHomeAsset, pageMeta, Content;

    Content = app.getModel('Content');
    accountHomeAsset = Content.get('myaccount-home');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountHomeAsset);

    app.getView().render('account/user/personalinfosummary');
}

function deleteVehicle(){

	var vehicles;
	if(customer.authenticated){
    	vehicles = JSON.parse(customer.profile.custom.vehiclesJson);
    }
	else {
		vehicles = JSON.parse(session.privacy.preferredvehicles);
	}

    var vehicleKey = request.httpParameterMap.vehiclekey.value;
    var responseFlag = false;

    if(!empty(vehicles)){
    	// Delete element from current JSON
	    if(!empty(vehicleKey)){
	    	for(var i=0;i<vehicles.length;i++){
	        	if(vehicles[i].key == vehicleKey ){
	        		vehicles.splice(i,1);
	        		break;
	        	}
	        }
	    }

	    // If Customer is logged in save to Profile else save to Session
	    if(customer.authenticated){
	    	var Customer = app.getModel("Customer");
	        responseFlag=Customer.updateVehiclesJson(JSON.stringify(vehicles));;
	    }
	    else{
	    	session.privacy.preferredvehicles = JSON.stringify(vehicles) ;
	    }

	    var responseString = Resource.msg('account.service.response.vehicledeleted', 'account', null);

	    let r = require("~/cartridge/scripts/util/Response");
	    r.renderJSON({
	        success: responseFlag,
	        response:responseString
	    });
    }

}


function deleteWheel(){

	var wheels;
	var responseFlag=false;

	if(customer.authenticated){
    	wheels = JSON.parse(customer.profile.custom.wheelsJson);
    }
	else {
		wheels = JSON.parse(session.privacy.preferredwheels);
	}

    var wheelKey = request.httpParameterMap.wheelkey.value;
    if(!empty(wheels)){
	    // Delete element from current JSON
	     if(!empty(wheelKey)){
	    	for(var i=0;i<wheels.length;i++){
	    		if(wheels[i].key == wheelKey ){
	    			wheels.splice(i,1);
	    			break;
	    		}
	    	}
	     }

	    // If Customer is logged in save to Profile else save to Session
		 if(customer.authenticated){
			var Customer = app.getModel("Customer");
			responseFlag=Customer.updateWheelsJson(JSON.stringify(wheels));
		}
		else{
			session.privacy.preferredwheels = JSON.stringify(wheels);
		}

		var responseString = Resource.msg('account.service.response.wheeldeleted', 'account', null);

	    let r = require("~/cartridge/scripts/util/Response");
	    r.renderJSON({
	        success: responseFlag,
	        response: responseString
	    });
    }
}


function deleteTire(){

	var tires;
	var responseFlag=false;

	if(customer.authenticated){
    	tires = JSON.parse(customer.profile.custom.tiresJson);
    }
	else{
		tires = JSON.parse(session.privacy.preferredtires);
	}

    var tireKey = request.httpParameterMap.tirekey.value;

    if(!empty(tires)){
	    // Delete element from current JSON
	     if(!empty(tireKey)){
	    	for(var i=0;i<tires.length;i++){
	    		if(tires[i].key == tireKey){
	    			tires.splice(i,1);
	    		}
	    	}
	     }

	    // If Customer is logged in save to Profile else save to Session
		if(customer.authenticated){
			var Customer = app.getModel("Customer");
			responseFlag=Customer.updateTiresJson(JSON.stringify(tires));
		}
		else{
			session.privacy.preferredtires = JSON.stringify(tires);
		}

		var responseString = Resource.msg('account.service.response.tiredeleted', 'account', null);

		let r = require("~/cartridge/scripts/util/Response");
	    r.renderJSON({
	        success: responseFlag,
	        response:responseString
	    });
    }
}


function saveVehicle(){

	// Calling dummy service to create vehicleJson
	var response = callService();
	var responseFlag=false;
	var responseString;
	var action= request.httpParameterMap.action.value;
	var descriptions = JSON.parse(response.object).Descriptions;

	var vehicles =[];

	if(customer.authenticated){
		vehicles = JSON.parse(customer.profile.custom.vehiclesJson);
	}
	else {
		vehicles = JSON.parse(session.privacy.preferredvehicles);
	}

	if(empty(vehicles)){
		vehicles=[];
	}

	for(var i =0; i< descriptions.length;i++){

		var keyValue= {};
		var value= {};

		value["MakeName"] = descriptions[i].MakeName;
		value["ModelName"] = descriptions[i].ModelName;
		value["Year"]= descriptions[i].Year;
		value["TrimName"] = descriptions[i].TrimName;
		value["ConfigurationID"] = descriptions[i].ConfigurationID;
		value["Color"] = "FF0000"; // Default Color
		value["ImageURL"] = "imageurl"; // Image URL
		value["TireSize"] = getDummyTireJson().value;
		value["WheelSize"] = getDummyWheelJson().value;

		keyValue["key"] = UUIDUtils.createUUID();
		keyValue["value"] = value;

		vehicles.push(keyValue);

	}

	if(customer.authenticated){
		var Customer = app.getModel("Customer");
		var responseFlag=Customer.updateVehiclesJson(JSON.stringify(vehicles));
	}
	else{
		session.privacy.preferredvehicles = JSON.stringify(vehicles);
	}

	if(action == Resource.msg('account.service.action.save', 'account', null)){
		responseString = Resource.msg('account.service.response.vehicleadded', 'account', null);
	}
	else if(action == Resource.msg('account.service.action.edit', 'account', null)){
		responseString = Resource.msg('account.service.response.vehiclesaved', 'account', null);
	}

	let r = require("~/cartridge/scripts/util/Response");
    r.renderJSON({
        success: responseFlag,
        response : responseString
    });

}

function saveWheel(){

	var wheels,response;
	var action= request.httpParameterMap.action.value;
	var responseFlag=false;
	var responseString;

	if(customer.authenticated){
		wheels = JSON.parse(customer.profile.custom.wheelsJson);
	}
	else {
		wheels = JSON.parse(session.privacy.preferredwheels);
	}

	 if(empty(wheels)){
		 wheels=[];
	 }

	 wheels.push(getDummyWheelJson());

	// If Customer is logged in save to Profile else save to Session
	 if(customer.authenticated){
		var Customer = app.getModel("Customer");
		var responseFlag=Customer.updateWheelsJson(JSON.stringify(wheels));
	}
	else{
		session.privacy.preferredwheels = JSON.stringify(wheels);
	}

	if(action == Resource.msg('account.service.action.save', 'account', null)){
		responseString = Resource.msg('account.service.response.wheeladded', 'account', null);
	}
	else if(action == Resource.msg('account.service.action.edit', 'account', null)){
		responseString = Resource.msg('account.service.response.wheelsaved', 'account', null);
	}

	let r = require("~/cartridge/scripts/util/Response");
    r.renderJSON({
        success: responseFlag,
        response : responseString
    });

}

function saveTire(){

	var tires = [];
	var response ;
	var responseFlag=false;
	var responseString;

	var action= request.httpParameterMap.action.value;

	if(customer.authenticated){
		tires = JSON.parse(customer.profile.custom.tiresJson);
	}
	else {
		tires = JSON.parse(session.privacy.preferredtires);
	}

	if(empty(tires)){
	 tires=[];
	}

	tires.push(getDummyTireJson());

	// If Customer is logged in save to Profile else save to Session
	 if(customer.authenticated){
		var Customer = app.getModel("Customer");
		responseFlag = Customer.updateTiresJson(JSON.stringify(tires));
	}
	else{
		session.privacy.preferredtires = JSON.stringify(tires);
	}

	if(action == Resource.msg('account.service.action.save', 'account', null)){
		responseString = Resource.msg('account.service.response.tireadded', 'account', null);
	}
	else if(action == Resource.msg('account.service.action.edit', 'account', null)){
		responseString = Resource.msg('account.service.response.tiresaved', 'account', null);
	}

	let r = require("~/cartridge/scripts/util/Response");
    r.renderJSON({
        success: responseFlag,
        response : responseString
    });
}


/**
 * To shop for saved tires/vehicles/wheels
 *	Parameters:
 *		action = from where the search was initialized in My Account // values : "vehicle" or "tire" or "wheel"
 *		category = what to search for (only in case when action is "vehicle") // values : "tires" or "wheels"
 *
 */
function shopProductsForVehicle() {

	var vehicleJson,tireJson,wheelJson;
	var action = request.httpParameterMap.action.value;
	var category = request.httpParameterMap.category.value;
	var searchUrl = URLUtils.https('Search-Show');

	var id = request.httpParameterMap.id.value;

	switch (action){
		case 'vehicle':
			if(customer.registered && customer.authenticated){
				vehicleJson = !empty(customer.profile.custom.vehiclesJson)?JSON.parse(customer.profile.custom.vehiclesJson):[];
			}
			else {
				vehicleJson = !empty(session.privacy.preferredvehicles) ? JSON.parse(session.privacy.preferredvehicles) : [];
			}
			var shopForObj;
			var j=1;
			if(!empty(category)){
				if(category === 'tires'){
					j=1;
					searchUrl.append(Resource.msg('account.cgid.label', 'account', null),Resource.msg('account.category.tires.label', 'account', null));
					for(var i=0; i< vehicleJson.length ; i++){
						if(vehicleJson[i].key === id){
							shopForObj = vehicleJson[i].value.TireSize;
							if(!empty(shopForObj)){
								addSearchParams(searchUrl, shopForObj);
							}
						}
					}
				} else if(category === 'wheels') {
					j=1;
					searchUrl.append(Resource.msg('account.cgid.label', 'account', null),Resource.msg('account.category.wheels.label', 'account', null));
					for(var i=0; i< vehicleJson.length ; i++){
						if(vehicleJson[i].key === id){
							shopForObj = vehicleJson[i].value.WheelSize;
							if(!empty(shopForObj)){
								addSearchParams(searchUrl, shopForObj);
							}
						}
					}
				}
			}
			break;
		case 'tire':
			if(customer.registered && customer.authenticated){
				tireJson = JSON.parse(customer.profile.custom.tiresJson);
			}
			else {
				tireJson = !empty(session.privacy.preferredtires) ? JSON.parse(session.privacy.preferredtires) : [];
			}

			if(!empty(tireJson)){
				searchUrl.append(Resource.msg('account.cgid.label', 'account', null),Resource.msg('account.category.tires.label', 'account', null));
				for(var i=0; i< tireJson.length ; i++){
					if(tireJson[i].key === id){
						var tireObj = tireJson[i].value;
						j=1;
						if(!empty(tireObj)){
							addSearchParams(searchUrl, tireObj);
						}
					}
				}
			}

			break;
		case 'wheel':
			if(customer.registered && customer.authenticated){
				wheelJson= JSON.parse(customer.profile.custom.wheelsJson);
			}
			else {
				wheelJson = !empty(session.privacy.preferredwheels) ? JSON.parse(session.privacy.preferredwheels) : [];
			}
			if(!empty(wheelJson)){
				searchUrl.append(Resource.msg('account.cgid.label', 'account', null),Resource.msg('account.category.wheels.label', 'account', null));
				for(var i=0; i< wheelJson.length ; i++){
					if(wheelJson[i].key === id){
						var wheelObj = wheelJson[i].value;
						j=1;
						if(!empty(wheelObj)){
							addSearchParams(searchUrl, wheelObj);
						}
					}
				}
			}
			break;
		default:
			break;
	}

	response.redirect(searchUrl);
}

function addSearchParams(searchUrl, jsonObj) {

	if(!empty(jsonObj.front) && !empty(jsonObj.rear)){
		for (var prop in jsonObj.front) {
			var propName = Resource.msg('account.product.attribute.'+prop, 'account', null);
			if(propName != "null"){
				searchUrl.append(Resource.msg('account.filter.prefn.label', 'account', null)+j,propName);
				searchUrl.append(Resource.msg('account.filter.prefv.label', 'account', null)+j++,jsonObj.front[prop] + "|" + jsonObj.rear[prop]);
			}
		}
	}
	else if(empty(jsonObj.front) && empty(jsonObj.rear)){
		return;
	}
	else {
		var json;
		if(!empty(jsonObj.front)){
			json = jsonObj.front;
		}
		if(!empty(jsonObj.rear)){
			json=jsonObj.rear;
		}

		for (var prop in json) {
			var propName = Resource.msg('account.product.attribute.'+prop, 'account', null);
			if(propName != "null"){
				searchUrl.append(Resource.msg('account.filter.prefn.label', 'account', null)+j,propName);
				searchUrl.append(Resource.msg('account.filter.prefv.label', 'account', null)+j++,json[prop]);
			}
		}
	}

	return;
}

function saveEntryData(){
	var requestBody = request.httpParameterMap.requestBodyAsString;
	var jsonData = JSON.parse(requestBody);

	var data = {};

    for(var key in jsonData){
        switch(key){
            case "vehicle":
            case 'tires':
            case 'wheels':
            	session.privacy.custEntryType = key;
                data[key]=jsonData[key];
        }
    }
	// Add object to session to be displayed
	session.privacy.custEntryData = JSON.stringify(data);
    let r = require("~/cartridge/scripts/util/Response");
    r.renderJSON({
        success: true
    });
}
function showEntryData(){
    var jsonData = {
        componentName: "VehicleInfo",
        storeData: app.getController('ReactComponent').PopulateStoreInformation(app.getController('Stores').GetPreferredOrNearestStore(), "tiny"),
        getVehicleSelectorDataUrl: URLUtils.url('ReactComponent-GetVehicleSelector').toString(),
        vehicleRenderApiUrl: Site.current.preferences.custom.vehicleRenderApi + "?key=" + Site.current.preferences.custom.vehicleRenderApiKey,
        vehicleDetails: session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).vehicle,
        tiresDetails: session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).tires,
        wheelsDetails: session.privacy.custEntryData && JSON.parse(session.privacy.custEntryData).wheels,
        noVehicleMessage: Resource.msg('account.novehicleentry.label','account',null),
        category: request.httpParameterMap.category.value || null,
        entry: request.httpParameterMap.entry.value
    };

	app.getView({jsonData: JSON.stringify(jsonData)}).render('react/staticshow');
}

function showWheelVisualizerData(){
	var custEntryData = !empty(session.privacy.custEntryData) && JSON.parse(session.privacy.custEntryData);
	var jsonData = {
		componentName: "WheelVisualizer",
        vehicleRenderApiUrl : Site.current.preferences.custom.vehicleRenderApi + "?key=" + Site.current.preferences.custom.vehicleRenderApiKey,
        colorSelectorHeading : Resource.msg('account.visualizer.choosecolor','account',null),
        colorCancelLabel: Resource.msg('account.visualizer.cancel','account',null),
        vehicleCategoryId: custEntryData && !empty(custEntryData.categoryId)?custEntryData.categoryId:null
    };

	app.getView({jsonData: JSON.stringify(jsonData)}).render('react/staticshow');
}

function convertJSONtoArray(jsonData){
	var array = [];
	for(var key in jsonData){
		var obj = {};
		obj.key=key;
		obj.value=jsonData[key];
		array.push(obj);
	}
	return array;
}

function formatAttributes(className,title,json){
	var keyName = "";
	var obj={};
	for(var key in json){
		keyName = keyName + Resource.msgf('account.entryobject.'+key, 'account', null,json[key]);
	}
	obj.key=className;
	if(!empty(title)){
		obj.title=title+':';
	}
	obj.value=keyName;
	return obj;
}

function callService(){
	/*
	 * Added only for testing purpose
	 */

	var configID = request.httpParameterMap.configid.value;
	var year=request.httpParameterMap.year.value;

	var service =  LocalServiceRegistry.createService("ridestyler.rest.descriptions", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {

	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	service.addParam("Year",year);
	service.addParam("VehicleModel",configID);
	return service.call();
}

function getDummyWheelJson(){
	var keyValue= {};
	var front={};
	var rear={};
	var value={};
	front["boltpattern"] = "100x7";
	front["diameter"] = "20";
	rear["boltpattern"] = "100x7";
	rear["diameter"] = "20";
	value["front"] = front;
	value["rear"] = rear;
	keyValue["key"] = UUIDUtils.createUUID();
	keyValue["value"] = value;

	return keyValue;
}

function getDummyTireJson(){
	var keyValue = {};
	var value ={};
	var front = {};
	var rear = {};

	// Creating Dummy Tire Json to save
	front["Description"] = "220/77R18";
	rear["Description"] = "220/77R18";
	front["Width"] = "220";
	rear["Width"] = "220";
	front["AspectRatio"] = "77";
	rear["AspectRatio"] = "77";
	front["InsideDiameter"] = "18";
	rear["InsideDiameter"] = "18";

	keyValue["key"] =  UUIDUtils.createUUID();
	value["front"] = front;
	value["rear"] = rear;
	keyValue["value"] = value;

	return keyValue;
}

/* Web exposed methods */



/** Renders the account overview.
 * @see {@link module:controllers/Account~addAddressFields} */
//exports.AddAddressFields = guard.ensure(['get', 'https', 'loggedIn'], addAddressFields);
/** Renders the account overview.
 * @see {@link module:controllers/Account~saveTextSettings} */
//exports.SaveTextSettings = guard.ensure(['post', 'https', 'loggedIn'], saveTextSettings);
/** Renders the account overview.
 * @see {@link module:controllers/Account~settingsForm} */
//exports.SettingsForm = guard.ensure(['post', 'https', 'loggedIn'], settingsForm);
/** Renders the account overview.
 * @see {@link module:controllers/Account~settings} */
//exports.Settings = guard.ensure(['get', 'https', 'loggedIn'], settings);
/** Renders the account overview.
 * @see {@link module:controllers/Account~show} */
//exports.Show = guard.ensure(['get', 'https', 'loggedIn'], show);
/** Updates the profile of an authenticated customer.
 * @see {@link module:controllers/Account~editProfile} */
//exports.EditProfile = guard.ensure(['get', 'https', 'loggedIn'], editProfile);
/** Handles the form submission on profile update of edit profile.
 * @see {@link module:controllers/Account~editForm} */
//exports.EditForm = guard.ensure(['post', 'https', 'loggedIn', 'csrf'], editForm);
/** Renders the password reset dialog.
 * @see {@link module:controllers/Account~passwordResetDialog} */
//exports.PasswordResetDialog = guard.ensure(['get', 'https'], passwordResetDialog);
/** Renders the password reset screen.
 * @see {@link module:controllers/Account~passwordReset} */
//exports.PasswordReset = guard.ensure(['get', 'https'], passwordReset);
/** Handles the password reset form.
 * @see {@link module:controllers/Account~passwordResetDialogForm} */
//exports.PasswordResetDialogForm = guard.ensure(['post', 'https', 'csrf'], passwordResetDialogForm);
/** The form handler for password resets.
 * @see {@link module:controllers/Account~passwordResetForm} */
//exports.PasswordResetForm = guard.ensure(['post', 'https'], passwordResetForm);
/** Renders the screen for setting a new password.
 * @see {@link module:controllers/Account~setNewPassword} */
//exports.SetNewPassword = guard.ensure(['get', 'https'], setNewPassword);
/** Handles the set new password form submit.
 * @see {@link module:controllers/Account~setNewPasswordForm} */
//exports.SetNewPasswordForm = guard.ensure(['post', 'https'], setNewPasswordForm);
/** Start the customer registration process and renders customer registration page.
 * @see {@link module:controllers/Account~startRegister} */
//exports.StartRegister = guard.ensure(['https'], startRegister);
/** Handles registration form submit.
 * @see {@link module:controllers/Account~registrationForm} */
//exports.RegistrationForm = guard.ensure(['post', 'https', 'csrf'], processRegisterForm);
/** Renders the account navigation.
 * @see {@link module:controllers/Account~includeNavigation} */
exports.IncludeNavigation = guard.ensure(['get'], includeNavigation);
/** Renders the user personal info section.
* @see {@link module:controllers/Account~show} */
//exports.ShowUserInfo = guard.ensure(['get', 'https', 'loggedIn'], showUserInfo);
/** Handles the deletion of a vehicle from user preferred vehicles
*	@see {@link module:controllers/Account~deleteVehicle}*/
//exports.DeleteVehicle = guard.ensure(['post', 'https', 'loggedIn'], deleteVehicle);
/**	Handles the deletion of a wheel from user preferred wheels
*	@see {@link module:controllers/Account~deleteWheel}*/
//exports.DeleteWheel = guard.ensure(['post', 'https', 'loggedIn'], deleteWheel);
/**	Handles the deletion of a tire from user preferred tires
*	@see {@link module:controllers/Account~deleteTire}*/
//exports.DeleteTire = guard.ensure(['post', 'https', 'loggedIn'], deleteTire);

// TODO : Change this request to POST and not GET after FE Team integrates save vehicle UI Part
//exports.SaveVehicle = guard.ensure(['get', 'https'], saveVehicle);

// TODO : Change this request to POST and not GET after FE Team integrates save vehicle UI Part
//exports.SaveWheel = guard.ensure(['get', 'https'], saveWheel);

// TODO : Change this request to POST and not GET after FE Team integrates save vehicle UI Part
//exports.SaveTire = guard.ensure(['get', 'https'], saveTire);

/**	Renders the screen when User Clicks on 'Shop For Tires' or 'Shop For Wheels' from My Account Page
*	@see {@link module:controllers/Account~shopProductsForVehicle}*/
//exports.ShopProductsForVehicle = guard.ensure(['get', 'https'], shopProductsForVehicle);

/*
 * Saves customer entry data into session -- Vehicle/Tire/Wheel
 */
exports.SaveEntryData = guard.ensure(['post', 'https'], saveEntryData);

/*
 * Renders Saved Vehicle/Tire/Wheel on Search Results and PDP
 */
exports.ShowEntryData = guard.ensure(['get'], showEntryData);


exports.ShowWheelVisualizerData = guard.ensure(['get'], showWheelVisualizerData);
