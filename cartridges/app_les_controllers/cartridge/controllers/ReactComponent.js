'use strict';

/**
 * Controller that renders the react json components.
 *
 * @module controllers/ReactComponent
 */

/* API includes */
var ContentMgr = require('dw/content/ContentMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Countries = require('app_les_core/cartridge/scripts/util/Countries');
var Site = require("dw/system/Site");

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var viewHelpers = require('app_les_core/cartridge/scripts/util/ViewHelpers');
var guard = require('~/cartridge/scripts/guard');

function staticShow() {
	var content,
		cid = request.httpParameterMap.cid.value;
	if (!empty(cid)) {
		content = ContentMgr.getContent(cid);
	}
	app.getView({jsonData: content.custom.body.source}).render('react/staticshow');
}

function populateInitialState(){
    var jsonData = {
		staticLibraryURL: URLUtils.staticURL(URLUtils.CONTEXT_SITE, null, "").toString(),
		userStore: populateStoreInformation(app.getController('Stores').GetPreferredOrNearestStore())
    };

	//app.getView({jsonData: JSON.stringify(jsonData)}).render('react/staticshow');
	return JSON.stringify(jsonData);
}

function populateHeader(){
	var jsonData ={};
	/*========================Main navigation========================*/
	var topCategories = viewHelpers.getSubcategoriesInMenuForCategory();
	jsonData.componentName = Resource.msg('navigation.header','reactjson',null);
	jsonData.logo = {};
	jsonData.logo.url = URLUtils.httpsHome().toString();
	jsonData.logo.imageURL = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/logo.svg").toString(),
	jsonData.logo.title = Resource.msg('navigation.logo.title','reactjson',null);
	jsonData.hamburgerIconURL = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/icons/hamburger.svg").toString(),
	jsonData.closeIconRedURL = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/icons/closeIconRed.svg").toString(),
	jsonData.pinIconURL = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/icons/pin.svg").toString(),
	jsonData.billPay = {};
	jsonData.billPay.label = Resource.msg('navigation.billPay.label','reactjson',null);
	jsonData.billPay.url = URLUtils.url("Search-ShowContent", "fdid", "bill-pay").toString();
	jsonData.search = {};
	jsonData.search.title = Resource.msg('navigation.search.title','reactjson',null) ;
	jsonData.search.placeholder = Resource.msg('navigation.search.placeholder','reactjson',null) ;
	jsonData.search.url = URLUtils.url('Search-Show').toString();
	jsonData.search.suggestionsUrl = URLUtils.url('Search-GetSuggestions').toString();
	jsonData.categories = [];
	if(!empty(topCategories))
	{
		var categoryObject;
	    for(let i=0;i<topCategories.length;i++){
	       categoryObject = {};
	       categoryObject.label = (topCategories[i].displayName == 'Shocks-Struts') ? 'Shocks' : topCategories[i].displayName;
		   if(topCategories[i].displayName === 'Tires' || topCategories[i].displayName === 'Wheels'){
			   categoryObject.url = "";
		   }else{
			   categoryObject.url = viewHelpers.getCategoryUrl(topCategories[i]).toString();
			}
	       categoryObject.panel ={};
	       categoryObject.panel.vehicleSelector = populateVehicleSelectorJSON(topCategories[i].getID());
	       categoryObject.panel.services ={};
	       categoryObject.panel.services.title = topCategories[i].custom.titleSubCategory || "";
	       categoryObject.panel.services.items =[];
	       categoryObject.panel.services.bottomItems =[];
	       var subCategories = viewHelpers.getSubcategoriesInMenuForCategory(topCategories[i]);
	       if(!empty(subCategories)){
	    	   var itemObject;
	         for (let j=0;j<subCategories.length;j++){
	           	itemObject = {};
	           	//TO-DO PLEASE CLEAN THIS UP TO BE LESS HARDCODED
	           	if(subCategories[j].displayName === "Tire") {
	           	//	categoryObject.panel.services.bottomItems.push({label: "Shop Wheels", url: URLUtils.url('Search-Show', "cgid", "wheel").toString(),	text: Resource.msg('navigation.bottomText.tire','reactjson',null)})
	           	}
	           	else if (subCategories[j].displayName === "Wheel") {
	           	//	categoryObject.panel.services.bottomItems.push({label: "Shop Tires", url: URLUtils.url('Search-Show', "cgid", "tire").toString(), text: Resource.msg('navigation.bottomText.wheel','reactjson',null)});
	           	}
	           	//end of hardcoded links
	           	else {
	           		itemObject.label = (subCategories[j].displayName == 'Trailer' || subCategories[j].displayName == 'Specialty') ? subCategories[j].displayName + ' Tires' : subCategories[j].displayName;
	           		itemObject.url = URLUtils.url('Search-Show', "cgid", subCategories[j].ID).toString();
	           		categoryObject.panel.services.items.push(itemObject);
	           	}
	          }
	    	}
	       if(!empty(topCategories[i].custom.globalModuleData)){
	           var globalDataObject =  JSON.parse(topCategories[i].custom.globalModuleData);
	           categoryObject.panel.learnMoreUrl = globalDataObject.learnMoreUrl;
	           categoryObject.panel.learnMoreLabel = globalDataObject.learnMoreLabel;
	           categoryObject.panel.makeAnAppointmentUrl = globalDataObject.makeAnAppointmentUrl;
	           categoryObject.panel.makeAnAppointmentLabel = globalDataObject.makeAnAppointmentLabel;
	        }
	       if(!empty(topCategories[i].custom.warrantyData)){
	           var warrantyDataObject = JSON.parse(topCategories[i].custom.warrantyData);
	           warrantyDataObject.engUrl = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, warrantyDataObject.engUrl).toString();
	           warrantyDataObject.spanishUrl = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, warrantyDataObject.spanishUrl).toString();
	           categoryObject.panel.warranty = warrantyDataObject;
	        }
	       if(!empty(topCategories[i].custom.brochureData)){
	           var brochureDataObject = JSON.parse(topCategories[i].custom.brochureData);
	           brochureDataObject.ctaUrl = URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, brochureDataObject.ctaUrl).toString();
	           categoryObject.panel.brochure = brochureDataObject;
	        }
	        jsonData.categories.push(categoryObject);
		}
	}
	//========================Customer information========================
	jsonData.customer = {
		logoutLabel: Resource.msg('global.logout', 'locale', null),
		logoutUrl: URLUtils.url('Login-Logout').toString()
	};
	// User has session, but not authenticated
	if (customer.registered && !customer.authenticated) {
		jsonData.customer.title = Resource.msg('global.accountlink', 'locale', null);
		jsonData.customer.links = [{
			href: URLUtils.https('Account-Show').toString(),
			label: Resource.msg('global.accountlink', 'locale', null),
			title: Resource.msgf('global.goto.label', 'locale', null, Resource.msg('global.accountlink', 'locale', null))
		}];
	// User has session and is validated
	} else if (customer.registered) {
		jsonData.customer.title = Resource.msgf('global.user.name', 'locale', null, customer.profile.firstName);
		jsonData.customer.links = [{
			href: URLUtils.https('Account-Show').toString(),
			label: Resource.msg('global.accountlink', 'locale', null),
			title: Resource.msgf('global.goto.label', 'locale', null, Resource.msg('global.accountlink', 'locale', null))
		}, {
			href:  URLUtils.https('Account-Settings').toString(),
			label: Resource.msg('global.accountsettingslink', 'locale', null),
			title: Resource.msgf('global.goto.label', 'locale', null, Resource.msg('global.accountsettingslink', 'locale', null))
		}];
	}else{
		// user has no session user if neither of these above are true
		jsonData.customer.loginLabel = Resource.msg('global.login', 'locale', null);
		jsonData.customer.loginUrl = URLUtils.url('Login-Show').toString();
	}

	//========================Store information section========================
	var storeData = app.getController('Stores').GetPreferredOrNearestStore();
	jsonData.storeData = populateStoreInformation(storeData, "header");
	jsonData.storeSearchForm = getStoreSearchForm(true);

	return jsonData;
}
/**storeData=storeId || {
	store:
	distance:
	isNearest
}*/
function populateStoreInformationFromId(storeId, type, distance){
	if(!empty(storeId)){
		return populateStoreInformation({store: StoreMgr.getStore(storeId)}, type, distance);
	}
	return null;
}
function populateStoreInformation(storeData, type, distance) {
	//Store information section
	var store = storeData.store,
		storeDistance = distance || storeData.distance || null,
		isNearest = storeData.isNearest || false;

	if (empty(store)) {
		storeData = app.getController('Stores').GetClosestStore();
		store = storeData.store;
		distance = storeData.distance;
		isNearest = storeData.isNearest;
	}
	var preferredStore = app.getController('Stores').GetUserPreferredStore(),
		isMystore = false;
	if(!empty(preferredStore) && preferredStore.ID === store.ID){
		isMystore = true;
	}
	if(!empty(store)){
		return {
			address1: store.address1,
			postalCode: store.postalCode,
			creditPageCtaLabel:Resource.msg('storelocator.credit.options', 'storelocator', null),
			creditPageCtaUrl: URLUtils.url("Search-ShowContent","fdid", "credit").toString(),
			isNearest: isNearest,
			makeMyStoreUrl: URLUtils.url('Stores-UpdatePreferredStore').toString(),
			stopByForInstallation:Resource.msg('pdp.stopby', 'storelocator', null),
			type: typeof type === 'string'?type:'brief',
	        appointmentLabel: Resource.msg('storelocator.appointment.label', 'storelocator', null),
	        callUsLabel: Resource.msg('storelocator.callus', 'storelocator', null),
			changeStoreUrl: URLUtils.url('Stores-ShowStores').toString(),
	        changeStoreLabel: Resource.msg('storelocator.change.store', 'storelocator', null),
	        city: store.city,
	        componentName: "StoreDetails",
	        description: !empty(store.custom.description) ? store.custom.description : "",
	        directionsLabel: Resource.msg('storelocator.getdirections.label', 'storelocator', null),
	        distance: storeDistance,
	        findAnotherStoreLabel: Resource.msg('mystore.find.another.store.label', 'storelocator', null),
	        hours: !empty(store.storeHours) ? store.storeHours.toString():'',
	        hoursLabel: Resource.msg('storelocator.hours','storelocator',null),
	        id: store.ID,
	        isMystore: isMystore,
	        latitude: store.latitude,
	        longitude: store.longitude,
	        makeMyStoreLabel: Resource.msg('storelocator.makemystore.label', 'storelocator', null),
	        manager: store.custom.manager,
	        managerImage: !empty(store.custom.managerImage)?store.custom.managerImage.getURL().toString():"",
	        managerLabel: Resource.msg('storelocator.manager','storelocator',null),
	        myStoreHeader: Resource.msg('mystore.header', 'storelocator', null),
	        myStoreLabel: Resource.msg('storelocator.mystore.label', 'storelocator', null),
	        nearestStoreHeader: Resource.msg('neareststore.header', 'storelocator', null),
	        phone: store.phone,
	        noPhoneMessage:  Resource.msg('storelocator.noPhoneMessage', 'storelocator', null),
	        stateCode: store.stateCode,
	        storeDetailsLabel: Resource.msg('storelocator.storedetails.label', 'storelocator', null),
	        storeDetailsUrl: URLUtils.url('Stores-ShowStoreDetails', 'storeId', store.ID, 'distance', storeDistance).toString(),
	        storeImages: !empty(store.image) ? [store.image.getURL().toString()] : [],
	        title: store.address1 + " " + Resource.msg('storelocator.storeheading','storelocator',null),
	        walkinsText: Resource.msg('extended.walkins','storelocator',null),
	        weSpeakSpanish: store.custom.spanishSpoken?Resource.msg('storelocator.spanishSpoken','storelocator',null):"",
	        enableAppointment: !empty(store.custom.enableAppointment)?store.custom.enableAppointment:false
		};
	}
}

function getStoreSearchForm(noMap, geoLocation, isStateSearch) {
	var latitude = null, longitude = null;
	if(geoLocation && geoLocation.latitude && geoLocation.longitude){
		latitude = geoLocation.latitude;
		longitude = geoLocation.longitude;
	}
	return {
		componentName: 'StoreSearchForm',
		noMap: noMap?true:false,
		countryCode: Countries.getCurrent({CurrentRequest: {locale: request.locale}}).countryCode,
		fieldName: 'sq',
		fieldPlaceholder: Resource.msg('global.entercitystatezip', 'locale', null),
		findByAddressAction: 'find',
		findByAddressTitle: Resource.msg('store.search.byaddress.title', 'reactjson', null),
		findByCurrentLocationAction: 'findByCurrentLocation',
		findByCurrentLocationTitle: Resource.msg('store.search.bylocation.title', 'reactjson', null),
		formLabel: Resource.msg('storelocator.header', 'storelocator', null),
		latitudeField: 'latitude',
		postUrl: URLUtils.url('Stores-ShowStores').toString(),
		longitudeField: 'longitude',
		latitude: latitude,
		longitude: longitude,
		isStateSearch: isStateSearch,
		inputLabelText: Resource.msg('storelocator.search.label', 'storelocator', null),
	}
}

//TODO: break down switch statement into separate methods
function dynamicShow() {

	var jsonData = {};
	if (request.httpParameterMap.type.value != null) {
		switch (request.httpParameterMap.type.value) {
			case 'storeList':
				//TODO: Mikeel - this is the reverse conversion from my comment in Stores.js
				var storesData = JSON.parse(request.httpParameterMap.stores.value),
					storesPopulated = [];
					storesData.forEach(function(storeData, index){
						storesPopulated.push(populateStoreInformationFromId(storeData.id, 'brief', storeData.distance));
					});
				jsonData = {
					componentName: "StoreList",
					storeList: storesPopulated,
					noResultsMessage: Resource.msgf('storelocator.noResults.message', 'storelocator', null, Site.getCurrent().getCustomPreferenceValue('storeLookupMaxDistance').value),
					noResultsSubMessage:  Resource.msg('storelocator.noResults.subMessage', 'storelocator', null)
				};
				break;
			case 'getPreferredOrNearestStore':
				var storeData = app.getController('Stores').GetPreferredOrNearestStore(),
					view = "tiny";
				if(request.httpParameterMap.view){
					view = request.httpParameterMap.view.value || 'tiny';
				}
				jsonData = populateStoreInformation(storeData, view);
				break;
			case 'confirmStore':
				//var storeData = app.getController('Stores').GetClosestStore(),
				var storeData = app.getController('Stores').GetPreferredOrNearestStore(),
					view = "confirmation";
				if(request.httpParameterMap.view){
					view = request.httpParameterMap.view.value || 'tiny';
				}
				jsonData = {
						componentName: "ConfirmStore",
						storeData: populateStoreInformation(storeData, view),
						changeStoreUrl: URLUtils.url('Stores-ShowStores').toString(),
				        changeStoreLabel: Resource.msg('storelocator.change.store', 'storelocator', null),
						makeMyStoreLabel: Resource.msg('storelocator.makemystore.label', 'storelocator', null),
						makeMyStoreUrl: URLUtils.url('Stores-UpdatePreferredStore').toString(),
						confirmYourStoreLabel: Resource.msg('storelocator.confirmYourStoreLabel.label', 'storelocator', null),
						locationInfoLabel:Resource.msg('storelocator.locationInfoLabel.label', 'storelocator', null),
						confirmOnly: request.httpParameterMap.confirmOnly.value === "true"
				};
				break;
			case 'priceDisplay':
				var priceData = {};
				if(request.httpParameterMap.priceData){
					priceData = request.httpParameterMap.priceData.value || null;
				}
				jsonData = JSON.parse(priceData);
				jsonData.componentName = "PriceDisplay";
				break;
			case 'getHeader':
				jsonData = populateHeader();
				break;
			case 'getVehicleSelector':
				//TODO: used only in HP content asset - need to retire
				jsonData = {
					componentName: "VehicleSelectorCTA",
					storeData: populateStoreInformation(app.getController('Stores').GetPreferredOrNearestStore(), "tiny"),
					ctaLabel: Resource.msg('vehicle.selector.cta.label', 'global', null),
					helpTip: Resource.msg('vehicle.entry.description', 'search', null),
					getVehicleSelectorDataUrl: URLUtils.url('ReactComponent-GetVehicleSelector').toString(),
					vehicleSelector: populateVehicleSelectorJSON(request.httpParameterMap.category.value || 'all'),
					byVehicleLabel: Resource.msg('vehicle.entry.byVehicleType.label', 'search', null),
					byTireLabel: Resource.msg('vehicle.entry.byTire.label', 'search', null),
				};
				break;
			case 'storeSearchForm':
				jsonData = getStoreSearchForm();
				break;
			case 'storeDetails':
				jsonData = populateStoreInformationFromId(request.httpParameterMap.storeId.value, request.httpParameterMap.view.value || 'brief', request.httpParameterMap.distance.value);
				break;
			default:
				jsonData.error = "Unknown request type";
		}
	}
	var jsonString = JSON.stringify(jsonData);
	app.getView({jsonData: jsonString}).render('react/staticshow');
}


function populateVehicleSelectorJSON(categoryId) {
	//due to all wheel/wheels situation:
	if(categoryId === "tire" || categoryId === "wheel"){
		categoryId += "s";
	}
	if(categoryId !== "tires" && categoryId !== "wheels" && categoryId !== "all"){
		return null;
	}
	var vehicleSelector = {
			   title:categoryId === "wheels"?Resource.msg('vehicle.entry.title.wheels', 'search', null):Resource.msg('vehicle.entry.title.tires', 'search', null),
			   description:Resource.msg('vehicle.entry.description', 'search', null),
			   tireSizeMessage:Resource.msgf('vehicle.entry.tireSizeMessage', 'search', null, 'SETTIRESIZEURL'),
			   tireSizeCTA:Resource.msg('vehicle.entry.tireSizeCTA', 'search', null),
			   tireSizeInfoMessage:Resource.msg('vehicle.entry.tireSizeInfoMessage', 'search', null),
			   viewAllLabel:categoryId === "wheels"?Resource.msg('vehicle.entry.wheel.viewAllLabel', 'search', null):Resource.msg('vehicle.entry.tire.viewAllLabel', 'search', null),
			   viewAllUrl:categoryId === "wheels"?URLUtils.url('Search-Show', 'cgid', 'wheels').toString():URLUtils.url('Search-Show', 'cgid', 'tires').toString(),
			   addAnotherVehicleLabel:Resource.msg('vehicle.entry.addAnotherVehicleLabel', 'search', null),
			   staggeredMessage:Resource.msg('vehicle.entry.byVehicleType.staggeredMessage', 'search', null),
			   apiUrls:{
				   PostVehicleData: URLUtils.url("Account-SaveEntryData").toString(),
				   GetYears: URLUtils.url("RideStyler-GetYears").toString(),
				   GetMakes: URLUtils.url("RideStyler-GetMakes").toString(),
				   GetModels: URLUtils.url("RideStyler-GetModels").toString(),
				   GetDescriptions: URLUtils.url("RideStyler-GetDescriptions").toString(),
				   GetFitments: URLUtils.url("RideStyler-GetFitments").toString(),
				   GetBoltpatterns: URLUtils.url("RideStyler-GetBoltpatterns").toString(),
				   GetTireOptionDetails: URLUtils.url("RideStyler-GetTireOptionDetails").toString(),
				   GetWheelDiameter: URLUtils.url("Search-GetWheelDiameter").toString(),
				   GetTireAspectRatio: URLUtils.url("Search-GetTireAspectRatio").toString(),
				   GetTireRimSize: URLUtils.url("Search-GetTireRimSize").toString(),
			   },
			   items:[
			      {
			         id:"byVehicleType",
					 category: categoryId,
			         label:Resource.msg('vehicle.entry.byVehicleType.label', 'search', null),
			         title:Resource.msg('vehicle.entry.byVehicleType.title', 'search', null),
			         yearPlaceholder:Resource.msg('vehicle.entry.byVehicleType.yearPlaceholder', 'search', null),
			         makePlaceholder:Resource.msg('vehicle.entry.byVehicleType.makePlaceholder', 'search', null),
			         modelPlaceholder:Resource.msg('vehicle.entry.byVehicleType.modelPlaceholder', 'search', null),
			         trimPlaceholder:Resource.msg('vehicle.entry.byVehicleType.trimPlaceholder', 'search', null),
			         tireSizePlaceholder:Resource.msg('vehicle.entry.byVehicleType.tireSizePlaceholder', 'search', null),
			         tireSizeHelpTipLabel:Resource.msg('vehicle.entry.byVehicleType.tireSizeHelpTipLabel', 'search', null),
			         tireSizeHelpTipTitle:Resource.msg('vehicle.entry.byVehicleType.tireSizeHelpTipTitle', 'search', null),
			         tireSizeHelpTipImageSrc: URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/tireSizeMobile_480.png").toString(),
			         tireSizeHelpTipImageSrcSet: URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/tireSizeMobile_480.png").toString() + " 1x, " + URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/tireSizeDesktop_960.png").toString() + " 2x",
			         tireSizeHelpTipAlt:Resource.msg('vehicle.entry.byVehicleType.inforgraphicHelpTitle', 'search', null),
			         shopLabel: categoryId === "wheels"?Resource.msg('vehicle.entry.wheel.shopLabel', 'search', null):Resource.msg('vehicle.entry.tire.shopLabel', 'search', null),
			         shopUrl: categoryId === "wheels"?URLUtils.url('Search-Show', 'cgid', 'wheel').toString():URLUtils.url('Search-Show', 'cgid', 'tire').toString(),
					 saveLabel:Resource.msg('vehicle.entry.byVehicleType.saveLabel', 'search', null),
			      }
			   ]
			};
	if(categoryId === "tires" || categoryId === "all"){
		vehicleSelector.items.push({
		   id:"byTire",
		   label:Resource.msg('vehicle.entry.byTire.label', 'search', null),
		   title:Resource.msg('vehicle.entry.byTire.title', 'search', null),
		   frontTiresSubtitle:Resource.msg('vehicle.entry.byTire.frontTiresSubtitle', 'search', null),
		   rearTiresSubtitle:Resource.msg('vehicle.entry.byTire.rearTiresSubtitle', 'search', null),
		   widthPlaceholder:Resource.msg('vehicle.entry.byTire.widthPlaceholder', 'search', null),
		   aspectRatioPlaceholder:Resource.msg('vehicle.entry.byTire.aspectRatioPlaceholder', 'search', null),
		   tireSizeHelpTipLabel:Resource.msg('vehicle.entry.byVehicleType.tireSizeHelpTipLabel', 'search', null),
		   tireSizeHelpTipTitle:Resource.msg('vehicle.entry.byVehicleType.tireSizeHelpTipTitle', 'search', null),
		   tireSizeHelpTipImageSrc: URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/tireSizeMobile_480.png").toString(),
		   tireSizeHelpTipImageSrcSet: URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/tireSizeMobile_480.png").toString() + " 1x, " + URLUtils.staticURL(URLUtils.CONTEXT_LIBRARY, null, "/images/tireSizeDesktop_960.png").toString() + " 2x",
       tireSizeHelpTipAlt:Resource.msg('vehicle.entry.byVehicleType.inforgraphicHelpTitle', 'search', null),
		   rimPlaceholder:Resource.msg('vehicle.entry.byTire.rimPlaceholder', 'search', null),
		   addDifferentRearTireSizeLabel:Resource.msg('vehicle.entry.byTire.addDifferentRearTireSizeLabel', 'search', null),
		   removeDifferentRearTireSizeLabel:Resource.msg('vehicle.entry.byTire.removeDifferentRearTireSizeLabel', 'search', null),
		   widths: JSON.parse(Site.getCurrent().getCustomPreferenceValue('tireWidth')),
		   shopLabel:Resource.msg('vehicle.entry.tire.shopLabel', 'search', null),
		   shopUrl:URLUtils.url('Search-Show', 'cgid', 'tire').toString(),
		   saveLabel:Resource.msg('vehicle.entry.byTireType.saveLabel', 'search', null),
		   frontEntryLabel:Resource.msg('vehicle.entry.byTire.frontEntryLabel', 'search', null),
		   rearEntryLabel:Resource.msg('vehicle.entry.byTire.rearEntryLabel', 'search', null),
		});
	}
	if(categoryId === "wheels" || categoryId === "all"){
		vehicleSelector.items.push({
		   id:"byWheel",
		   label:Resource.msg('vehicle.entry.byWheel.label', 'search', null),
		   title:Resource.msg('vehicle.entry.byWheel.title', 'search', null),
		   frontWheelSubtitle:Resource.msg('vehicle.entry.byWheel.frontWheelSubtitle', 'search', null),
		   rearWheelSubtitle:Resource.msg('vehicle.entry.byWheel.rearWheelSubtitle', 'search', null),
		   boltPatternPlaceholder:Resource.msg('vehicle.entry.byWheel.boltPatternPlaceholder', 'search', null),
		   diameterPlaceholder:Resource.msg('vehicle.entry.byWheel.diameterPlaceholder', 'search', null),
		   addDifferentRearWheelSizeLabel:Resource.msg('vehicle.entry.byWheel.addDifferentRearWheelSizeLabel', 'search', null),
		   removeDifferentRearWheelSizeLabel:Resource.msg('vehicle.entry.byWheel.removeDifferentRearWheelSizeLabel', 'search', null),
		   patterns: JSON.parse(Site.getCurrent().getCustomPreferenceValue('wheelBoltPattern')),
		   shopLabel:Resource.msg('vehicle.entry.wheel.shopLabel', 'search', null),
		   shopUrl:URLUtils.url('Search-Show', 'cgid', 'wheel').toString(),
		   saveLabel:Resource.msg('vehicle.entry.byWheelType.saveLabel', 'search', null),
		   frontEntryLabel:Resource.msg('vehicle.entry.byWheel.frontEntryLabel', 'search', null),
		   rearEntryLabel:Resource.msg('vehicle.entry.byWheel.rearEntryLabel', 'search', null),
		});
	}
	return vehicleSelector;
}

function vehicleRenderWheelPDP(){
	var custEntryData = !empty(session.privacy.custEntryData) && JSON.parse(session.privacy.custEntryData).vehicle;
    var jsonData = {
        componentName: "VehicleRender",
        vehicleRenderApiUrl: Site.current.preferences.custom.vehicleRenderApi + "?key=" + Site.current.preferences.custom.vehicleRenderApiKey,
        vehicleDetails: custEntryData || null,
		wheelSku: request.httpParameterMap.sku.value || null
    };

	app.getView({jsonData: JSON.stringify(jsonData)}).render('react/staticshow');
}


function getVehicleSelector(){
	var category = request.httpParameterMap.category.value,
		jsonData = populateVehicleSelectorJSON(category || 'all');

	app.getView({jsonData: JSON.stringify(jsonData)}).render('react/apiresponse');
}

function updateSessionComponent(){
    var jsonData = {
		componentName: "UpdateSession",
		postUserLocationUrl: URLUtils.url('Stores-SaveUserLocation').toString(),
		postStoreUrl: URLUtils.url('Stores-UpdatePreferredStore').toString(),
		postVehicleDetailsUrl: URLUtils.url("Account-SaveEntryData").toString()
	};

	app.getView({jsonData: JSON.stringify(jsonData)}).render('react/staticshow');
}


exports.getStoreSearchForm=getStoreSearchForm;

/* Web exposed methods */
/** Returns static json based on the content asset it passed.
 * @see module:controllers/ReactComponent~staticShow */
exports.StaticShow = guard.ensure(['get'], staticShow);
/** Returns static json based on the content asset it passed.
 * @see module:controllers/ReactComponent~dynamicShow */
exports.DynamicShow = guard.ensure(['get'], dynamicShow);
/** Returns json for the VehicleRender react component
* @see module:controllers/ReactComponent~vehicleRenderWheelPDP */
exports.VehicleRenderWheelPDP = guard.ensure(['get'], vehicleRenderWheelPDP);
/** Returns json for the VehicleSelector react component
* @see module:controllers/ReactComponent~getVehicleSelector */
exports.GetVehicleSelector = guard.ensure(['get'], getVehicleSelector);
/** Returns json for the UpdateSession react component
* @see module:controllers/ReactComponent~updateSessionComponent */
exports.UpdateSessionComponent = guard.ensure(['get'], updateSessionComponent);
/** Populates store JSON
* @see module:controllers/ReactComponent~populateStoreInformation */
exports.PopulateStoreInformation = guard.ensure(['get'], populateStoreInformation);
/** Populates initial react state JSON
* @see module:controllers/ReactComponent~populateInitialState */
exports.PopulateInitialState = guard.ensure(['get'], populateInitialState);
