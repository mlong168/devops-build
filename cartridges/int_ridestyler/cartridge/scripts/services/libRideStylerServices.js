/* API Modules */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Resource = require('dw/web/Resource');

var params = request.httpParameterMap;

exports.fetchYearsService = function (){

	var service =  LocalServiceRegistry.createService("ridestyler.rest.years", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
}

exports.fetchMakesService = function (){

	var service =  LocalServiceRegistry.createService("ridestyler.rest.makes", {
	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;

}

exports.fetchModelsService = function (){

	var service=  LocalServiceRegistry.createService("ridestyler.rest.models", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;

}

exports.fetchDescriptionService = function (){

	var service =  LocalServiceRegistry.createService("ridestyler.rest.descriptions", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;

}

exports.fetchTireOptionDetailsService = function (){

	var service =  LocalServiceRegistry.createService("ridestyler.rest.tireoptiondetails", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;

}

exports.fetchFitmentsService = function (){

	var service =  LocalServiceRegistry.createService("ridestyler.rest.fitments", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;

}
exports.fetchBoltpatternsService = function (){

	var service =  LocalServiceRegistry.createService("ridestyler.rest.boltpatterns", {

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.addHeader(Resource.msg('service.header.accept','service',null), Resource.msg('service.header.applicationjson','service',null));
	    	 setHeaders(svc);
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;

}

function setHeaders(svc){
	svc.addHeader(Resource.msg('service.header.contenttype','service',null), Resource.msg('service.header.applicationxml','service',null));
	svc.addHeader(Resource.msg('service.header.charset','service',null), Resource.msg('service.header.charset.utf8','service',null));

}
