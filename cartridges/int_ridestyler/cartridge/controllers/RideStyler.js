'use strict';

/**
 * Controller that calls riderstyler webservices to get the response for searching vehicle by type
 *
 * @module controllers/RideStyler
 */

var params = request.httpParameterMap;

/* Script Modules */
var app = require('app_les_controllers/cartridge/scripts/app');
var guard = require('app_les_controllers/cartridge/scripts/guard');
var rideStyler = require('~/cartridge/scripts/services/libRideStylerServices');

/* API Modules */
var Resource = require('dw/web/Resource');
var Result = require('dw/svc/Result');



/**
 * Calls rideStyler/GetYears webservice
 *
 */
function getYears() {
	var service = rideStyler.fetchYearsService();

	addServiceParameters(service);

	executeService(service);
}

function getMakes() {
	var service = rideStyler.fetchMakesService();

	addServiceParameters(service);

	executeService(service);
}

function getModels() {
	var service = rideStyler.fetchModelsService();

	addServiceParameters(service);

	executeService(service);

}

function getDescriptions() {

	var service = rideStyler.fetchDescriptionService();

	addServiceParameters(service);

	executeService(service);
}

function getTireOptionDetails() {

	var service = rideStyler.fetchTireOptionDetailsService();

	addServiceParameters(service);

	executeService(service);

}

function getFitments() {

	var service = rideStyler.fetchFitmentsService();

	addServiceParameters(service);

	executeService(service);
}
function getBoltpatterns() {

	var service = rideStyler.fetchBoltpatternsService();

	addServiceParameters(service);

	executeService(service);
}

/* Common Utility Function To Add All HTTPParameters to Service Object*/
function addServiceParameters(service){

	var parameterNames = params.getParameterNames().toArray();
	for(i=0;i<params.getParameterCount();i++){
		service.addParam(parameterNames[i],params.get(parameterNames[i]));
	}
}

/* Common Utility Function To Execcute a Service and Return JSON Response*/
function executeService(service){

	var result = service.call();

	if(result.status == Resource.msg('service.response.ok','service',null)) {
		app.getView({
			jsonData: result.getObject()
		}).render('serviceresponse');
	 } else {
		 app.getView({
				jsonData: Resource.msg('service.response.erroroccured','service',null)
			}).render('serviceresponse');
	 }
}

/**
 * Returns the webservice response as "Years" in JSON
 * Parameters Expected:
 *	"yearmin" -- Optional
 *	"yearmax" -- Optional
 * @see module:controllers/Vehicle~getYears
 */
exports.GetYears = guard.ensure(['get'],getYears);

/**
 * Returns the webservice response as "Vehicle Makes" in JSON.
 * Parameters Expected:
 *	"year" -- Optional
 * @see module:controllers/Vehicle~getMakes
 */
exports.GetMakes = guard.ensure(['get'],getMakes);

/**
 * Returns the webservice response as "Vehicle Models" in JSON.
 * Parameters Expected:
 *	"vehiclemake" -- Mandatory
 *	"start" -- Optional
 *	"count" -- Optional
 * @see module:controllers/Vehicle~getModels
 */
exports.GetModels = guard.ensure(['get'],getModels);

/**
 * Returns the webservice response as "Vehicle Models" in JSON.
 * Parameters Expected:
 *	"vehiclemodel" -- Mandatory
 *	"year" -- Optional
 * @see module:controllers/Vehicle~getDescriptions
 */
exports.GetDescriptions = guard.ensure(['get'],getDescriptions);

/**
 * Returns the webservice response as "Vehicle Models" in JSON.
 * Parameters Expected:
 *	"vehicleconfiguration" -- Mandatory
 * @see module:controllers/Vehicle~getTireOptionDetails
 */
exports.GetTireOptionDetails = guard.ensure(['get'],getTireOptionDetails);
/**
 * Returns the webservice response as "Vehicle Fitments" in JSON.
 * Parameters Expected:
 *	"vehicleconfiguration" -- Mandatory
 * @see module:controllers/Vehicle~getFitments
 */
exports.GetFitments = guard.ensure(['get'],getFitments);
/**
 * Returns the webservice response as "Boltpatternss" in JSON.
 * @see module:controllers/Vehicle~getBoltpatterns
 */
exports.GetBoltpatterns = guard.ensure(['get'],getBoltpatterns);
