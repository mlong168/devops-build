/**
* This controller is used for sending Email using SendGrid Api
*
* @module  controllers/SendGrid
*/

'use strict';

/* Script Modules */
var app = require('app_les_controllers/cartridge/scripts/app');
var guard = require('app_les_controllers/cartridge/scripts/guard');
var libSendGrid = require('~/cartridge/scripts/service/libSendGrid');

/* API Modules */
var Resource = require('dw/web/Resource');
var Result = require('dw/svc/Result');

/**
* This function sends email through SendGrid Api
*
* @return {dw.system.Status} The string 'sendMail'
*/
var sendMail = function(subject, toEmail, fromEmail, toName, fromName, timeStamp, custName, custPhone, custEmail, custComment, templateID) {
	var service = libSendGrid.getEmailService();
	var req = libSendGrid.getEmailRequest(subject, toEmail, fromEmail, toName, fromName, timeStamp, custName, custPhone, custEmail, custComment, templateID);
	var result = service.call(req);

	return result.status;
}

/* Exports of the controller */
/**
* @see {@link module:controllers/SendGrid~sendMail}
*/
exports.SendMail = sendMail;