/* API Modules */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');

var params = request.httpParameterMap;

exports.getEmailService = function () {

	var service =  LocalServiceRegistry.createService("sendgrid.rest.SendMail", { 

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Authorization','Bearer ' + Site.getCurrent().getCustomPreferenceValue('sendGridApiKey'));
	     	 svc.addHeader('Content-Type','application/json');
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
};

exports.getEmailRequest = function (subject, toEmail, fromEmail, toName, fromName, timeStamp, custName, custPhone, custEmail, custComment, templateID) {
	var req = new Object();
	var personalization = {};
	var recepient = {}, from = {}, reply_to = {}, substitutions = {};
	
	recepient.email =  toEmail;
	recepient.name = toName;
	
	from.email = fromEmail;
	from.name = fromName;
	
	reply_to.email = custEmail;
	reply_to.name =  custName;

	substitutions.TIME_STAMP = timeStamp;
	substitutions.CUST_NAME = custName;
	substitutions.CUST_PHONE = custPhone;
	substitutions.CUST_EMAIL = custEmail;
	substitutions.CUST_COMMENT = custComment;
	
	personalization.to = [];
	personalization.to.push(recepient);
	personalization.subject = subject;
	personalization.substitutions = substitutions;
	
	req.personalizations = [];
	req.personalizations.push(personalization);
	req.from = from;
	
	req.template_id = templateID;
			
	return JSON.stringify(req); 
};