/* API Modules */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');

var params = request.httpParameterMap;

exports.getAssignedServices = function () {

	var service =  LocalServiceRegistry.createService("appointmentplus.rest.GetAssignedServices", { 

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/json');
	     	 service.addParam('response_type','json');
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
};


exports.getOpenSlots = function () {
	var serviceObj = LocalServiceRegistry.createService("appointment.rest.GetOpenSlots", { 
	    
		 createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/x-www-form-urlencoded');
	     	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});
	return serviceObj;
};

exports.createAppointments = function () {

	var service =  LocalServiceRegistry.createService("appointmentplus.rest.CreateAppointments", { 

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/json');
	     	 service.addParam('response_type','json');
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
};


exports.cancelAppointments = function () {

	var service =  LocalServiceRegistry.createService("appointmentplus.rest.CancelAppointments", { 

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/json');
	     	 service.addParam('response_type','json');
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
};

exports.getLocations = function () {

	var service =  LocalServiceRegistry.createService("appointmentplus.rest.GetLocations", { 

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/json');
	     	 service.addParam('response_type','json');
	     	
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
};

exports.getAppointments = function () {

	var service =  LocalServiceRegistry.createService("appointmentplus.rest.GetAppointments", { 

	     createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/json');
	     	 service.addParam('response_type','json');
	    	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});

	return service;
};

exports.createCustomers = function () {
	var serviceObj = LocalServiceRegistry.createService("appointment.rest.CreateCustomers", { 
		 createRequest: function(svc:HTTPService,  requestObj : Object) : Object {
	     	 svc.setRequestMethod("POST");
	     	 svc.addHeader('Content-Type','application/json');
	     	 return requestObj;
	     },
	     parseResponse : function(svc:HTTPService, client : HttpClient) {
	         return client.text;
	     }
	});
	return serviceObj;
};
