/**
* This controller provides different endpoints for calling Appointment Plus services
*
* @module  controllers/AppointmentPlus
*/

'use strict';

/* Script Modules */
var appointmentPlus = require('~/cartridge/scripts/services/libAppointmentPlus');

/* API Modules */
var Resource = require('dw/web/Resource');

/**
* Description of the function
*
* @return {Object} The service response of 'GetAssignedServices' service
*/
 var getAssignedServices = function(locationID){
	var service = appointmentPlus.getAssignedServices();
	 service.addParam('location_id',locationID);
	var result = service.call();
	
	if(result.status == Resource.msg('service.response.ok','service',null)) {
		return JSON.parse(result.getObject());
	} else {
		return null;
	}
 };
 
 
 /**
 * Description of the function
 *
 * @return {Object} The service response of 'GetOpenSlots' service
 */
 var getOpenSlots = function(params){
		var service = appointmentPlus.getOpenSlots();
		service.addParam('response_type','json');
		var result = service.call(params);
		if(result.status == Resource.msg('service.response.ok','service',null)) {
			return JSON.parse(result.getObject());
		} else {
			return null;
		}
 };
 
 /**
 * Description of the function
 *
 * @return Appointment JSON of the created appointment in service response of 'CreateAppointments' service
 */
  var createAppointments = function(customerId, 
		  							internalNotes, 
		  							createLocationId, 
		  							appointmentServiceArr,
		  							additionalFieldsObj
		  							) {
 	var service = appointmentPlus.createAppointments();
 	var reqObj = new Object();
 	reqObj.id = null;
 	reqObj.customerId= customerId;
 	reqObj.internalNotes = internalNotes;
 	reqObj.createLocationId = createLocationId;
 	reqObj.createEmployeeId = 0;
 	reqObj.lastUpdateEmployeeId = 0;
	
 	reqObj.appointmentService = appointmentServiceArr;
 	
 	reqObj.additionalFields = additionalFieldsObj;
 	
 	var result = service.call(JSON.stringify(reqObj));
 	
 	if(result.status == Resource.msg('service.response.ok','service',null)) {
 		return JSON.parse(result.getObject());
 	} else {
 		return null;
 	}
 };
 
 /**
  * Description of the function
  *
  * @return Appointment JSON of the cancelled appointment in service response of 'CancelAppointments' service
  */
   var cancelAppointments = function(appointmentID){
  	var service = appointmentPlus.cancelAppointments();
  	var reqObj = new Object();

  	reqObj.id = appointmentID;
  	reqObj.lastUpdateEmployeeId = 0;
  	
  	var result = service.call(JSON.stringify(reqObj));
  	
  	if(result.status == Resource.msg('service.response.ok','service',null)) {
  		return JSON.parse(result.getObject());
  	} else {
  		return null;
  	}
  };

 /**
 * Description of the function
 *
 * @return {Object} The service response of 'GetLocations' service
 */
  var getLocations = function(storeId){
 	var service = appointmentPlus.getLocations(storeId);
 	service.addParam('store_number', storeId);
 	var result = service.call();
 	
 	if(result.status == Resource.msg('service.response.ok','service',null)) {
 		return JSON.parse(result.getObject());
 	} else {
 		return null;
 	}
 };
 
 var getAppointments = function(appointmentId, locationId){
		var service = appointmentPlus.getAppointments();
		var req = new Object();
		
		//Paramters should be array[Integer]
		req.appointmentId = appointmentId;	
		req.locationId = locationId;

		var result = service.call(JSON.stringify(req));
		
		if(result.status == Resource.msg('service.response.ok','service',null)) {
			return JSON.parse(result.getObject());
		} else {
			return null;
		}
	 };
 
	 
 var createCustomers = function(params){
		var service = appointmentPlus.createCustomers();
		service.addParam('response_type','json');
		service.addParam('locationId',params.locationId);
		service.addParam('last_name',params.last_name);
		service.addParam('first_name',params.first_name);
		service.addParam('day_phone',params.day_phone);
		service.addParam('email',params.email);
		var result = service.call();
		if(result.status == Resource.msg('service.response.ok','service',null)) {
			return JSON.parse(result.getObject());
		} else {
			return null;
		}
};	 
/* Exports of the controller */
/**
 * @see {@link module:controllers/AppointmentPlus~getAssignedServices} */
exports.GetAssignedServices = getAssignedServices;
/**
 * @see {@link module:controllers/AppointmentPlus~getOpenSlots} */
exports.GetOpenSlots = getOpenSlots;
/**
 * @see {@link module:controllers/AppointmentPlus~createAppointments} */
exports.CreateAppointments = createAppointments;
/**
 * @see {@link module:controllers/AppointmentPlus~cancelAppointments} */
exports.CancelAppointments = cancelAppointments;
/**
 * @see {@link module:controllers/AppointmentPlus~getLocations} */
exports.GetLocations = getLocations;
/**
 * @see {@link module:controllers/AppointmentPlus~getAppointments} */
exports.GetAppointments = getAppointments;
/**
 * @see {@link module:controllers/AppointmentPlus~createCustomers} */
exports.CreateCustomers = createCustomers;