import ReactTestUtils from 'react-dom/test-utils';

import ShopForTiresCTA from '../cartridges/app_les_core/cartridge/js_react/containers/ShopForTiresCTA.es6';

describe("ShopForTiresCTA", () => {
	let component = TestUtils.renderIntoDocument(<ShopForTiresCTA ctaLabel="shop for tires"/>);
	let componentNode = React.findDOMNode(component);
	it("Renders button CTA", () => {
		expect(componentNode.querySelector('button').textContent).toEqual('shop for tires');
	});
});
