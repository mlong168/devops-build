const imagemin = require('imagemin');
const path = require('path');
const fs = require("fs-extra");
const glob = require('glob');
const jpegCompressor = require('imagemin-jpeg-recompress');
const pngCompressor = require('imagemin-pngquant');
const sizeOf = require('image-size');

const input = './raw/**/*.{jpg,png}';
const output = './sites/site_content/libraries/LesSchwabLibrary/static/default/images/';

//imagemin does not preserve directory structure when compressing images, that's why we first create image map with all paths
const fileMap = ((items) => items.reduce((target, file) => {
return Object.assign({}, target, {[path.basename(file)]: path.relative("./raw",path.dirname(file))+"/"})
}, {}))(glob.sync('./raw/**/*.{jpg,png}'));

imagemin([input], output, {
	plugins: [
		jpegCompressor({
			accurate: true,
			max: 80,
			min: 60
		}),
		pngCompressor({quality: '65-80'})
	]
}).then(files => {
	//imagemin creates files in site_export root, then we're moving them into correct location based on fileMap we created earlier
	files.forEach((f) => {
		let dest = output + fileMap[path.basename(f.path)];
		//creating new directories if needed
		fs.ensureDirSync(dest);
		//moving files to their final destinations
		console.log("Moving: ", f.path, " to ", dest + path.basename(f.path));
		let dimensions = sizeOf(f.path);
		fs.rename(f.path, dest + (path.basename(f.path).replace(/(@2x)?(\.[^.]+)$/,'')+"_"+dimensions.width+path.extname(f.path)), (err)=>{if(err)console.log(err);})
	})

});
