const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

const autoprefixer = require('autoprefixer');

const rootDir = '<%= dw_properties.folders.code %>';

let plugins = [
	new ExtractTextPlugin("./css/style.css"),
	new StyleLintPlugin({syntax: "scss", fix: true}),
	new webpack.DefinePlugin({
	    'process.env': {
	      'NODE_ENV': JSON.stringify('development')
	    }
	}),
	new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery'",
      "window.$": "jquery"
  })
];
module.exports = {
	context: __dirname,
	devtool: 'source-map',
	entry: {
		app: ["whatwg-fetch", "promise-polyfill", "babel-polyfill", rootDir + '/js_react/index.es6', "jquery-ui/ui/widgets/dialog", "jquery-ui/ui/widgets/tabs", rootDir + '/js/app.js']
	},
	output: {
		path: path.resolve(__dirname, rootDir + "/static/default"),
		filename: './js/[name].js',
		publicPath: "./"
	},
	resolve: {
		alias: {
			config: path.join(__dirname, rootDir + '/config/config.dev.js'),
		},
		modules: [
			path.resolve(__dirname, 'node_modules'),
			path.resolve(__dirname, rootDir + "/js"),
			path.resolve(__dirname, rootDir + "/js_react"),
			path.resolve(__dirname, rootDir)
		],
		extensions: ['.js', '.es6', '.scss']
	},
	module: {
		rules: [{
				test: /\.([j|e]+s[x|6]?){1}$/,
				include: [
					 path.resolve(__dirname, rootDir),
					 path.resolve(__dirname, rootDir + "/js"),
					 path.resolve(__dirname, rootDir + "/js_react")
				],
				exclude: /node_modules/,
				use: [{
						loader: 'babel-loader',
						options: {
							presets: ["react", ["env", {
								"targets": {
									"browsers": ["last 2 versions", "ie >= 10"]
								}
							}], "stage-2"],
							plugins: ["transform-class-properties"]
						}
					},
					{
						loader: 'eslint-loader',
						options: {
							"fix": true
						}
					}
				]
			},
			// sass-loader compiles sass into css.
			// "postcss" loader applies autoprefixer to our CSS.
			// "css" loader resolves paths in CSS and adds assets as dependencies.
			// "style" loader normally turns CSS into JS modules injecting <style>,
			// but we do something different here:
			// `ExtractTextPlugin` first applies the "postcss" and "css" loaders
			// (second argument), then grabs the result CSS and puts it into a
			// separate file in our build process. This way we actually ship
			// a single CSS file in production instead of JS code injecting <style>
			// tags.
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					publicPath: '../',
					use: [{
							loader: 'css-loader',
							options: {
								importLoaders: 2,
								sourceMap: true,
								localIdentName: '[name]__[local]--[hash:base64:5]'
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								// Necessary for external CSS imports to work
								// https://github.com/facebookincubator/create-react-app/issues/2677
								ident: 'postcss',
								sourceMap: true,
								plugins: [
									require('postcss-flexbugs-fixes'),
									autoprefixer({
										flexbox: 'no-2009'
									})
								]
							}
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: false
							}
						}
					]
				})
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [{
						loader: 'url-loader',
						options: {
							limit: 8192,
							name: 'images/[hash].[ext]'
						}
					},
					'img-loader'
				]
			},
			{
				test: /\.(ttf|woff|woff2)$/i,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: 'fonts/[name].[ext]',
				},
			}
		]
	},
	plugins: plugins
};
