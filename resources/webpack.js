const webpackConfig = require('../../webpack.config.js');

module.exports = {
    build: {
        stats: {
          colors: false,
          modules: true,
          reasons: true
        },
        storeStatsTo: 'webpackStats',
        progress: true,
        failOnError: true,
        watch: true,
        }
      };
