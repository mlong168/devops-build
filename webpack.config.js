const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const StyleLintPlugin = require('stylelint-webpack-plugin');
const autoprefixer = require('autoprefixer');

const rootDir = "./cartridges/app_les_core/cartridge";

let plugins = [
	new MiniCssExtractPlugin({filename: "css/style.css"}),
	new StyleLintPlugin({syntax: "scss", fix: true}),
	new webpack.ProvidePlugin({$: "jquery", jQuery: "jquery", "window.jQuery": "jquery'", "window.$": "jquery"})
];
module.exports = {
	mode: "development",
	context: __dirname,
	devtool: 'eval-source-map',
	stats: "minimal",
	entry: {
		app: [
			"whatwg-fetch", "promise-polyfill", "babel-polyfill", rootDir + '/js_react/index.es6'
		],
		legacy: ["jquery-ui/ui/widgets/dialog",
			"jquery-ui/ui/widgets/tabs",
			rootDir + '/js/app.js'
		]
	},
	output: {
		path: path.resolve(__dirname, rootDir + "/static/default"),
		filename: './js/[name].js',
		publicPath: "./"
	},
	resolve: {
		alias: {
			config: path.join(__dirname, rootDir + '/config/config.dev.js')
		},
		modules: [
			path.resolve(__dirname, 'node_modules'),
			path.resolve(__dirname, rootDir + "/js"),
			path.resolve(__dirname, rootDir + "/js_react"),
			path.resolve(__dirname, rootDir)
		],
		extensions: [
			'.js', '.es6', '.scss'
		],
		symlinks: false
	},
	performance: {
		maxAssetSize: 1000000,
		maxEntrypointSize: 1000000
	},
	optimization: {
		minimize: true
	},
	module: {
		rules: [
			{
				test: /\.([j|e]+s[x|6]?){1}$/,
				include: [
					path.resolve(__dirname, rootDir + "/js"),
					path.resolve(__dirname, rootDir + "/js_react")
				],
				exclude: /node_modules/,
				use: [
					'cache-loader', {
						loader: 'babel-loader',
						options: {
							presets: [
								"react",
								[
									"env", {
										"targets": {
											"browsers": ["last 2 versions", "ie >= 10"]
										}
									}
								],
								"stage-2"
							],
							plugins: ["transform-class-properties"]
						}
					}, {
						loader: 'eslint-loader',
						options: {
							"fix": true
						}
					}
				]
			},
			// sass-loader compiles sass into css.
			// "postcss" loader applies autoprefixer to our CSS.
			// "css" loader resolves paths in CSS and adds assets as dependencies.
			// "style" loader normally turns CSS into JS modules injecting <style>,
			// but we do something different here:
			// `ExtractTextPlugin` first applies the "postcss" and "css" loaders
			// (second argument), then grabs the result CSS and puts it into a
			// separate file in our build process. This way we actually ship
			// a single CSS file in production instead of JS code injecting <style>
			// tags.
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				include: [path.resolve(__dirname, rootDir + "/scss")],
				use: [
					//'cache-loader',
					MiniCssExtractPlugin.loader, {
						loader: 'css-loader',
						options: {
							importLoaders: 2,
							sourceMap: true,
							localIdentName: '[name]__[local]--[hash:base64:5]'
						}
					}, {
						loader: 'postcss-loader',
						options: {
							// Necessary for external CSS imports to work
							// https://github.com/facebookincubator/create-react-app/issues/2677
							ident: 'postcss',
							sourceMap: false,
							plugins: [
								require('postcss-flexbugs-fixes'),
								autoprefixer({flexbox: 'no-2009'})
							]
						}
					}, {
						loader: 'sass-loader',
						options: {
							sourceMap: false
						}
					}
				]
			}, {
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [
					{
						//loader: 'url-loader',//turning off url-loader to cut down CSS file size
						loader: 'file-loader',
						options: {
							limit: 8192,
							name: '[name]-[sha512:hash:base64:7].[ext]',
							publicPath: '../images',
							outputPath: 'images/'
						}
					},
					'img-loader'
				]
			}, {
				test: /\.(ttf|woff|woff2)$/i,
				loader: 'file-loader',
				options: {
					name: '[name]-[sha512:hash:base64:7].[ext]',
					publicPath: '../fonts',
					outputPath: 'fonts/'
				}
			}
		]
	},
	plugins: plugins
};
