var shelljs = require('shelljs');

var gitRoot = shelljs.exec("git rev-parse --show-toplevel").stdout.trim(),
	hookDir = gitRoot + "/.git/hooks";

shelljs.cp('-f', 'pre-commit', hookDir);
shelljs.cp('-f', 'commit-msg', hookDir);
shelljs.sed('-i', /\bNPM_DIR\b/g, process.cwd(), hookDir + '/pre-commit');
shelljs.chmod('+x', hookDir + "/pre-commit");
shelljs.sed('-i', /\bNPM_DIR\b/g, process.cwd(), hookDir + '/commit-msg');
shelljs.chmod('+x', hookDir + "/commit-msg");
